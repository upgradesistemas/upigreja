const Upgrade = require('./lib_backend/main/Upgrade');
const ioServer = require('./app/ioServer');
const databases = require('./lib_backend/classes/BancoIgrejas');


Upgrade.iniciarAplicacao({
  controller: 'app/Controller',
  // configTemp: 'app/ConfigTempApp',
  sincronizador: {
    pasta: 'entidade',
  },  
  ioServer: {
    onInicio: (io) => ioServer(io),
  },
  banco: {
    host: '127.0.0.1',
    port: '3307',
    user: 'root',
    database: 'igreja',
    password: 'mysql',
  },
  bancos : databases.databases,
});
