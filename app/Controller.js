const moment = require('moment');
const fs = require('fs');
const clipboardy = require('clipboardy');
const buscaCep = require('busca-cep');
const path = require('path');
const { whereExclusao } = require('../lib_backend/funcoes/variaveis').variaveis;
const GenericController = require('../lib_backend/main/GenericController');
const database = require('../lib_backend/funcoes/database');
const autenticacao = require('../lib_backend/funcoes/autenticacao');
const Pessoa = require('../entidade/Pessoa');
const PessoaIgreja = require('../lib_backend/entidade/PessoaIgreja');
const ConfigCampos = require('../lib_backend/entidade/ConfigCampos');
const Perfil = require('../lib_backend/entidade/Perfil');
const CR = require('../entidade/CR');
const CP = require('../entidade/CP');
const Escala = require('../entidade/Escala');
const EscalaPessoa = require('../entidade/EscalaPessoa');
const EscalaGrupo = require('../entidade/EscalaGrupo');
const EscalaNome = require('../entidade/EscalaNome');
const Compra = require('../entidade/Compra');
const Cantina = require('../entidade/Cantina');
const tipoMov = require('../src/tipo');
const Nivel = require('../entidade/Nivel');
const Cidade = require('../entidade/Cidade');
const Caixa = require('../entidade/Caixa');
const Igreja = require('../entidade/Igreja');
const Departamento = require('../entidade/Departamento');
const CelulaPessoa = require('../entidade/CelulaPessoa');
const Celula = require('../entidade/Celula');
const Distrito = require('../entidade/Distrito');
const Area = require('../entidade/Area');
const Setor = require('../entidade/Setor');
const Tipo = require('../src/tipo');
const Chamada = require('../entidade/Chamada');
const Produto = require('../entidade/Produto');
const Evento = require('../entidade/Evento');
const Curso = require('../entidade/Curso');
const Pedido = require('../entidade/SolicitacaoVisOra');

/**
 * Classe do controlador específico dessa aplicação.
 */
module.exports = class Controller {

  /**
   * Realiza o login no sistema, sobe um erro se o login/senha está errado.
   * @param {Object} dados Os dados da requisição
   * @param {String} dados.login o login do usuário
   * @param {String} dados.senha a senha do cara
   */
  static async login(login, senha, codigoCliente, senhaCriptografada) {
    const nomeBanco = await database.getBanco(codigoCliente);

    // se o getBanco não retornou nada é porque não encontrou o código de cliente passado.
    if (!nomeBanco) {
      throw new Error('Código de Cliente não encontrado');
    }

    // Passa a senha para maiusculo:
    senha = senha.toUpperCase();

    // Primeiro vamos validar se a Igreja atual está cadastrada e liberada para usar o sistema:
    if (nomeBanco !== 'igreja') {
      await this.verificaNoGestao(nomeBanco);
    }

    // Depois vamos validar o login e se ele existe:
    let ret = await database.query(`
      SELECT * FROM ${nomeBanco}.tab_pessoa
      WHERE usuario = "${login}"
    `);

    if (!ret || !ret.length) {
      // Não tem usuario com essa chave:
      throw new Error(`Usuário ${login} não encontrado!`);
    }

    // Validar se o registro encontrado é um Usuário
    if (ret[0].status_usuario === 'N') {
      throw new Error('O(A) ' + login + ' não é um Usuário do sistema');
    }

    // Validar se o registro é diferende de ADMIN e pertence há alguma igreja
    if (ret[0].cod_nivel !== 1 && !ret[0].cod_igreja) {
      throw new Error('O(A) ' + login + ' não pertence a nenhuma igreja');
    }

    const nivelPessoa = await database.queryFindOne(`SELECT 1
    FROM ${nomeBanco}.tab_nivel a WHERE cod_nivel = '${ret[0].cod_nivel}'`);
    if (!nivelPessoa) {
      throw new Error('O(A) ' + login + ' não tem nível configurado!');
    }

    // Agora vamos validar a senha:
    const cod_pessoa = ret[0].cod_pessoa;
    ret = await database.query(`SELECT a.*, senha = md5("${senha}") as correto
    FROM ${nomeBanco}.tab_pessoa a WHERE cod_pessoa = '${cod_pessoa}'`);

    let senhaErrada = !ret[0].correto;

    if (senhaCriptografada) {
      // Se temos a senha criptografada, vamos validar se igual a senha que já temos ou não
      senhaErrada = ret[0].senha !== senhaCriptografada;
    }

    if (!ret[0].senha && !senha) {
      // Se a senha do cara está vazia assim como a senha do usuario no banco, deixa passar
      senhaErrada = false;
    }

    if (senhaErrada) {
      // Não tem usuario com essa chave e login:
      throw new Error('Senha incorreta!');
    }

    // Se chegamos até aqui é porque o usuário está com a senha e login correto,
    // então vamos gerar e mandar pra ele o token de autenticação:
    const token = autenticacao.putToken(ret[0]);

    const usuario = ret[0];
    usuario.codigoCliente = codigoCliente;
    delete usuario.correto; // A validação

    await this.TrocarIgreja({
      codIgreja: usuario.cod_igreja,
      codPessoa: cod_pessoa,
      codigoCliente,
    });

    return {
      token,
      usuario,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna os dados inicias após o login para o frontend.
   * Toda vez que for validado a autenticação do usuário no frontend
   * (por exemplo após o login), esse método será chamado e devemos enviar
   * as informações que ficaram no this.props.usuario no front.
   */
  async onLoginFrontend(usuario, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    // Carrega todas as Configs do Sistema
    const permissoesTelas =
      await GenericController.getTelasLiberadas(usuario.cod_pessoa, nomeBanco);
    const config = await GenericController.getConfigs(usuario.cod_pessoa, nomeBanco);
    const departamento = await this.departamentoIgreja(usuario, nomeBanco);
    const nivel = await Nivel.buscarPorId(usuario.cod_nivel, nomeBanco);
    const igrejaLogada =
      await this.IgrejaLogada({ codigoCliente, pessoaLogada: usuario.cod_pessoa });
    const caixa = await Caixa.caixaAberto(igrejaLogada.Igreja, nomeBanco);
    usuario = {
      ...usuario,
      nivel,
      departamento_igreja: departamento ? departamento.cod_departamento : 0,
      igrejaLogada,
      caixa,
    };

    return {
      usuario,
      permissoesTelas,
      config,
    };
  }

  // ------------------------------------------------------------------------------------

  static async abrirFecharCaixa(caixa, caixaState, pessoaExecutando, igreja, codigoCliente,
    abertura) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return await Caixa.abrirFecharCaixa(caixa, caixaState, pessoaExecutando, igreja,
      nomeBanco, abertura);
  }

  // ------------------------------------------------------------------------------------

  async verificaNoGestao(nomeBanco) {
    const naoLiberado = 'Igreja não liberada, favor verificar com o Administrador do sistema!';
    const gestao = await database.queryFindOne(`
      SELECT * FROM gestao.tab_pessoa WHERE nome_banco_dados_pessoa = "${nomeBanco}"
    `);

    if (gestao.status_liberado === 'N') {
      throw new Error(naoLiberado);
    }
  }

  // ------------------------------------------------------------------------------------
  static async retornaPresidente(codigoCliente, igrejaLogada) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return await Igreja.retornaPresidente(nomeBanco, igrejaLogada);
  }

  // ------------------------------------------------------------------------------------

  async TrocarIgreja(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const igreja = await this.retornaNome(
      filtro.codigoCliente, 'tab_igreja', 'nome_igreja', 'cod_igreja', filtro.codIgreja);
    await PessoaIgreja.set(
      filtro.codPessoa, filtro.codIgreja, igreja.nome_igreja, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  async departamentoIgreja(usuario, nomeBanco) {
    return await Departamento.departamentoIgreja(usuario.cod_igreja, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Object} a lista de perfis para o cadastro de cliente.
   */
  static async getDadosCadPessoa(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const perfis = await Perfil.buscarTodos('', nomeBanco);
    return {
      perfis,
    };
  }

  // ------------------------------------------------------------------------------------

  async IgrejaLogada(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    return Igreja.retornaIgrejaLogada(filtro.pessoaLogada, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  static async existeRegistro(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const sql = `SELECT 1 FROM ${nomeBanco}.tab_${filtro.tabela} a
      WHERE a.${filtro.campo} = "${filtro.valor}"` +
      (filtro.pkValor ? ` AND ${filtro.pkNome} <> ${filtro.pkValor} ` : '');

    const registros = await database.query(sql);
    const existe = registros.length > 0;
    return existe;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Baixa uma conta a receber / pagar.
   * @param {String|Number} codFuncionario O Código da pessoa que irá receber / pagar a conta.
   * @param {CR} cr Um objeto de contas a receber.
   * @param {String} tipo Quem chamou esse método (CR, CP)
   */
  static async baixarModal(codFuncionario, conta, tipo, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    if (tipo === 'CR') {
      const valor = conta.vr_recebido;
      delete conta.vr_recebido;
      await CR.salvar(conta, nomeBanco);
      return await CR.baixar(codFuncionario, conta.cod_cr, valor, nomeBanco);

    } else if (tipo === 'CP') {
      const valor = conta.vr_pago;
      delete conta.vr_pago;
      await CP.salvar(conta, nomeBanco);
      return await CP.baixar(codFuncionario, conta.cod_cp, valor, nomeBanco);

    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Gera parcelas de contas a receber / pagar.
   * @param {Object[]} parcelas As parcelas a gerar.
   * @param {Number} parcelas.numero O Número sequencial da parcela.
   * @param {Number} parcelas.valor O Valor dessa parcela.
   * @param {String} parcelas.data A Data da parcela no formato DD/MM/YYYY.
   * @param {String|Number} codPessoa O Código da pessoa das parcelas.
   * @param {String|Number} codFunc O Código do funcionário que gerou as parcelas.
   * @param {String} tipo Quem chamou esse método (CR, CP)
   */
  static async gerarParcelas(parcelas, codPessoa, codFunc, codCaixa, tipo, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    if (tipo === 'CR') {
      return await CR.gerarParcelas(parcelas, codPessoa, codFunc, codCaixa, nomeBanco);

    } else if (tipo === 'CP') {
      return await CP.gerarParcelas(parcelas, codPessoa, codFunc, codCaixa, nomeBanco);

    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Estorna uma conta a receber / pagar.
   * @param {String|Number} codFuncionarioEstorno O Funcionario que estornou.
   * @param {String|Number} codConta O Código do contas a receber / pagar.
   * @param {String} tipo Quem chamou esse método (CR, CP)
   */
  static async estornarConta(codFuncionarioEstorno, codConta, tipo, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    if (tipo === 'CR') {
      return await CR.estornar(codFuncionarioEstorno, codConta, nomeBanco);

    } else if (tipo === 'CP') {
      return await CP.estornar(codFuncionarioEstorno, codConta, nomeBanco);

    }
  }

  // ------------------------------------------------------------------------------------

  static async salvarConfigCampos(config, tabela, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    // Exclui toda a config da tabela:
    await ConfigCampos.excluirTodos(` tabela = "${tabela}" `, nomeBanco);
    for (const cfg of config) {
      // Salva a configuração do campo:
      if (cfg.checked) {
        await ConfigCampos.salvar({
          nome_config_campos: cfg.nome,
          tabela,
        }, nomeBanco);
      }
    }
  }

  // ------------------------------------------------------------------------------------

  static async salvarEscala(escala, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const {
      pessoas,
      grupos,
    } = escala;
    escala.cod_escala = await Escala.salvar(escala, nomeBanco);

    // Para não dar conflitos, primeiramente vamos excluir todas as pessoas e
    // grupos dessa escala, para inseri-los novamente depois.
    await EscalaPessoa.excluirTodos(`cod_escala = '${escala.cod_escala}'`, nomeBanco);
    await EscalaGrupo.excluirTodos(`cod_escala = '${escala.cod_escala}'`, nomeBanco);

    for (const pessoa of pessoas) {
      delete pessoa.cod_escala_pessoa; // PK
      pessoa.cod_escala = escala.cod_escala;
      await EscalaPessoa.salvar(pessoa, nomeBanco);
    }

    // Agora vamos salvar todos os grupos:
    for (const grupo of grupos) {
      // Vamos achar o nome da escala na qual o nome do grupo foi criado
      grupo.cod_escala_nome = await EscalaNome.buscarPorNome(grupo.nome_escala_nome, nomeBanco);
      grupo.cod_escala = escala.cod_escala;
      delete grupo.cod_escala_grupo; // PK
      const id = await EscalaGrupo.salvar(grupo, nomeBanco);

      for (const pessoa of grupo.pessoas) {
        delete pessoa.cod_escala_pessoa; // PK
        pessoa.cod_escala_grupo = id;
        pessoa.cod_escala = escala.cod_escala;
        await EscalaPessoa.salvar(pessoa, nomeBanco);
      }
    }

  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda uma data e hora de uma escala.
   * @param {String|Number} codEscala O Código da escala.
   * @param {String} novaDataHora A Nova data e hora no formato YYYY-MM-DD HH:mm:ss
   */
  static async mudarDataEscala(codEscala, novaDataHora, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    await database.query(`
      update ${nomeBanco}.tab_escala set dthr_escala = ? where cod_escala = ?`,
      [novaDataHora, codEscala]);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna a lista de informações para os cadastros do frontend. Todos
   * os cadastros irão chamar esse método para realizar suas buscas.
   * @param {Object} filtro Objeto de Filtros.
   * @param {String} filtro.filtro O Texto de filtro.
   * @param {String} filtro.tabela A Tabela sendo requisitada.
   * @param {String|Number} filtro.de Limit de até.
   * @param {String|Number} filtro.ate Limit de até.
   */
  async getCadGenerico(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const Entidade = new GenericController().encontrarEntidade(filtro.tabela);
    let lista = [];
    let qtdeRegistros = 0;
    filtro = {
      ...filtro,
      nomeBanco,
    };

    if (filtro.de || filtro.ate) {
      // Chama o método específico do cadastro:
      lista = await Entidade.buscarDadosCadastro(filtro);
    } else {
      // Cadastro não é paginável, apenas busca os dados respeitando o filtro
      // da página:
      lista = await Entidade.buscarDadosCadastro(filtro);
    }
    qtdeRegistros = await Entidade.getQtdeRegistros(filtro.nomeBanco);
    return {
      lista,
      qtdeRegistros,
    };
  }

  // ------------------------------------------------------------------------------------

  async getConfigCampos(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const config = await ConfigCampos.buscarTodos(`tabela = "${filtro.tabela}"`, nomeBanco);
    const campos = (await database.listarCampos(filtro.tabela, nomeBanco)).map(x => ({
      nome: x,
      // eslint-disable-next-line
      checked: config.find(q => q.nome_config_campos === x) ? true : false,
    }));
    return campos;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca e retorna escalas já com o nome do primeiro grupo para serem
   * renderizadas no calendário.
   */
  async getEscalasCalendario(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    return await Escala.buscarEscalasCalendario(filtro.codCalendario, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda a senha da pessoa especificada.
   * @param {String|Number} codPessoa O código da pessoa.
   * @param {String} senha A senha da pessoa.
   */
  static async mudarSenhaFuncionario(codPessoa, senha, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return await Pessoa.mudarSenha(codPessoa, senha, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda o código da igreja do usuário enviado por parâmetro
   * @param {String|Number} codUsuario O código do usuário que irá modificar a igreja.
   * @param {String} igreja A igreja a ser salva.
   */
  static async mudarIgrejaUsuario(codUsuario, igreja, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return await Pessoa.mudarIgreja(codUsuario, igreja, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca e retorna uma escala de acordo com o código especificado.
   * @param {Object} filtro O filtro.
   * @param {Number} filtro.codEscala O código da escala.
   */
  async getEscalaModal(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    return await Escala.buscarEscalaModal(filtro.codEscala, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca e retorna as informações do caixa
   * @param {Object} filtro O filtro.
   * @param {Number} filtro.Igreja O código da igreja que está logada
   * @param {*} filtro.dep O departamento escolhido na tela
   * @param {*} filtro.pessoa A pessoa escolhida na tela
   * @param {String} filtro.codigoCliente O código do cliente passado logado
   */
  async getCaixa(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const valoresCaixa = await Caixa.retornaCaixa(filtro, nomeBanco);
    return valoresCaixa;
  }

  // ------------------------------------------------------------------------------------

  async getDashboard(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    const igreja = filtro.Igreja || 0;
    const n = null;
    const t = true;

    const valoresCaixa = await Caixa.valoresCaixaDashboard(filtro, nomeBanco);
    const igrejas = await Igreja.retornaTodasIgrejas(nomeBanco);
    const usuarios = await Pessoa.retornaUsuarios(igreja, nomeBanco);
    const membrosLocal = await Pessoa.retornaMembrosCongregados(igreja, nomeBanco, t);
    const congregadosLocal = await Pessoa.retornaMembrosCongregados(igreja, nomeBanco, n, t);
    const membrosCampo = await Pessoa.retornaMembrosCongregados(igreja, nomeBanco, n, n, t);
    const congregadosCampo = await Pessoa.retornaMembrosCongregados(igreja, nomeBanco, n, n, n, t);
    const aniversario = await Pessoa.retornaAniversariantes(igreja, nomeBanco);
    const aniversarioCasamento = await Pessoa.retornaAniversariantesCasamento(igreja, nomeBanco);

    return {
      credFuturo: valoresCaixa.credFuturo,
      debFuturo: valoresCaixa.debFuturo,
      saldoFuturo: valoresCaixa.saldoFuturo,
      credito: valoresCaixa.credito,
      debito: valoresCaixa.debito,
      saldo: valoresCaixa.saldo,
      igrejas: igrejas.qtde,
      usuarios: usuarios.qtde,
      membrosLocal: membrosLocal.qtde,
      congregadosLocal: congregadosLocal.qtde,
      membrosCampo: membrosCampo.qtde,
      congregadosCampo: congregadosCampo.qtde,
      aniversario: aniversario.aniversariantes,
      aniversarioCasamento: aniversarioCasamento.niverCasamento,
    };
  }

  // ------------------------------------------------------------------------------------

  static async gerarRelatorio(params) {
    const nomeBanco = params.codigoCliente ?
      await database.getBanco(params.codigoCliente) :
      params.nomeBanco;
    params = {
      ...params,
      nomeBanco,
    };
    params.codigoCliente && delete params.codigoCliente;
    return new Promise((resolve) => {
      const child_process = require('child_process');
      const caminhoDelphi = `${path.join(global.appDir, '../delphi')}\\Principal.exe`;
      const parametros = JSON.stringify(params).replace(/"/g, '$');
      // const logTxt = path.join(global.appDir, '../delphi/log.txt');
      // fs.appendFileSync(logTxt, caminhoDelphi + '######//');
      // fs.appendFileSync(logTxt, parametros + '######//');
      clipboardy.writeSync(parametros);
      child_process.exec(`"${caminhoDelphi}" "${parametros}"`, (err, res) => {
        if (res !== '') {
          let arquivo = res.trim();
          // fs.appendFileSync(logTxt, res + '######//');
          arquivo = arquivo.split('.pdf');
          arquivo = arquivo[0] + '.pdf';

          const HOMOLOGACAO = global.dadosAplicacao.ambiente === 0; // 0 = HOMOLOGACAO
          const publicBuild = HOMOLOGACAO ? 'public' : 'build/static';
          const pastaPublic = path.join(global.appDir, publicBuild); // dir da pasta public / static
          const nomeRandomico = new Date().getTime().toString() + '.pdf'; // tempo em millisegundos
          fs.copyFileSync(arquivo, path.join(pastaPublic, nomeRandomico));
          const result = '/' + (HOMOLOGACAO ? nomeRandomico : 'static/' + nomeRandomico);
          resolve(result);


          // const base64 = fs.readFileSync(arquivo).toString('base64');
          // resolve(base64);
          fs.unlinkSync(arquivo);
        } else {
          resolve(res);
        }
      });
    });
  }

  // ------------------------------------------------------------------------------------

  async Generico(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    return await database.query(`SELECT * FROM ${nomeBanco}.${filtro.tabela}`);
  }

  // ------------------------------------------------------------------------------------

  async GenericoUnico(filtro) {
    const nomeBanco = await database.getBanco(filtro.codigoCliente);
    return await database.queryFindOne(`SELECT * FROM ${nomeBanco}.${filtro.tabela}
      WHERE ${filtro.nomePk} = ${filtro.codigo}`);
  }

  // ------------------------------------------------------------------------------------

  async getMensagemSistema(filtro) {
    return await database.queryFindOne(`
      SELECT * FROM gestao.tab_mensagem_sistema
      WHERE cod_sistema = ${filtro.codSoftware}
      AND status_ativo = "S"
    `);
  }

  // ------------------------------------------------------------------------------------

  static async registrosAssociativa(codigoCliente, tabela, tabela2, pkNome, pkAtual,
    pkAssociNome, codBusca, coluna, codBusca2, coluna2, tabela3) {

    const lista = [];
    const nomeBanco = await database.getBanco(codigoCliente);
    const associ = await database.query(`SELECT * FROM ${nomeBanco}.${tabela}
      WHERE ${pkNome} = ${pkAtual} AND ${whereExclusao}`);
    for (const a of associ) {
      const b = await this.retornaNome(codigoCliente, tabela2, coluna, codBusca, a[pkAssociNome]);

      if (codBusca2) {
        const c = await this.retornaNome(codigoCliente, tabela3, coluna2, codBusca2, a[codBusca2]);
        lista.push({ [coluna]: b[coluna], [coluna2]: c[coluna2] });
      } else {
        lista.push({ [coluna]: b[coluna] });
      }
    }

    return lista;
  }

  // ------------------------------------------------------------------------------------

  async retornaNome(codigoCliente, tabela, coluna, codBusca, valor) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return await database.queryFindOne(`SELECT ${coluna} FROM ${nomeBanco}.${tabela}
      WHERE ${codBusca} = ${valor}`);
  }

  // ------------------------------------------------------------------------------------

  static async addRemovAssoci(codigoCliente, tabela, remove, pkAssociNome, valor, pkNome, valor2,
    registro) {
    const nomeBanco = await database.getBanco(codigoCliente);
    if (remove) {
      await database.query(`DELETE FROM ${nomeBanco}.${tabela}
        WHERE ${pkAssociNome} = ${valor} AND ${pkNome} = ${valor2}`);
      return;
    }
    delete registro.undefined;
    await database.query(`INSERT INTO ${nomeBanco}.${tabela} SET ?`, registro);
  }

  // ------------------------------------------------------------------------------------

  static async buscarCep(codigoCliente, cep) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const resposta = buscaCep(cep, { sync: true });
    if (!resposta.hasError) {
      let bairro = await database.queryFindOne(`SELECT cod_bairro FROM ${nomeBanco}.tab_bairro
      WHERE nome_bairro like "${resposta.bairro}"`);

      const cidade = await database.queryFindOne(`SELECT a.cod_cidade FROM ${nomeBanco}.tab_cidade a
      INNER JOIN tab_estado b USING(cod_estado)
      WHERE a.nome_cidade LIKE "${resposta.localidade}" AND b.sigla LIKE "${resposta.uf}"`);

      if (!bairro && resposta.bairro) {
        bairro = await database.query(`INSERT INTO ${nomeBanco}.tab_bairro SET ?`, {
          nome_bairro: resposta.bairro,
        });
      } else { bairro = { ...bairro, insertId: 0 }; }

      const dados = {
        ...resposta,
        bairro: bairro.cod_bairro ? bairro.cod_bairro : bairro.insertId,
        cidade: cidade ? cidade.cod_cidade : 0,
      };

      return dados;
    }
    throw new Error(`Erro: ${resposta.message}`);
  }

  // ------------------------------------------------------------------------------------

  static async executarBat(caminhoArquivo) {
    try {
      const { /*exec,*/ execFile } = require('child_process');
      return new Promise((resolve) => {
        execFile(caminhoArquivo, (err, stdout) => {
          if (err) {
            resolve(`Erro: ${err}`);
          }
          if (stdout) {
            resolve(stdout);
          }
        });
      });
    } catch (error) {
      return `Erro: ${error}`;
    }

    // exec(`ping 192.168.1.16`, (err, stdout) => {
    //   if (err) {
    //     console.log(err);
    //   }
    //   console.log(stdout);
    // });

  }

  // ------------------------------------------------------------------------------------

  static async dataMes() {
    const dt = await database.primUltDiaMes();
    return dt;
  }

  // ------------------------------------------------------------------------------------

  static async gerarFechamento(filtro, codigoCliente, codPessoa) {
    const nomeBanco = await database.getBanco(codigoCliente);

    await database.query(`DELETE FROM ${nomeBanco}.tab_transferencia
    WHERE mes_transferido = '${filtro.mes}' AND ano_transferido = '${filtro.ano}'`);

    const dt_ = filtro.ano + '-' + filtro.mes + '-1';
    const dt = await database.primUltDiaMes(dt_);
    const igrejas = await database.query(`SELECT a.cod_igreja, a.nome_igreja, a.margem_porc
      FROM ${nomeBanco}.tab_igreja a WHERE a.status_igreja <> 'S'`);

    for (const igreja of igrejas) {
      const credito = await database.query(`SELECT a.cod_cr, SUM(a.vr_recebido) vr_recebido
        FROM ${nomeBanco}.tab_cr a WHERE a.cod_igreja = ${igreja.cod_igreja}
        AND a.dt_hr_recebido BETWEEN "${dt.Primeira}" AND "${dt.Ultima}"`);

      const debito = await database.query(`SELECT a.cod_cp, SUM(a.vr_pago) vr_pago
        FROM ${nomeBanco}.tab_cp a WHERE a.cod_igreja = ${igreja.cod_igreja}
        AND a.dt_hr_pago BETWEEN "${dt.Primeira}" AND "${dt.Ultima}"`);

      const saldo = (credito[0].vr_recebido || 0) - (debito[0].vr_pago || 0);
      igreja.valorPorc = (igreja.margem_porc * saldo) / 100;

      await database.query(`INSERT INTO ${nomeBanco}.tab_transferencia set ?`, {
        cod_igreja: igreja.cod_igreja,
        nome_igreja: igreja.nome_igreja,
        cod_usuario_transferencia: codPessoa,
        dt_transferencia: moment(new Date()).format('YYYY-MM-DD HH:mm'),
        vr_transferido: igreja.valorPorc,
        porc_transferido: igreja.margem_porc,
        mes_transferido: filtro.mes,
        ano_transferido: filtro.ano,
        status_transferido: 'N',
      });
    }
    return igrejas;
  }

  // ------------------------------------------------------------------------------------

  static async buscaTransfereciaIgreja(filtro, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const igrejas = await database.query(
      `SELECT *, if(status_transferido = "S", "Sim", "Não") status,
      CONCAT(a.porc_transferido, " %") porc,
      CONCAT("R$ ", FORMAT(a.vr_transferido, 2,'de_DE')) valor
      FROM ${nomeBanco}.tab_transferencia a
      WHERE a.mes_transferido = ${filtro.mes} AND a.ano_transferido = ${filtro.ano}`);

    return igrejas;
  }

  // ------------------------------------------------------------------------------------

  static async transferirIgrejaSede(codigoCliente, igrejaSede, item, codPessoa, codCaixa) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const cr = {
      cod_caixa: codCaixa,
      cod_igreja: igrejaSede,
      dt_hr_cadastro: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      dt_hr_receber: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      dt_hr_recebido: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      vr_receber: item.vr_transferido,
      vr_recebido: item.vr_transferido,
      obs: 'Transferência de Valores',
      obs_baixa: 'Transf. da Igreja: ' + item.nome_igreja,
      cod_funcionario: codPessoa,
      cod_funcionario_baixa: codPessoa,
      cod_forma_pagamento: 0,
      cod_tipo_movimento: 13,
      cod_igreja_transferencia: item.cod_igreja,
    };
    await database.query(`INSERT INTO ${nomeBanco}.tab_cr set ?`, cr);

    const cp = {
      cod_caixa: codCaixa,
      cod_igreja: item.cod_igreja,
      dt_hr_cadastro: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      dt_hr_pagar: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      dt_hr_pago: moment(new Date()).format('YYYY-MM-DD HH:mm'),
      vr_pagar: item.vr_transferido,
      vr_pago: item.vr_transferido,
      obs: 'Transferência Para Sede',
      obs_baixa: 'Transferência Para Sede',
      cod_funcionario: codPessoa,
      cod_funcionario_baixa: codPessoa,
      cod_forma_pagamento: 0,
      cod_tipo_movimento: 14,
    };
    await database.query(`INSERT INTO ${nomeBanco}.tab_cp set ?`, cp);

    await database.query(`UPDATE ${nomeBanco}.tab_transferencia SET status_transferido = "S"
      WHERE cod_transferencia = ?`, item.cod_transferencia);

    return;
  }

  // ------------------------------------------------------------------------------------

  static async abrirRegistro(codigoCliente, registro, movimento) {
    const nomeBanco = await database.getBanco(codigoCliente);
    if (movimento === tipoMov.movimento.compra) {
      Compra.abrirCompra(registro.cod_compra, movimento, nomeBanco);
    } else if (movimento === tipoMov.movimento.cantina) {
      Cantina.abrirCantina(registro.cod_cantina, movimento, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  static async corrigirBanco(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const cidades = await Cidade.buscarTodos('', nomeBanco);

    for (const cidade of cidades) {
      await Cidade.salvar(cidade, nomeBanco);
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  static async retornaValorCaixaAnterior(igrejaLogada, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    return Caixa.retornaValorCaixaAnterior(igrejaLogada, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  static async buscarMembrosCelula(codCelula, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const membros = await CelulaPessoa.buscarMembrosCelula(codCelula, nomeBanco);
    return membros;
  }

  // ------------------------------------------------------------------------------------

  static async multiplicarCelula(nomeCelula, ladoEsquerdo, ladoDireito, codPessoaExclusao,
    codigoCliente) {
    const retorno = {
      status: true,
      erro: '',
    };
    const nomeBanco = await database.getBanco(codigoCliente);

    if (ladoDireito.length === 0) {
      retorno.status = false;
      retorno.erro = 'Não foi selecionado membros para a nova célula!';

    } else if (ladoEsquerdo.length === 0) {
      retorno.status = false;
      retorno.erro = 'Não está ficando membros para a célula multiplicada!';

    } else {
      await Celula.multiplicarCelula(nomeCelula, ladoDireito, codPessoaExclusao, nomeBanco);
    }

    return retorno;
  }

  // ------------------------------------------------------------------------------------

  static async buscarVisualizaInformacoes(objetoFiltro) {
    const nomeBanco = await database.getBanco(objetoFiltro.codigoCliente);
    const info = await database.queryFindOne(` SELECT ${objetoFiltro.campos}
      FROM ${nomeBanco}.${objetoFiltro.tabela}
      WHERE ${objetoFiltro.where}`);

    return info;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @param {String} nome O Nome da Query Ex: CadastroAlgumaCoisa
   * @param {Object} filtro Objeto de Filtros
   */
  static async buscarDados(nome, filtro) {
    switch (nome) {
      case 'CadGenerico':
        return this.getCadGenerico(filtro);
      case 'ConfigCampos':
        return this.getConfigCampos(filtro);
      case 'EscalaModal':
        return this.getEscalaModal(filtro);
      case 'EscalasCalendario':
        return this.getEscalasCalendario(filtro);
      case 'Generico':
        return this.Generico(filtro);
      case 'GenericoUnico':
        return this.GenericoUnico(filtro);
      case 'Caixa':
        return this.getCaixa(filtro);
      case 'getDashboard':
        return this.getDashboard(filtro);
      case 'TrocarIgreja':
        return this.TrocarIgreja(filtro);
      case 'MensagemSistema':
        return this.getMensagemSistema(filtro);
      default:
        throw new Error('Método de busca não encontrado! (' + nome + ')');
    }
  }

  // ------------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------------
  // ---------------------------------Pessoa---------------------------------------------------

  static async buscarMembros(codPessoa, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    return Pessoa.buscarMembros(codPessoa, nomeBanco, codigoCliente);
  }

  static async alteraStatusPessoa(codPessoa, status, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const pessoa = await Pessoa.alteraStatusAtivoPessoa(codPessoa, status, nomeBanco);
    return pessoa;
  }

  static async salvarPessoa(pessoa, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    return await Pessoa.salvar(pessoa, nomeBanco, true);
  }

  // ----------------------------------Célula--------------------------------------------------

  static async retornaDistrito(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const distritos = await Distrito.buscarTodos(nomeBanco);
    return distritos;
  }

  static async retornaArea(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const areas = await Area.buscarTodos(nomeBanco);
    return areas;
  }

  static async retornaSetor(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const setores = await Setor.buscarTodos(nomeBanco);
    return setores;
  }

  static async retornaCelula(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const celulas = await Celula.buscarTodos(nomeBanco);
    return celulas;
  }

  // ----------------------------------Tesouraria-------------------------------------------

  static async retornaDizimo(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const dizimos = await this.retornaCrCp('CR', Tipo.movimento.dizimo, nomeBanco);
    return dizimos;
  }

  static async retornaOferta(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const ofertas = await this.retornaCrCp('CR', Tipo.movimento.oferta, nomeBanco);
    return ofertas;
  }

  static async salvaDizimo(registro, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const cr = {
      ...registro,
      cod_tipo_movimento: Tipo.movimento.dizimo,
    };
    const dizimo = await this.salvaCrCp('CR', cr, nomeBanco);
    return dizimo;
  }

  static async salvaOferta(registro, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const cr = {
      ...registro,
      cod_tipo_movimento: Tipo.movimento.oferta,
    };
    const oferta = await this.salvaCrCp('CR', cr, nomeBanco);
    return oferta;
  }

  // ----------------------------------Util--------------------------------------------------

  async retornaCrCp(tipo, tipoMovimento, nomeBanco) {
    const filtro = {
      where: ` cod_tipo_movimento = ${tipoMovimento} `,
      nomeBanco,
    };

    if (tipo === 'CR') {
      const crs = await CR.buscarDadosCadastro(filtro);
      return crs;

    } else if (tipo === 'CP') {
      const cps = await CP.buscarDadosCadastro(filtro);
      return cps;

    }
  }

  async salvaCrCp(tipo, registro, nomeBanco) {

    if (tipo === 'CR') {
      const cr = await CR.salvar(registro, nomeBanco);
      return cr;

    } else if (tipo === 'CP') {
      const cp = await CP.salvar(registro, nomeBanco);
      return cp;

    }
  }

  // --------------------------------------Secretaria------------------------------------

  static async retornaDepartamento(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const departamentos = await Departamento.buscarTodos('', nomeBanco);
    return departamentos;
  }

  static async salvaChamada(chamada, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const chamadaSalva = await Chamada.salvar(chamada, false, nomeBanco);
    return chamadaSalva;
  }

  static async finalizarChamada(chamada, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const chamadaSalva = await Chamada.salvar(chamada, true, nomeBanco);
    return chamadaSalva;
  }

  // ------------------------------------Produto------------------------------------------------

  static async retornaProduto(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const produtos = await Produto.buscarTodos('', nomeBanco);
    return produtos;
  }

  static async alteraStatusProduto(codProduto, status, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const produto = await Produto.alteraStatusAtivoProduto(codProduto, status, nomeBanco);
    return produto;
  }

  static async salvarProduto(produto, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const produtoSalva = await Produto.salvar(produto, nomeBanco);
    return produtoSalva;
  }

  // ------------------------------------Evento------------------------------------------------

  static async retornaEvento(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const eventos = await Evento.buscarTodos('', nomeBanco);
    return eventos;
  }

  static async alteraStatusEvento(codEvento, status, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const evento = await Evento.alteraStatusAtivoEvento(codEvento, status, nomeBanco);
    return evento;
  }

  static async salvarEvento(evento, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const eventoSalva = await Evento.salvar(evento, nomeBanco);
    return eventoSalva;
  }

  // ------------------------------------------------------------------------------------

   // ------------------------------------Curso------------------------------------------------

   static async retornaCurso(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const cursos = await Curso.buscarTodos('', nomeBanco);
    return cursos;
  }

  static async alteraStatusCurso(codCurso, status, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const curso = await Curso.alteraStatusAtivoCurso(codCurso, status, nomeBanco);
    return curso;
  }

  static async salvarCurso(curso, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);

    const cursoSalva = await Curso.salvar(curso, nomeBanco);
    return cursoSalva;
  }

  // ------------------------------------------------------------------------------------

  static async retornaCR(codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const crs = await CR.buscarTodos('', nomeBanco);
    return crs;
  }

  // -------------------------------------------------------------------------------------------

  static async salvaPedido(pedido, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const pedidoSalva = await Pedido.salvar(pedido, nomeBanco);
    return pedidoSalva;
  }

};
