const Pessoa = require('../entidade/Pessoa');
const MsgChat = require('../lib_backend/entidade/MsgChat');
const sockets = require('../lib_backend/funcoes/sockets');
const database = require('../lib_backend/funcoes/database');
const moment = require('moment');

/**
 * Emite a lista de funcionários para todos os funcionários logados
 * no sistema no momento. Esse método é chamado toda vez que um funcionário
 * conecta ou desconecta.
 * @param {IO} io O Servidor de socket.
 */
async function emitirFuncionariosChat(io, nomeBanco) {
  const a = nomeBanco || 'igreja';
  const newUsuarios = (await Pessoa.getUsuarios(a)).map(x => Object.assign({}, x));

  for (const key in io.sockets.sockets) {
    if (io.sockets.sockets.hasOwnProperty(key)) {
      const socket = io.sockets.sockets[key];
      (async function () {
        // Agora vamos emitir a lista de usuários com 
        // as suas respectivas conversas pro frontend:
        for (const usuario of newUsuarios) {
          const conexoes = sockets.buscarSockets(io, 'codPessoa', usuario.cod_pessoa);
          usuario.online = conexoes.length > 0;
          usuario.conversa = await MsgChat.getConversa(usuario.cod_pessoa, socket.codPessoa);
        }
        socket.emit('funcionariosChat', newUsuarios);
      }());
    }
  }
}

module.exports = (io) => {
  io.on('connection', (socket) => {

    /**
     * Registra esse socket como uma pessoa.
     * Chamado pelo frontend assim que o socket se conecta.
     */
    socket.on('registrar', async (filtro) => {
      const nomeBanco = await database.getBanco(filtro.codigoCliente);
      socket.codPessoa = filtro.Pessoa;
      emitirFuncionariosChat(io, nomeBanco);
    });

    socket.on('disconnect', () => {
      emitirFuncionariosChat(io);
    });

    /**
     * Marca as mensagens da conversa entre a pessoa
     * desse socket e a pessoa do parâmetro como "lidas".
     * Esse método é chamado somente quando a pessoa do socket
     * entra na conversa, para mensagens em tempo real é chamado
     * o método de cima.
     */
    socket.on('marcarMensagensComoLida', async (codPessoa) => {
      await MsgChat.marcarMensagensComoLida(socket.codPessoa, codPessoa);

      // Agora vamos alertar a pessoa na qual entramos na conversa que suas
      // mensagens foram lidos por nós:
      const conexoes = sockets.buscarSockets(io, 'codPessoa', codPessoa);
      for (const sock of conexoes) {
        sock.emit('marcarMensagensComoLida', socket.codPessoa);
      }
    });

    socket.on('mensagemChat', async (dados) => {

      const data = dados.dt_hr_envio;
      dados.dt_hr_envio = moment(data, 'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
      dados.cod_msg_chat = await MsgChat.salvar(dados);

      dados.dt_hr_envio = data; // Volta data ao formato original

      const conexoes = sockets.buscarSockets(io, 'codPessoa', dados.cod_pessoa_destino);
      for (const sock of conexoes) {
        // Emite a mensagem e aguarda ela ser lida, se ela não retornar
        // do callback, não será marcado como lida.
        sock.emit('mensagemChat', dados);
      }
    });

  });
};
