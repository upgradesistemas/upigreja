exports.softWare = {

  /**
   * Verifica e retorna se o tipo de softWare é valido.
   */
  valido: function (tipo) {
    const keys = Object.keys(this);
    const ultimaProp = this[keys[keys.length - 1]];
    return tipo && tipo <= ultimaProp;
  },

  gestao      : 1,
  igreja      : 2,
  logistica   : 3,
};

exports.getNomeMovimento = (tipo) => {
  switch (tipo) {
    case 1: return 'GESTÃO';
    case 2: return 'IGREJA';
    case 3: return 'LOGÍSTICA';
    default: throw new Error('Tipo de softWare inválido!');
  }
};
