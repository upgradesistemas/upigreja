import * as Upgrade from './lib_frontend/main/Upgrade';
import Dashboard from './componentes/Dashboard';

// Cadastros ------------
import CadastroBairro from './componentes/CadastroBairro';
import CadastroCidade from './componentes/CadastroCidade';
import CadastroCargo from './componentes/CadastroCargo';
import CadastroIgreja from './componentes/CadastroIgreja';
import CadastroEstado from './componentes/CadastroEstado';
import CadastroGrauParentesco from './componentes/CadastroGrauParentesco';
import CadastroPessoa from './componentes/CadastroPessoa';
import CadastroNivel from './componentes/CadastroNivel';
import CadastroSexo from './componentes/CadastroSexo';
import CadastroPerfil from './componentes/CadastroPerfil';
import CadastroMinisterio from './componentes/CadastroMinisterio';
import CadastroCalendario from './componentes/CadastroCalendario';
import CadastroDepartamento from './componentes/CadastroDepartamento';
import CadastroEventos from './componentes/CadastroEventos';
import CadastroSetorIgreja from './componentes/CadastroSetorIgreja.jsx';
import CadastroClasse from './componentes/CadastroClasse';
import CadastroCurso from './componentes/CadastroCurso';
import CadastroProduto from './componentes/CadastroProduto';
import CadastroSetor from './componentes/CadastroSetor';
import CadastroDistrito from './componentes/CadastroDistrito';
import CadastroArea from './componentes/CadastroArea';
import CadastroCelula from './componentes/CadastroCelula';
import CelulaMultiplicacao from './componentes/CelulaMultiplicacao.jsx';
import CadastroCulto from './componentes/CadastroCulto';
import CadastroCentroCusto from './componentes/CadastroCentroCusto';
import CadastroEstruturaPC from './componentes/CadastroEstruturaPC';
import CadastroGrupoPC from './componentes/CadastroGrupoPC';
import CadastroSubGrupoPC from './componentes/CadastroSubGrupoPC';
import CadastroContaAnalisePC from './componentes/CadastroContaAnalisePC';
import CadastroDivisaoSubGrupoPC from './componentes/CadastroDivisaoSubGrupoPC';
import SolicitacaoVisOra from './componentes/SolicitacaoVisOra';
import CadastroSetorBalanco from './componentes/CadastroSetorBalanco';
import CadastroBalanco from './componentes/CadastroBalanco';


// Utilitários ------------
import ModalMudarSenhaFunc from './componentes/ModalMudarSenhaFunc.jsx';
import ModalTrocarIgreja from './componentes/ModalTrocarIgreja.jsx';
import ModalMudarUsuarioIgreja from './componentes/ModalMudarUsuarioIgreja.jsx';
import ControleVersao from './componentes/ControleVersao.jsx';
import ModalCorrigirBanco from './componentes/ModalCorrigirBanco.jsx';

// Movimentações ------------
import MovContasReceber from './componentes/MovContasReceber.jsx';
import MovContasPagar from './componentes/MovContasPagar.jsx';
import MovOferta from './componentes/MovOferta';
import MovDizimo from './componentes/MovDizimo';
import MovCaixa from './componentes/MovCaixa.jsx';
import ModalCaixa from './componentes/ModalCaixa.jsx';
import MovPromissoria from './componentes/MovPromissoria';
import MovRevistaEbd from './componentes/MovRevistaEbd';
import MovTransferencia from './componentes/MovTransferencia.jsx';
import MovCompra from './componentes/MovCompra.jsx';
import MovCantina from './componentes/MovCantina.jsx';

// Documentos ------------
import RelCartas from './componentes/relatorio/RelCartas.jsx';

// Configurações ------------
import ConfiguracaoApp from './componentes/config/ConfiguracaoApp';

// Relatórios ------------
import RelatorioPessoa from './componentes/relatorio/RelatorioPessoa.jsx';
import RelatorioAniversariantes from './componentes/relatorio/RelatorioAniversariantes';
import RelatorioEvento from './componentes/relatorio/RelatorioEvento';
import RelatorioMembrosDizimistas from './componentes/relatorio/RelatorioMembrosDizimistas';
import RelatorioDespesas from './componentes/relatorio/RelatorioDespesas';
import RelatorioTransferencia from './componentes/relatorio/RelatorioTransferencia';
import Carteirinha from './componentes/relatorio/Carteirinha';
import RelatorioCompraDetalhada from './componentes/relatorio/RelatorioCompraDetalhada';
import RelatorioEntradas from './componentes/relatorio/RelatorioEntradas';
import RelatorioFinanceiroCaixa from './componentes/relatorio/RelatorioFinanceiroCaixa.jsx';
import RelatorioOfertantes from './componentes/relatorio/RelatorioOfertantes.jsx';
import RelatorioBalanco from './componentes/relatorio/RelatorioBalanco.jsx';


// Estilos ------------
import './app.css';

Upgrade.iniciarAplicacao({
  menus: (state, createItem) => ([
    createItem('dashboard', 'Dashboard', 'fa-tachometer', Dashboard),

    createItem('cadastros', 'Cadastros', 'fa-edit', null, [
      createItem('bairro', 'Bairro', 'fa-thumb-tack', CadastroBairro),
      createItem('cidade', 'Cidade', 'fa-globe', CadastroCidade),
      createItem('cargo', 'Cargo', 'fa-gears', CadastroCargo),
      createItem('classe', 'Classes', 'fa-clipboard', CadastroClasse),
      createItem('culto', 'Culto', 'fa-fire', CadastroCulto),
      createItem('curso', 'Curso', 'fa-book', CadastroCurso),
      createItem('departamento', 'Departamento', 'fa-archive', CadastroDepartamento),
      createItem('estado', 'Estado', 'fa-globe', CadastroEstado),
      createItem('eventos', 'Eventos', 'fa-calendar', CadastroEventos),
      createItem('grau-parentesco', 'Grau de Parentesco', 'fa-group', CadastroGrauParentesco),
      createItem('igreja', 'Igrejas', 'fa-bank', CadastroIgreja),
      createItem('ministerio', 'Ministério', 'fa-cubes', CadastroMinisterio),
      createItem('nivel', 'Nivel de Acesso', 'fa-key', CadastroNivel),
      createItem('perfil', 'Perfil de Acesso', 'fa fa-user', CadastroPerfil),
      createItem('pessoa', 'Pessoas', 'fa-user-plus', CadastroPessoa),
      createItem('produto', 'Produto', 'fa-apple', CadastroProduto),
      createItem('sexo', 'Sexo', 'fa-intersex', CadastroSexo),
      createItem('setor', 'Setor de Igreja', 'fa-sitemap', CadastroSetorIgreja),
    ]),

    createItem('tesouraria', 'Tesouraria', 'fa-usd', null, [
      createItem('centro-custos', 'Centro de Custo', 'fa-money', CadastroCentroCusto),
      createItem('dizimo', 'Lançamento de Dízimo', 'fa-money', MovDizimo),
      createItem('oferta', 'Lançamento de Oferta', 'fa-money', MovOferta),
      createItem('Modal-caixa', 'Abrir / Fechar Caixa', 'fa-usd', ModalCaixa),
      createItem('caixa', 'Caixa', 'fa-lock', MovCaixa),
      createItem('contas-pagar', 'Contas a Pagar', 'fa-arrow-down', MovContasPagar),
      createItem('contas-receber', 'Contas a Receber', 'fa-arrow-up', MovContasReceber),
      createItem('transferencia', 'Tranferência', 'fa-refresh', MovTransferencia),
    ]),

    createItem('balanco', 'Balanço Patrimonial', 'fa-users', null, [
      createItem('setor', 'Setores', 'fa-users', CadastroSetorBalanco),
      createItem('objeto', 'Lançamento dos Objetos', 'fa-users', CadastroBalanco),
    ]),

    createItem('capelania', 'Capelania', 'fa-users', null, [
      createItem('solicitacoes', 'Pedidos de Orações e Visita', 'fa-users', SolicitacaoVisOra),
    ]),

    createItem('celula', 'Célula', 'fa-users', null, [
      createItem('distrito', 'Distrito', 'fa-globe', CadastroDistrito),
      createItem('area', 'Área', 'fa-globe', CadastroArea),
      createItem('setor', 'Setor', 'fa-sitemap', CadastroSetor),
      createItem('celula', 'Célula', 'fa-cubes', CadastroCelula),
      createItem('celula-multiplicacao', 'Multiplicação de Célula', 'fa-users',
        CelulaMultiplicacao),
    ]),

    createItem('cantina', 'Cantina', 'fa-edit', null, [
      createItem('promissoria', 'Nota Promissória', 'fa-edit', MovPromissoria),
      createItem('cantina', 'Cantina', 'fa-edit', MovCantina),
    ]),

    createItem('calendario', 'Escalas e Agenda', 'fa-calendar', CadastroCalendario),

    createItem('ebd', 'Escola Bíblica', 'fa-edit', null, [
      createItem('membrosRevistas', 'Membros x Revistas', 'fa-edit', MovRevistaEbd),
    ]),

    createItem('documentos', 'Documentos', 'fa-book', null, [
      createItem('cartas', 'Cartas', 'fa-envelope', RelCartas),
      createItem('carteirinha', 'Carteirinha de Membro', 'fa-address-card', Carteirinha),
      // createItem('certificados', 'Certificados', 'fa-certificate'),
      // createItem('credenciais', 'Credenciais', 'fa-check'),
      // createItem('atas', 'Ata de Reunião', 'fa-bars'),
    ]),

    createItem('movimento', 'Movimento', 'fa-refresh', null, [
      //createItem('chamada', 'Chamada', 'fa-edit'),
      createItem('compra', 'Compra', 'fa-cart-plus', MovCompra),
    ]),

    createItem('util', 'Utilitários', 'fa-cube', null, [
      createItem('Modal-mudar-senha', 'Alterar Senha', 'fa-edit', ModalMudarSenhaFunc),
      createItem('Modal-trocar-igreja', 'Trocar Igreja', 'fa-refresh', ModalTrocarIgreja),
      createItem('Modal-mudar-usu-igreja', 'Mudar Pessoa Igreja', 'fa-edit',
        ModalMudarUsuarioIgreja),
      createItem('controle-versao', 'Versionamento', 'fa-bookmark', ControleVersao),
      createItem('Modal-corrigir-banco', 'Corrigir Banco', 'fa-database', ModalCorrigirBanco),
      createItem('backup', 'Backup', 'fa-database', ModalCorrigirBanco),
      createItem('ajuda', 'Ajuda', 'fa-database', ModalCorrigirBanco),
    ]),

    createItem('relatorio', 'Relatórios Cadastrais', 'fa-line-chart', null, [
      createItem('Pessoa', 'Pessoa', 'fa-bar-chart', RelatorioPessoa),
      createItem('Evento', 'Evento', 'fa-bar-chart', RelatorioEvento),
      createItem('balanco', 'Balanço', 'fa-bar-chart', RelatorioBalanco),
    ]),

    createItem('relatorio', 'Relatórios Gerenciais', 'fa-line-chart', null, [
      createItem('Aniversariantes', 'Aniversariantes do Mês', 'fa-bar-chart',
        RelatorioAniversariantes),
      createItem('CompraDetalhada', 'Compra Detalhada', 'fa-bar-chart', RelatorioCompraDetalhada),
      createItem('MembrosDizimos', 'Membros x Dízimos', 'fa-bar-chart', RelatorioMembrosDizimistas),
      createItem('Despesas', 'Despesas', 'fa-bar-chart', RelatorioDespesas),
      createItem('Entradas', 'Entradas', 'fa-bar-chart', RelatorioEntradas),
      createItem('financeiro-caixa', 'Financeiro de Caixa', 'fa-bar-chart',
        RelatorioFinanceiroCaixa),
      createItem('Transferencia', 'Transferência', 'fa-bar-chart', RelatorioTransferencia),
      createItem('ofertantes', 'Ofertantes', 'fa-bar-chart', RelatorioOfertantes),
    ]),

    createItem('configPlano', 'Confiig. Plano de Contas', 'fa-cog', null, [
      createItem('estrutura', 'Estrutura', 'fa-cog', CadastroEstruturaPC),
      createItem('grupo', 'Grupo', 'fa-cog', CadastroGrupoPC),
      createItem('subGrupo', 'Sub-Grupo', 'fa-cog', CadastroSubGrupoPC),
      createItem('divisao', 'Divisão', 'fa-cog', CadastroDivisaoSubGrupoPC),
      createItem('contaAnalise', 'Conta Análise', 'fa-cog', CadastroContaAnalisePC),
    ]),

    createItem('config', 'Configurações', 'fa-cog', ConfiguracaoApp),
  ]),
  eventos: {
    /**
     * Setamos um evento para ouvir o setConfig para que sempre que a configuração
     * do sistema for setada a gente veririca se o layout branco está em uso para
     * mudar o className do body.
     */
    setConfig: ({ config }) => {
      document.body.className = document.body.className.replace('skin-blue-light ', 'skin-blue ');
      if (config.usuario && config.usuario.status_usa_layout_branco === 'S') {
        document.body.className = document.body.className.replace('skin-blue ', 'skin-blue-light ');
      }
    },
  },
  options: {
    usarChatFuncionario: false,
    usarFiltro: true,
  },
  nomeAplicacao: 'UpIgreja',
  cookie: 'upIgrejaAuth',
  icone: 'fa-bank',
});
