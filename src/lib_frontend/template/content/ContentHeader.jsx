import React from 'react';

export default props => (
  <section className='content-header flex-wrap'>
    <h1>{props.title} 
    </h1>
    <small>{props.small}</small>
  </section>
);
