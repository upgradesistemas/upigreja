import React from 'react';

export default (props) => (
  <footer className='main-footer'>
    <strong>
      Copyright &copy; 2018
      <a href={props.link} target='_blank' rel="noopener noreferrer">
        {' ' + props.titulo}</a>.
      Suporte - (44) 9 9830-6856 / (44) 9 9833-0366.
    </strong>
  </footer>
);
