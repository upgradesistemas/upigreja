import React from 'react';
import { connect } from 'react-redux';
import Navbar from './Navbar';

import logo from '../../../imagens/Login/logo.png'

const header = (props) => (
  <header className='main-header'>

    <a href={props.rota} className='logo fundo-logo'>
      {/* O Logo pequeno: */}
      <span className='logo-mini'>
        {props.tituloPequeno}
      </span>

      {/* O Logo grande com ícone: */}
      {/* <span className='logo-lg'>
        {props.icon && <i className={'fa ' + props.icon} />}
        <span style={{ marginLeft: '5px' }} />
        {props.tituloGrande}
      </span> */}

      <img src={logo} alt="logo upigreja" className="logo-header" />
    </a>

    {/* A Barra: */}
    <nav className='navbar navbar-static-top barraCima'>
      <a className='sidebar-toggle' data-toggle='offcanvas'>
        <span className='removerSpan'>.</span>
      </a>

      <div className='voce-esta'>
        <span className='voce-esta-em'>
          Você está em: <span className='nome-igreja-header'> {props.igrejaLogada}<span className="titulo-header"> / {props.tituloContentWrapper.titulo}</span></span>
        </span>
      </div>



      <Navbar usuario={props.usuario}
        options={props.options}
        onLogout={props.onLogout} />
    </nav>

  </header>
);

export default connect(({ tituloContentWrapper }) => ({
  tituloContentWrapper,
}))(header);
