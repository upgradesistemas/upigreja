import { connect } from 'react-redux';
import React from 'react';
import Button from '../../componentes/Button.jsx';

let Nivel;
let NomePessoa;

/**
 * Mostra informações do usuário na barra superior da aplicação, mostra foto dele
 * e o nome, ao clicar no componente um painel aparece pra realizar o logout.
 */
class Navbar extends React.Component {

	constructor(props) {
		super(props);
		Nivel = props.usuario.nivel.nome_nivel;
		NomePessoa = props.usuario.nome_pessoa;
	}

  // ------------------------------------------------------------------------------------

	state = {
		open: false,
	}

  // ------------------------------------------------------------------------------------

	/**
	 * Abre o menu sobreposto.
	 */
	openMenu() {
		this.setState({ open: true });
	}

  // ------------------------------------------------------------------------------------

	/**
	 * Fecha o menu sobreposto.
	 */
	closeMenu() {
		this.setState({ open: false });
	}

  // ------------------------------------------------------------------------------------

	/**
	 * Abre o Chat.
	 */
	abreChat() {
		// alert('colocar o chat aqui');
	}

  // ------------------------------------------------------------------------------------

	render() {
		return (
			<div className="navbar-custom-menu">
				<ul className="nav navbar-nav">

					{/* <li className="dropdown-toogle" data-toogle="dropdown"
						onClick={this.abreChat.bind(this)}
						aria-expanded="false">
						<a className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i className="fa fa-envelope-o"></i>
							{/* <span className="label label-warning">10</span> *
						</a>
					</li> */}

					<li onMouseEnter={this.openMenu.bind(this)}
						onMouseLeave={this.closeMenu.bind(this)}
						className={`dropdown user user-menu ${this.state.open ? 'open' : ''}`}>
						<a aria-expanded={this.state.open ? 'true' : 'false'}
							className="dropdown-toggle">
							<i className="fa fa-user-circle " />
							<span className="hidden-xs">{NomePessoa}</span>
						</a>

						<ul className="dropdown-menu">

							{/* Painel principal: */}
							<li className="user-header">
								<p>
									{NomePessoa}
									<br /><br />
									{'Nível de Usuário - ' + Nivel}
								</p>
							</li>

							{/* Rodapé do menu que aparece */}
							<li className="user-footer shadow-up-1">
								{/* Botão de sair: */}
								<Button className="btn-danger btn-block btn-flat"
									label="Sair"
									icon="fa-sign-out"
									onClick={this.props.onLogout} />

								<Button className="btn-info btn-block btn-flat"
									label="Perfil"
									icon="fa-user-o" />
							</li>
						</ul>
					</li>
				</ul>
			</div >
		);
	}
}

export default connect((state) => ({
	usuario: state.usuario,
}))(Navbar);
