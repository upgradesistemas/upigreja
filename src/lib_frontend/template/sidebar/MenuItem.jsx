import React from 'react';
import { Link } from 'react-router-dom';

export default props => (
	<li> 
		{props.onClick ? (
			// Se tem um 'onClick' o clique será tratado por fora, então
			// não podemos colocar um link para outra página.
			<a to={props.path} onClick={props.onClick}>
				<i className={`fa ${props.icon}`} /> 
				<span>{props.label}</span>
			</a>
		) : (
			// onClick não foi informado, ou seja, teremos que tratar o caminho
			// diretamente por aqui mesmo
			<Link to={props.path}>
				<i className={`fa ${props.icon}`} /> 
				<span>{props.label}</span>
			</Link>
		)}
	</li>
);
