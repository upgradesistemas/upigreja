import React from 'react';
import { connect } from 'react-redux';
import Input from '../../componentes/Input.jsx';
import dadosAplicacao from '../../main/dadosAplicacao.jsx';

class SideBar extends React.Component {

  onChangeFiltro(e) {
    this.props.dispatch({
      type: 'setFiltroItens',
      valor: e,
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o filtro de itens dos itens do menu.
   */
  renderFiltroItens() {
    return (
      <div className="app-input-pesquisa-container">
        <Input value={this.props.filtroItens}
          placeholder='Pesquisar...'
          id="app-input-pesquisa"
          name={new Date().getTime().toString()}
          onChange={this.onChangeFiltro.bind(this)} />
        <i className="fa fa-search" />
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <aside className='main-sidebar relative'>
        <section className='sidebar'>

          <div className="user-panel">
            <div className="pull-left-image" />

            <div className="pull-left-info">
              <p>{this.props.usuario}</p>
              <a><i className="fa fa-circle text-success" /> Online</a>
            </div>
          </div>

          {dadosAplicacao.options.usarFiltro && this.renderFiltroItens()}

          <ul className='sidebar-menu'>
            {/* Items do menu */}
            {this.props.children}
          </ul>

        </section>
      </aside>
    );
  }

}

export default connect((state) => ({
  filtroItens: state.filtroItens,
}), (dispatch) => ({
  dispatch,
}))(SideBar);

