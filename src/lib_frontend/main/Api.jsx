import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import Cookies from './Cookies.jsx';
import dadosAplicacao from './dadosAplicacao.jsx';

function validarCookie() {
  const cookie = dadosAplicacao.cookie;
  if (Cookies.get(cookie)) {
    axios.defaults.headers.token = Cookies.get(cookie);
  }
}

/**
 * Realiza um post na url informada no constructor na rota
 * especificada e com os parâmetros especificados, o token do cookie
 * já é colocado automaticamente no header do post.
 */
async function post(rota, params) {
  validarCookie();
  const res = await axios.post(dadosAplicacao.urlApi + '/' + rota, [...params]);
  return res.data;
}

/**
 * Realiza um get na url informada no constructor na rota
 * especificada e com os parâmetros especificados, o token do cookie
 * já é colocado automaticamente no header do get.
 */
async function get(rota, params) {
  validarCookie();
  const res = await axios.get(dadosAplicacao.urlApi + '/' + rota, { params });
  return res.data;
}

export default async function (rota, ...params) {
  try {
    const method = String(rota).startsWith('get') ? 'get' : 'post';
    if (method === 'get') {
      return await get(rota, ...params);
    } 
    return await post(rota, params);
  } catch (ex) {
    if (ex.message === 'Network Error') {
      toastr.error('Erro de conexão!', 
        'Não foi possível atingir o servidor (' + rota + ')',
        { attention: true });
    }
    throw ex;
  }
};
