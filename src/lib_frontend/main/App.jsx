import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { toastr } from 'react-redux-toastr';
import Header from '../template/header/Header.jsx';
import SideBar from '../template/sidebar/SideBar.jsx';
import MenuItem from '../template/sidebar/MenuItem.jsx';
import MenuTree from '../template/sidebar/MenuTree.jsx';
import ContentWrapper from '../componentes/ContentWrapper.jsx';
import Footer from '../template/footer/Footer.jsx';
import Cookies from './Cookies.jsx';
import Api from './Api.jsx';
import UpValidadorSessao from '../../lib_frontend/componentes/UpValidadorSessao.jsx';
import dadosAplicacao from './dadosAplicacao.jsx';
import Socket from '../componentes/Socket.jsx';
import Chat from '../componentes/chat/Chat.jsx';
import { trocarRota } from '../../utils/funcoesFront';

let NomeIgrejaLogada;

/**
 * Tela da aplicação.
 */
class App extends React.Component {

  state = {
    validado: false, // Diz se a autenticação foi validada ou não
  }

  // ------------------------------------------------------------------------------------

  async componentWillMount() {
    // skin-purple-light fixed sidebar-mini sidebar-collapse
    document.body.className = 'skin-blue sidebar-mini ';
    const ret = await Api('validarAuth');
    if (ret.status) {
      // Da os dispatches padrões:
      const { dispatch } = this.props;
      dispatch({ type: 'setUsuario', usuario: ret.dados.usuario });
      dispatch({ type: 'setPermissaoTelas', permissoesTelas: ret.dados.permissoesTelas });
      dispatch({ type: 'setConfig', config: ret.dados.config });

      NomeIgrejaLogada = this.props.usuario.igrejaLogada.NomeIgreja;

      // Validação da configuração de menu aberto:
      if (ret.dados.config.gerais.status_usa_menu_aberto !== 'S') {
        document.body.className += ' sidebar-collapse ';
      }

      // Vamos setar como autenticação validada para renderizar o conteúdo:
      this.setState({ validado: true });
    } else {
      console.log(ret);
      toastr.error('Atenção!', ret.erro, { attention: true });
      trocarRota('/');
    }
  }

  // ------------------------------------------------------------------------------------

  componentWillReceiveProps(nextProps) {
    const { mensagensNaoLidas } = nextProps.socket;
    // Valida o título para colocar aquele (1) paras as mensagens:
    if (mensagensNaoLidas && mensagensNaoLidas.length > 0) {
      // Quantidade de mensagens lidas mudaram
      document.title = `(${mensagensNaoLidas.length}) ${dadosAplicacao.nomeAplicacao}`;
    } else if (document.title !== dadosAplicacao.nomeAplicacao) {
      // Qtde não mudou, vamos apenas atualizar o título se está diferente
      document.title = dadosAplicacao.nomeAplicacao;
    }
  }

  // ------------------------------------------------------------------------------------

  getConteudo() {
    return dadosAplicacao.getMenus(this.props.redux, true);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza o logout da aplicação, isso apaga o token de autenticação
   * e retorna à pagina de login.
   */
  realizarLogout() {
    Cookies.erase(dadosAplicacao.cookie);
    this.props.history.push('/');
    this.props.socket.desconectar();
    localStorage.removeItem('senhaCriptografada');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza os itens de menu da aplicação.
   */
  renderMenuItens() {

    /**
     * Função interna para centralizar a renderização
     * dos itens do menu.
     */
    function renderMenuItem(caminhoItem, item) {
      /**
       * Renderiza um modal no menu item.
       */
      function renderModal(menuItem) {
        const refModal = menuItem.refModal;
        if (refModal) {
          refModal.getWrappedInstance && refModal.getWrappedInstance().show();
          refModal.show && refModal.show();
        }

        // Remove a div temporária caso ela exista:
        document.getElementById('modal-div') && document.getElementById('modal-div').remove();
        const tempDiv = document.createElement('div');
        tempDiv.id = 'modal-div';
        document.body.append(tempDiv);

        ReactDOM.render(
          <Provider store={dadosAplicacao.store}>
            <item.render ref={e => menuItem.refModal = e} mostrarAutomaticamente />
          </Provider>,
          document.getElementById('modal-div')
        );
      }

      const nomeDaTela = item.render ? item.render.displayName || item.render.name : '';
      const nomeRota = item.rota ? item.rota : '';

      return (
        <MenuItem key={item.rota}
          path={caminhoItem}
          label={item.label}
          icon={item.icone}
          onClick={nomeDaTela.includes('Modal') || nomeRota.includes('Modal') ?
            renderModal.bind(this, item) :
            null} />
      );
    }

    const conteudosVisiveis = this.getConteudo().filter(x => !x.invisivel);
    return conteudosVisiveis.map(x => {
      if (x.children.length) {
        // O Menu possui sub-menus dentro dele
        const filhosVisiveis = x.children.filter(q => !q.invisivel);
        return (
          <MenuTree key={x.rota} label={x.label} icon={x.icone}>
            {filhosVisiveis.map(q => renderMenuItem(`/app/${x.rota}/${q.rota}`, q))}
          </MenuTree>
        );
      }
      return renderMenuItem(`/app/${x.rota}`, x);
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza os componentes de rotas com base nos menus
   * que estão presentes na aplicação. Os menus filtrados pelo input de 
   * pesquisa ainda tem suas rotas renderizadas.
   */
  renderRoutes() {
    const conteudos = this.getConteudo();
    const routes = [];
    for (const conteudo of conteudos) {
      if (conteudo.children.length > 0) {
        // Cairá aqui se a rota possui filhos dentro dela, e nesse
        // caso iremos renderizar apenas os filhos.
        for (const child of conteudo.children) {
          // Percorremos todos os filhos
          // Temos que fazer essa validação é para que seja possível colocar
          // modals diretamente nos menus do index:
          routes.push(
            <Route exact
              key={child.rota}
              path={`/app/${conteudo.rota}/${child.rota}`}
              component={(child.render || ContentWrapper)} />
          );
        }
      } else {
        routes.push(
          <Route exact
            key={conteudo.rota}
            path={'/app/' + conteudo.rota}
            component={conteudo.render || ContentWrapper} />
        );
      }
    }
    return routes;
  }

  // ------------------------------------------------------------------------------------

  render() {
    const usuario = this.props.usuario.apelido;
    return this.state.validado && (
      <UpValidadorSessao history={this.props.history}>
        <Socket />
        {dadosAplicacao.options.usarChatFuncionario && <Chat />}

        <Header tituloGrande={dadosAplicacao.nomeAplicacao}
          tituloPequeno={<i className={'fa ' + dadosAplicacao.icone} />}
          icon={dadosAplicacao.icone}
          rota='/app/dashboard'
          igrejaLogada={NomeIgrejaLogada}
          onLogout={this.realizarLogout.bind(this)} />

        <SideBar usuario={usuario}>
          {this.renderMenuItens()}
        </SideBar>

        <Switch>
          {this.renderRoutes()}
          <Route component={ContentWrapper} />
        </Switch>

        <Footer titulo="Upgrade Sistemas" link="https://www.sistemaupigreja.com" />
      </UpValidadorSessao>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
  filtroItens: state.filtroItens,
  socket: state.socket,
  config: state.config,
  redux: state,
}), (dispatch) => ({
  dispatch,
}))(App);

