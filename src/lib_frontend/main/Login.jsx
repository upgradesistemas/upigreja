import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import Input from '../componentes/Input.jsx';
import Button from '../componentes/Button.jsx';
import Api from './Api.jsx';
import Cookies from './Cookies.jsx';
import UpFadeIn from '../../lib_frontend/componentes/UpFadeIn.jsx';
import dadosAplicacao from './dadosAplicacao.jsx';

import '../style/login.css';

import logo from '../../imagens/Login/logo.png';
import ilustracao from '../../imagens/Login/ilustracao.png';

/**
 * Tela de login padrão utilizada nos sistemas, é possível passar uma cor
 * para ele de forma a customizar ela.
 */
class Login extends React.Component {

  constructor() {
    super();
    this.state = {
      codigoCliente: '',
      login: '',
      senha: '',
      senhaCriptografada: '',
    };
    document.body.className = 'hold-transition';
  }

  /**
   * @override
   */
  async componentDidMount() {
    // Foca o input no Código de Cliente:
    this.inputCodigoCliente.getWrappedInstance().focus();
    // Faz a tela aparecer lentamente:
    this.upFadeIn.fadeIn();

    const codigoCliente = localStorage.getItem('codigoCliente');
    const login = localStorage.getItem('login');
    const senhaCriptografada = localStorage.getItem('senhaCriptografada');

    await this.setState({
      codigoCliente,
      login,
      senhaCriptografada,
    });
    if (this.state.codigoCliente && this.state.login && this.state.senhaCriptografada) {
      this.realizarLogin();
    }
  }

  /**
   * Trata as validações antes de realizar o login.
   */
  realizarValidacoes() {
    if (!this.state.codigoCliente) {
      this.inputCodigoCliente.getWrappedInstance().focus();
      toastr.error('Atenção!', 'Informe o seu Código de Cliente!');
      return;
    }
    if (!this.state.login) {
      this.inputLogin.getWrappedInstance().focus();
      toastr.error('Atenção!', 'Informe o login!');
      return;
    }
    return true;
  }

  /**
   * Realiza o login no backend.
   */
  async realizarLogin() {
    if (!this.realizarValidacoes()) {
      return;
    }

    const { login, senha, codigoCliente, senhaCriptografada } = this.state;
    const ret = await Api('login', login, senha, codigoCliente, senhaCriptografada);
    if (ret.status) {
      // Sucesso no login!
      const dezAnos = new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 365 * 10));
      Cookies.set(dadosAplicacao.cookie, ret.dados.token, dezAnos);

      localStorage.setItem('codigoCliente', codigoCliente);
      localStorage.setItem('login', ret.dados.usuario.usuario);
      localStorage.setItem('senhaCriptografada', ret.dados.usuario.senha);

      this.props.history.push('/app/dashboard');
    } else {
      console.log(ret);
      // toastr.error('Atenção!', ret.erro, { attention: true });
      toastr.error('Atenção!', ret.erro);
    }
  }

  render() {
    return (
      <UpFadeIn ref={e => this.upFadeIn = e}>
        <div className="login-container">

          <div className="coluna-50-1">
            <img src={logo} alt="UpIgreja" />

            <div className="formulario">
              <Input placeholder="Código da Igreja"
                value={this.state.codigoCliente}
                uppercase
                ref={e => this.inputCodigoCliente = e}
                onKeyEnter={() => this.inputLogin.getWrappedInstance().focus()}
                onChange={(e) => this.setState({ codigoCliente: e })} />

              <Input placeholder="Seu Login"
                value={this.state.login}
                uppercase
                ref={e => this.inputLogin = e}
                onKeyEnter={() => this.inputSenha.getWrappedInstance().focus()}
                onChange={(e) => this.setState({ login: e })} />

              <Input placeholder="SUA SENHA"
                value={this.state.senha}
                ref={e => this.inputSenha = e}
                onChange={(e) => this.setState({ senha: e })}
                onKeyEnter={this.realizarLogin.bind(this)}
                uppercase={false}
                type="password" />

              <Button icon="fa-sign-in"
                label="Entrar"
                className="botao-login"
                onClick={this.realizarLogin.bind(this)} />
            </div>
          </div>

          <div className="coluna-50-2">
            <img className="ilustracao" src={ilustracao} alt="Ilustração" />
          </div>
        </div>
      </UpFadeIn>
    );
  }

}

export default connect(null, (dispatch) => ({
  dispatch,
}))(Login);
