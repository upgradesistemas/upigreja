import { removerAcentos } from '../../utils/funcoesFront';

/**
 * Classe que representa os dados iniciais
 * que são passados para o Upgrade.iniciarAplicacao na index do SRC
 */
class Dados {
  cookie = 'authUp'; // O Nome do cookie de autenticação usado
  urlApi = 'http://localhost'; // A Url completa de requisição do backend
  nomeAplicacao = 'Up ...'; // O Nome da aplicação
  icone = 'fa'; // Ícone font-awesome principal da aplicação 
  menus = () => ([]); // Os menus da sidebar
  reducers = {}; // Reducers específicos da aplicação
  store = null; // Store do redux
  options = { // Opções da aplicação
    usarChatFuncionario: false, // Se vai usar chat entre funcionários
    usarFiltro: false, // Indica que a aplicação deve usar um input para filtrar itens do menu
  };

  // Eventos específicos que a aplicação pode escutar,
  // Os eventos são reducers, ou seja, se um nome for igual a uma
  // action de um reducer, será disparado:
  eventos = {
  };

  /**
   * Atualiza os dados desse objeto.
   * @param {Object} dados Os novos dados.
   */
  set(dados) {
    for (const key in this) {
      if (dados[key]) {
        this[key] = dados[key];
      }
    }
    this.urlApi = 'http://' + window.location.hostname + ':8100';
  }

  /**
   * @param {Store} redux O state do redux
   * @param {Boolean} filtrarPermissaoTelas Se os menus devem ser filtrados por permissão de telas. 
   *  Isso é utilizado para retornar por exemplo todos os menus ou não, 
   * dependendo da onde quer utilizar.
   * @return {Menu[]} o array de menus específicado pela propriedade
   *  menus lá em cima. Como a propriedade "menus" é um método, esse get já executa
   *  o método passando a store e retorna os dados para quem pediu.
   */
  getMenus(redux, filtrarPermissaoTelas) {
    // Vamos passar para os menus essa função, que quando chamada
    // apenas retorna um objeto que vai formar cada nó do treeview.
    // Essa função também seta o pai de todas as rotas filhas
    // para tratar melhor depois
    const createItem = (rota, label, icone, render, children = []) => {
      const objeto = { rota, label, icone, render, children, invisivel: false };
      for (const child of children) {
        child.pai = objeto;
      }

      return objeto;
    };

    let menus = this.menus(redux, createItem);
    if (filtrarPermissaoTelas) {
      // Devemos filtrar os menus por permissão de telas
      const permissoes = redux.permissoesTelas;
      menus = menus.filter(x => {
        const permissaoDessaRota = permissoes.find(q => q.status_habilita_tela === 'S' &&
          q.rota_tela === x.rota);
        if (x.children.length > 0) {
          x.children = x.children.filter(p => {
            // Vamos filtrar os filhos
            return permissoes.find(q => q.status_habilita_tela === 'S' &&
              q.rota_tela === x.rota + '/' + p.rota);
          });
          if (x.children.length === 0) {
            // Se depois de filtrar os filhos não tem nenhum sobrando,
            // não vamos deixar o pai.
            return false;
          }
        }
        // Vamos deixar o pai se tem a permissão da rota ou tem filhos:
        return permissaoDessaRota || x.children.length > 0;
      });
    }

    // Ok, agora vamos percorrer a treeview retornada e validar se tem
    // algum menu que bate com o filtro da aplicação, se não bater,
    // o valor da propriedade "invisivel" ficará true. A Validação
    // de mostrar ou não é feito no App.jsx
    if (this.options.usarFiltro) {
      const filtroUpper = removerAcentos(redux.filtroItens).toUpperCase();
      for (const menu of menus) {
        let peloMenosUmFilho = false; // Filtro bateu com pelo menos um filho?
        for (const child of menu.children) {
          const labelFilho = removerAcentos(child.label).toUpperCase();
          if (labelFilho.indexOf(filtroUpper) < 0) {
            child.invisivel = true;
          } else {
            peloMenosUmFilho = true;
          }
        }
        if (!peloMenosUmFilho) {
          menu.invisivel = true;
        }

        const labelItem = removerAcentos(menu.label).toUpperCase();
        if (labelItem.indexOf(filtroUpper) >= 0) {
          menu.invisivel = false;
        }
      }
    }

    return menus;
  }
}

const instancia = new Dados();
export default instancia;
