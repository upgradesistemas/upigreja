import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { reducer as toastrReducer } from 'react-redux-toastr';
import usuarioReducer from '../reducers/usuarioReducer';
import filtroItensReducer from '../reducers/filtroItensReducer';

import Login from './Login.jsx';
import App from './App.jsx';
import UpToaster from '../../lib_frontend/componentes/UpToaster.jsx';
import permissoesTelas from '../reducers/telasReducer';
import configReducer from '../reducers/configReducer';
import socketReducer from '../reducers/socketReducer';
import tituloContentWrapperReducer from '../reducers/tituloContentWrapperReducer';
import dadosAplicacao from './dadosAplicacao';

import '../../lib_frontend/style/index.css';
import '../style/styles.jsx';

/**
 * Registra uma store para ouvir eventos do redux e dispara
 * qualquer evento recebido que tenha sido registrado na aplicação.
 * @param {Store} store A Store do redux
 * @param {Object} eventos Um objeto com os eventos do redux.
 */
function registrarEventos(store, eventos) {
  store.subscribe(async () => {
    const lastAction = store.getState().lastAction;
    for (const key in eventos) {
      if (key === lastAction.type) {
        await eventos[key](lastAction);
      }
    }
  });
}

/**
 * Inicia a aplicação principal do sistema.
 * Inicia a aplicação com os dados especificados.
 * @param {dadosAplicacao} dados Os dados da aplicação.
 */
export function iniciarAplicacao(dados) {
  /**
   * Uma informação importante (e plausivel): Existe uma baleia solitária que emite
   * sons em uma frequencia que nenhuma outra baleia consegue ouvir, ela nunca mais
   * vai achar um parceiro. :'(   
           .-'                   \
      '--./ /     _.---.      \   |
      '-,  (__..-`       \  \  |   |
        \          x     |   |  |   | BUUUUuuuu
          `,.__.   ,__.--/  /  |   |
            '._/_.'___.-`     /   |
                                 /
   * Essa aplicação é em homenagem a baleia solitária.
   */

  /**
   * Cria a store do redux e combina todos os reducers
   * passado por parâmetro mais os reducers padrões.
   */
  const store = createStore(combineReducers({
    ...dados.reducers,
    usuario: usuarioReducer,
    toastr: toastrReducer,
    tituloContentWrapper: tituloContentWrapperReducer,
    filtroItens: filtroItensReducer,
    config: configReducer,
    socket: socketReducer,
    permissoesTelas, 
    lastAction: (state = null, action) => action,
  }));
  dados.store = store;
  dadosAplicacao.set(dados);

  // Registra os eventos:
  registrarEventos(store, dados.eventos);

  // Renderiza a aplicação:
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <div>
          <Route component={UpToaster} />
          <Switch>            
            <Route exact path="/" component={Login} />
            <Route path="/app" component={App} />
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>,
    document.getElementById('app')
  );
}
