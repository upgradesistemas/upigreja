export default (state = {}, action) => {
  if (action.type === 'setUsuario') {
    return {
      ...action.usuario,
    };
  }
  return state;
};
