export default (state = '', action) => {
  if (action.type === 'setFiltroItens') {
    return action.valor;
  }
  return state;
};
