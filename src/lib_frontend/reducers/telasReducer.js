export default (state = [], action) => {
  if (action.type === 'setPermissaoTelas') {
    return [
      ...action.permissoesTelas,
    ];
  }
  return state;
};
