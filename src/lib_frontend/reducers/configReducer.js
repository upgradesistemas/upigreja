export default (state = {}, action) => {
  if (action.type === 'setConfig') {
    return {
      ...action.config.gerais,
      ...action.config.usuario,
    };
  }
  return state;
};
