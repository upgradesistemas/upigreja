export default (state = {}, action) => {
  if (action.type === 'setSocket') {
    return {
      ...action,
    };
  }
  return state;
};
