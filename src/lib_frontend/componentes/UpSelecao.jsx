// import ReactDOM from 'react-dom';
import React from 'react';
import { toastr } from 'react-redux-toastr';
import Button from './Button.jsx';
import Api from '../main/Api.jsx';

let codigoCliente;

export default class UpSelecao extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  state = {
    // ladoEsq: ['João Gabriel Sales do Nascimento', 'Jonathas', 'Pedro'],
    // ladoDir: ['Matheus', 'Lopes', 'Rafael', 'Marcos', 'Antônio', 'Nicolas'],
    // ladoEsqSel: 'João',
    // ladoDirSel: 'Matheus',
    ladoEsq: [],
    ladoDir: [],
    ladoEsqSel: '',
    ladoDirSel: '',
  }

  // ------------------------------------------------------------------------------------

  componentWillReceiveProps() {
    if (Number(this.props.cod_celula_multiplicar) > 0) {
      this.buscarMembrosCelula(this.props.cod_celula_multiplicar);
    }
  }

  // ------------------------------------------------------------------------------------

  async buscarMembrosCelula(codCelula) {
    const membros = await Api('buscarMembrosCelula', codCelula, codigoCliente);
    if (membros.status) {
      await this.setState({ ladoEsq: membros.dados });
    } else {
      toastr.error('Erro!', `Erro ao buscar os membros da célula. ${membros.erro}`);
    }
    // this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  // manda o primeiro registro da esq. pra direita
  mandarPraDireita() {
    if (this.state.ladoEsq.findIndex((x) => x === this.state.ladoEsqSel) < 0) {
      return; // não tem nada
    }
    this.state.ladoDir.push(this.state.ladoEsqSel); // adiciona ao lado direito
    this.state.ladoEsq.splice(this.state.ladoEsq
      .findIndex((x) => x === this.state.ladoEsqSel), 1); // remove do lado esquerdo
    this.setState({ ladoDir: this.state.ladoDir, ladoEsq: this.state.ladoEsq });
  }

  // ------------------------------------------------------------------------------------

  // manda o primeiro registro da dir. pra esq.
  mandarPraEsquerda() {
    if (this.state.ladoDir.findIndex((x) => x === this.state.ladoDirSel) < 0) {
      return; // não tem nada
    }
    this.state.ladoEsq.push(this.state.ladoDirSel); // adiciona ao lado esquerdo
    this.state.ladoDir.splice(this.state.ladoDir
      .findIndex((x) => x === this.state.ladoDirSel), 1); // remove do lado esquerdo
    this.setState({ ladoDir: this.state.ladoDir, ladoEsq: this.state.ladoEsq });
  }

  // ------------------------------------------------------------------------------------

  rendersetas() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Button className='btn-primary mr-5 botaoSetas'
          icon='fa-hand-o-right'
          onClick={() => this.mandarPraDireita()} />
        <Button className='btn-primary mr-5 botaoSetas'
          icon='fa-hand-o-left'
          onClick={() => this.mandarPraEsquerda()} />
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <div className="col-sm-4">
          <div className="tabelaMultiplicacao">
            <div style={{ background: '', flex: 1 }}>
              {this.state.ladoEsq.map(x => (
                <div className="registroEsquerdo" onClick={() => this.setState({ ladoEsqSel: x })}
                  style={{
                    color: this.state.ladoEsqSel === x ? 'white' : 'black',
                    background: this.state.ladoEsqSel === x ? 'rgb(99, 145, 219)' : null,
                  }}>{x.nome_membro}
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="col-sm-4">
          {this.rendersetas()}
        </div>

        <div className="col-sm-4">
          <div className="tabelaMultiplicacao">
            <div style={{ background: '', flex: 1 }}>
              {this.state.ladoDir.map(x => (
                <div className="registroDireito" onClick={() => this.setState({ ladoDirSel: x })}
                  style={{
                    color: this.state.ladoDirSel === x ? 'white' : 'black',
                    background: this.state.ladoDirSel === x ? 'rgb(99, 145, 219)' : null,
                  }}>{x.nome_membro}</div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }

}
