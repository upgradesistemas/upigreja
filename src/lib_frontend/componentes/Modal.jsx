import React from 'react';
import Button from './Button.jsx';

/**
 * Classe de modal do sistema, todas as dialogs DEVEM utilizar esse
 * componente para gerenciar a janela.
 */
export default class Modal extends React.Component {

  // Id sequencial para não repetir o ID no dom node:
  static seqId = 0;

  constructor() {
    super();
    this.uniqueId = 'modal-' + Modal.seqId++;
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (this.props.mostrarAutomaticamente) {
      // Devemos mostrar o modal assim que tivermos uma referência a ele (no caso agora)
      this.show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Ativa o evento de onClose caso haja um.
   */
  ativarOnClose() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Mostra esse dialog na tela
   */
  show() {
    window.$('#' + this.uniqueId).modal('show');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Esconde esse dialog da tela
   */
  hide() {
    !this.props.naoFechar && window.$('#' + this.uniqueId).modal('hide');
    this.ativarOnClose();
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="modal fade"
        id={this.uniqueId}
        tabIndex="-1"
        role="dialog"
        data-backdrop="static"
        data-keyboard="false"
        onClick={this.hide.bind(this)}
        onKeyDown={e => e.keyCode === 27 && this.hide()}>

        <div className="modal-dialog" role="document">
          <div className="modal-content" onClick={e => e.stopPropagation()}>

            {/* Cabeçalho */}
            <div className="modal-header">
              {/* <Button className="close"
                      data-dismiss="modal"
                      icon='fa-times'
                      onClick={this.ativarOnClose.bind(this)} /> */}
              <h4 className="modal-title" id="myModalLabel">
                {this.props.title || this.props.titulo || 'Sem título'}
              </h4>
            </div>

            {/* Corpo: */}
            <div className="modal-body">
              {this.props.children}
            </div>

            {/* Rodapé: */}
            <div className="modal-footer">
              {this.props.buttons.map(x => (
                <Button className={x.className}
                        key={x.label}
                        icon={x.icon}
                        disabled={x.disabled}
                        onClick={() => {
                          if (x.onClick) x.onClick();
                          if (x.dismiss) this.hide();
                        }}
                        label={x.label} />
              ))}
            </div>

          </div>
        </div>

      </div>
    );
  }

}
