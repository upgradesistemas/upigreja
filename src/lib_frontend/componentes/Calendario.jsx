import React from 'react';
import $ from 'jquery';
import 'fullcalendar';
import 'fullcalendar/dist/fullcalendar.css';

let id = 0;

export default class Calendario extends React.Component {

  constructor() {
    super();
    this.id = `calendar${++id}`;
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResizeBinded);
    $(`#${this.id}`).fullCalendar({
      dayClick: (event) => { // Quando clica em um dia
        this.dispararEvento('onClickDia', event.format('YYYY-MM-DD HH:mm:ss'));
      },
      eventClick: (event) => { // Quando clica em um evento
        this.dispararEvento('onClickEvento', event);
      },
      eventDrop: (event, delta, revertFunc) => { // Quando muda um evento de dia
        this.dispararEvento('onMudarDataEvento', event, revertFunc);
      },
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay',
      },
      buttonText: {
        today: 'Hoje',
        month: 'Mês',
        week: 'Semana',
        day: 'Dia',
      },
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 
        'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 
        'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
      events: [],
      height: 'parent',
      editable: true,
      droppable: false, // this allows things to be dropped onto the calendar !!!
    });
  }

  setEventos(eventos) {
    $(`#${this.id}`).fullCalendar('removeEvents');
    $(`#${this.id}`).fullCalendar('addEventSource', eventos);
    $(`#${this.id}`).fullCalendar('refetchEvents');
  }

  dispararEvento(nome, ...parametros) {
    $(`#${this.id}`).fullCalendar('option', 'events', []);
    if (this.props[nome]) {
      this.props[nome](...parametros);
    }
  }

  render() {
    return (
      <div id={this.id} />
    );
  }

}
