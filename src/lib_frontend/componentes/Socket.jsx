import React from 'react';
import { connect } from 'react-redux';
import io from 'socket.io-client';
import dadosAplicacao from '../main/dadosAplicacao.jsx';

let Pessoa;
let codigoCliente;


class Socket extends React.Component {

  constructor(props) {
    super(props);
    this.funcionariosChat = [];
    this.dispatch(props);
    Pessoa = props.usuario.cod_pessoa;  
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    this.conectar();
    this.dispatch();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Encontra uma pessoa dentre os funcionários do chat.
   * @param {String|Number} codPessoa O Código da pessoa. 
   */
  getFuncionario(codPessoa) {
    return this.funcionariosChat.find(x => x.cod_pessoa === codPessoa);
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Object} Um objeto de mensagens não lidas
   */
  getMensagensNaoLidas() {
    const msgs = [];
    for (const a of this.funcionariosChat) {
      for (const cv of a.conversa) {
        if (cv.cod_pessoa_destino === Pessoa &&
          cv.status !== 'lido') {
          msgs.push(cv);
        }
      }
    }
    return msgs;
  }

  // ------------------------------------------------------------------------------------

  conectar() {
    this.socket = io(dadosAplicacao.urlApi);
    this.socket.on('connect', () => {
      this.socket.emit('registrar', { codigoCliente, Pessoa });
      this.dispatch();
    });

    /**
     * Esse evento é disaparado quando recebemos uma nova mensagem
     * de uma certa pessoa. Isso irá apenas adicionar no array de conversas
     * da pessoa e dar um dispatch para atualizar a tela.
     */
    this.socket.on('mensagemChat', (dados) => {
      // Func será quem mandou a mensagem: 
      this.funcionariosChat = [...this.funcionariosChat];
      const funcionario = this.getFuncionario(dados.cod_pessoa_origem);
      funcionario.conversa.push(dados);
      // this.marcarMensagensComoLida(dados.cod_pessoa_origem);
      this.dispatch();
    });

    /**
     * Quando recebemos esse evento precisamos deixar todas as mensagens
     * da pessoa do parâmetro como "lidas".
     */
    this.socket.on('marcarMensagensComoLida', (codPessoa) => {
      const funcionario = this.getFuncionario(codPessoa);
      for (const conversa of funcionario.conversa) {
        if (conversa.cod_pessoa_origem === Pessoa) {
          conversa.status = 'lido';
        }
      }
      this.dispatch();
    });

    this.socket.on('funcionariosChat', (funcionariosChat) => {
      this.funcionariosChat = funcionariosChat.filter(x => (
        // Temos que filtrar nós mesmos da lista de contatos:
        x.cod_pessoa !== Pessoa
      ));
      this.dispatch();
    });
    this.socket.connect();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Marca todas as mensagens da pessoa do parâmetro como "lidas".
   * @param {String|Number} codPessoa O Código da pessoa da conversa. 
   */
  marcarMensagensComoLida(codPessoa) {
    const funcionario = this.getFuncionario(codPessoa);
    for (const conversa of funcionario.conversa) {
      if (conversa.cod_pessoa_destino === Pessoa) {
        if (!conversa.dt_hr_lido) {
          conversa.dt_hr_lido = new Date();
        }
        conversa.status = 'lido';
      }
    }
    this.socket.emit('marcarMensagensComoLida', codPessoa);
    this.dispatch();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Desconecta esse socket.
   */
  desconectar() {
    this.socket.off();
    this.socket.io.off();
    this.socket.disconnect();
  }

  // ------------------------------------------------------------------------------------

  dispatch(props = this.props) {
    props.dispatch({
      type: 'setSocket',
      mensagensNaoLidas: this.getMensagensNaoLidas(),
      marcarMensagensComoLida: this.marcarMensagensComoLida.bind(this),
      getFuncionario: this.getFuncionario.bind(this),
      desconectar: this.desconectar.bind(this),
      ...this,
    });
  }

  // ------------------------------------------------------------------------------------

  render() {
    return null;
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}))(Socket);
