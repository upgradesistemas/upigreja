import React from 'react';
import Input from './Input.jsx';

export default props => (
  <div className="labeled-input-container">
    <label htmlFor={props.name}>{props.label}</label>
    <Input value={props.value}
           onChange={props.onChange}
           onChange2={props.onChange2}
           onBlur={props.onBlur}
           placeholder={props.placeholder}
           readOnly={props.readOnly}
           disabled={props.disabled}
           type={props.type}
           min={props.min}
           ref={props.inputRef}
           max={props.max}
           onKeyEnter={props.onKeyEnter}
           className={props.className}
           uppercase={props.uppercase} />
  </div>
);
