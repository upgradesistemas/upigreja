import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import ContentWrapper from '../ContentWrapper.jsx';
import Button from '../Button.jsx';
import Checkbox from '../Checkbox.jsx';
import LabeledInput from '../LabeledInput.jsx';
import GenericUtils from '../../main/GenericUtils.jsx';
import InputDate from '../InputDate.jsx';
import InputTime from '../InputTime.jsx';
import InputCalc from '../InputCalc.jsx';
import InputMask from '../InputMask.jsx';
import UpPanel from '../UpPanel.jsx';
import { RadioButton } from '../RadioGroup.jsx';
import Api from '../../main/Api.jsx';

let Pessoa;
let codigoCliente;

/**
 * Componente de configurações da lib, esse componente é generico 
 * e pode ser em usado em qualquer sistema, a configuração do sistema
 * específico geralmente se chama ConfiguracaoApp. Essa classe apenas
 * renderiza ae trata as configs passadas pela ConfiguracaoApp e algumas
 * configurações gerais de todas as aplicações.
 */
class ConfiguracaoLib extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    configs: {
      // As configurações gerais para todos os usuários ficam aqui
    },
    usuario: {
      // Configurações específicas para esse usuário ficam aqui
    },
  };

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    // Vamos requisitar essas tabelas no back:
    const ret = await Api('getConfigs', [Pessoa, '', codigoCliente]);
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
    }
    this.setState({
      configs: ret.dados.gerais,
      usuario: ret.dados.usuario,
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Handler de um change de input
   */
  onInputChange(nome, valor, tipo = 'configs') {
    this.setState({
      [tipo]: {
        ...this.state[tipo],
        [nome]: valor,
      },
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Handler de um change de checkbox.
   */
  onCheckboxChange(nome, valor, tipo = 'configs') {
    this.setState({
      [tipo]: {
        ...this.state[tipo],
        [nome]: valor ? 'S' : 'N',
      },
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Salva as configurações no banco de dados.
   */
  async salvar() {
    GenericUtils.setElementoCarregando(this.divConfigs, true);
    await Api('setConfigs', this.state, Pessoa, codigoCliente);
    GenericUtils.setElementoCarregando(this.divConfigs, false);

    // Recarrega as configs do sistema:
    const ret = await Api('getConfigs', [Pessoa, '', codigoCliente]);
    this.props.dispatch({
      type: 'setConfig',
      config: ret.dados,
    });
    window.location.reload();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um checkbox.
   */
  renderCheckbox(tipo = 'configs', text, campo, className) {
    return (
      <Checkbox text={text}
        className={className}
        checked={this.state[tipo][campo] === 'S'}
        onChange={e => this.onCheckboxChange(campo, e, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label porém que só pode numeros.
   */
  renderInputNumber(tipo = 'configs', label, campo, min, max) {
    return (
      <LabeledInput label={label}
        value={this.state[tipo][campo]}
        min={min}
        max={max}
        type="number"
        onChange={e => this.onInputChange(campo, e, tipo)}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label
   */
  renderLabeledEdit(tipo = 'configs', label, campo) {
    return (
      <LabeledInput label={label}
        value={this.state[tipo][campo]}
        onChange={e => this.onInputChange(campo, e, tipo)}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza uma radioButton.
   */
  renderRadioButton(tipo = 'configs', campo, label, valor) {
    return (
      <RadioButton name={campo}
        text={label}
        checked={this.state[tipo][campo] === valor}
        onChange={() => this.onInputChange(campo, valor, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputDate(tipo = 'configs', label, campo) {
    return (
      <InputDate value={this.state[tipo][campo]}
        label={label}
        onChange={e => this.onInputChange(campo, e.target.value, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita pontos flutuantes.
   */
  renderInputCalc(tipo = 'configs', label, campo) {
    return (
      <InputCalc value={this.state[tipo][campo]}
        label={label}
        onChange={e => this.onInputChange(campo, e, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita horários.
   */
  renderInputTime(tipo = 'configs', label, campo) {
    return (
      <InputTime value={this.state[tipo][campo]}
        label={label}
        onChange={e => this.onInputChange(campo, e.target.value, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um Up Panel com autoCoplete
   */
  renderUpPanel(tipo = 'configs', label, campo, tabela, chave, desc, where) {
    return (      
      <UpPanel value={this.state[tipo][campo]}
        label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        onChange={e => this.onInputChange(campo, e, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputMask(tipo = 'configs', label, campo, props) {
    return (
      <InputMask value={this.state[tipo][campo]}
        label={label}
        {...props}
        onChange={e => this.onInputChange(campo, e, tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza uma configuração do sistema. 
   * Esse método renderiza a configuração passando todos os componentes que podem
   * ser utilizados por ela, os componentes são acessados do outro lado através
   * dos props. 
   * - Ex: this.props.input(), this.props.radioButton(), etc..
   * @param {Object} dados.componente Aqui o parâmetro seria
   *  um componente de config, por exemplo ConfigGeral ou ConfigPreferencias.
   * @param {String} dados.tipo Aonde a configuração vai ser salva 
   *  (tabela de configs, de configs de usuário, etc ...)
   */
  renderConfig(dados, i) {
    return (
      <dados.componente key={i}
        checkbox={this.renderCheckbox.bind(this, dados.tipo)}
        input={this.renderLabeledEdit.bind(this, dados.tipo)}
        inputCalc={this.renderInputCalc.bind(this, dados.tipo)}
        inputDate={this.renderInputDate.bind(this, dados.tipo)}
        inputMask={this.renderInputMask.bind(this, dados.tipo)}
        inputNumber={this.renderInputNumber.bind(this, dados.tipo)}
        inputTime={this.renderInputTime.bind(this, dados.tipo)}
        radioButton={this.renderRadioButton.bind(this, dados.tipo)}
        upPanel={this.renderUpPanel.bind(this, dados.tipo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <ContentWrapper title="Configurações do sistema" small="Configure seu sistema!">
        <div className="relative mb-10" ref={e => this.divConfigs = e}>
          {/* Funciona assim: Essa é a configuração da lib, ela recebe todas
            as configurações da "ConfiguracaoApp", passada pelas childs.
            Então esse componente só pega as configurações passadas e renderiza
            elas na tela, passando todos os componentes que podem ser utilizados
            no método "this.renderConfig()". Se você olhar no renderConfig vai
            ver que tem vários métodos render, que são todos os componentes utilizados
            nas configurações (ex ConfigGeral, ConfigPreferencias). */}
          {this.props.configs.map(this.renderConfig.bind(this))}
        </div>
        <Button icon="fa-check"
          label="Gravar alterações"
          className="btn-success"
          onClick={this.salvar.bind(this)} />
      </ContentWrapper>
    );
  }
}
export default connect((state) => ({
  usuario: state.usuario,
}))(ConfiguracaoLib);
