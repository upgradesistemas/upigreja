import React from 'react';
import { connect } from 'react-redux';
import ChatBox from './ChatBox.jsx';
import './chat.css';

/**
 */
class Chat extends React.Component {

  render() {
    return (
      <div>
        <div className="chat-boxes">
          <ChatBox tipo="funcionario" />
          {/* <ChatBox tipo="suporte" /> */}
        </div>
      </div>
    );
  }

}

export default connect((state) => ({
  socket: state.socket,
}), (dispatch) => ({ 
  dispatch,
}))(Chat);
