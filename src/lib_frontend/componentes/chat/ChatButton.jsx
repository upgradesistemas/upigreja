import React from 'react';
import { connect } from 'react-redux';

/**
 * Representa uma das bolinhas de chat da aplicação, essa
 * bolinha automaticamente se ajusta conforme o layout.
 * O props.tipo define o className que a bolinha do chat irá receber.
 */
class ChatButton extends React.Component {

  render() {
    return (
      <div>
        <div className={'chat-ball chat-' + this.props.tipo} />
      </div>
    );
  }

}

export default connect((state) => ({
  socket: state.socket,
}), (dispatch) => ({ 
  dispatch,
}))(ChatButton);
