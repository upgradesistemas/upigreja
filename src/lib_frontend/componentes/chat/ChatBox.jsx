import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { connect } from 'react-redux';
import Input from '../Input';
import Cookie from '../../main/Cookies.jsx';

let Pessoa;

/**
 * Caixinha de chat para conversar com outro contato.
 * A Classe principal é o "tipo" passado pelos props.
 */
class ChatBox extends React.Component {

  constructor(props) {
    super(props);
    this.qtdeMsgs = 0;
    Pessoa = props.usuario.cod_pessoa;
  }

  // ------------------------------------------------------------------------------------

  state = {
    open: Cookie.get('upChat') === true,
    titulo: 'Chat',
    icone: 'fa-bank',
    valor: '',
    codPessoa: null, // Se não for  nulo, é o cod_pessoa da conversa atual
  }

  // ------------------------------------------------------------------------------------

  /**
   * Usado para re-renderizar as conversas quando uma nova mensagem for incluida.
   */
  componentWillReceiveProps(nextProps) {
    // Pega o funcionário que estamos conversando atualmente
    const funcionarioNovo = nextProps.socket.getFuncionario(this.state.codPessoa);
    // Verifica se a quantidade de mensagens mudou:
    if (funcionarioNovo && funcionarioNovo.conversa.length !== this.qtdeMsgs) {
      // Qtde de mensagens dele mudou, vamos marcar todas as mensagens dele como lida:
      nextProps.socket.marcarMensagensComoLida(this.state.codPessoa);

      // eslint-disable-next-line
      const node = ReactDOM.findDOMNode(this.conteudo);
      const scrollNoFim =
        (node.scrollHeight - node.scrollTop >= (node.clientHeight - 2)) &&
        (node.scrollHeight - node.scrollTop <= (node.clientHeight + 2));
      if (scrollNoFim) {
        setTimeout(() => node.scrollTop = node.scrollHeight, 15);
      }
    }
  }

  // ------------------------------------------------------------------------------------

  toggleChat() {
    const dezAnos = new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 365 * 10));
    Cookie.set('upChat', !this.state.open, dezAnos);
    this.setState({ open: !this.state.open });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre a conversa de um funcionário.
   * @param {Pessoa} funcionario O Funcionario da conversa. 
   */
  abrirConversa(funcionario) {
    // Marca as mensagens enviadas que ainda não foram lidas
    // como lidas, logo ao entrar na conversa:
    this.props.socket.marcarMensagensComoLida(funcionario.cod_pessoa);

    this.setState({
      titulo: funcionario.apelido || funcionario.nome_pessoa,
      codPessoa: funcionario.cod_pessoa,
    }, () => {
      this.input.getWrappedInstance().focus();
      // eslint-disable-next-line
      const node = ReactDOM.findDOMNode(this.conteudo);
      // Joga o scroll para o fim:
      node.scrollTop = node.scrollHeight;
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Envia uma mensagem para a conversa aberta.
   */
  enviarMensagem() {
    // Criamos o objeto de mensagem:
    const msg = {
      cod_pessoa_destino: this.state.codPessoa,
      cod_pessoa_origem: Pessoa,
      mensagem: this.state.valor,
      dt_hr_envio: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      tipo: 'funcionario',
    };

    // Acha o funcionário correspondente:
    const func = this.props.socket.funcionariosChat.find(
      x => x.cod_pessoa === this.state.codPessoa
    );

    // Emite para o backend:
    this.props.socket.socket.emit('mensagemChat', msg);
    func.conversa.push(msg);

    // eslint-disable-next-line
    const node = ReactDOM.findDOMNode(this.conteudo);
    const scrollNoFim =
      (node.scrollHeight - node.scrollTop >= (node.clientHeight - 2)) &&
      (node.scrollHeight - node.scrollTop <= (node.clientHeight + 2));
    this.setState({ valor: '', codPessoa: func.cod_pessoa }, () => {
      if (scrollNoFim) {
        // Se o scroll estava no fim, então quando adicionamos uma mensagem
        // devemos manter ele no fim, caso contrário vamos manter onde ele estava.
        node.scrollTop = node.scrollHeight;
      }
    });
  }

  // ------------------------------------------------------------------------------------

  somarMensagensNaoLidas(codPessoa) {
    const { mensagensNaoLidas } = this.props.socket;
    let soma = 0;
    for (const a of (mensagensNaoLidas || [])) {
      if (codPessoa) {
        soma += (a.cod_pessoa_origem === codPessoa ? 1 : 0);
      } else {
        soma++;
      }
    }
    return soma;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o layout da conversa por fora, ou seja, apenas um dos
   * itens da lista de conversas. É mostrado se o usuário está online ou não.
   * @param {Pessoa} funcionario O Funcionario da conversa. 
   */
  renderConversaFora(funcionario) {
    const ultimaMsg = funcionario.conversa[funcionario.conversa.length - 1];
    const soma = this.somarMensagensNaoLidas(funcionario.cod_pessoa);
    return (
      <div className="conversa" onClick={this.abrirConversa.bind(this, funcionario)}>
        <div className={'status ' + (funcionario.online ? ' online ' : '')} />
        <div className="conversa-fora-content">

          <div className="titulo-cv">
            <h1>{funcionario.apelido || funcionario.nome_pessoa}</h1>
            {soma > 0 && <span className="qtde-conversas">{soma}</span>}
          </div>

          <div className="ultima-msg">
            {ultimaMsg && ultimaMsg.cod_pessoa_origem === Pessoa &&
              <i className={'fa fa-check ultima-msg-status ' +
                (ultimaMsg.status === 'lido' ? ' lido ' : '')} />}
            {ultimaMsg ? ultimaMsg.mensagem : ''}
          </div>
        </div>

        <div className="ultima-hora">
          {ultimaMsg ? moment(ultimaMsg.dt_hr_envio, 'DD/MM/YYYY HH:mm:ss').format('HH:mm') : ''}
        </div>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderBalao(dados) {
    // Fui eu quem enviei?
    const meu = dados.cod_pessoa_origem === Pessoa;
    const lido = dados.status === 'lido';
    const dt = moment(dados.dt_hr_envio, 'DD/MM/YYYY HH:mm:ss').format('HH:mm');
    return (
      <div className={'balao-container ' +
        (meu ? ' meu ' : ' ') +
        (lido ? ' lido ' : ' ')}>
        <div className="balao">
          {dados.mensagem}
          <span>{dt}</span>
          {meu && <i className="fa fa-check" />}
        </div>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderConversaDentro() {
    const func = this.props.socket.getFuncionario(this.state.codPessoa);
    this.qtdeMsgs = func.conversa.length;
    return (
      <div className="conversa-dentro">
        <div className="conteudo" ref={e => this.conteudo = e}>
          {func.conversa.map(this.renderBalao.bind(this))}
        </div>
        <div className="input-container">
          <Input placeholder="Digite aqui sua mensagem..."
            uppercase={false}
            value={this.state.valor}
            onChange={e => this.setState({ valor: e })}
            ref={e => this.input = e}
            onKeyEnter={this.enviarMensagem.bind(this)} />
        </div>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const titulo = this.state.titulo;
    return (
      <div className={'chat-box ' + this.props.tipo + ' ' +
        (this.state.open ? ' open ' : '') +
        (this.state.codPessoa ? ' dentro ' : '')}>

        <div className="titulo" onClick={this.toggleChat.bind(this)} >
          {this.state.codPessoa && (
            <i className="fa fa-arrow-left"
              onClick={(e) => {
                e.stopPropagation();
                this.setState({ codPessoa: null, titulo: 'Chat' });
              }} />
          )}

          <div className="titulo-text-container">
            <h1>{titulo}</h1>

            {!this.state.codPessoa && this.props.socket.mensagensNaoLidas.length > 0 && (
              <span className="qtde-conversas">{this.somarMensagensNaoLidas()}</span>
            )}

          </div>

          <i className="fa fa-comments-o" />
        </div>

        <div className="chat-content">
          {this.state.codPessoa && this.renderConversaDentro()}
          {!this.state.codPessoa &&
            this.props.socket.funcionariosChat.map(this.renderConversaFora.bind(this))}
        </div>
      </div>
    );
  }

}

export default connect((state) => ({
  socket: state.socket,
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}))(ChatBox);
