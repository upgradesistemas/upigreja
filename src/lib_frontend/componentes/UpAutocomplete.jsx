import React from 'react';
import ReactDOM from 'react-dom';
import { toastr } from 'react-redux-toastr';
import Input from './Input';
import GenericUtils from '../main/GenericUtils';
import Api from '../main/Api.jsx';

let id = 0;

/**
 */
export default class UpAutocomplete extends React.Component {

  constructor(props) {
    super(props);
    this.id = `upautocomplete${++id}`;
    this.state = {
      aberto: false,
      registros: [],
      registro: {}, // A sugestão selecionada
      sugestoes: [],
      registroSetinha: null,
    };
  }

  onChangeDesc(e) {
    if (this.timeoutPesquisa) {
      clearTimeout(this.timeoutPesquisa);
    }

    // Seleciona nulo para zerar tudo, pois já que digitamos temos que escolher novamente
    // o registro:
    if (this.props.value !== e) {
      this.selecionarRegistro(null);
    }

    this.props.onChange && this.props.onChange(e);
    this.setState({ aberto: true }, () => {      
      GenericUtils.setElementoCarregando(this.painelResultados, true);
      this.timeoutPesquisa = setTimeout(() => {
        this.pesquisarRegistros();
      }, 500);
    });
  }

  /**
   * Chamado quando o input perde o foco.
   */
  onBlur() {
    const { onChange, permitirVazio } = this.props;
    onChange && !this.state.registro && !permitirVazio && onChange('');
    this.setState({ aberto: false });
  }

  /**
   * Chamado quando o input de texto é focado.
   */
  onFocus() {    
    if (this.props.value.length >= 0) {
      this.setState({ aberto: true }, () => {
        this.input.getWrappedInstance().focus(); // Seleciona todo o texto do input
        this.pesquisarRegistros();
      });
    }
  }

  getRegistroSetinha() {
    return this.state.registroSetinha;
  }

  /**
   * Realiza a busca dos registros com base no like do "state.desc".
   * Os registros são colocados no state.registros
   */
  async pesquisarRegistros() {
    try {
      const ret = await Api('buscarUpPanel', 
        this.props.tabela, 
        this.props.campo, 
        this.props.value,
        this.props.where,
        this.props.codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      this.setState({ registros: ret.dados });
    } finally {
      GenericUtils.setElementoCarregando(this.painelResultados, false);
    }
  }

  /**
   * Seleciona um registro, setando os valores do registro no campo
   * de código e de descrição. Caso o registro seja nulo, os campos
   * são apagados.
   * @param {Object} registro O Registro selecionado. 
   */
  selecionarRegistro(registro) {
    const texto = registro ? registro[this.props.campo] : '';
    this.props.onChange && this.props.onChange(texto);
    this.setState({ registro, aberto: false }, () => {
      this.props.onSelecionarRegistro && this.props.onSelecionarRegistro(registro);
    });
  }

  tratarSetinha(direcao) {
    let index = this.state.registros.findIndex(x => x === this.state.registroSetinha);
    let registroSetinha = null;
    if (direcao === 'baixo') {
      registroSetinha = index >= this.state.registros.length - 1 ? 
        this.state.registros[0] : this.state.registros[++index];
    } else {
      registroSetinha = index <= 0 ? 
        this.state.registros[this.state.registros.length - 1] : 
        this.state.registros[--index];
    }
    this.setState({ registroSetinha }, () => {
      const element = document.getElementById(`${this.props.id}${index}`);
      if (element) {
        const offset = element.offsetTop;
        ReactDOM.findDOMNode(this.painelResultados).parentElement.scrollTop = offset;
      }
    });
  }

  /**
   * Foca o input de texto.
   */
  focus() {
    this.input && this.input.getWrappedInstance().focus();
  }

  /**
   * Renderiza um registro da lista de registros do painel
   * de resultados. 
   */
  renderRegistro(registro, index) {
    const bate = this.state.registroSetinha === registro;
    return (
      <div className={`up-panel-registro ${bate ? 'active' : ''}`}
        onMouseDown={() => this.selecionarRegistro(registro)}
        id={`${this.props.id}${index}`}>
        {registro[this.props.campo]}
      </div>
    );
  }

  /**
   * Renderiza a mensagem sem registros no painel de buscar.
   */
  renderMensagemSemRegistros() {
    return (
      <div className="up-panel-sem-registros">
        <h1>Nenhum registro encontrado!</h1>
        <span>Nada foi encontrado.</span>
      </div>
    );
  }

  render() {
    const { registros } = this.state; 
    return (
      <div className={this.props.className}>
        {this.props.label && <label>{this.props.label}</label>}
        <div className="autocomplete-container">

          <Input value={this.props.value}
            onChange={this.onChangeDesc.bind(this)}
            onBlur={this.onBlur.bind(this)}
            onFocus={this.onFocus.bind(this)}
            placeholder={this.props.placeholder}
            disabled={this.props.disabled}
            ref={e => this.input = e}
            className="up-panel-desc" />

          {this.state.aberto && <div className="autocomplete-resultados">
            <div className="up-panel-results-inner" ref={e => this.painelResultados = e}>
              {registros.length === 0 && this.renderMensagemSemRegistros()}
              {registros.length > 0 && registros.map(this.renderRegistro.bind(this))}
            </div>
          </div>}

        </div>
      </div>
    );
  }

}
