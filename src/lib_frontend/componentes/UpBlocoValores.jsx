import React from 'react';

export class UpBlocoValores extends React.Component {

  render() {
    const cor = 'bloco-valores small-box bg-' + this.props.cor;
    const icone = 'fa fa-' + this.props.icone;
    
    return (
      <div className="col-lg-4 col-xs-4">
        <div className={cor}>
          <div className="inner">
            <h3>{this.props.children}</h3>

            <p>{this.props.titulo}</p>
          </div>
          <div className="icon">
            <i className={icone}></i>
          </div>
        </div>
      </div>
    );
  }

}

export default UpBlocoValores;
