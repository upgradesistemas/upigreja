import React from 'react';
import $ from 'jquery';
import InputMascara from './InputMask';

/**
 * Componente padrão de escolher data.
 */
export default class DateInput extends React.Component {

  static uniqueId = 0;

  constructor() {
    super();
    this.id = 'date-input-' + (DateInput.uniqueId++);
  }

  /**
   * @override
   */
  componentDidMount() {
    // Configura o datepicker:
    const options = {
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      autoclose: true,
      forceParse: true,
      onSelect: this.triggerOnChange.bind(this),
    };
    $('#' + this.id).datepicker(options);
  }

  /**
   * Chamado quando o usuário sai do campo.
   */
  onBlur() {
    this.props.onBlur && this.props.onBlur();
    try {
      $.datepicker.parseDate('dd/mm/yy', this.props.value);
    } catch (e) {
      // Data inválida.
      this.triggerOnChange('');
    }
  }

  /**
   * Ativa o onchange do input.
   */
  triggerOnChange(value) {
    this.props.onChange({
      target: { value },
    });
    this.props.onBlur && this.props.onBlur();
  }

  render() {
    return (
      <div>
        <InputMascara {...this.props}
          id={this.id}
          data
          label={this.props.label}
          className="pull-right"
          onBlur={this.onBlur.bind(this)} />
      </div>
    );
  }

}
