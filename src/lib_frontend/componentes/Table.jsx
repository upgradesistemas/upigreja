import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import $ from 'jquery';
import Button from './Button';

let id = 0;
let larguraLinha;

/**
 * Componente foda esse mano, nossssss.
 */
class _Table extends React.Component {

  constructor(props) {
    super(props);
    this.tBodyId = 'tbody-' + (++id);
    larguraLinha = props.config.largura_linha_grid;
  }

  componentWillReceiveProps() {
    this.ajustarMargemHeader();
  }

  // ------------------------------------------------------------------------------------

  onKeyDown(e) {
    if (this.props.selectable) {
      const sel = this.props.selectedItem;
      const index = this.props.data.findIndex(x => x === sel);
      let item = null;
      if (e.keyCode === 38) {
        // Seta pra cima
        if (index > 0) {
          // Não estamos no primeiro registro
          item = this.props.data[index - 1];
        }
      } else if (e.keyCode === 40) {
        // Seta para baixo
        if (this.props.data[index + 1]) {
          // Não estamos no primeiro registro
          item = this.props.data[index + 1];
        }
      }

      if (item) {
        this.selecionarItem(item);
      }
    }
    this.props.onKeyDown && this.props.onKeyDown(e);
  }

  // ------------------------------------------------------------------------------------

  getRowStyle(item, col) {
    const style = {};
    if (this.props.selectedItem === item) {
      style.backgroundColor = 'rgba(60, 141, 188, .92)';
      style.fontWeight = 'bold';
      style.color = 'white';
    }
    if (col.props.style) {
      style.width = col.props.style.width;
    }
    if (col.props.width) {
      style.width = col.props.width;
    }
    style.lineHeight = larguraLinha || '2';

    // para tela de Solicitações de Visita e Oração
    if (item.grau_pedido && item.grau_pedido === 'U' &&
      item.status_solicitacao && item.status_solicitacao === 'P') {
      style.backgroundColor = 'red';
      style.color = 'white';
      if (this.props.selectedItem === item) {
        style.backgroundColor = 'rgba(232, 102, 102, 0.92)';
      }
    }
    return style;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seleciona o item atual, marcando ele como selecionado pelos props.
   * @param {Object} item O Item da linha atual.
   */
  selecionarItem(item) {
    if (this.props.onSelectItem && this.props.selectable) {
      this.props.onSelectItem(item === this.props.selectedItem ? null : item);
    }
  }

  // ------------------------------------------------------------------------------------

  ajustarMargemHeader() {
    setTimeout(() => {
      if (this.tbody) {
        // React DOM pega o DOM Node correspondente ao objeto da referência.
        // Uma referencia é um acesso ao objeto definido no render
        // eslint-disable-next-line
        const domNode = ReactDOM.findDOMNode(this.tbody);
        // eslint-disable-next-line
        const tHead = ReactDOM.findDOMNode(this.thead);
        const obj = $(domNode);
        const isScroll = domNode.scrollHeight > obj.height();
        tHead.style.width = `calc(100% - ${isScroll ? '18px' : '0px'})`;
      }
    }, 10);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Extrai um valor da linha atual com base no campo.
   * @param {String} campo O Campo.
   * @param {Object} item O Dado da linha atual.
   */
  extrairValorLinha(item, campo, coluna) {
    if (coluna.props.render) {
      return coluna.props.render(item);
    }
    let valor = item[campo];
    if (campo && String(campo).indexOf('.') >= 0) {
      valor = item[campo.split('.')[0]];
      valor = valor ? valor[campo.split('.')[1]] : '';
    }
    return valor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o tbody.
   */
  renderLinhas() {
    const bodyId = this.props.bodyId || this.tBodyId;
    return (
      <tbody id={bodyId} ref={e => this.tbody = e}>
        {this.props.data.map((item, index) => (
          <tr key={index} ref={e => this['tr-' + index] = e}>
            {this.props.children.map((col, i) => (
              <td key={i}
                className={(this.props.selectable && ' selectable-td ') + col.props.className}
                style={this.getRowStyle(item, col)}
                onClick={this.selecionarItem.bind(this, item)}>
                {this.extrairValorLinha(item, col.props.field, col)}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um botão de ação, tanto no cabeçalho quanto na table.
   * @param {Object} acao A ação. 
   */
  renderBotaoAcao(acao, index) {
    return (
      <Button key={index}
        className={'btn-primary mr-5 ' + acao.className}
        icon={acao.icone}
        label={acao.label}
        disabled={acao.disabled}
        onClick={acao.onClick} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a mensagem de tabela vazia que aparece no meio da 
   * tabela quando ela não está preenchida.
   */
  renderMsgVazio() {
    return (
      <div className="table-msg-vazio-container">
        <h1>Tabela vazia</h1>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const { data, actions } = this.props;
    const headId = this.props.headId;
    return (
      <div className={'relative ' + this.props.className}>

        {actions.length > 0 && <div className="table-action-container">
          {actions.map(this.renderBotaoAcao.bind(this))}
        </div>}

        <div className="table-container">
          <table className="table" onKeyDown={this.onKeyDown.bind(this)} tabIndex="0">
            <thead ref={e => this.thead = e}
              id={headId}>
              <tr>
                {this.props.children.map((col, i) => {
                  const style = {};
                  if (col.props.width) {
                    style.width = col.props.width;
                  }
                  return <th key={i} style={style}>{col.props.header}</th>;
                })}
              </tr>
            </thead>

            {data && data.length > 0 && this.renderLinhas()}
            {data && data.length === 0 && this.renderMsgVazio()}
          </table>
        </div>
      </div>
    );
  }

}

class _Column extends React.Component {

}

export const Table = connect(state => ({
  config: state.config,
}))(_Table);

export const Column = connect(state => ({
  config: state.config,
}))(_Column);
