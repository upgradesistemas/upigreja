import React from 'react';

/**
 * Componente de fadein, todos os componentes que são filhos desse
 * serão renderizados inicialmente com um fade-in.
 */
export default class UpFadeIn extends React.Component {

  constructor() {
    super();
    this.state = {
      ...this.state,
      visivel: false,
    };
  }

  /**
   * Faz a tela aparecer
   */
  fadeIn() {
    setTimeout(() => {
      this.setState({ visivel: true });
    }, 50);
  }

  /**
   * Faz a tela desaparecer
   */
  fadeOut() {
    this.setState({ visivel: false });
  }

  render() {
    return (
      <div className={'up-fade-in ' + 
        (this.state.visivel ? ' active ' : '') +
        this.props.className}>
        {this.props.children}
      </div>
    );
  }
  
}
