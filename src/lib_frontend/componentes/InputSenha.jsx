import React from 'react';
import Input from './Input.jsx';

export default class InputSenha extends React.Component {

  state = {
    visivel: false,
  };

  focus() {
    this.input.getWrappedInstance().focus();
  }

  render() {
    return (
      <div className="input-senha">
        <Input {...this.props}
          value={this.props.value}
          onChange={this.props.onChange} 
          ref={e => this.input = e}
          type={this.state.visivel ? '' : 'password'} />
        <i className={'icone fa fa-eye ' + (this.state.visivel && 'active')}
          onClick={() => this.setState({ visivel: !this.state.visivel })} />
      </div>
    );
  }

}
