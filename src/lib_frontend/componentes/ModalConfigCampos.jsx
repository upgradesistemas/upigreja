import React from 'react';
import Modal from './Modal.jsx';
import Checkbox from './Checkbox.jsx';
import Api from '../main/Api.jsx';
import LabeledInput from './LabeledInput.jsx';
import Button from './Button.jsx';

let codigoCliente;

/**
 * Classe de configuração de campos de uma tabela. 
 */
export default class ModalConfigCampos extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  state = {
    dados: [],
    filtro: '',
  };

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    const ret =
      await Api('buscarDados', 'ConfigCampos',
        { tabela: this.props.tabela, codigoCliente: codigoCliente });
    if (!ret.status) {
      return;
    }
    this.setState({ dados: ret.dados });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Mostra esse dialog.
   */
  async show() {
    this.modal.show();
    setTimeout(() => this.input.getWrappedInstance().focus(), 600);
  }

  // ------------------------------------------------------------------------------------

  async marcarDesmarcarTodos(marcar) {
    const dados = this.state.dados;
    for (const dado of dados) {
      dado.checked = marcar;
    }
    this.setState({ dados: [...this.state.dados] });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Confirma as alterações nas configs de campos.
   */
  async confirmarConfigCampos() {
    await Api('salvarConfigCampos', this.state.dados, this.props.tabela, codigoCliente);
    this.modal.hide();
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      icon: 'fa-check',
      label: 'Confirmar',
      className: 'btn-success',
      onClick: this.confirmarConfigCampos.bind(this),
    }, {
      icon: 'fa-times',
      label: 'Cancelar',
      className: 'btn-danger',
      dismiss: true,
    }];
    return (
      <Modal buttons={botoes}
        title="Configuração de campos"
        ref={e => this.modal = e}
        onClose={this.props.onClose}>

        <div className="config-campos-container">
          <LabeledInput label="Filtre um campo:"
            value={this.state.filtro}
            inputRef={e => this.input = e}
            onChange={e => this.setState({ filtro: e })}
            placeholder="Busque por qualquer nome de campo" />

          {this.state.dados
            .filter(x => x.nome && x.nome.indexOf(this.state.filtro) >= 0)
            .map(x => (
              <Checkbox text={x.nome}
                checked={x.checked}
                key={x.nome}
                onChange={(e) => {
                  x.checked = e;
                  this.forceUpdate();
                }} />
            ))}
        </div>

        <div className="config-campos-buttons">
          <Button icon="fa-check"
            className="btn-primary"
            onClick={this.marcarDesmarcarTodos.bind(this, true)} />
          <Button icon="fa-times"
            className="btn-primary"
            onClick={this.marcarDesmarcarTodos.bind(this, false)} />
        </div>
      </Modal>
    );
  }

}
