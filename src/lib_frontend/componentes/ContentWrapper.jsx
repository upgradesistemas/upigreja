import React from 'react';
import { connect } from 'react-redux';
import UpFadeIn from '../../lib_frontend/componentes/UpFadeIn.jsx';
import Api from '../main/Api.jsx';
import sistemas from '../../sistemas';

let msg;

class ContentWrapper extends React.Component {

  componentDidMount() {
    this.upFadeIn.fadeIn();
    if (this.props.title) {
      this.props.dispatch({
        type: 'setTituloContentWrapper',
        titulo: this.props.title,
        subTitulo: this.props.small,
      });
    }
    this.montaMensagemSistema();
    window.dispatchEvent(new Event('resize'));
  }

  async montaMensagemSistema() {
    const mensagem = await Api('buscarDados', 'MensagemSistema',
      { codSoftware: sistemas.softWare.igreja });
    if (mensagem.status && (mensagem.dados && mensagem.dados.status_ativo === 'S')) {
      msg = (
        <div className="mensagemUsuario">
          <h4>
            {mensagem.dados.mensagem}
          </h4>
        </div>
      );
    }
  }

  telaNaoHabilitada() {
    return (
      <div className="tela-nao-habilitada">
        <i className="fa fa-lock" />
        Acesso Negado :(
        <span>Ops! Esta Tela não está disponível.</span>
      </div>
    );
  }

  render() {
    return (
      <div>
        <UpFadeIn ref={e => this.upFadeIn = e}>
          <div className="content-wrapper">
            <section className="content">
              {msg}
              {this.props.children ?
                this.props.children :
                this.telaNaoHabilitada()}
            </section>
          </div>
        </UpFadeIn>
      </div>
    );
  }

}

export default connect(null, dispatch => ({
  dispatch,
}))(ContentWrapper);
