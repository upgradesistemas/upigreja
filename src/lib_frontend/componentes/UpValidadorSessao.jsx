import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import UpFadeIn from './UpFadeIn.jsx';
import Api from '../main/Api.jsx';

/**
 * Valida a sessão do usuário, caso o usuário não possua permissão para
 * visitar a página ela não será mostrada a ele.
 */
class UpValidadorSessao extends React.Component {

  state = {
    validado: false,
  }

  componentDidMount() {
    // Valida a autenticação
    this.validarAuth();
  }

  /**
   * Valida a autenticação do usuário, caso a autenticação dele esteja
   * de alguma maneira falha, ele é redirecionado para a página de login
   * novamente.
   */
  async validarAuth() {
    const ret = await Api('validarAuth');
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
      this.props.history.push('/');
    } else {
      // Porém se estamos, libera a tela:
      this.setState({ validado: true }, () => {
        this.upFadeIn.fadeIn();
      });
    }
  }

  render() {
    return (
      <UpFadeIn ref={e => this.upFadeIn = e}>
        {this.state.validado ? this.props.children : <div />}
      </UpFadeIn>
    );
  }

}

export default connect(null, (dispatch) => ({
  dispatch,
}))(UpValidadorSessao);
