import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './Modal.jsx';

let id = 0;

export default class ModalConfirmacao extends React.Component {

  // ------------------------------------------------------------------------------------

  static abrir(dados) {
    return new Promise((resolve) => {
      const idElemento = `modal-confirmacao-${++id}`;

      document.getElementById(idElemento) && 
        document.getElementById(idElemento).remove();
  
      const div = document.createElement('div');
      div.id = idElemento;
      document.body.append(div);
  
      ReactDOM.render(
        <ModalConfirmacao {...dados} 
          mostrarAutomaticamente
          idElemento={idElemento}
          onClose={resolve} />,
        div,
      );
    });
  }

  // ------------------------------------------------------------------------------------

  onClose() {
    setTimeout(() => {
      document.getElementById(this.props.idElemento) && 
      document.getElementById(this.props.idElemento).remove();      
    }, 500);
    this.props.onClose && this.props.onClose(this.tipo);
  }

  // ------------------------------------------------------------------------------------

  onClickBotao(tipo) {
    this.tipo = tipo;
    this.modal.hide();
  }

  // ------------------------------------------------------------------------------------

  criarBotoes() {
    const { botoes } = this.props;
    if (botoes === 'ok') {
      return [{
        icon: 'fa-check',
        label: 'ok',
        className: 'btn-default',
        onClick: this.onClickBotao.bind(this, 'ok'),
      }];
    } else if (!botoes || botoes === 'confirmar') {
      return [{
        icon: 'fa-check',
        label: 'Confirmar',
        className: 'btn-success',
        onClick: this.onClickBotao.bind(this, 'confirmar'),
      }, {
        icon: 'fa-times',
        label: 'Cancelar',
        className: 'btn-danger',
        onClick: this.onClickBotao.bind(this, 'cancelar'),
      }];
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Modal {...this.props} 
        buttons={this.criarBotoes()}
        onClose={this.onClose.bind(this)}
        ref={e => this.modal = e}>
        {this.props.mensagem}
      </Modal>
    );
  }

}
