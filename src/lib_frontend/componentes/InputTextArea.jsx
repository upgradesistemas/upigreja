import React from 'react';

export default class InputTextArea extends React.Component {
  
  /**
   * Evento chamado quando o componente de input é mudado.
   */
  onChange(e) {
    if (this.props.uppercase === undefined || this.props.uppercase === true) {
      e.target.value = e.target.value.toUpperCase();
    }
    if (this.props.onChange) {
      this.props.onChange(e.target.value);
    }
  }

  render() {
    const { uppercase, ...rest } = this.props;
    return (
      <div>
        <label>{this.props.label}</label>
        <textarea {...rest}
                  className={'form-control text-uppercase ' + this.props.classname}
                  onChange={this.onChange.bind(this)} />
      </div>
    );
  }

};