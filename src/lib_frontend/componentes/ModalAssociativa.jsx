import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { Table, Column } from './Table.jsx';
import Modal from './Modal';
import UpPanel from './UpPanel.jsx';
import Api from '../main/Api.jsx';
import Button from './Button.jsx';

let codigoCliente;

class ModalAssociativa extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  state = {
    params: {
      tabela: '',
      pkNome: '',
      pkAtual: 0,
      pkAssociNome: '',
      codBusca: '',
      coluna: '',
      tabela2: '',
      codBusca2: '',
      coluna2: '',
      tabela3: '',
    },
    valor: 0,
    valor2: 0,
  };

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   * @param {String} params.tabela A tabelar onde irá salvar a associativa
   * @param {String} params.pkNome Campo onde irá salvar o cod da tela que chamou
   * @param {Integer} params.pkAtual Código da tela que chamou
   * @param {String} params.pkAssociNome Campo onde irá salvar o cod da associativa
   */
  async show(params) {
    await this.setState({ params });
    await this.carregarRegistros(params);
    this.modal.show();
    setTimeout(() => this.upPanel.focus(), 1000);
  }

  // ------------------------------------------------------------------------------------

  async carregarRegistros(params) {
    const { tabela, tabela2, pkNome, pkAtual, pkAssociNome, codBusca, coluna,
      codBusca2, coluna2, tabela3 } = params;
    const nome = await Api('registrosAssociativa',
      codigoCliente, tabela, tabela2, pkNome, pkAtual, pkAssociNome, codBusca, coluna,
      codBusca2, coluna2, tabela3);

    if (nome.status) {
      await this.setState({
        registros: nome.dados,
      });
    } else {
      toastr.error('Erro!', nome.erro, { attention: true });
    }
  }

  // ------------------------------------------------------------------------------------

  async addRemov(remover) {
    const params = this.state.params;
    const { valor, valor2 } = this.state;
    const { tabela, pkNome, pkAtual, pkAssociNome, codBusca2 } = params;
    const registro = {
      [pkAssociNome]: valor,
      [pkNome]: pkAtual,
      [codBusca2]: valor2,
    };
    await Api('addRemovAssoci',
      codigoCliente, tabela, remover, pkAssociNome, valor, pkNome, pkAtual, registro);
    await this.carregarRegistros(params);
    this.setState({ valor: 0, valor2: 0 });
    setTimeout(() => this.upPanel.focus(), 1000);
  }

  // ------------------------------------------------------------------------------------

  render2(preenche, p) {
    if (preenche) {
      return (
        <div className="col-sm-9">
          <UpPanel label={p.params.label2}
            tabela={p.params.tabela3}
            chave={p.params.codBusca2}
            desc={p.params.coluna2}
            value={p.valor2}
            ref={e => this.upPanel2 = e}
            onChange={e => this.setState({ valor2: e })} />
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderColuna(preenche, p) {
    if (preenche) {
      return (
        <Column field={p.params.coluna2} header={p.params.label2} />
      );
    }
    return (<Column width="0px" key={0} field="" header="" />);
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-warning',
      label: 'Sair',
      icon: 'fa-sign-out',
      dismiss: true,
    }];
    const p = this.state;
    return (
      <Modal title={this.props.children}
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="modal-gerar-parcelas p-10">

          <div className="row">
            <div className="col-sm-9">
              <UpPanel label={p.params.label1}
                tabela={p.params.tabela2}
                chave={p.params.codBusca}
                desc={p.params.coluna}
                value={p.valor}
                ref={e => this.upPanel = e}
                onChange={e => this.setState({ valor: e })} />
            </div>

            {this.render2(p.params.codBusca2, p)}

            <div className="col-sm-1">
              <div className="botao-associativa">
                <Button className='btn-success'
                  label=''
                  icon='fa-check'
                  disabled={false}
                  onClick={this.addRemov.bind(this, false)} />
              </div>
            </div>

            <div className="col-sm-1">
              <div className="botao-associativa">
                <Button className='btn-danger'
                  label=''
                  icon='fa-trash'
                  disabled={false}
                  onClick={this.addRemov.bind(this, true)} />
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <Table header="" data={this.state.registros} actions={[]}>
                <Column width="0px" key={0} field="" header="" />
                <Column key={1} field={this.state.params.coluna} header={p.params.label1} />
                {this.renderColuna(p.params.codBusca2, p)}
              </Table>
            </div>
          </div>
        </div>

      </Modal>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalAssociativa);
