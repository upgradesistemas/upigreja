import React from 'react';
import { connect } from 'react-redux';

let Maiusculo;

class Input extends React.Component {

  constructor(props) {
    super(props);
    Maiusculo = props.config.status_input_maiusculo;
  }

  componentDidMount() {
    this.input.addEventListener('keydown', (a) => {
      // keyCode 13 é a tecla enter
      if (a.keyCode === 13) {
        this.talvezDeixarTextoMaiusculo();
        this.onKeyEnter && this.onKeyEnter();
      }
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Chamado quando o componente de input perde o foco.
   */
  onBlur() {
    this.talvezDeixarTextoMaiusculo();

    if (this.props.type === 'number') {
      // Se for número, vamos tratar o minimo e o máximo.
      const num = Number(this.props.value);
      if (this.props.min && num < Number(this.props.min)) {
        this.dispararMudanca(this.props.min);
      }
      else if (this.props.max && num > Number(this.props.max)) {
        this.dispararMudanca(this.props.max);
      }
    }

    this.props.onBlur && this.props.onBlur();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Evento chamado quando o componente de input é mudado.
   */
  onChange(e) {
    const campo = this.props.type === 'checkbox' ? 'checked' : 'value';
    this.dispararMudanca(e.target[campo]);
  }

  // ------------------------------------------------------------------------------------

  /**
   * ativa o Key Enter
   */
  onKeyEnter() {
    this.props.onKeyEnter && this.props.onKeyEnter();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o classname.
   */
  getClassName() {
    const isFormControl = (
      !this.props.type ||
      this.props.type === 'input' ||
      this.props.type === 'password' ||
      this.props.type === 'number'
    );
    return `
      ${isFormControl ? ' form-control ' : ''}
      ${this.textoDeveSerMaiusculo() ? ' text-uppercase ' : ''}
      ${this.props.className}
    `;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna se o texto deve ser maiusculo, com base nas configurações de props
   * desse input, e também das configurações gerais do sistema.
   * @return {Boolean} True para sim, false para não.
   */
  textoDeveSerMaiusculo() {
    if (this.props.type) {
      return;
    }
    if (this.props.uppercase === undefined) {
      return Maiusculo === 'S';
    }
    return this.props.uppercase;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Talvez deixa o texto como maiusculo, com base no método 
   * textoDeveSerMaiusculo() logo acima.
   * @see textoDeveSerMaiusculo
   */
  talvezDeixarTextoMaiusculo() {
    if (this.textoDeveSerMaiusculo()) {
      // Texto deve ser maiusculo, vamos disparar uma mudança com a correção:
      this.dispararMudanca((this.props.value || '').toUpperCase());
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Ativa o onChange de quem está ouvindo, isso faz com que o
   * fornecedor desse componente atualize seus dados.
   */
  dispararMudanca(value) {
    this.props.onChange && this.props.onChange(value);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Coloca o foco da página nesse input, já selecionando todo o seu texto.
   */
  focus() {
    this.input.focus();
    if (this.props.type !== 'number') {
      // Não se pode focar inputs de número
      this.input.setSelectionRange(0, this.input.value.length);
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    // eslint-disable-next-line
    const { onKeyEnter, uppercase, ...rest } = this.props;
    this.onKeyEnter = onKeyEnter;
    return (
      <input {...rest}
             style={this.props.style}
             ref={e => this.input = e}
             onChange={this.onChange.bind(this)}
             onBlur={this.onBlur.bind(this)}
             className={this.getClassName()} />
    );
  }
}

export default connect((state) => ({
  config: state.config,
}), null, null, { withRef: true })(Input);
