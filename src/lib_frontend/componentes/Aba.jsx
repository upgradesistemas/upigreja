import React from 'react';

let uniqueId = 0;

export class Aba extends React.Component {

}

/**
 * 
 */
export class PageControl extends React.Component {

  constructor() {
    super();
    this.id = 'page-control-' + (++uniqueId);
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    // if (this.state.selected === null) {
    //   const firstNotDisabled = this.getChildren()[0] || { key: null };
    //   this.setState({ selected: this.getItemKey(firstNotDisabled.key) });
    // }
  }

  // ------------------------------------------------------------------------------------

  componentWillReceiveProps(nextProps) {
    // Se o state.selected for nulo, quer dizer que nenhuma aba foi selecionada, temos que 
    // selecionar uma por padrão. 
    // Se não foi encontrado um item com o id do state.selected, é porque a aba que estava
    // selecionada foi desabilitada, então vamos selecionar a primeira válida também.
    // eslint-disable-next-line
    if (!this.getChildren(nextProps).find(x => x.key == this.props.aba)) {
      // Pega a primeira aba habilitada e seta no state:
      const firstNotDisabled = this.getChildren()[0] || { key: null };
      this.props.onChange && this.props.onChange(firstNotDisabled.key);
    }
  }

  // ------------------------------------------------------------------------------------

  onSelectAba(key) {
    this.props.onChange && this.props.onChange(key);
  }

  // ------------------------------------------------------------------------------------

  getItemKey(key) {
    return this.id + key;
  }

  // ------------------------------------------------------------------------------------

  getChildren(props = this.props) {
    return (props.children.length ? props.children : [props.children])
      .filter(x => (!x.props.disabled));
  }

  // ------------------------------------------------------------------------------------

  renderAba(item, index) {
    return (
      // eslint-disable-next-line
      <li key={index} className={this.props.aba == item.key ? 'active' : ''}>
        <a data-toggle='tab'
          onClick={() => {
            if (item.props.onClick) {
              item.props.onClick();
            }
            this.onSelectAba(item.key);
          }}
          data-target={this.getItemKey(item.key)}>
          <i className={`fa fa-${item.props.icone}`} />
          {item.props.label}
        </a>
      </li>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um rodapé opcional da aba, esse rodapé vai ficar fixo no fim da 
   * página em todo o momento.
   */
  renderRodape() {
    return this.props.renderRodape && (
      <div className="page-control-rodape">
        {this.props.renderRodape()}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderConteudo(item) {
    return (
      <div id={this.getItemKey(item.key)}
        key={item.key}
        // eslint-disable-next-line
        className={`tab-pane ${this.props.aba == item.key ? 'active' : ''}`}>
        <div className="page-control-tab-inner-content">
          {item.props.children}
        </div>
        {this.renderRodape()}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className='nav-tabs-custom'>
        <ul className='nav nav-tabs'>
          {this.getChildren().map(this.renderAba.bind(this))}
        </ul>
        <div className="tab-content relative">
          {this.getChildren().map(this.renderConteudo.bind(this))}
        </div>
      </div>
    );
  }

}
