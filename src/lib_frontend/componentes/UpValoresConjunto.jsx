import React from 'react';
import Button from './Button.jsx';

export class UpValoresBox extends React.Component {
  render() {
    return (
      <div className="col-lg-12 col-xs-12">
        <div className="box">
          <div className="box-header with-border">
            <h3 className="box-title">{this.props.titulo}</h3>
            <div className="box-footer">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export class UpValorBotao extends React.Component {
  render() {
    const icone = 'fa ' + this.props.icone;
    return (
      <div className={this.props.classNameDiv}>
        <div className="description-block border-right">
          <Button label={this.props.label}
            icon={icone}
            onClick={this.props.onClick.bind(this)}
            className={this.props.className} />
        </div>
      </div>
    );
  }
}

export class UpValorFilho extends React.Component {
  render() {
    const { className, valor, titulo } = this.props;
    const cor = 'description-header text-' + this.props.cor;

    return (
      <div className={className}>
        <div className="description-block border-right">
          <h5 className={cor}
            style={{ fontSize: '30px' }}>{valor}</h5>
          <span className="description-text">{titulo}</span>
        </div>
      </div>
    );
  }

}
