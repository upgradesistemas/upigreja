import React from 'react';


export default class ComboList extends React.Component {

  renderOpcao(opcao, index) {
    const { valor, campo } = this.props;
    return (
      <option key={index} value={opcao[valor]}>{opcao[campo]}</option>
    );
  }

  render() {
    const props = this.props;
    return (
      <Combo {...props}>
        {props.lista.map(this.renderOpcao.bind(this))}
      </Combo>
    );
  }

}


class Combo extends React.Component {

  static uniqueId = 0;

  constructor() {
    super();
    this.id = 'combo-' + Combo.uniqueId++;
  }

  onChange(e) {
    if (this.props.onChange) {
      this.props.onChange(e.target.value);
    }
  }

  render() {
    return (
      <div>
        {this.props.label && <label htmlFor={this.id}>{this.props.label}</label>}
        <select id={this.id} 
                className='form-control'
                onChange={this.onChange.bind(this)}
                disabled={this.props.disabled}
                value={this.props.value}>
          <option defaultValue value={0}>{this.props.defaultText || ''}</option>
          {this.props.children}
        </select>
      </div>
    );
  } 
}
