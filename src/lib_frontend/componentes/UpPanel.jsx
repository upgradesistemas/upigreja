import React from 'react';
import ReactDOM from 'react-dom';
import Input from './Input';
import UpAutocomplete from './UpAutocomplete.jsx';
import Api from '../main/Api.jsx';

let codigoCliente;

/**
 * Componente padrão da Upgrade de buscar registros no banco de dados.
 * Esse componente foi abençoado pela fada upgrade: 
 * 	╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ
 *                  . . .
 *                 . . . .
 */
export default class UpPanel extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
    this.state = {
      registro: null, // O Registro selecionado no momento
      desc: '',
    };
  }

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    if (this.props.value) {
      await this.buscarRegistroPorId(this.props.value);
    }
  }

  // ------------------------------------------------------------------------------------

  async componentWillReceiveProps(nextProps) {
    if (this.props.value && !nextProps.value &&
      document.activeElement !== ReactDOM.findDOMNode(this.autocomplete.input) &&
      document.activeElement !== ReactDOM.findDOMNode(this.campoCodigo)) {
      this.setState({ registro: null });
      this.autocomplete.selecionarRegistro(null);
    } else if (this.props.value !== nextProps.value) {
      const ret = await Api('buscarPorId', this.props.tabela, nextProps.value, codigoCliente);
      if (this.autocomplete) {
        if (ret.dados) {
          this.setState({ registro: ret.dados });
          this.autocomplete.selecionarRegistro(ret.dados);
        }
      }
    }
  }

  onKeyEnter() {
    const registro = this.autocomplete.getRegistroSetinha();
    registro && this.onSelecaoRegistroAutocomplete(registro);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Chamado quando o autocomplete seleciona um registro, vamos preencher
   * o input da chave primária.
   */
  onSelecaoRegistroAutocomplete(registro) {
    this.setState({ registro });
    this.props.onSelecaoRegistro && this.props.onSelecaoRegistro(registro);
    this.props.onChange && this.props.onChange(registro ? registro[this.props.chave] : '0');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta o registro e campo de descrição de acordo com o valor
   * do código do registro.
   */
  async onChange(e) {
    await this.buscarRegistroPorId(e);
    this.props.onChange && this.props.onChange(e);
  }

  // ------------------------------------------------------------------------------------

  onBlur() {
    if (!this.state.registro) {
      if (this.campoCodigo && this.campoCodigo.props.type === 'number') {
        this.props.onChange && this.props.onChange('0');
      } else {
        this.props.onChange && this.props.onChange('');
      }
    }
  }

  // ------------------------------------------------------------------------------------

  async buscarRegistroPorId(id) {
    const ret = await Api('buscarPorId', this.props.tabela, id, codigoCliente);
    this.setState({ registro: ret.dados });
    this.autocomplete.selecionarRegistro(ret.dados);
  }

  // ------------------------------------------------------------------------------------

  focus() {
    this.autocomplete.focus();
  }

  tratarKeyDown(e) {
    if (e.keyCode === 40) {
      this.autocomplete.tratarSetinha('baixo');
    }
    else if (e.keyCode === 38) {
      this.autocomplete.tratarSetinha('cima');
    }
    else if (e.keyCode === 13) {
      this.onKeyEnter();
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className={'up-panel-container ' + this.props.className}
        onKeyDown={(e) => this.tratarKeyDown(e)}>
        {this.props.label && <label>{this.props.label}</label>}

        <div>
          <Input value={this.props.value}
            onChange={this.onChange.bind(this)}
            onKeyEnter={this.onKeyEnter.bind(this)}
            onBlur={this.onBlur.bind(this)}
            ref={e => this.campoCodigo = e}
            disabled={this.props.disabled}
            min="0"
            type={!this.props.alfanumerico && 'number'}
            className="up-panel-codigo" />

          <UpAutocomplete value={this.state.desc}
            campo={this.props.desc}
            tabela={this.props.tabela}
            where={this.props.where}
            codigoCliente={codigoCliente}
            onBlur={this.props.onBlurUp && this.props.onBlurUp()}
            placeholder={this.props.placeholder}
            className="up-panel-descricao"
            onChange={e => this.setState({ desc: e })}
            ref={e => this.autocomplete = e}
            disabled={this.props.disabled}
            permitirVazio={this.props.permitirVazio}
            onSelecionarRegistro={this.onSelecaoRegistroAutocomplete.bind(this)} />
        </div>
      </div>
    );
  }

}
