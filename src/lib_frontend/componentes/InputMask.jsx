import React from 'react';
import InputMask from 'react-input-mask';

/**
 * @param {String} separadorMascara Informar o tipo do separador de Mascara / Default: "_".
 * @param {String} label Atribui uma Label para o campo InputMask.
 */
export default class InputMascara extends React.Component {

  getMascara() {
    if (this.props.cpf) {
      return '999.999.999-99';
    } else if (this.props.cnpj) {
      return '99.999.999/9999-99';
    } else if (this.props.data) {
      return '99/99/9999';
    } else if (this.props.telefone) {
      return '(99)9999-9999';
    } else if (this.props.celular) {
      return '(99)99999-9999';
    } else if (this.props.cep) {
      return '99999-999';
    } else if (this.props.hora) {
      return '99:99';
    }
  }

  getPlaceholder() {
    if (this.props.cpf) {
      return 'Ex: 000.000.000-00';
    } else if (this.props.cnpj) {
      return 'Ex: 00.000.000/0000-00';
    } else if (this.props.data) {
      return 'Ex: 00/00/0000';
    } else if (this.props.telefone) {
      return 'Ex: (00)0000-0000';
    } else if (this.props.celular) {
      return 'Ex: (00)00000-0000';
    } else if (this.props.cep) {
      return 'Ex: 00000-000';
    } else if (this.props.hora) {
      return 'Ex: 00:00';
    }
  }

  getIconMask() {
    if (this.props.telefone) {
      return (
        <div className="input-group-addon"> <i className="fa fa-phone" /> </div>
      );
    } else if (this.props.data) {
      return (
        <div className="input-group-addon"> <i className="fa fa-calendar" /> </div>
      );
    } else if (this.props.hora) {
      return (
        <div className="input-group-addon"> <i className="fa fa-clock-o" /> </div>
      );
    }
  }

  getGrupoClass() {
    if (this.props.data || this.props.telefone || this.props.hora) {
      return 'input-group';
    }
  }

  render() {
    return (
      <div>
        <label htmlFor={this.props.name}>{this.props.label}</label>
        <div className={this.getGrupoClass()}>
          {this.getIconMask()}
          <InputMask style={this.props.style}
            className={'form-control ' + this.className}
            ref={e => this.inputMask = e}
            id={this.props.id}
            mask={this.getMascara()}
            placeholder={this.getPlaceholder()}
            maskChar={this.props.separadorMascara}
            value={this.props.value}
            disabled={this.props.disabled}
            onChange={this.props.onChange}
            onBlur={this.props.onBlur} />
        </div>
      </div>
    );
  }
}