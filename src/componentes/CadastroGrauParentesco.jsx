import React from 'react';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';

/**
 */
export default class CadastroGrauParentesco extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_grau_parentesco;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_grau_parentesco: null,
      nome_grau_parentesco: '',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_grau_parentesco', true)}
          </div>
          <div className="col-sm-10">
            {componentes.input('Grau de Parentesco:', 'nome_grau_parentesco')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_grau_parentesco"
        titulo="Cadastro de Parentesco"
        subTitulo="Configure os Graus de Parentesco!"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)} >
        <Column key={1} width="80px" field="cod_grau_parentesco" header="Código" />
        <Column key={2} field="nome_grau_parentesco" header="Grau de Parentesco" />
      </Cadastro>
    );
  }

}

