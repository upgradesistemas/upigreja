import React from 'react';
import moment from 'moment';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';

/**
 */
export default class CadastroCurso extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_curso;
  }

  // ------------------------------------------------------------------------------------

  getValidacoes(registro) {
    if (!registro.nome_curso) {
      toastr.error('Atenção!', 'Digite o nome da curso!', { attention: true });
      return false;
    }
    if (!Number(registro.cod_cidade)) {
      toastr.error('Atenção!', 'Selecione a cidade!', { attention: true });
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_curso: '',
      cod_cidade: 0,
      nome_curso: '',
      local: '',
      dt_inicio: moment(new Date()).format('DD/MM/YYYY HH:mm'),
      dt_fim: moment(new Date()).format('DD/MM/YYYY HH:mm'),
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-6">
            {componentes.input('Curso:', 'nome_curso')}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data Início:', 'dt_inicio')}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data Fim:', 'dt_fim')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {componentes.upPanel('Cidade:', 'cod_cidade', 'tab_cidade',
              'cod_cidade', 'nome_cidade_comp')}
          </div>
          <div className="col-sm-5">
            {componentes.input('Local:', 'local')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_curso"
        titulo="Cadastro de Curso"
        subTitulo="Configure os seus Cursos!"
        renderForm={this.renderForm.bind(this)}
        paginavel
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getValidacoes={this.getValidacoes.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={1} width="80px" field="cod_curso" header="Código" />
        <Column key={2} field="nome_curso_comp" header="Curso" />
      </Cadastro>
    );
  }

}
