import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import Button from '../lib_frontend/componentes/Button.jsx';
import InputCalc from '../lib_frontend/componentes/InputCalc.jsx';
import LabeledInput from '../lib_frontend/componentes/LabeledInput.jsx';
import { trocarRota } from '../utils/funcoesFront';

let Igreja;
let Pessoa;
let Caixa;
let RotaAtual;
let codigoCliente;

class ModalCaixa extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
    Pessoa = props.usuario.cod_pessoa;
    Caixa = props.usuario.caixa;
    RotaAtual = window.location.href;
    codigoCliente = localStorage.getItem('codigoCliente');
    this.buscarCaixaAberto();
  }

  // ------------------------------------------------------------------------------------

  state = {
    obs: '',
    vr_inicial: 0,
    vr_recebimento: 0,
    vr_pagamento: 0,
    vr_saldo: 0,
  };

  // ------------------------------------------------------------------------------------

  /**
   * Handler de mudanças dos campos de formulário na alteração e inclusão,
   * todos os campos chamam esse método durante a alteração do valor.
   */
  setValorRegistro(nome, valor) {
    const valorReal = valor.target ? valor.target.value : valor;
    this.setState({
      [nome]: valorReal,
    });
  }

  // ------------------------------------------------------------------------------------

  async buscarCaixaAberto() {
    await this.setState({ caixaAberto: false });
    if (Caixa) {
      await this.setState({
        obs: Caixa.obs,
        vr_inicial: Caixa.vr_inicial || Caixa.vr_saldo_anterior,
        caixaAberto: true,
      });
    } else {
      // Vou lá buscar o saldo anterior para colocar no valor inicial desse novo caixa.
      const ret = await Api('retornaValorCaixaAnterior', Igreja, codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      await this.setState({
        vr_inicial: ret.dados,
      });
    }
  }

  // ------------------------------------------------------------------------------------

  async abrirCaixa() {
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      const ret =
        await Api('abrirFecharCaixa', Caixa, this.state, Pessoa, Igreja, codigoCliente, true);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      toastr.success('Sucesso !', 'O Caixa foi Aberto !');
      this.modal.hide();
      trocarRota(RotaAtual.replace('#', ''));
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  async fecharCaixa() {
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      const ret =
        await Api('abrirFecharCaixa', Caixa, this.state, Pessoa, Igreja, codigoCliente, false);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      toastr.success('Sucesso !', 'O Caixa foi Fechado !');
      this.modal.hide();
      trocarRota('/app/dashboard');
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  show() {
    this.modal.show();
  }

  // ------------------------------------------------------------------------------------

  mensagemCaixaAberto() {
    if (!Caixa) {
      return (
        <div className="caixa-fechado">
          <i className="fa fa-lock" />
          Caixa Fechado !
          <span style={{ fontSize: '40%' }}>Para continuar clique em "Abrir Caixa"...</span>
          <hr />
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-close',
      onClick: () => Caixa || trocarRota('/app/dashboard'),
      dismiss: true,
    }];
    return (
      <Modal {...this.props}
        title="Abrir / Fechar Caixa"
        buttons={botoes}
        ref={e => this.modal = e}
        naoFechar={!Caixa} >
        <div className="relative p-10" ref={e => this.divPrincipal = e}>

          {this.mensagemCaixaAberto()}
          <div className="row">
            <div className="col-sm-5">
              <InputCalc value={this.state.vr_inicial}
                label="Valor Inicial:"
                onChange={this.setValorRegistro.bind(this, 'vr_inicial')} />
            </div>
            <div className="col-sm-7">
              <LabeledInput label="Observação:"
                value={this.state.obs}
                onChange={this.setValorRegistro.bind(this, 'obs')}
                className="mb-5" />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-3">
              <Button className='btn-success'
                label='Abrir Caixa'
                icon='fa-unlock'
                disabled={this.state.caixaAberto}
                onClick={this.abrirCaixa.bind(this)} />
            </div>

            <div className="col-sm-3">
              <Button className='btn-warning'
                label='Fechar Caixa'
                icon='fa-lock'
                disabled={!this.state.caixaAberto}
                onClick={this.fecharCaixa.bind(this)} />
            </div>
          </div>

        </div>
      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalCaixa);
