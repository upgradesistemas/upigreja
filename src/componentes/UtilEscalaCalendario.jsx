import React from 'react';
import { toastr } from 'react-redux-toastr';
import Calendario from '../lib_frontend/componentes/Calendario';
import ModalEscala from './ModalEscala';
import Api from '../lib_frontend/main/Api.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';

let codigoCliente;

/**
 * Componente que mostra um calendário e todas as escalas
 * associadas com um dado cod_calendario da tab_calendario.
 * 
 * Deve obrigatoriamente ser especificado um 'codCalendario'
 * nas props.
 */
export default class UtilEscalaCalendario extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  componentDidMount() {
    this.carregarEscalas();
  }

  // ------------------------------------------------------------------------------------

  componentWillReceiveProps(nextProps) {
    if (this.props.codCalendario !== nextProps.codCalendario) {
      this.carregarEscalas(nextProps.codCalendario);
    }
  }

  // ------------------------------------------------------------------------------------

  async onClickDia(data) {
    try {
      this.props.onEdicaoOuInclusao && await this.props.onEdicaoOuInclusao();
      this.abrirModalEscala(null, data);
    } catch (ex) {
      toastr.error('Atenção!', ex, { attention: true });
      // cairá aqui se não preencheu o nome do calendário
    }
  }

  // ------------------------------------------------------------------------------------

  async onClickEvento(e) {    
    try {
      this.props.onEdicaoOuInclusao && await this.props.onEdicaoOuInclusao();
      this.abrirModalEscala(e.cod_escala);
    } catch (ex) {
      toastr.error('Atenção!', ex, { attention: true });
      // cairá aqui se não preencheu o nome do calendário
    }
  }

  // ------------------------------------------------------------------------------------

  abrirModalEscala(codEscala, dataHora) {
    this.modalEscala.getWrappedInstance().show(codEscala, dataHora, this.props.codCalendario);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Carrega todas as escalas que possuem o codCalendario passado nas props.
   */
  async carregarEscalas(codCalendario = this.props.codCalendario) {
    GenericUtils.setElementoCarregando(this.divCalendario, true);
    try {
      const ret = await Api('buscarDados', 'EscalasCalendario', { codCalendario, codigoCliente });
      if (!ret.status) {
        toastr.error('Atenção!', ret.erro, { attention: true });
        return false;
      }
      this.calendario.setEventos(ret.dados.map(x => ({
        title: '',
        cod_escala: x.cod_escala,
        allDay: true,
        start: new Date(x.dthr_escala),
        backgroundColor: 'rgb(44, 144, 223)',
      })));
    } finally {
      GenericUtils.setElementoCarregando(this.divCalendario, false);
    }
  }

  // ------------------------------------------------------------------------------------

  async mudarDataEscala(escala, reverterAcao) {
    GenericUtils.setElementoCarregando(this.divCalendario, true);
    try {
      const novaDataHora = escala.start.format('YYYY-MM-DD HH:mm:ss');
      const ret = await Api('mudarDataEscala', escala.cod_escala, novaDataHora, codigoCliente);
      if (!ret.status) {
        reverterAcao();
      }
    } catch (ex) {
      reverterAcao();
    } finally {      
      GenericUtils.setElementoCarregando(this.divCalendario, false);
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="escala-calendario-container">
        <div className="relative" ref={e => this.divCalendario = e}>
          <Calendario ref={e => this.calendario = e}
            onClickDia={this.onClickDia.bind(this)} 
            onClickEvento={this.onClickEvento.bind(this)}
            onMudarDataEvento={this.mudarDataEscala.bind(this)} />
        </div>
        
        <ModalEscala ref={e => this.modalEscala = e}
          onConfirmar={this.carregarEscalas.bind(this)} />
      </div>
    );
  }

}
