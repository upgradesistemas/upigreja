import React from 'react';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';

/**
 */
export default class CadastroCentroCusto extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_cidade;
  }

  // ------------------------------------------------------------------------------------

  getValidacoes(registro) {
    if (!registro.nome_centro_custo) {
      toastr.error('Atenção!', 'Digite o nome do Centro de Custo !', { attention: true });
      return false;
    }
    if (!Number(registro.cod_tp_centro)) {
      toastr.error('Atenção!', 'Selecione o Tipo !', { attention: true });
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_centro_custo: '',
      nome_centro_custo: '',
      cod_tp_centro: '',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_centro_custo', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Centro de Custo:', 'nome_centro_custo')}
          </div>
          <div className="col-sm-3">
            {componentes.upPanel('Tipo:', 'cod_tp_centro', 'tab_tp_centro_custo',
              'cod_tp_centro_custo', 'nome_tp')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_centro_custo"
        titulo="Cadastro de Centro de Custo"
        subTitulo="Configure os Seus Custo!"
        renderForm={this.renderForm.bind(this)}
        paginavel
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getValidacoes={this.getValidacoes.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={1} width="80px" field="cod_centro_custo" header="Código" />
        <Column key={2} field="nome_centro_custo" header="Centro de Custo" />
        <Column key={3} field="tp_centro.nome_tp" header="Tipo de Conta" />
      </Cadastro>
    );
  }

}
