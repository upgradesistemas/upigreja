import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import ModalBaixarCR from './ModalBaixarCR';
import tipo from '../tipo';
import ModalCaixa from './ModalCaixa.jsx';

let Pessoa;
let Igreja;
let DepIgreja;
let Logada;
let Caixa;

/**
 */
class MovRevistaEbd extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    Igreja = props.usuario.cod_igreja;
    DepIgreja = props.usuario.departamento_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    return valor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cr: '',
      cod_pessoa: '',
      cod_igreja: Igreja,
      cod_forma_pagamento: 1,
      cod_departamento: DepIgreja,
      cod_tipo_movimento: tipo.movimento.ebd,
      cod_funcionario: Pessoa,
      cod_funcionario_baixa: Pessoa,
      dt_hr_recebido: '',
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_receber: '',
      vr_receber: 0,
      obs: '',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_cr', true)}
          </div>
          <div className="col-sm-2">
            {componentes.inputDate('Data da Entrega:', 'dt_hr_cadastro')}
          </div>
          <div className="col-sm-2">
            {componentes.inputDate('Data de Vencimento:', 'dt_hr_receber')}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Membro:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'status_membro = "M"')}
          </div>
          <div className="col-sm-2">
            {componentes.inputCalc('Valor:', 'vr_receber')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-12">
            {componentes.input('Observação:', 'obs')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a coluna de recebido.
   */
  renderColunaRecebido(item) {
    if (item.dt_hr_recebido && item.status_baixa_parcial === 'S') {
      return <div className="mov-cr-cp-coluna parcial fa fa-star-half-o" />;
    } else if (item.dt_hr_recebido) {
      return <div className="mov-cr-cp-coluna completa fa fa-star" />;
    }
    return <div className="mov-cr-cp-coluna fa fa-times" />;
  }

  // ------------------------------------------------------------------------------------

  render() {
    const whereInner = {
      tabelaPrincipal: 'cr', // Tabela atual que estamos pesquisando
      tabelaInner: 'pessoa', // Tabela que vamos pesquisar no Inner
      ligacaoTabela1: 'cod_pessoa', // Campo chave de ligação da Tabela Principal
      ligacaoTabela2: 'cod_pessoa', // Campo chave de ligação da Tabela do Inner
      campoWhere: 'nome_pessoa', // Campo do Inner para fazer o Like
    };

    const props = {
      tabela: 'tab_cr',
      titulo: 'Revista EBD',
      paginavel: true,
      selecionavel: true,
      where: `cod_tipo_movimento = ${tipo.movimento.ebd} AND cod_igreja = ${Logada}`,
      renderForm: this.renderForm.bind(this),
      getRegistro: this.getRegistro.bind(this),
      onAlteracaoCampo: this.onAlteracaoCampo.bind(this),
      whereInner: whereInner,
      whereAlias: `a.cod_tipo_movimento = ${tipo.movimento.ebd} AND a.cod_igreja = ${Logada}`,
      actions: [{
        icone: 'fa-money',
        label: 'Baixar conta',
        className: 'btn-success',
        onClick: (item) => {
          this.modalBaixarCR.getWrappedInstance().show(item.cod_cr);
        },
        disabled: (cr) => !cr || cr.dt_hr_recebido,
      }],
    };

    return (
      <div>
        <Cadastro ref={e => this.cadastro = e} {...props}>
          <Column key={1} width="80px" field="cod_cr" header="Código" />
          <Column key={2} width="150px" field="pessoa.nome_pessoa" header="Membro" />
          <Column key={3} width="150px" field="vr_total_formatado" header="Valor" />
          <Column key={4} width="150px" field="dt_hr_cadastro" header="Data de Entrega" />
          <Column width="80px" header="Recebido"
            render={this.renderColunaRecebido.bind(this)} />
        </Cadastro>

        <ModalBaixarCR ref={e => this.modalBaixarCR = e}
          onClose={() => this.cadastro.getWrappedInstance().carregarRegistros()} />

        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }
}

// ------------------------------------------------------------------------------------

export default connect(state => ({
  usuario: state.usuario,
}))(MovRevistaEbd);
