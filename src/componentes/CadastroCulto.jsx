import React from 'react';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import Button from '../lib_frontend/componentes/Button.jsx';
import ModalAssociativa from '../lib_frontend/componentes/ModalAssociativa.jsx';

/**
 * Cadastro padrão de culto.
 */
class CadastroCulto extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_culto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_culto: null,
      nome_culto: '',
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    const paramsDiaSemana = {
      label1: 'Dia da Semana',
      tabela: 'tab_culto_dia_semana',
      pkNome: 'cod_culto',
      pkAtual: registro.cod_culto,
      pkAssociNome: 'cod_dia_semana',
      codBusca: 'cod_dia_semana',
      coluna: 'nome_dia_semana',
      tabela2: 'tab_dia_semana',
    };
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_culto', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Culto:', 'nome_culto')}
          </div>

          <div className="botao-associativa">
            <Button className='btn-info'
              label='Dia da Semana'
              icon='fa-list-ol'
              disabled={!paramsDiaSemana.pkAtual}
              onClick={() =>
                this.modalAssociativaCulto.getWrappedInstance().show(paramsDiaSemana)} />
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <Cadastro tabela="tab_culto"
          titulo="Cadastro de Culto"
          subTitulo="Configure os seus Cultos!"
          renderForm={this.renderForm.bind(this)}
          getNomeRegistro={this.getNomeRegistro.bind(this)}
          getRegistro={this.getRegistro.bind(this)}>
          <Column key={1} width="80px" field="cod_culto" header="Código" />
          <Column key={2} field="nome_culto" header="Culto" />
        </Cadastro>

        <ModalAssociativa ref={e => this.modalAssociativaCulto = e}>
          Dias da Semana</ModalAssociativa>
      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
  redux: state,
}))(CadastroCulto);

