import React from 'react';
import uuid from 'uuid/v1';
import { toastr } from 'react-redux-toastr';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import Api from '../lib_frontend/main/Api.jsx';

let codigoCliente;

export default class ModalMudarUsuarioIgreja extends React.Component {

  constructor() {
    super();
    this.nomeAleatorioInput = uuid();
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    igreja: '',
    codPessoa: '',
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (this.props.mostrarAutomaticamente) {
      if (!this.state.codPessoa) {
        setTimeout(() => this.inputUsuario.focus(), 500);
      } else {
        setTimeout(() => this.inputIgreja.focus(), 500);
      }
    }
  }

  // ------------------------------------------------------------------------------------

  onBlur() {
    if (this.inputIgreja) {
      this.inputIgreja.focus();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Boolean} se foi pode salvar ou não.
   */
  realizarValidacoes() {
    if (!this.state.codPessoa) {
      toastr.error('Atenção!', 'Preencha o Usuário!', { attention: true });
      this.inputUsuario.focus();
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Chamado quando o usuário clicar em confirmar.
   */
  async salvar() {
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      if (!this.realizarValidacoes()) {
        return;
      }

      const { codPessoa, igreja } = this.state;
      const ret = await Api('mudarIgrejaUsuario', codPessoa, igreja, codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      toastr.success('Tudo certo!', 'A igreja do usuário foi alterada!');
      this.modal.hide();
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  show(codPessoa) {
    this.setState({
      igreja: '',
      codPessoa: codPessoa || '',
    });
    this.modal.show();
    setTimeout(() => this.inputIgreja.focus(), 500);
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Confirmar',
      icon: 'fa-check',
      onClick: this.salvar.bind(this),
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      dismiss: true,
    }];

    return (
      <Modal {...this.props}
        title="Mudar igreja da Pessoa"
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="relative" ref={e => this.divPrincipal = e}>
          <div className="row p-10">
            <div className="col-sm-12">
              <UpPanel tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                label="Pessoa:"
                value={this.state.codPessoa}
                onChange={e => this.setState({ codPessoa: e })}
                // onBlurUp={this.onBlur.bind(this)}
                ref={e => this.inputUsuario = e} />
            </div>

            <div className="col-sm-12">
              <UpPanel tabela="tab_igreja"
                chave="cod_igreja"
                desc="nome_igreja"
                label="Igreja:"
                value={this.state.igreja}
                onChange={e => this.setState({ igreja: e })}
                ref={e => this.inputIgreja = e} />
            </div>

          </div>
        </div>

      </Modal>
    );
  }

}
