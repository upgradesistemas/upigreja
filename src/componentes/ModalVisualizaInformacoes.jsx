import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import Api from '../lib_frontend/main/Api.jsx';

let codigoCliente;
let nomeTela;
let coisas = '';

class ModalVisualizaInformacoes extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  async buscarInformacoes(info) {
    const objetoFiltro = {
      tabela: info.tabela,
      where: `${info.nomeCodPk} = ${info.codPk}`,
      campos: info.campos,
      codigoCliente,
    };
    const ret = await Api('buscarVisualizaInformacoes', objetoFiltro);
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
      return false;
    }
    this.preencheInformacoes(ret.dados);
  }

  // ------------------------------------------------------------------------------------

  async preencheInformacoes(registro) {
    coisas = '';
    for (const key in registro) {
      if (registro.hasOwnProperty(key)) {
        coisas += `
          <div class="col-sm-4">
            <p style="margin-bottom: 0px;" >${key}</p>
            <label class="visualiza-info-label">${registro[key]}</label>
          </div>
        `;
      }
    }
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  async show(info) {
    nomeTela = info.nomeTela;
    await this.buscarInformacoes(info);
    this.modal.show();
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Fechar',
      icon: 'fa-check',
      dismiss: true,
    }];

    return (
      <Modal {...this.props}
        title={nomeTela}
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="relative" ref={e => this.divPrincipal = e}>
          <div className="row p-25" dangerouslySetInnerHTML={{ __html: coisas }}>
          </div>
        </div>

      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalVisualizaInformacoes);
