import React from 'react';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';

/**
 */
export default class CadastroCidade extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_cidade;
  }

  // ------------------------------------------------------------------------------------

  getValidacoes(registro) {
    if (!registro.nome_cidade) {
      toastr.error('Atenção!', 'Digite o nome da cidade!', { attention: true });
      return false;
    }
    if (!Number(registro.cod_estado)) {
      toastr.error('Atenção!', 'Selecione o estado!', { attention: true });
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cidade: '',
      nome_cidade: '',
      cod_ibge: 0,
      cod_estado: '',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_cidade', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Cidade:', 'nome_cidade')}
          </div>
          <div className="col-sm-2">
            {componentes.inputNumber('IBGE:', 'cod_ibge', 0)}
          </div>
          <div className="col-sm-3">
            {componentes.upPanel('Estado:', 'cod_estado', 'tab_estado',
              'cod_estado', 'nome_estado')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_cidade"
        titulo="Cadastro de Cidade"
        subTitulo="Configure as suas Cidades!"
        renderForm={this.renderForm.bind(this)}
        paginavel
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getValidacoes={this.getValidacoes.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={1} width="80px" field="cod_cidade" header="Código" />
        <Column key={2} field="nome_cidade" header="Cidade" />
        <Column key={3} field="estado.nome_estado" header="Estado" />
        <Column key={4} field="cod_ibge" header="Cód. IBGE" />
      </Cadastro>
    );
  }

}
