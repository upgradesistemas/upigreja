import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import Api from '../../lib_frontend/main/Api.jsx';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup.jsx';

let nomeRelatorio;

class RelatorioFinanceiroCaixa extends React.Component {

  state = {
    dtInicio: '',
    dtFinal: '',
    recebeuData: false,
  }

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    await this.getdataMes();
  }

  setRelatorio(registro) {
    nomeRelatorio = registro.registro.separa_igrejas === 'S' ?
      'Relatorio Financeiro de Caixa' :
      'Relatorio Financeiro de Caixa Geral';
  }

  // ------------------------------------------------------------------------------------

  getNomeRelatorio() {
    return nomeRelatorio;
  }

  // ------------------------------------------------------------------------------------

  async getdataMes() {
    const data = await Api('dataMes');
    if (data.status) {
      await this.setState({
        dtInicio: moment(data.dados.Primeira).format('DD/MM/YYYY HH:mm'),
        dtFinal: moment(data.dados.Ultima).format('DD/MM/YYYY HH:mm'),
      });
    } else {
      console.log(data);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      dtInicio: '',
      dtFinal: '',
      separa_igrejas: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    if (!this.state.recebeuData && this.state.dtInicio) {
      registro.registro.dtInicio = this.state.dtInicio;
      registro.registro.dtFinal = this.state.dtFinal;
      this.setState({ recebeuData: true });
    }
    this.setRelatorio(registro);
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <br />
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Separar por Igrejas?', 'separa_igrejas', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}

        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-3">
              {componentes.inputDate('Data Inicial:', 'dtInicio')}
            </div>
            <div className="col-sm-3">
              {componentes.inputDate('Data Final:', 'dtFinal')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório Financeiro de Caixa' subTitulo='Relatórios!'
          getNomeRelatorio={this.getNomeRelatorio.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}

export default connect(state => ({
  usuario: state.usuario,
}))(RelatorioFinanceiroCaixa);
