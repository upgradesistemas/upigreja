import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup';
import RelatorioPadrao from './RelatorioPadrao.jsx';

let Igreja;

class RelatorioMembrosDizimistas extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      dtInicio: moment(new Date()).format('DD/MM/YYYY HH:mm'),
      dtFinal: moment(new Date()).format('DD/MM/YYYY HH:mm'),
      status_membro: 'T',
      cod_pessoa: 0,
      cod_ministerio: 0,
      cod_departamento: 0,
      status_mostra_valor: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    const reg = registro.registro;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">

          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Mostra Valor', 'status_mostra_valor', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>

          <div className="col-sm-10">
            <RadioGroup title="Filtrar Por" className="mt-10">
              <div className="row">
                <div className="col-sm-3 col-xs-4">
                  {componentes.radioButton('Todos', 'status_membro', 'T')}
                </div>
                <div className="col-sm-3 col-xs-4">
                  {componentes.radioButton('Membro', 'status_membro', 'M')}
                </div>
                <div className="col-sm-3 col-xs-4">
                  {componentes.radioButton('Ministério', 'status_membro', 'MIN')}
                </div>
                <div className="col-sm-3 col-xs-4">
                  {componentes.radioButton('Departamento', 'status_membro', 'DEP')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>

        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}

        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-3">
              {componentes.inputDate('Data Inicial:', 'dtInicio')}
            </div>
            <div className="col-sm-3">
              {componentes.inputDate('Data Final:', 'dtFinal')}
            </div>
            <div className="col-sm-6">
              {componentes.upPanel('Membro:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa',
                'nome_pessoa', 'status_membro = "M" AND cod_igreja = ' + Igreja,
                reg.status_membro !== 'M')}
            </div>
            <div className="col-sm-4">
              {componentes.upPanel('Ministério:', 'cod_ministerio', 'tab_ministerio',
                'cod_ministerio', 'nome_ministerio', '', reg.status_membro !== 'MIN')}
            </div>
            <div className="col-sm-4">
              {componentes.upPanel('Departamento:', 'cod_departamento', 'tab_departamento',
                'cod_departamento', 'nome_departamento', '', reg.status_membro !== 'DEP')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório de Membros x Dízimos' subTitulo='Relatórios!'
          nomeRelatorio='Relatorio de Membros Dizimistas'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}

export default connect(state => ({
  usuario: state.usuario,
}))(RelatorioMembrosDizimistas);
