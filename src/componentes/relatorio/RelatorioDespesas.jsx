import React from 'react';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup';

export default class RelatorioDespesas extends React.Component {

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      situacao: 'T',
      cod_tipo_despesa: 0,
      cod_pessoa: 0,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-12">
              <RadioGroup title="Filtrar Por" className="mt-10">
                <div className="row">
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Todos', 'situacao', 'T')}
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Em Aberto', 'situacao', 'A')}
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Fechado', 'situacao', 'F')}
                  </div>
                </div>
              </RadioGroup>
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-4">
              {componentes.upPanel('Filtrar por tipo de Despesa:', 'cod_tipo_despesa',
                'tab_tipo_despesa', 'cod_tipo_despesa', 'nome_despesa')}
            </div>
            <div className="col-sm-4">
              {componentes.upPanel('Filtrar por Pessoa:', 'cod_pessoa',
                'tab_pessoa', 'cod_pessoa', 'nome_pessoa')}
            </div>
            <div className="col-sm-2">
              {componentes.inputDate('Data Inicial:', 'dtInicio')}
            </div>
            <div className="col-sm-2">
              {componentes.inputDate('Data Final:', 'dtFinal')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório de Despesas' subTitulo='Relatórios!'
          nomeRelatorio='Relatorio de Despesas'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}
