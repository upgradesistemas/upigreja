import React from 'react';
import { connect } from 'react-redux';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup';
import RelatorioPadrao from './RelatorioPadrao.jsx';

let Igreja;

class Carteirinha extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.igrejaLogada.Igreja;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_pessoa: 0,
      status_membro: 'N', // significa todos os membros
    };
  }

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    const reg = registro.registro;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Trazer todos os Membros', 'status_membro', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>

        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}

        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-6">
              {componentes.upPanel('Membro:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa',
                'nome_pessoa', 'status_membro = "M" AND cod_igreja = ' + Igreja,
                reg.status_membro === 'S')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Carteirinha de Membro' subTitulo='Relatórios!'
          nomeRelatorio='Carteirinha'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}

export default connect(state => ({
  usuario: state.usuario,
}))(Carteirinha);
