import React from 'react';
import { connect } from 'react-redux';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup.jsx';
import Api from '../../lib_frontend/main/Api.jsx';

let IgrejaLogada;
let codigoCliente;

class RelCartas extends React.Component {

  constructor(props) {
    super(props);
    IgrejaLogada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      tp_carta: 'R',
      estilo_carta: 'I',
      igreja_destino: '',
      estado: 0,
      cidade: 0,
      pessoa1: 0,
      pessoa2: 0,
      presidente: '',
      obs: '',
      esta_comunhao: 'S',
      funcao_ass: '',
      periodo_membro: '',
      assinante: 0,
    };
  }

  // ------------------------------------------------------------------------------------

  async retornaPresidente(registro) {
    const presidente = await Api('retornaPresidente', codigoCliente, IgrejaLogada);
    if (presidente.status && presidente.dados && presidente.dados.nome_pessoa) {
      registro.registro.presidente = presidente.dados.nome_pessoa;
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    const reg = registro.registro;
    this.retornaPresidente(registro);
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Está em Comunhão ?', 'esta_comunhao', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
          <div className="col-sm-4">
            <RadioGroup title="Tipo de Carta" className="mt-10">
              <div className="row">
                <div className="col-sm-5 col-xs-5">
                  {componentes.radioButton('Recomendação', 'tp_carta', 'R')}
                </div>
                <div className="col-sm-3 col-xs-3">
                  {componentes.radioButton('Mudança', 'tp_carta', 'M')}
                </div>
              </div>
            </RadioGroup>
          </div>
          <div className="col-sm-5">
            <RadioGroup title="Estilo de Carta" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Individual', 'estilo_carta', 'I')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Família', 'estilo_carta', 'F')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('2 Pessoas', 'estilo_carta', 'P')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            {componentes.input('Igreja Destino:', 'igreja_destino')}
          </div>
          <div className="col-sm-2">
            {componentes.upPanel('Estado:', 'estado', 'tab_estado', 'cod_estado', 'sigla')}
          </div>
          <div className="col-sm-5">
            {componentes.upPanel('Cidade:', 'cidade', 'tab_cidade', 'cod_cidade',
              'nome_cidade_comp')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {componentes.upPanel('Membro:', 'pessoa1', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'status_membro = "M" AND cod_igreja = ' + IgrejaLogada)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Membro:', 'pessoa2', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'status_membro = "M" AND cod_igreja = ' + IgrejaLogada, reg.estilo_carta !== 'P')}
          </div>
          <div className="col-sm-4">
            {componentes.input('Observação:', 'obs')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
        {/* ------------------------- LINHA 4 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {componentes.input('Pastor Presidente:', 'presidente', true)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Assinante:', 'assinante', 'tab_pessoa', 'cod_pessoa',
              'nome_pessoa', 'status_membro = "M"')}
          </div>
          <div className="col-sm-4">
            {componentes.input('Função ou Cargo do Assinante:', 'funcao_ass')}
          </div>
          <div className="col-sm-4">
            {componentes.input('Período do membro na Igreja:', 'periodo_membro')}
          </div>
        </div>
        {/* ------------------------- LINHA 4 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Carta' subTitulo='Relatórios!'
          nomeRelatorio='Carta'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
}))(RelCartas);
