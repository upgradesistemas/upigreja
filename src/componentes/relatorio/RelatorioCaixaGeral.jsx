import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import RelatorioPadrao from './RelatorioPadrao.jsx';

let Igreja;

class RelatorioCaixaGeral extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      dtInicio: moment(new Date()).format('DD/MM/YYYY HH:mm'),
      dtFinal: moment(new Date()).format('DD/MM/YYYY HH:mm'),
    };
  }

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    const reg = registro.registro;
    return (
      <div>
        {/* ------------------------- LINHA 2 ------------------------- */}

        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-3">
              {componentes.inputDate('Data Inicial:', 'dtInicio')}
            </div>
            <div className="col-sm-3">
              {componentes.inputDate('Data Final:', 'dtFinal')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório Caixa Geral' subTitulo='Relatórios!'
          nomeRelatorio='Relatorio Financeiro de Caixa'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}

export default connect(state => ({
  usuario: state.usuario,
}))(RelatorioCaixaGeral);
