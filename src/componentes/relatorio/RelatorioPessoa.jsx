import React from 'react';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import ComboList from '../../lib_frontend/componentes/ComboList.jsx';

let nomeRelatorio;

export default class RelatorioPessoa extends React.Component {

  setRelatorio(state, registro) {
    nomeRelatorio = registro;
  }

  // ------------------------------------------------------------------------------------

  getNomeRelatorio() {
    return nomeRelatorio;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      status_ativo: 'T',
      status_fornecedor: 'T',
      status_membro: 'T',
      status_usuario: 'T',
    };
  }

  // ------------------------------------------------------------------------------------

  listaRelatorio = [
    { nome: 'Relatorio de Membro' },
    { nome: 'Relatorio de Fornecedor' },
    { nome: 'Relatorio de Usuário' },
  ];

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">

            <div className="col-sm-5">
              <ComboList value={this.listaRelatorio.valor}
                lista={this.listaRelatorio}
                campo="nome"
                label="Opções de Relatório:"
                valor="nome"
                // disabled={registro.cod_pessoa}
                defaultText="NENHUM"
                onChange={this.setRelatorio.bind(this, registro)} />
            </div>

          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">

            <div className="col-sm-5">
              <RadioGroup title="Status" className="mt-10">
                <div className="row">
                  <div className="col-sm-3 col-xs-4">
                    {componentes.radioButton('Todos', 'status_ativo', 'T')}
                  </div>
                  <div className="col-sm-3 col-xs-4">
                    {componentes.radioButton('Ativo', 'status_ativo', 'S')}
                  </div>
                  <div className="col-sm-3 col-xs-4">
                    {componentes.radioButton('Inativo', 'status_ativo', 'N')}
                  </div>
                </div>
              </RadioGroup>
            </div>

            <div className="col-sm-7">
              <RadioGroup title="Status" className="mt-10">
                <div className="row">
                  <div className="col-sm-3 col-xs-4">
                    {componentes.radioButton('Todos', 'status_fornecedor', 'T')}
                  </div>
                  <div className="col-sm-3 col-xs-4">
                    {componentes.radioButton('Fornecedor', 'status_fornecedor', 'S')}
                  </div>
                  <div className="col-sm-6 col-xs-4">
                    {componentes.radioButton('Não Fornecedor', 'status_fornecedor', 'N')}
                  </div>
                </div>
              </RadioGroup>
            </div>

          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">

            <div className="col-sm-12">
              <RadioGroup title="Membro / Visitante" className="mt-10">
                <div className="row">
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Todos', 'status_membro', 'T')}
                  </div>
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Nenhum', 'status_membro', 'N')}
                  </div>
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Membro', 'status_membro', 'M')}
                  </div>
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Visitante', 'status_membro', 'V')}
                  </div>
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Congregado', 'status_membro', 'C')}
                  </div>
                  <div className="col-sm-2 col-xs-4">
                    {componentes.radioButton('Simpatizante', 'status_membro', 'S')}
                  </div>
                </div>
              </RadioGroup>
            </div>

          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório de Pessoa' subTitulo='Relatórios!'
          getNomeRelatorio={this.getNomeRelatorio.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}
