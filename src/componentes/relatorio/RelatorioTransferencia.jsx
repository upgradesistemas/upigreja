import React from 'react';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup.jsx';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import ComboList from '../../lib_frontend/componentes/ComboList.jsx';

let nomeRelatorio;
let Igreja;

export default class RelatorioTransferencia extends React.Component {

  state = {
    valor: 0,
    codPessoa: '',
    status_transf: 'T',
  };

  // ------------------------------------------------------------------------------------

  setRelatorio(registro) {
    nomeRelatorio = registro;
  }

  // ------------------------------------------------------------------------------------

  getNomeRelatorio() {
    return nomeRelatorio;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      valor: '',
      cod_igreja: Igreja,
    };
  }

  // ------------------------------------------------------------------------------------

  listaMes = [
    { mes: 'Janeiro', valor: '1' },
    { mes: 'Fevereiro', valor: '2' },
    { mes: 'Março', valor: '3' },
    { mes: 'Abril', valor: '4' },
    { mes: 'Maio', valor: '5' },
    { mes: 'Junho', valor: '6' },
    { mes: 'Julho', valor: '7' },
    { mes: 'Agosto', valor: '8' },
    { mes: 'Setembro', valor: '9' },
    { mes: 'Outubro', valor: '10' },
    { mes: 'Novembro', valor: '11' },
    { mes: 'Dezembro', valor: '12' },
  ];

  // ------------------------------------------------------------------------------------

  listaAno = [
    { ano: '2019' },
    { ano: '2020' },
    { ano: '2021' },
    { ano: '2022' },
  ];

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">

            <div className="col-sm-12">
              <RadioGroup title="Filtrar por transferido" className="mt-10">
                <div className="row">
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Todos', 'status_transf', 'T')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Sim', 'status_transf', 'S')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Não', 'status_transf', 'N')}
                  </div>
                </div>
              </RadioGroup>
            </div>

            <div className="col-sm-3">
              <ComboList value={this.listaMes.valor}
                lista={this.listaMes}
                ref={e => this.comboListMes = e}
                campo="mes"
                label="Filtrar por Mês:"
                valor="valor"
                defaultText="Selecione..." />
            </div>

            <div className="col-sm-2">
              <ComboList value={this.listaAno.ano}
                lista={this.listaAno}
                ref={e => this.comboListAno = e}
                campo="ano"
                label="Filtrar por Ano:"
                valor="ano"
                defaultText="Selecione..." />
            </div>

            <div className="col-sm-5">
              {componentes.upPanel('Filtrar por Igreja:', 'cod_igreja', 'tab_igreja',
                'cod_igreja', 'nome_igreja')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório de Transferência' subTitulo='Relatórios!'
          getNomeRelatorio={this.getNomeRelatorio.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}
