import React from 'react';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import ComboList from '../../lib_frontend/componentes/ComboList.jsx';
import { RadioGroup } from '../../lib_frontend/componentes/RadioGroup';

export default class RelatorioAniversariantes extends React.Component {

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      valor: '',
      tp_aniversario: 'I', // "I" Idade e "C" de Casamento
    };
  }

  // ------------------------------------------------------------------------------------

  async setRelatorio(registro, valor) {
    if (registro.registro) {
      registro.registro.valor = valor;
    }
  }

  // ------------------------------------------------------------------------------------

  listaRelatorio = [
    { nome: 'Janeiro', valor: '01' },
    { nome: 'Fevereiro', valor: '02' },
    { nome: 'Março', valor: '03' },
    { nome: 'Abril', valor: '04' },
    { nome: 'Maio', valor: '05' },
    { nome: 'Junho', valor: '06' },
    { nome: 'Julho', valor: '07' },
    { nome: 'Agosto', valor: '08' },
    { nome: 'Setembro', valor: '09' },
    { nome: 'Outubro', valor: '10' },
    { nome: 'Novembro', valor: '11' },
    { nome: 'Dezembro', valor: '12' },
  ];

  // ------------------------------------------------------------------------------------

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-5">
            <RadioGroup title="Escolha um Tipo de Aniversário" className="mt-10">
                <div className="row">
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Idade', 'tp_aniversario', 'I')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Casamento', 'tp_aniversario', 'C')}
                  </div>
                </div>
              </RadioGroup>
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-5">
              <ComboList value={this.listaRelatorio.valor}
                lista={this.listaRelatorio}
                campo="nome"
                label="Escolha um Mês:"
                valor="valor"
                defaultText="NENHUM"
                onChange={this.setRelatorio.bind(this, registro)} />
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Relatório de Aniversariantes' subTitulo='Relatórios!'
          nomeRelatorio='Relatorio de Aniversariantes'
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}
