import React from 'react';
import RelatorioPadrao from './RelatorioPadrao.jsx';
import UpPanel from '../../lib_frontend/componentes/UpPanel.jsx';

let nomeRelatorio;
let Igreja;

export default class RelatorioDespesas extends React.Component {

  state = {
    valor: 0,
    codPessoa: '',
  };

  // ------------------------------------------------------------------------------------

  setRelatorio(registro) {
    nomeRelatorio = registro;
  }

  // ------------------------------------------------------------------------------------

  getNomeRelatorio() {
    return nomeRelatorio;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      valor: '',
    };
  }

  /**
 * Renderiza o formulário de inclusão/alteração.
 * @param {Object} registro O Registro sendo alterado/incluido.
 * @param {Object} componentes A Lista de componentes disponíveis.
 * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
 * @param {(label, campo, disable) => {}} componentes.input Input.
 * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
 * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
 * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
 * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
 * @param {(label, campo) => {}} componentes.inputTime Input de time.
 * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
 * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
 */
  renderForm(registro, componentes) {
    const where = 'status_fornecedor = "S" AND cod_igreja = ' + Igreja;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">
            <div className="col-sm-5">
              <UpPanel label="Fornecedor:"
                tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                where={where}
                ref={e => this.panelPessoa = e}
                value={this.state.codPessoa}
                onChange={e => this.setState({ codPessoa: e })} />
            </div>

            <div className="col-sm-2">
              {componentes.inputDate('Data Inicial:', 'dtInicio')}
            </div>

            <div className="col-sm-2">
              {componentes.inputDate('Data Final:', 'dtFinal')}
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <RelatorioPadrao titulo='Compra Detalhada' subTitulo='Relatórios!'
          getNomeRelatorio={this.getNomeRelatorio.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          renderForm={this.renderForm.bind(this)} />
      </div>
    );
  }
}
