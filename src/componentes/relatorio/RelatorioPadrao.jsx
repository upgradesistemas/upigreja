import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Api from '../../lib_frontend/main/Api.jsx';
import ContentWrapper from '../../lib_frontend/componentes/ContentWrapper.jsx';
import { Aba, PageControl } from '../../lib_frontend/componentes/Aba.jsx';
import Checkbox from '../../lib_frontend/componentes/Checkbox.jsx';
import LabeledInput from '../../lib_frontend/componentes/LabeledInput.jsx';
import UpPanel from '../../lib_frontend/componentes/UpPanel.jsx';
import InputDate from '../../lib_frontend/componentes/InputDate.jsx';
import InputTime from '../../lib_frontend/componentes/InputTime.jsx';
import InputCalc from '../../lib_frontend/componentes/InputCalc.jsx';
import { RadioButton } from '../../lib_frontend/componentes/RadioGroup.jsx';
import InputMask from '../../lib_frontend/componentes/InputMask.jsx';
import Button from '../../lib_frontend/componentes/Button.jsx';
import { trocarRota } from '../../utils/funcoesFront';

let Logada;
let codigoCliente;

class RelatorioPadrao extends React.Component {

  constructor(props) {
    super(props);
    Logada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
    this.state = {
      botaoDesabilitado: false,
      registro: this.props.getRegistro(),
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Handler de mudanças dos campos de formulário na alteração e inclusão,
   * todos os campos chamam esse método durante a alteração do valor.
   */
  setValorRegistro(nome, valor) {
    // if (this.props.onAlteracaoCampo) {
    //   valor = this.props.onAlteracaoCampo(this.state.registro, nome, valor);
    // }
    this.setState({
      registro: {
        ...this.state.registro,
        [nome]: valor,
      },
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna todos os componentes disponíveis do form.
   */
  getComponentesForm() {
    return {
      checkbox: this.renderCheckbox.bind(this),
      input: this.renderLabeledEdit.bind(this),
      inputCalc: this.renderInputCalc.bind(this),
      inputDate: this.renderInputDate.bind(this),
      inputMask: this.renderInputMask.bind(this),
      inputNumber: this.renderInputNumber.bind(this),
      inputTime: this.renderInputTime.bind(this),
      radioButton: this.renderRadioButton.bind(this),
      upPanel: this.renderUpPanel.bind(this),
    };
  }

  // ------------------------------------------------------------------------------------

  async gerarRelatorio() {
    let relatorio;
    if (this.props.getNomeRelatorio) {
      relatorio = this.props.getNomeRelatorio();
    } else {
      
      relatorio = this.props.nomeRelatorio;
    }
    const state = this.state.registro;
    // Desabilitar pra gerar:
    this.setState({ botaoDesabilitado: true });
    const ret = await Api('gerarRelatorio', {
      codigoCliente,
      nomeRelatorio: relatorio,
      state,
      IgrejaLogada: Logada,
    });
    if (ret.status) {
      const relat = window.location.origin + ret.dados;
      trocarRota(relat, '_blank');
      // GenericUtils.abrirPdfBase64(ret.dados, relatorio);
      toastr.success('Relatorio', 'O Relatório foi gerado com sucesso!');
    } else {
      toastr.error('Erro!', 'Relatório não encontrado', { attention: true });
    }
    this.setState({ botaoDesabilitado: false });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um checkbox.
   */
  renderCheckbox(text, campo, className) {
    return (
      <Checkbox text={text}
        className={className}
        checked={this.state.registro[campo] === 'S'}
        onChange={e => this.setValorRegistro(campo, e ? 'S' : 'N')} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label porém que só pode numeros.
   */
  renderInputNumber(label, campo, min, max) {
    let valor = String(this.state.registro[campo]);
    valor = min === 0 ? valor.replace('-', '') : valor;
    return (
      <LabeledInput label={label}
        value={valor}
        min={min}
        max={max}
        type="number"
        onChange={this.setValorRegistro.bind(this, campo)}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label
   */
  renderLabeledEdit(label, campo, disabled) {
    return (
      <LabeledInput label={label}
        value={this.state.registro[campo]}
        disabled={disabled}
        onChange={this.setValorRegistro.bind(this, campo)}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o componente padrão de registros da Upgrade.
   */
  renderUpPanel(label, campo, tabela, chave, desc, where, disabled) {
    return (
      <UpPanel label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        disabled={disabled}
        value={this.state.registro[campo]}
        onChange={e => this.setValorRegistro(campo, e)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputMask(label, campo, props) {
    return (
      <InputMask value={this.state.registro[campo]}
        label={label}
        {...props}
        onChange={e => this.setValorRegistro(campo, e.target.value)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza uma radioButton.
   */
  renderRadioButton(label, campo, valor) {
    return (
      <RadioButton name={campo}
        text={label}
        checked={this.state.registro[campo] === valor}
        onChange={() => this.setValorRegistro(campo, valor)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputDate(label, campo, disabled) {
    return (
      <InputDate value={this.state.registro[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        disabled={disabled} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita pontos flutuantes.
   */
  renderInputCalc(label, campo) {
    return (
      <InputCalc value={this.state.registro[campo]}
        label={label}
        onChange={this.setValorRegistro.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita horários.
   */
  renderInputTime(label, campo) {
    return (
      <InputTime value={this.state.registro[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)} />
    );
  }

  // ------------------------------------------------------------------------------------

  renderForm() {
    return (
      <React.Fragment>
        <PageControl>
          <Aba label="Filtros Relatório"
            icone="">
            <div className="relative">
              {/* Renderização do form: */}
              {this.props.renderForm(this.state, this.getComponentesForm())}

              <Button className="btn btn-success"
                icon="fa-print"
                label="Gerar Relatório"
                onClick={this.gerarRelatorio.bind(this)}
                disabled={this.state.botaoDesabilitado} />

            </div>
          </Aba>
        </PageControl>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div>
        <ContentWrapper title={this.props.titulo} small={this.props.subTitulo}>
          {this.renderForm()}
        </ContentWrapper>
      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
}))(RelatorioPadrao);
