import React from 'react';
import uuid from 'uuid/v1';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import InputSenha from '../lib_frontend/componentes/InputSenha.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import Api from '../lib_frontend/main/Api.jsx';

let Igreja;
let Pessoa;
let codigoCliente;

class ModalMudarSenhaFunc extends React.Component {

  constructor(props) {
    super(props);
    this.nomeAleatorioInput = uuid();
    Igreja = props.usuario.cod_igreja;
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    senha: '',
    codFuncionario: Pessoa,
    funcionarioDesabilitado: true,
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    this.setState({ codFuncionario: Pessoa });
    if (this.props.mostrarAutomaticamente) {
      if (!this.state.codFuncionario) {
        setTimeout(() => this.inputFuncionario.focus(), 500);
      } else {
        setTimeout(() => this.inputSenha.focus(), 500);
      }
    }
  }

  // ------------------------------------------------------------------------------------

  onSelecaoRegistroFunc() {
    if (this.inputSenha) {
      this.inputSenha.focus();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Boolean} se foi pode salvar ou não.
   */
  realizarValidacoes() {
    if (!this.state.codFuncionario) {
      toastr.error('Atenção!', 'Preencha o funcionário!', { attention: true });
      this.inputFuncionario.focus();
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Chamado quando o usuário clicar em confirmar.
   */
  async salvar() {
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      if (!this.realizarValidacoes()) {
        return;
      }

      const { codFuncionario, senha } = this.state;
      const ret = await Api('mudarSenhaFuncionario', codFuncionario, senha, codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      toastr.success('Tudo certo!', 'A senha do usuário foi alterada!');
      this.modal.hide();
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  show(codFuncionario) {
    this.setState({
      senha: '',
      codFuncionario: codFuncionario || '',
      funcionarioDesabilitado: codFuncionario,
    });
    this.modal.show();
    setTimeout(() => this.inputSenha.focus(), 500);
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Confirmar',
      icon: 'fa-check',
      onClick: this.salvar.bind(this),
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      dismiss: true,
    }];

    const where = 'status_usuario = "S" AND cod_igreja = ' + Igreja;

    return (
      <Modal {...this.props}
        title="Mudar senha do Usuário"
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="relative" ref={e => this.divPrincipal = e}>
          <div className="row p-10">
            <div className="col-sm-6">
              <UpPanel tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                label="Usuário:"
                where={where}
                disabled={this.state.funcionarioDesabilitado}
                value={this.state.codFuncionario}
                onChange={e => this.setState({ codFuncionario: e })}
                onSelecaoRegistro={this.onSelecaoRegistroFunc.bind(this)}
                ref={e => this.inputFuncionario = e} />
            </div>

            <div className="col-sm-6">
              <label htmlFor="novaSenha">Nova senha:</label>
              <InputSenha value={this.state.senha}
                onChange={e => this.setState({ senha: e })}
                name={this.nomeAleatorioInput}
                ref={e => this.inputSenha = e} />
            </div>
          </div>
        </div>

      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalMudarSenhaFunc);
