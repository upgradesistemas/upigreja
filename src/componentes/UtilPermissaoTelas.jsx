import React from 'react';
import { connect } from 'react-redux';
import dadosAplicacao from '../lib_frontend/main/dadosAplicacao.jsx';
import Checkbox from '../lib_frontend/componentes/Checkbox.jsx';

/**
 * Representa uma única permissão na treeview, ou seja, um checkbox.
 */
class Permissao extends React.Component {

  state = {
    checked: false, // Se essa permissão está marcada
    open: false, // Se essa permissão está aberta (filhos mostrando)
  };

  onChange() {
    if (this.props.onChange) {
      this.props.onChange();
    }
  }

  /**
   * Chamado quando um filho muda de estado (marcado ou não).
   * @param {Boolean} checked Se o filho está marcado ou não.
   */
  onClickFilho(checked, callback) {
    const filhoMarcado = this.props.rota.children.find(x => x.ref && x.ref.state.checked);
    // eslint-disable-next-line
    this.setState({ checked: filhoMarcado || checked ? true : false }, callback);
  }

  /**
   * Seta essa permissão como marcada ou não.
   * @param {Boolean} marcado 
   */
  set(marcado) {
    const { checked, open } = this.state;

    // Vamos tratar para que o checkbox só seja alterado
    // se ele tiver aberto e tenha filhos
    let valorVerdadeiro = marcado;
    if (!open && this.props.rota.children.length > 0) {
      valorVerdadeiro = checked;
    }
    
    this.setState({ checked: valorVerdadeiro, open: true }, () => {
      if (this.props.onClick) {
        this.props.onClick(marcado, () => {
          this.onChange();
        });
      } else {
        this.onChange();
      }
    });

    if (open || this.props.rota.children.length === 0) {
      // Marcamos o pai, vamos marcar todos os filhos:
      for (const child of this.props.rota.children) {
        child.ref.set(marcado);
      }
    }
  }
  
  render() {
    return (
      <div style={this.props.style}>
        <div className="permissao-container">
          <i className={'fa arrow-permissao ' + 
            (this.props.rota.children.length > 0 ? ' filhos ' : '') +
            (this.state.open ? 'fa-caret-down' : 'fa-caret-right')} 
            onClick={() => this.setState({ open: !this.state.open })} />
          <Checkbox text={this.props.rota.label}
                    checked={this.state.checked}
                    onChange={e => this.set(e)}
                    className="inline" />
        </div>

        {this.props.rota.children.map(child => (
          <Permissao rota={child} 
              style={{ 
                marginLeft: '10px',
                display: this.state.open ? '' : 'none',
              }} 
              onClick={this.onClickFilho.bind(this)} 
              onChange={this.onChange.bind(this)}
              ref={e => child.ref = e} />
        ))}
      </div>
    );
  }

}

/**
 * Componente de utilidade para mostrar as rotas da aplicação em um estilo
 * de treeview, permitindo marcar e desmarcar as rotas caso desejar.
 */
class UtilPermissaoTelas extends React.Component {

  constructor(props) {
    super(props);
    this.menus = dadosAplicacao.getMenus(props.redux);
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (this.props.value) {
      this.setarCampoPeloValue(this.props.value);
    }
  }

  // ------------------------------------------------------------------------------------

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      // Value foi setado ou re-setado, devemos atualizar os checkboxes:
      this.setarCampoPeloValue(nextProps.value);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Ativado quando qualquer checkbox de permissão mudar na árvore.
   */
  onChange() {
    if (!this.props.value) {
      return;
    }
    this.props.value.length = 0;
    for (const menu of this.menus) {
      this.setarValuePeloCampo(menu);
    }
  }

  // ------------------------------------------------------------------------------------

  setarCampoPeloValue(value = this.props.value) {
    const a = (menu, rotaPai = '') => {
      if (rotaPai) {
        rotaPai += '/';
      }
      const item = value.find(x => x.rota_tela === rotaPai + '' + menu.rota);
      if (item && menu.ref) {
        menu.ref.setState({ checked: item.status_habilita_tela === 'S' });
      } else if (menu.ref) {
        menu.ref.setState({ checked: false });
      }
      for (const child of menu.children) {
        a(child, menu.rota);
      }
    };
    for (const menu of this.menus) {
      a(menu);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta a propriedade do props.value de acordo com esse menu do parâmetro.
   */
  setarValuePeloCampo(menu, rotaPai = '') {
    if (rotaPai) {
      rotaPai += '/';
    }

    const item = this.props.value.find(x => x.rota_tela === rotaPai + '' + menu.rota);
    if (!item) {
      this.props.value.push({
        status_habilita_tela: menu.ref && menu.ref.state.checked ? 'S' : 'N',
        rota_tela: rotaPai + menu.rota,
      });
    } else {
      item.status_habilita_tela = menu.ref && menu.ref.state.checked ? 'S' : 'N';
      item.rota_tela = rotaPai + menu.rota;
    }

    for (const child of menu.children) {
      this.setarValuePeloCampo(child, menu.rota);
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        {this.menus.map(menu => (
          <Permissao key={menu.rota} rota={menu} 
            ref={e => menu.ref = e} 
            onChange={this.onChange.bind(this)} />
        ))}
      </div>
    );
  }

}

export default connect((state) => ({
  redux: state,
}))(UtilPermissaoTelas);
