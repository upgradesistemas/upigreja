import React from 'react';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';

/**
 * Cadastro padrão de distrito.
 */
export default class CadastroDistrito extends React.Component {
  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_distrito;
  }
  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_distrito: null,
      nome_distrito: '',
      lider_distrito: 0,
      cod_igreja: 0,
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_distrito', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Distrito:', 'nome_distrito')}
          </div>
          <div className="col-sm-5">
            {componentes.upPanel('Lider do Distrito:', 'lider_distrito', 'tab_pessoa',
              'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-7">
            {componentes.upPanel('Igreja:', 'cod_igreja', 'tab_igreja',
              'cod_igreja', 'nome_igreja')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_distrito"
        titulo="Cadastro de Distrito"
        subTitulo="Configure os seus Distritos !"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={2} field="nome_distrito" header="Distrito" />
        <Column key={2} field="pessoa.nome_pessoa" header="Líder do Distrito" />
        <Column key={2} field="igreja.nome_igreja" header="Igreja" />
      </Cadastro>
    );
  }

}

