import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import Api from '../lib_frontend/main/Api.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { Table, Column } from '../lib_frontend/componentes/Table.jsx';
import { Aba, PageControl } from '../lib_frontend/componentes/Aba.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import InputDate from '../lib_frontend/componentes/InputDate.jsx';
import ComboList from '../lib_frontend/componentes/ComboList.jsx';
import ModalCaixa from './ModalCaixa.jsx';

let Pessoa;
let IgrejaSede;
let Caixa;
let codigoCliente;

class MovTransferencia extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    IgrejaSede = props.usuario.cod_igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    registros: [],
    itemSelecionado: null,
    mes: '',
    ano: '',
    atualizou: false,
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  async setValorRegistro(nome, valor) {
    await this.setState({
      ...this.state.registro,
      [nome]: valor,
      atualizou: false,
    });
    this.buscarFechamento();
  }

  // ------------------------------------------------------------------------------------

  async gerarFechamento() {
    const ret = await Api('gerarFechamento', this.state, codigoCliente, Pessoa);
    if (ret.status) {
      this.buscarFechamento();
    } else {
      console.log(ret.erro);
      toastr.error('Erro!', ret.erro, { attention: true });
    }
  }

  // ------------------------------------------------------------------------------------

  async buscarFechamento() {
    if ((!this.state.mes || !this.state.ano) ||
      (this.state.mes === '0' || this.state.ano === '0')) {
      return;
    }

    const ret = await Api('buscaTransfereciaIgreja', this.state, codigoCliente);
    if (ret.status) {
      this.setState({
        atualizou: true,
        registros: ret.dados,
      });
    } else {
      console.log(ret.erro);
      toastr.error('Erro!', ret.erro, { attention: true });
    }
  }

  // ------------------------------------------------------------------------------------

  async transferir(igreja) {
    if (igreja.status_transferido === 'N') {
      const ret =
        await Api('transferirIgrejaSede', codigoCliente, IgrejaSede, igreja, Pessoa, Caixa);
      if (ret.status) {
        this.setState({ itemSelecionado: null });
        this.buscarFechamento();
      } else {
        console.log(ret.erro);
        toastr.error('Erro!', ret.erro, { attention: true });
      }
    }
  }

  // ------------------------------------------------------------------------------------

  async transferirTodos(registros) {
    for (const igreja of registros) {
      this.transferir(igreja);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o componente padrão de registros da Upgrade.
   */
  upPanel(label, campo, tabela, chave, desc, where) {
    return (
      <UpPanel label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        value={this.state[campo]}
        onChange={e => this.setValorRegistro(campo, e)} />
      // ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  inputDate(label, campo, disabled) {
    return (
      <InputDate value={this.state[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        disabled={disabled} />
      // ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  listaMes = [
    { mes: 'Janeiro', valor: '1' },
    { mes: 'Fevereiro', valor: '2' },
    { mes: 'Março', valor: '3' },
    { mes: 'Abril', valor: '4' },
    { mes: 'Maio', valor: '5' },
    { mes: 'Junho', valor: '6' },
    { mes: 'Julho', valor: '7' },
    { mes: 'Agosto', valor: '8' },
    { mes: 'Setembro', valor: '9' },
    { mes: 'Outubro', valor: '10' },
    { mes: 'Novembro', valor: '11' },
    { mes: 'Dezembro', valor: '12' },
  ];

  // ------------------------------------------------------------------------------------

  listaAno = [
    { ano: '2019' },
    { ano: '2020' },
    { ano: '2021' },
    { ano: '2022' },
  ];

  // ------------------------------------------------------------------------------------

  renderCampos() {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            <ComboList value={this.listaMes.valor}
              lista={this.listaMes}
              ref={e => this.comboListMes = e}
              campo="mes"
              label="Escolha um Mês:"
              valor="valor"
              defaultText="Selecione..."
              onChange={e => (this.setValorRegistro(this.comboListMes.props.campo, e))} />
          </div>
          <div className="col-sm-3">
            <ComboList value={this.listaAno.ano}
              lista={this.listaAno}
              ref={e => this.comboListAno = e}
              campo="ano"
              label="Escolha um Ano:"
              valor="ano"
              defaultText="Selecione..."
              onChange={e => (this.setValorRegistro(this.comboListAno.props.campo, e))} />
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderForm() {
    const disabled = (!this.state.mes || !this.state.ano) ||
      (this.state.mes === '0' || this.state.ano === '0');

    const actions = [{
      className: 'btn-primary',
      icone: 'fa-refresh',
      label: 'Atualizar Lista',
      disabled: disabled,
      onClick: this.buscarFechamento.bind(this),
    }, {
      className: 'btn-danger',
      icone: 'fa-exchange',
      label: 'Gerar Fechamento',
      disabled: disabled || (!this.state.atualizou) ||
        (this.state.registros.length > 0),
      onClick: this.gerarFechamento.bind(this),
    }, {
      className: 'cad-acao-separador',
    }, {
      className: 'btn-success',
      icone: 'fa-play',
      label: 'Transferir',
      disabled: !this.state.itemSelecionado ||
        this.state.itemSelecionado.status_transferido === 'S',
      onClick: this.transferir.bind(this, this.state.itemSelecionado),
    }, {
      className: 'btn-success',
      icone: 'fa-forward',
      label: 'Transferir Todos',
      disabled: this.state.itemSelecionado || disabled,
      onClick: this.transferirTodos.bind(this, this.state.registros),
    }];

    return (
      <React.Fragment>
        <PageControl>
          <Aba label="- Transferência"
            icone="usd">
            <div className="cadastro-table-container">
              {this.renderCampos()}
              <div>
                <Table className="h-100"
                  header="Registros"
                  ref={e => this.table = e}
                  data={this.state.registros}

                  selectable
                  selectedItem={this.state.itemSelecionado}
                  onSelectItem={e => this.setState({ itemSelecionado: e })}
                  actions={actions} >

                  <Column key={2} field="nome_igreja" header="Igreja" />
                  <Column key={3} field="porc" header="Porcentagem" />
                  <Column key={4} field="valor" header="Valor a Transferir" />
                  <Column key={4} field="status" header="Transferido" />
                </Table>
              </div>
            </div>
          </Aba>
        </PageControl>
        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </React.Fragment>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="cadastro movimentacao h-100">
        <ContentWrapper title="Transferência"
          small="">
          {this.renderForm()}
        </ContentWrapper>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(MovTransferencia);
