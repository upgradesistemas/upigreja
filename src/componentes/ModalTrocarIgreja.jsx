import React from 'react';
import uuid from 'uuid/v1';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import { Table, Column } from '../lib_frontend/componentes/Table.jsx';
import Api from '../lib_frontend/main/Api.jsx';

let Pessoa;
let codigoCliente;

class ModalTrocarIgreja extends React.Component {

  constructor(props) {
    super(props);
    this.nomeAleatorioInput = uuid();
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  state = {
    igrejas: {},
    cod_igreja: '',
  }

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    const ret = await Api('buscarDados', 'Generico', { tabela: 'tab_igreja', codigoCliente });

    if (!ret.status) {
      toastr.error('Atenção!', 'Erro ao encontrar os dados!', { attention: true });
    }

    this.setState({
      igrejas: ret.dados,
    });
    setTimeout(() => this.upPanel.focus(), 900);
  }

  // -------------------------------------------------------------------------------------

  async trocarIgreja() {
    if (!this.state.cod_igreja) {
      toastr.error('Atenção!', 'Escolha uma igreja para trocar', { attention: true });
      return false;
    }
    const ret = await Api('buscarDados', 'TrocarIgreja',
      { codIgreja: this.state.cod_igreja, codPessoa: Pessoa, codigoCliente });
    if (!ret.status) {
      toastr.error('Atenção!', ret.erro, { attention: true });
      return false;
    }

    toastr.success('Tudo certo!', 'Igreja Trocada com Sucesso!');
    this.modal.hide();
    window.location.reload();
  }

  // -------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Confirmar',
      icon: 'fa-check',
      onClick: this.trocarIgreja.bind(this),
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      dismiss: true,
    }];

    return (
      <Modal {...this.props}
        title="Trocar Igreja"
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="row p-10">
          <div className="col-sm-12">
            <UpPanel label="Igrejas:"
              tabela="tab_igreja"
              chave="cod_igreja"
              desc="nome_igreja"
              value={this.state.cod_igreja}
              ref={e => this.upPanel = e}
              onChange={e => this.setState({ cod_igreja: e })} />
          </div>
        </div>

        <div className="relative" ref={e => this.divPrincipal = e}>
          <div className="row p-10">
            <div className="col-sm-12">
              <Table header="Igrejas Filiadas"
                ref={e => this.table = e}
                data={this.state.igrejas}
                actions={{}}>

                <Column key={1} width="80px" field="cod_igreja" header="Código" />
                <Column key={2} field="nome_igreja" header="Nome da Igreja" />
              </Table>
            </div>
          </div>
        </div>

      </Modal>
    );
  }

}

export default connect((state) => ({
  config: state.config,
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalTrocarIgreja);

