import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { Table, Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import Cadastro from './Cadastro';
import Api from '../lib_frontend/main/Api';
import { PageControl, Aba } from '../lib_frontend/componentes/Aba';
import { UpBlocoValores } from '../lib_frontend/componentes/UpBlocoValores.jsx';
import { formataDinheiro } from '../utils/funcoesFront';
import tipo from '../tipo';
import ModalCaixa from './ModalCaixa.jsx';

let Pessoa;
let Igreja;
let Logada;
let codigoCliente;
let Caixa;

/**
 */
class MovCantina extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    Igreja = props.usuario.cod_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    produtoPesquisado: 0,
  };

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  onExitCampo(registro, campo, valor) {
    if (campo === 'cod_produto' && valor > 0 && valor !== this.state.produtoPesquisado) {
      this.pesquisaProduto(registro, valor);
    }

    if (campo === 'qtde_parcela') {
      if (Number(valor) > 100) {
        valor = 100;
      } else if (Number(valor) < 0) {
        valor = 0;
      }
    }

    if (campo === 'vr_total' || campo === 'intervalo' ||
      campo === 'dt_hr_prim_parcela' || campo === 'qtde_parcela' ||
      campo === 'vr_total_acrescimo' || campo === 'vr_total_desconto') {
      this.gerarParcelas(registro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    return valor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    this.setState({ aba: 'abaDados' });
    return {
      // CANTINA
      cod_cantina: null,
      cod_pessoa: 0,
      cod_igreja: Igreja,
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_cantina: '',
      dt_hr_fechamento: '',
      vr_total: 0,
      vr_total_acrescimo: 0,
      vr_total_desconto: 0,
      obs_cantina: '',
      obs_pagamento: '',
      cod_usuario_cantina: Pessoa,
      status_cantina: 'A',
      nr_documento: '',
      itens: [],
      parcelas: [],
      // PRODUTOS
      cod_produto: 0,
      nome_produto: '',
      qtde_item: 0,
      vr_item: 0,
      vr_item_formatado: 0,
      vr_total_item: 0,
      vr_total_item_formatado: 0,
      vr_acrescimo_item: 0,
      vr_desconto_item: 0,
      obs_item: '',
      // PARCELAS
      intervalo: 30,
      qtde_parcela: 0,
      frm_pagamento: 'A',
      dt_hr_prim_parcela: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
    };
  }

  // ------------------------------------------------------------------------------------

  async gerarParcelas(registro) {
    const { dt_hr_prim_parcela, intervalo, qtde_parcela } = registro;
    registro.parcelas = [];
    const dtInicioDate = moment(dt_hr_prim_parcela, 'DD/MM/YYYY').toDate();
    for (let i = 0; i < qtde_parcela; i++) {
      await registro.parcelas.push(this.criarParcela(i, registro, qtde_parcela));
      dtInicioDate.setDate(dtInicioDate.getDate() + Number(intervalo));
    }
    this.setState({});
  }

  // ------------------------------------------------------------------------------------

  criarParcela(indice, registro) {
    const { dt_hr_prim_parcela, intervalo, vr_total, qtde_parcela } = registro;
    const valorPorParcela = (vr_total / qtde_parcela).toFixed(2); // toFixed - Qtde Casas decimais
    const dtInicioDate = moment(dt_hr_prim_parcela, 'DD/MM/YYYY').toDate();
    dtInicioDate.setDate(dtInicioDate.getDate() + (Number(intervalo) * indice));
    return {
      numero: indice + 1,
      valor: valorPorParcela,
      valor_formatado: formataDinheiro(valorPorParcela),
      data: moment(dtInicioDate).format('DD/MM/YYYY'),
      cod_igreja: Igreja,
    };
  }

  // ------------------------------------------------------------------------------------

  async pesquisaProduto(registro, codProduto) {
    const prod = await Api('buscarDados', 'GenericoUnico', {
      codigo: codProduto,
      nomePk: 'cod_produto',
      tabela: 'tab_produto',
      codigoCliente,
    });
    if (prod.status) {
      registro.nome_produto = prod.dados.nome_produto;
      registro.vr_item = prod.dados.valor;
      await this.setState({ produtoPesquisado: codProduto });
    } else {
      console.log(prod.erro);
    }
  }

  // ------------------------------------------------------------------------------------

  async excluirProduto(registro) {
    const index = registro.itens.findIndex(x => x.cod_produto === registro.cod_produto);
    await registro.itens.splice(index, 1);
    await this.limpaProduto(registro);
  }

  // ------------------------------------------------------------------------------------

  async addProduto(registro) {
    let produtoAtual = {};

    const produto = registro.itens.find(x => registro.cod_produto === x.cod_produto);
    if (produto) { // Se foi encontrado o produto na lista, edita-o
      produto.qtde_item = registro.qtde_item;
      produto.vr_item = registro.vr_item;
      produto.vr_item_formatado = formataDinheiro(produto.vr_item);
      produto.vr_acrescimo_item = registro.vr_acrescimo_item;
      produto.vr_desconto_item = registro.vr_desconto_item;
      produto.vr_total_item = ((registro.qtde_item * registro.vr_item) +
        produto.vr_acrescimo_item) - produto.vr_desconto_item;
      produto.vr_total_item_formatado = formataDinheiro(produto.vr_total_item);
      produto.obs_item = registro.obs_item;
    } else { // Se não foi, adiciona-o
      const total_item = ((registro.qtde_item * registro.vr_item) +
        registro.vr_acrescimo_item) - registro.vr_desconto_item;
      produtoAtual = await {
        cod_cantina: registro.cod_cantina,
        cod_produto: registro.cod_produto,
        nome_produto: registro.nome_produto,
        qtde_item: registro.qtde_item,
        vr_item: registro.vr_item,
        vr_item_formatado: formataDinheiro(registro.vr_item),
        vr_total_item: total_item,
        vr_total_item_formatado: formataDinheiro(total_item),
        vr_acrescimo_item: registro.vr_acrescimo_item,
        vr_desconto_item: registro.vr_desconto_item,
        obs_item: registro.obs_item,
      };
      await registro.itens.push(produtoAtual);
    }

    await this.limpaProduto(registro);
  }

  // ------------------------------------------------------------------------------------

  async fecharCantina(registro) {
    if (!registro.parcelas) {
      toastr.error('Erro!', 'Você ainda não gerou as parcelas para fazer o fechamento!',
        { attention: true });
      this.cadastro.getWrappedInstance().focarCampo('qtde_parcela');
      return false;
    }
    registro.fecharCantina = true;
    this.cadastro.getWrappedInstance().salvar();
  }

  // ------------------------------------------------------------------------------------

  async abrirCantina(registro) {
    const cantina = await Api('abrirRegistro', codigoCliente, registro, tipo.movimento.cantina);
    if (cantina.status) {
      toastr.success('Sucesso!', `Cantina ${registro.cod_cantina} aberta com sucesso!`);
      this.cadastro.getWrappedInstance().setPagina(1);
    } else {
      toastr.error('Erro!', `Erro ao abrir a cantina ${registro.cod_cantina}!`,
        { attention: true });
      console.log(cantina.erro);
    }
  }

  // ------------------------------------------------------------------------------------

  async limpaProduto(registro) {
    registro.cod_produto = 0;
    registro.nome_produto = '';
    registro.qtde_item = 0;
    registro.vr_item = 0;
    registro.vr_item_formatado = 0;
    registro.vr_total_item = 0;
    registro.vr_total_item_formatado = 0;
    registro.vr_acrescimo_item = 0;
    registro.vr_desconto_item = 0;
    registro.obs_item = '';
    await this.setState({});
  }

  // ------------------------------------------------------------------------------------

  async calculaValorTotal(registro) {
    if (registro.itens) {
      registro.vr_total = 0;
      for (const item of registro.itens) {
        registro.vr_total += item.vr_total_item;
      }
      registro.vr_total += registro.vr_total_acrescimo - registro.vr_total_desconto;
    }
  }

  // ------------------------------------------------------------------------------------

  renderTable(registro) {
    const actions = [{
      className: 'btn-success',
      icone: 'fa-plus',
      label: 'Adicionar',
      onClick: this.addProduto.bind(this, registro),
      disabled: (!registro.cod_produto) || (registro.cod_produto === '0'),
    }, {
      className: 'btn-danger',
      icone: 'fa-trash',
      label: 'Exluir Produto',
      onClick: this.excluirProduto.bind(this, registro),
      disabled: (!registro.cod_produto) || (registro.cod_produto === '0'),
    }];

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-12">
            <Table className="h-100"
              header="Registros"
              ref={e => this.tableProduto = e}
              data={registro.itens}
              actions={actions} >

              <Column key={1} field="nome_produto" header="Nome do Produto" />
              <Column key={2} field="qtde_item" header="Quantidade" />
              <Column key={3} field="vr_item_formatado" header="Valor por Produto" />
              <Column key={4} field="vr_total_item_formatado" header="Sub-Total(+)" />
              <Column key={5} field="obs_item" header="Obs" />
            </Table>
          </div>
        </div>
      </React.Fragment>
    );
  }

  // ------------------------------------------------------------------------------------

  renderTablePag(registro) {
    const actions = [{
      className: 'btn-success',
      icone: 'fa-save',
      label: 'Fechar Cantina',
      onClick: this.fecharCantina.bind(this, registro),
      disabled: (registro.vr_total === '0'),
    }];
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-12">
            <Table className="h-70"
              header="Registros"
              ref={e => this.tablePagamento = e}
              data={registro.parcelas}
              actions={actions} >

              <Column key={3} field="valor_formatado" header="Valor da Parcela" />
              <Column key={3} field="data" header="Data da Parcela" />
              <Column key={3} field="numero" header="Número da Parcela" />
            </Table>
          </div>
        </div>
      </React.Fragment>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    this.calculaValorTotal(registro);
    return (
      <div>
        <PageControl aba={this.state.aba} onChange={e => this.setState({ aba: e })}
          ref={f => this.formulario = f}>
          <Aba key="abaDados" icone="book" label="Dados da Cantina">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.input('Código:', 'cod_cantina', true)}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Data da Cantina:', 'dt_hr_cantina')}
              </div>
              <div className="col-sm-5">
                {componentes.upPanel('Pessoa:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa',
                  'nome_pessoa', 'status_fornecedor = "N" AND cod_igreja = ' + Igreja)}
              </div>
              <div className="col-sm-3">
                {componentes.input('N.º Nota Fiscal/Doc:', 'nr_documento')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}
            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-12">
                {componentes.input('Observação:', 'obs_cantina')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}
          </Aba>

          <Aba key="abaProduto" icone="book" label="Dados do Produto">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-5">
                {componentes.upPanel('Produto:', 'cod_produto', 'tab_produto',
                  'cod_produto', 'nome_produto')}
              </div>
              <div className="col-sm-1">
                {componentes.inputNumber('Quantidade:', 'qtde_item', 0)}
              </div>
              <div className="col-sm-2">
                {componentes.inputCalc('Desconto:', 'vr_desconto_item')}
              </div>
              <div className="col-sm-2">
                {componentes.inputCalc('Acrescimo:', 'vr_acrescimo_item')}
              </div>
              <div className="col-sm-2">
                {componentes.inputCalc('Valor:', 'vr_item')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}
            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-12">
                {componentes.input('Observação do Item:', 'obs_item')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {this.renderTable(registro)}
          </Aba>

          <Aba key="abaPagamento" icone="book" label="Dados de Pagamento">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="row col-sm-12">
                <div className="col-sm-5">
                  <RadioGroup title="Forma de Pagamento" className="mt-10">
                    <div className="row">
                      <div className="col-sm-5 col-xs-2">
                        {componentes.radioButton('À Vista', 'frm_pagamento', 'A')}
                      </div>
                      <div className="col-sm-5 col-xs-2">
                        {componentes.radioButton('À Prazo', 'frm_pagamento', 'P')}
                      </div>
                    </div>
                  </RadioGroup>
                </div>
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}
            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Tipo de Pagamento:', 'cod_forma_pagamento',
                  'tab_forma_pagamento', 'cod_forma_pagamento', 'nome_forma_pagamento')}
              </div>
              <div className="col-sm-2">
                {componentes.inputCalc('Desconto:', 'vr_total_desconto')}
              </div>
              <div className="col-sm-2">
                {componentes.inputCalc('Acréscimo:', 'vr_total_acrescimo')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Data de Vencimento:', 'dt_hr_prim_parcela')}
              </div>
              <div className="col-sm-1">
                {componentes.inputNumber('Intervalo:', 'intervalo', 0)}
              </div>
              <div className="col-sm-1">
                {componentes.inputNumber('Parcelas:', 'qtde_parcela', 0)}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}
            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-12">
                {componentes.input('Observação do Pagamento:', 'obs_pagamento')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {this.renderTablePag(registro)}
          </Aba>
          {/* ------------------------- LINHA 3 ------------------------- */}
        </PageControl>

        <div className="row">
          <UpBlocoValores cor="blue" titulo="Valor Total" icone="money" >
            {formataDinheiro(registro.vr_total)}
          </UpBlocoValores>
        </div>
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const actions = [{
      className: 'btn-warning',
      icone: 'fa-unlock',
      label: 'Abrir Cantina',
      onClick: (cantina) => this.abrirCantina(cantina),
      disabled: (cantina) => !cantina || cantina.status_cantina === 'A',
    }];
    return (
      <div>
        <Cadastro tabela="tab_cantina"
          titulo="Cantinas"
          ref={e => this.cadastro = e}
          where={'cod_igreja = ' + Logada}
          renderForm={this.renderForm.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          onExitCampo={this.onExitCampo.bind(this)}
          editarDesabilitado={(cantina) => cantina.status_cantina === 'F'}
          excluirDesabilitado={(cantina) => cantina.status_cantina === 'F'}
          onAlteracaoCampo={this.onAlteracaoCampo.bind(this)}
          actions={actions}
          paginavel >
          <Column key={1} width="80px" field="cod_cantina" header="Código" />
          <Column key={2} field="pessoa" header="Pessoa" />
          <Column key={3} width="150px" field="vr_total_formatado" header="Valor da Cantina" />
          <Column key={4} width="150px" field="dt_hr_cantina" header="Data da Cantina" />
          <Column key={5} width="150px" field="status" header="Status" />
        </Cadastro>

        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }

}

export default connect(state => ({
  usuario: state.usuario,
}))(MovCantina);
