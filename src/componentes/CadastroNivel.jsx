import React from 'react';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import Cadastro from './Cadastro';

/**
 */
export default class CadastroNivel extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_nivel;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_nivel: null,
      nome_nivel: '',
      status_ativo: 'S',
      permite_permissao_tela: 'N',
      dash_ve_tudo: 'N',
      dash_ve_pessoas_campo: 'N',
      dash_ve_igrejas_cadastradas: 'N',
      dash_ve_pessoas_local: 'N',
      dash_ve_usuarios_registrados: 'N',
      dash_ve_valores_futuros: 'N',
      dash_ve_valor_caixa_atual: 'N',
      dash_ve_aniversariantes: 'N',
      dash_ve_eventos: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código', 'cod_nivel', true)}
          </div>
          <div className="col-sm-6">
            {componentes.input('Nivel:', 'nome_nivel')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-12">
            <RadioGroup title="Opções Cadastro Pessoa" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Habilita Permissão Tela',
                    'permite_permissao_tela', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
        {/* ------------------------- LINHA 4 ------------------------- */}
        <div className="row">
          <div className="col-sm-12">
            <RadioGroup title="Opções Para Dashboard" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Vê Tudo', 'dash_ve_tudo', 'inline mr-10')}
                  {componentes.checkbox('Vê Pessoas do Campo',
                    'dash_ve_pessoas_campo', 'inline mr-10')}
                  {componentes.checkbox('Vê Pessoas do Local',
                    'dash_ve_pessoas_local', 'inline mr-10')}
                  {componentes.checkbox('Vê Igrejas Cadastradas',
                    'dash_ve_igrejas_cadastradas', 'inline mr-10')}
                  {componentes.checkbox('Vê Usuários Registrados',
                    'dash_ve_usuarios_registrados', 'inline mr-10')}
                  {componentes.checkbox('Vê Valores Futuros',
                    'dash_ve_valores_futuros', 'inline mr-10')}
                  {componentes.checkbox('Vê Valor Caixa Atual',
                    'dash_ve_valor_caixa_atual', 'inline mr-10')}
                  {componentes.checkbox('Vê Aniversariantes',
                    'dash_ve_aniversariantes', 'inline mr-10')}
                  {componentes.checkbox('Vê Eventos',
                    'dash_ve_eventos', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 4 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_nivel"
        titulo="Cadastro de Nível"
        subTitulo="Configure os seus Níveis!"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={1} width="80px" field="cod_nivel" header="Código" />
        <Column key={2} field="nome_nivel" header="Descrição" />
      </Cadastro>
    );
  }

}

