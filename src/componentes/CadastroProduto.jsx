import React from 'react';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';

let Igreja;
// let codigoCliente;

class CadastroProduto extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
    // codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_produto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_produto: null,
      cod_igreja: Igreja,
      nome_produto: '',
      valor: 0,
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_produto', true)}
          </div>
          <div className="col-sm-4">
            {componentes.input('Produto:', 'nome_produto')}
          </div>
          <div className="col-sm-2">
            {componentes.inputCalc('Valor do Produto:', 'valor')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_produto"
        titulo="Cadastro de produto"
        subTitulo="Configure os seus produtos!"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={1} width="80px" field="cod_produto" header="Código" />
        <Column key={2} field="nome_produto" header="Produto" />
        <Column key={3} field="valor" header="Valor do Produto" />
        {/* <Column key={4} field="cod_igreja" header="Igreja Destino" /> */}
      </Cadastro>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
  redux: state,
}))(CadastroProduto);
