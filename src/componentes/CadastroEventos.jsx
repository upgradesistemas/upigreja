import React from 'react';
import moment from 'moment';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import Cadastro from './Cadastro';

let Igreja;
let Logada;

class CadastroEventos extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_evento;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_evento: null,
      cod_igreja: Igreja,
      nome_evento: '',
      dth_inicio: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dth_fim: null,
      cod_cidade: 0,
      local_cod_igreja: Igreja,
      local_evento: '',
      obs_evento: '',
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  getValidacoes(registro) {
    const local = registro.local_evento !== '';
    const local_igreja = registro.local_cod_igreja !== 0;
    if (!local && !local_igreja) {
      toastr.error('Erro!',
        'É necessário ter algum local informado!', { attention: true });
      return false;
    }
    return true;
  }
  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_evento', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Nome do Evento:', 'nome_evento')}
          </div>
          <div className="col-sm-5">
            {componentes.upPanel('Igreja:', 'local_cod_igreja', 'tab_igreja',
              'cod_igreja', 'nome_igreja')}
          </div>
        </div>

        <div className="row">
          <div className="col-sm-4">
            {componentes.input('Local:', 'local_evento')}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Cidade:', 'cod_cidade', 'tab_cidade',
              'cod_cidade', 'nome_cidade_comp')}
          </div>
          <div className="col-sm-2">
            {componentes.inputDate('Data de Início:', 'dth_inicio')}
          </div>
          <div className="col-sm-2">
            {componentes.inputDate('Data Final:', 'dth_fim')}
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12">
            {componentes.input('Observação do Evento:', 'obs_evento')}
          </div>
        </div>
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <Cadastro tabela="tab_evento"
          titulo="Eventos"
          paginavel
          where={'cod_igreja = ' + Logada}
          renderForm={this.renderForm.bind(this)}
          getNomeRegistro={this.getNomeRegistro.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          getValidacoes={this.getValidacoes.bind(this)} >
          <Column key={1} width="80px" field="cod_evento" header="Código" />
          <Column key={2} field="nome_evento" header="Nome do Evento" />
          <Column key={3} field="local" header="Local do Evento" />
          <Column key={4} width="100px" field="dth_inicio" header="Data Inicial" />
          <Column key={5} width="100px" field="dth_fim" header="Data Final" />
        </Cadastro>
      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
}))(CadastroEventos);
