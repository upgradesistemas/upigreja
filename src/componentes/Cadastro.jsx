import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import Button from '../lib_frontend/componentes/Button.jsx';
import InputGroup from '../lib_frontend/componentes/InputGroup.jsx';
import { Table } from '../lib_frontend/componentes/Table.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { PageControl, Aba } from '../lib_frontend/componentes/Aba';
import Checkbox from '../lib_frontend/componentes/Checkbox.jsx';
import LabeledInput from '../lib_frontend/componentes/LabeledInput.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import InputDate from '../lib_frontend/componentes/InputDate.jsx';
import InputTime from '../lib_frontend/componentes/InputTime.jsx';
import InputCalc from '../lib_frontend/componentes/InputCalc.jsx';
import { RadioButton } from '../lib_frontend/componentes/RadioGroup.jsx';
import InputMask from '../lib_frontend/componentes/InputMask.jsx';
import ModalConfigCampos from '../lib_frontend/componentes/ModalConfigCampos.jsx';
import ModalConfirmacao from '../lib_frontend/componentes/ModalConfirmacao.jsx';
import tipo from '../tipo';
import { trocarRota } from '../utils/funcoesFront';

let idSequencial = 0;
let qtdeLinhas = 0;
let Pessoa;
let codigoCliente;

/**
 * ୧༼ಠ益ಠ༽︻╦╤─ ----- ATENÇÃO SOLDADOS ----- ୧༼ಠ益ಠ༽︻╦╤─
 * 
 * Componente de cadastro padrão que é utilizado em muitos lugares de muitos sistemas,
 * então cuidado ao mexer nisso, pelo amor de Deus.
 */
class Cadastro extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
    this.state = {
      filtro: '',
      itemSelecionado: null,
      acao: 'listando', // O que está sendo feito no momento (listando, editando, excluindo)
      registro: props.getRegistro(),
      registros: [],
      qtdeRegistros: 0,
      pagina: 1,
      abaAtual: 0,
    };

    qtdeLinhas = props.config.qtde_linhas_tela || 15;
    this.tbodyId = 'tbody-' + (++idSequencial);
    this.tHeadId = 'thead-' + (++idSequencial);
    this.configCampos = [];
    this.carregarConfigCampos();
    this.onResizeBinded = this.onResize.bind(this);
    Pessoa = props.usuario;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Carrega os registros assim que entrar na tela.
   */
  async componentDidMount() {
    window.addEventListener('resize', this.onResizeBinded);
    await this.carregarRegistros();
  }

  // ------------------------------------------------------------------------------------

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResizeBinded);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Evento que é setado no componentDidMount e é ativado toda vez que a tela
   * for redimensionada. Nesse caso é utilizado para redimensionar a tabela
   * para que fique do tamanho restante do conteúdo da aba.
   */
  onResize() {
    this.ajustarTamanhoTabela();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Chamado quando o modal de config de campos é fechado, esse método irá
   * puxar as configurações de campo do backend e renderizar novamente
   * a página para surtir efeito.
   */
  async onCloseModalConfigCampos() {
    await this.carregarConfigCampos();
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Handler de mudanças dos campos de formulário na alteração e inclusão,
   * todos os campos chamam esse método durante a alteração do valor.
   */
  setValorRegistro(nome, valor) {
    if (this.props.onAlteracaoCampo) {
      valor = this.props.onAlteracaoCampo(this.state.registro, nome, valor);
    }
    this.setState({
      registro: {
        ...this.state.registro,
        [nome]: valor,
      },
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna todos os componentes disponíveis do form.
   */
  getComponentesForm() {
    return {
      checkbox: this.renderCheckbox.bind(this),
      input: this.renderLabeledEdit.bind(this),
      inputCalc: this.renderInputCalc.bind(this),
      inputDate: this.renderInputDate.bind(this),
      inputMask: this.renderInputMask.bind(this),
      inputNumber: this.renderInputNumber.bind(this),
      inputTime: this.renderInputTime.bind(this),
      radioButton: this.renderRadioButton.bind(this),
      upPanel: this.renderUpPanel.bind(this),
    };
  }

  // ------------------------------------------------------------------------------------

  getQtdePaginas() {
    let paginas = Number(this.state.qtdeRegistros / qtdeLinhas);
    if (paginas % 1 !== 0) {
      // Número quebrado, tem uma página extra
      paginas = Math.ceil(paginas);
    }
    return paginas;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta a página.
   * @param {Number} pagina A Página atual.
   */
  setPagina(pagina) {
    this.setState({ pagina }, () => this.carregarRegistros());
  }

  // ------------------------------------------------------------------------------------

  avancarPagina() {
    if (this.state.pagina < this.getQtdePaginas()) {
      this.setPagina(this.state.pagina + 1);
    }
  }

  // ------------------------------------------------------------------------------------

  voltarPagina() {
    if (this.state.pagina > 1) {
      this.setPagina(this.state.pagina - 1);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Edita o registro selecionado, deixando somente a aba
   * de editar disponível para modificação.
   */
  editarRegistro() {
    GenericUtils.setElementoCarregando(this.table, true);
    this.setState({
      registro: this.state.itemSelecionado,
      itemSelecionado: null,
      acao: 'editando',
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Manda para a página da exclusão, perguntando se o usuário confirma.
   */
  excluirRegistro() {
    GenericUtils.setElementoCarregando(this.table, true);
    this.setState({
      registro: this.state.itemSelecionado,
      itemSelecionado: null,
      acao: 'excluindo',
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Ajusta o tamanho da tabela de registros, deixando ela do tamanho máximo
   * para preencher todo o conteúdo da aba, respeitando as paginações.
   */
  ajustarTamanhoTabela() {
    const tableBody = document.getElementById(this.tbodyId);
    const tableHead = document.getElementById(this.tHeadId);
    const tabContent = document.getElementsByClassName('tab-content')[0];
    if (tableBody && tabContent) {
      // Ok, o tbody da tabela e o conteúdo da aba estão disponíveis,
      // vamos pegar o conteudo da aba e diminuir um certo tamanho
      // para ficar bem certinho o conteúdo:
      let contentHeight = tabContent.clientHeight - 120;
      if (this.props.paginavel) {
        // Opa, tem paginação, nesse caso vamos diminuir com base no tamanho 
        // da div de paginação:
        const paginacao = document.getElementsByClassName('cadastro-paginacao-container')[0];
        if (paginacao) {
          contentHeight -= (paginacao.clientHeight + 12);
        }
      }
      contentHeight -= tableHead.clientHeight;
      tableBody.style.height = `${contentHeight}px`;
      tableBody.style.maxHeight = `${contentHeight}px`;
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Carrega as configurações de campos para mostrar no modal.
   */
  async carregarConfigCampos() {
    const ret =
      await Api('buscarDados', 'ConfigCampos',
        { tabela: this.props.tabela, codigoCliente: codigoCliente });
    if (ret.status) {
      this.configCampos = ret.dados;
      this.nenhumCampoConfig = !this.configCampos.find(x => x.checked);
    } else {
      toastr.error('Erro ao carregar a configCampos', ret.erro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e respeita a config de campos da variável configCampos.
   * @param {String} campo O Nome do campo do input/checkbox/etc.
   * @param {Ref} ref Referencia dos props.ref 
   */
  tratarConfigCampos(campo, ref) {
    // Vamos colocar a propriedade do campo na nossa classe, para poder
    // manipular as informações da classe.
    const self = this;
    this[campo] = ref;

    if (this.configCampos.length === 0 || this.nenhumCampoConfig) {
      // Vamos retornar e não fazer nada pois pode ser que não foi configurado
      // a config de campos ainda, e nesse caso, temos de mostrar todos os campos.
      return;
    }

    /**
     * Mostra ou exibe um o "ref" atual, com base no parâmetro.
     * @param {boolean} display True para mostrar o campo, false para não.
     */
    function tratarElemento(display) {
      if (!display) {
        // Se não vamos mostrar o campo vamos remover ele dos nossos refs, para
        // que seja tratado ao acessar esse ref e manipular ele, afinal não temos
        // que acessar se ele não está na tela.
        self[campo] = null;
      }
      // eslint-disable-next-line
      const domNode = ReactDOM.findDOMNode(ref);
      let parent = domNode.parentElement;
      while (parent) {
        // Percorre os parentes
        if (parent.className.indexOf('col-') >= 0 ||
          parent.className.indexOf('upcfgcampos-') >= 0) {
          // É um elemento de colunas, vamos esconder a coluna inteira.
          parent.style.display = display ? '' : 'none';
          if (!display) {
            parent.className = parent.className.replace(/col/g, 'upcfgcampos');
          } else {
            parent.className = parent.className.replace(/upcfgcampos/g, 'col');
          }
          break;
        }
        parent = parent.parentElement;
      }
      // Mostra ou esconde:
      domNode.style.display = display ? '' : 'none';
    }

    if (ref) {
      // Ref foi definida, tenta achar a config, se achou mostra o campo,
      // se não achou não mostra.
      const config = this.configCampos.find(x => x.nome === campo && x.checked);
      tratarElemento(config);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Carrega os registros da tabela.
   */
  async carregarRegistros() {

    GenericUtils.setElementoCarregando(this.table, true);
    try {
      const objetoFiltro = {
        tabela: this.props.tabela,
        filtro: this.state.filtro,
        where: (this.props.getWhere ? await this.props.getWhere() : this.props.where),
        whereInner: this.props.whereInner ? this.props.whereInner : {},
        whereAlias: this.props.whereAlias,
        codigoCliente,
      };
      if (this.props.paginavel) {
        objetoFiltro.de = (this.state.pagina * qtdeLinhas) - qtdeLinhas;
        objetoFiltro.ate = (this.state.pagina * qtdeLinhas);
      }
      const ret = await Api('buscarDados', 'CadGenerico', objetoFiltro);
      if (ret.status) {
        this.setState({
          itemSelecionado: null, // Zera o item selecionado
          registros: ret.dados.lista,
          qtdeRegistros: ret.dados.qtdeRegistros,
        }, this.ajustarTamanhoTabela.bind(this));
      } else {
        toastr.error('Erro!', ret.erro, { attention: true });
      }
    } finally {
      GenericUtils.setElementoCarregando(this.table, false);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Salva o registro no banco de dados, caso ele já exista ele é atualizado.
   * Se ele não existir ele é cadastrado.
   */
  async salvar(duplicar = false) {
    if (this.props.getValidacoes && !await this.props.getValidacoes(this.state.registro, this)) {
      return;
    }

    let nomeRegistro = '';
    if (this.props.getNomeRegistro) {
      nomeRegistro = this.props.getNomeRegistro(this.state.registro);
    }
    const ret =
      await Api('salvar', this.props.tabela, [this.state.registro, duplicar], codigoCliente);
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
      return false;
    }

    const { registro } = this.state;
    const dizimo = tipo.movimento.dizimo;
    const regDizimo = registro.cod_tipo_movimento ? registro.cod_tipo_movimento : 0;

    if (dizimo === regDizimo && registro.imprimir_comp === 'S') {
      const rel = await Api('gerarRelatorio', {
        codigoCliente,
        nomeRelatorio: 'Comprovante Dizimo',
        codigoPk: ret.dados,
      });
      if (rel.status) {
        const relat = window.location.origin + rel.dados;
        trocarRota(relat, '_blank');
        // GenericUtils.abrirPdfBase64(rel.dados, 'Comprovante Dizimo');
        toastr.success('Relatorio', 'O Relatório foi gerado com sucesso!');
      } else {
        toastr.error('Erro!', 'Relatório não encontrado', { attention: true });
      }
    }

    if (duplicar) {
      toastr.success('Tudo certo!', 'O Registro ' + nomeRegistro + ' foi duplicado com sucesso!');
    } else {
      toastr.success('Tudo certo!', 'O Registro ' + nomeRegistro + ' foi salvo com sucesso!');
    }

    // Tudo certo! volta ao estado original:
    this.carregarRegistros();
    this.resetarAbasEstadoInicial();
  }

  // ------------------------------------------------------------------------------------

  async duplicarRegistro() {
    GenericUtils.setElementoCarregando(this.table, true);
    await this.setState({
      registro: this.state.itemSelecionado,
      itemSelecionado: null,
      acao: 'listando',
    });
    this.salvar(true);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Exclui o registro no banco de dados
   */
  async excluir() {
    let nomeRegistro = '';
    nomeRegistro = this.props.getNomeRegistro && this.props.getNomeRegistro(this.state.registro);
    const ret = await Api('excluir', this.props.tabela, this.state.registro, codigoCliente, Pessoa);
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
      return false;
    }
    toastr.success('Tudo certo!', 'O Registro ' + nomeRegistro + ' foi excluido!');

    // Deu certo a exclusão, vamos carregar os registros e resetar as abas
    // para o estado inicial:
    this.carregarRegistros();
    this.resetarAbasEstadoInicial();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Foca um campo do formulário, isso funciona tanto para elementos
   * encapsulados com o redux quanto elementos normais, é tentando executar
   * o método "focus" do componente.
   */
  focarCampo(nomeCampo) {
    if (this[nomeCampo]) {
      if (this[nomeCampo].focus) {
        this[nomeCampo].focus();
      } else if (this[nomeCampo].getWrappedInstance && this[nomeCampo].getWrappedInstance().focus) {
        this[nomeCampo].getWrappedInstance().focus();
      }
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre a configuração de campos
   */
  abrirConfigForm() {
    this.modalConfigCampos.show();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Reseta as abas para o estado inicial, como se tivesse entrado na
   * tela nesse momento. Deixará apenas as abas de listar e incluir, sendo
   * selecionado a aba de listar. O Registro alterado/incluido será limpo.
   */
  resetarAbasEstadoInicial() {
    this.setState({
      acao: 'listando',
      registro: this.props.getRegistro(),
    }, () => {
      this.setState({ abaAtual: 0 }, () => {
        // Depois de resetar, vamos ajustar o tamanho da tabela:
        this.ajustarTamanhoTabela();
      });
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um checkbox.
   */
  renderCheckbox(text, campo, className) {
    return (
      <Checkbox text={text}
        className={className + ' checkboxComPadding'}
        checked={this.state.registro[campo] === 'S'}
        onChange={e => this.setValorRegistro(campo, e ? 'S' : 'N')}
        ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label porém que só pode numeros.
   */
  renderInputNumber(label, campo, min, max) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    let valor = String(this.state.registro[campo]);
    valor = min === 0 ? valor.replace('-', '') : valor;
    return (
      <LabeledInput label={label}
        value={valor}
        min={min}
        max={max}
        type="number"
        onChange={this.setValorRegistro.bind(this, campo)}
        inputRef={this.tratarConfigCampos.bind(this, campo)}
        onBlur={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label
   */
  renderLabeledEdit(label, campo, disabled) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    return (
      <LabeledInput label={label}
        value={this.state.registro[campo]}
        disabled={disabled}
        onChange={this.setValorRegistro.bind(this, campo)}
        inputRef={this.tratarConfigCampos.bind(this, campo)}
        onBlur={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o componente padrão de registros da Upgrade.
   */
  renderUpPanel(label, campo, tabela, chave, desc, where, disabled) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    return (
      <UpPanel label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        disabled={disabled}
        value={this.state.registro[campo]}
        onChange={e => this.setValorRegistro(campo, e)}
        onBlurUp={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }}
        ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputMask(label, campo, props) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    return (
      <InputMask value={registro[campo]}
        label={label}
        {...props}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        onBlur={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }}
        ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza uma radioButton.
   */
  renderRadioButton(label, campo, valor) {
    return (
      <RadioButton name={campo}
        text={label}
        checked={this.state.registro[campo] === valor}
        onChange={() => this.setValorRegistro(campo, valor)}
        ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que somente aceita datas.
   */
  renderInputDate(label, campo, disabled) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    return (
      <InputDate value={this.state.registro[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        disabled={disabled}
        ref={this.tratarConfigCampos.bind(this, campo)}
        onBlur={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita pontos flutuantes.
   */
  renderInputCalc(label, campo, disabled) {
    const { onExitCampo } = this.props;
    const { registro } = this.state;
    return (
      <InputCalc value={this.state.registro[campo]}
        label={label}
        disabled={disabled}
        onChange={this.setValorRegistro.bind(this, campo)}
        ref={this.tratarConfigCampos.bind(this, campo)}
        onBlur={() => {
          onExitCampo && onExitCampo(registro, campo, registro[campo]);
        }} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input que aceita horários.
   */
  renderInputTime(label, campo) {
    return (
      <InputTime value={this.state.registro[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza os botõezinhos que ficam no fim da página para indicar paginação.
   */
  renderPaginacao() {
    const qtdePaginas = this.getQtdePaginas();
    const dados = new Array(qtdePaginas).join('a').split('a');

    if (qtdePaginas <= 1) {
      // Se só tem uma página, não precisamos renderizar as paginações.
      return <div />;
    }

    return (
      <div className="cadastro-paginacao-container">

        <span className="setas">
          <Button icon="fa-angle-double-left"
            disabled={this.state.pagina === 1}
            onClick={() => this.setPagina(1)} />
          <Button icon="fa-caret-left"
            disabled={this.state.pagina === 1}
            onClick={() => this.voltarPagina()} />
        </span>

        {dados.map((x, i) => {
          const className = this.state.pagina === (i + 1) ? 'btn-primary' : 'btn-default';

          let dentroAlcance = false;
          if (this.state.pagina <= 2) {
            const margem = this.state.pagina === 2 ? 3 : 4;
            dentroAlcance = Math.abs((i + 1) - this.state.pagina) <= margem;
          } else if (this.state.pagina > dados.length - 2) {
            const margem = this.state.pagina === dados.length - 1 ? 3 : 4;
            dentroAlcance = Math.abs((i + 1) - this.state.pagina) <= margem;
          } else {
            dentroAlcance = Math.abs((i + 1) - this.state.pagina) <= 2;
          }

          return dentroAlcance ? (
            <Button key={i}
              className={className + ' botao-paginacao'}
              label={i + 1}
              onClick={this.setPagina.bind(this, i + 1)} />
          ) : <span />;
        })}

        <span className="setas">
          <Button icon="fa-caret-right"
            disabled={this.state.pagina === qtdePaginas}
            onClick={() => this.avancarPagina()} />
          <Button icon="fa-angle-double-right"
            disabled={this.state.pagina === qtdePaginas}
            onClick={() => this.setPagina(qtdePaginas)} />
        </span>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a página de listagem do cadastro.
   */
  renderTabela() {
    const { excluirDesabilitado, editarDesabilitado, duplicarDesabilitado } = this.props;

    const actions = [{
      className: 'btn-primary',
      icone: 'fa-pencil',
      label: 'Editar',
      disabled: !this.state.itemSelecionado ||
        (editarDesabilitado && editarDesabilitado(this.state.itemSelecionado)),
      onClick: this.editarRegistro.bind(this),
    }, {
      className: 'btn-danger',
      icone: 'fa-trash-o',
      label: 'Excluir',
      disabled: !this.state.itemSelecionado ||
        (excluirDesabilitado && excluirDesabilitado(this.state.itemSelecionado)),
      onClick: this.excluirRegistro.bind(this),
    }, {
      className: 'cad-acao-separador',
    }, {
      className: 'btn-info',
      icone: 'fa-copy',
      label: 'Duplicar',
      disabled: !this.state.itemSelecionado ||
        (duplicarDesabilitado && duplicarDesabilitado(this.state.itemSelecionado)),
      onClick: this.duplicarRegistro.bind(this),
    }, {
      className: 'cad-acao-separador',
    }];

    if (this.props.actions) {
      // Se actions extras foram passadas por parâmetro, também
      // renderiza elas:
      for (const action of this.props.actions) {
        actions.push({
          ...action,
          onClick: action.onClick.bind(this, this.state.itemSelecionado),
          disabled: action.disabled && action.disabled(this.state.itemSelecionado),
        });
      }
    }

    return (
      <div className="cadastro-table-container">

        <InputGroup value={this.state.filtro}
          onChange={e => this.setState({ filtro: e })}
          placeholder="Digite seu filtro"
          buttonLabel="Filtrar"
          buttonIcon="fa fa-search"
          buttonClassName="btn-primary"
          onKeyEnter={this.setPagina.bind(this, 1)}
          onClick={this.setPagina.bind(this, 1)} />

        <div>
          <Table header="Registros"
            data={this.state.registros}
            ref={e => this.table = e}
            bodyId={this.tbodyId}
            headId={this.tHeadId}
            selectable
            selectedItem={this.state.itemSelecionado}
            onSelectItem={e => this.setState({ itemSelecionado: e })}
            onPaginaAnterior={() => {
              if (this.state.pagina > 1) {
                this.setPagina(this.state.pagina - 1);
              }
            }}
            onPaginaSeguinte={() => {
              if (this.state.pagina < this.getQtdePaginas()) {
                this.setPagina(this.state.pagina + 1);
              }
            }}
            actions={actions}
            className="h-100">
            {this.props.children}
          </Table>
        </div>

        {this.props.paginavel && this.renderPaginacao()}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a parte de exclusão de registro.
   */
  renderExclusao() {
    const { getNomeRegistro, getMsgExcluir } = this.props;
    const nomeRegistro = getNomeRegistro ? getNomeRegistro(this.state.registro) : 'o registro';
    const { registro } = this.state;
    return (
      <div className="cad-msg-excluir">
        <i className="fa fa-trash" />
        {getMsgExcluir ? getMsgExcluir(registro) : (
          'Tem certeza que deseja excluir ' + nomeRegistro + '?'
        )}
        <span>Essa ação é irreversível e será gravada</span>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza os botões de confirmar e cancelar, que são colocados
   * nos forms e na hora de excluir algum registro.
   * @param {Function} onClickConfirmar O OnClick chamado ao clicar no confirmar.
   */
  renderBotoesConfirmarCancelar(onClickConfirmar) {
    return (
      <div className="p-10">
        <Button className="btn-success"
          label="Confirmar"
          onClick={onClickConfirmar}
          icon="fa-check" />
        <Button className="ml-5 btn-danger"
          label="Cancelar"
          onClick={async () => {
            this.props.recarregarAoCancelar && await this.carregarRegistros();
            this.resetarAbasEstadoInicial();
          }}
          icon="fa-times" />
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   */
  renderForm() {
    return (
      <div className="relative">
        {/* Botãozinho da config de campos: */}
        <i className="fa fa-cog config-campos-cog" onClick={this.abrirConfigForm.bind(this)} />
        {/* Renderização do form: */}
        {this.props.renderForm(this.state.registro, this.getComponentesForm())}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza os botões de confirmar e cancelar. Esse rodapé só é renderizado
   * quando o usuário está na aba de incluir/alterar ou na aba de excluir.
   */
  renderRodape() {
    switch (Number(this.state.abaAtual)) {
      case 1:
      case 2: return this.renderBotoesConfirmarCancelar(async () => {
        const dadosModal = this.props.dadosModalConfirmacaoSalvar;
        if (dadosModal && dadosModal(this.state.registro)) {
          const ret = await ModalConfirmacao.abrir(dadosModal(this.state.registro));
          ret === 'confirmar' && this.salvar();
        } else {
          this.salvar();
        }
      });
      case 3: return this.renderBotoesConfirmarCancelar(this.excluir.bind(this));
      default: return null;
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    const { acao } = this.state;
    return (
      <div className="cadastro h-100">
        <ContentWrapper title={this.props.titulo} small={this.props.subTitulo}>

          <PageControl aba={this.state.abaAtual}
            onChange={e => this.setState({ abaAtual: e })}
            renderRodape={this.renderRodape.bind(this)}>
            <Aba ref={e => this.abaListar = e}
              key={0}
              icone="bars"
              label="Listar"
              disabled={acao !== 'listando'}>
              {this.renderTabela()}
            </Aba>
            <Aba key={1} icone="plus" label="Incluir" disabled={acao !== 'listando'}>
              {this.renderForm()}
            </Aba>
            <Aba key={2} icone="pencil" label="Alterar" disabled={acao !== 'editando'}>
              {this.renderForm()}
            </Aba>
            <Aba key={3} icone="trash-o" label="Excluir" disabled={acao !== 'excluindo'}>
              {this.renderExclusao()}
            </Aba>
          </PageControl>

          <ModalConfigCampos ref={e => this.modalConfigCampos = e}
            tabela={this.props.tabela}
            onClose={this.onCloseModalConfigCampos.bind(this)} />
        </ContentWrapper>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
  config: state.config,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(Cadastro);
