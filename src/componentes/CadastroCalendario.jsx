import React from 'react';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import UtilEscalaCalendario from './UtilEscalaCalendario.jsx';
import Api from '../lib_frontend/main/Api';

let Maiusculo;
let Igreja;
let Logada;
let codigoCliente;

/**
 */
class CadastroCalendario extends React.Component {

  constructor(props) {
    super(props);
    Maiusculo = props.config.status_input_maiusculo;
    Igreja = props.usuario.cod_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Será chamado esse método quando alguém for fazer uma inclusão ou edição
   * no calendário. 
   * 
   * Usaremos esse método para salvar o nosso registro de calendário
   * caso ele ainda não tenha uma pk, para que seja possível associar o código do
   * calendário no componente UtilEscalaCalendario.
   */
  onEdicaoOuInclusaoCalendario() {
    return new Promise(async (resolve, reject) => {

      const registro = this.cadastro.getWrappedInstance().state.registro;
      if (!registro.nome_calendario) {
        // Se não tem nome vamos rejeitar a promise, porque para salvar precisamos
        // colocar o nome do calendário.
        this.cadastro.getWrappedInstance().focarCampo('nome_calendario');
        return reject('Preencha o nome da escala antes!');
      }

      if (!registro.cod_calendario) {
        // Se não tem o código de calendário irá cair aqui, nesse caso vamos
        // salvar o registro.

        if (Maiusculo === 'S') {
          // Antes temos que validar a configuração.
          registro.nome_calendario = registro.nome_calendario.toUpperCase();
        }

        const ret = await Api('salvar', 'tab_calendario', registro, codigoCliente);

        this.cadastro.getWrappedInstance().setValorRegistro('cod_calendario', ret.dados);
        return setTimeout(resolve, 200);
      }
      // Já tem código do calendário, não precisamos fazer nada.
      resolve();
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_calendario;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_calendario: '',
      nome_calendario: '',
      cod_igreja: Igreja,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_calendario', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Nome da escala:', 'nome_calendario')}
          </div>
        </div>

        <UtilEscalaCalendario codCalendario={registro.cod_calendario}
          onEdicaoOuInclusao={this.onEdicaoOuInclusaoCalendario.bind(this)} />
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_calendario"
        titulo="Cadastro de Escala"
        subTitulo="Configure as suas Escalas!"
        where={'cod_igreja = ' + Logada}
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}
        recarregarAoCancelar
        ref={e => this.cadastro = e}>
        <Column key={1} width="80px" field="cod_calendario" header="Código" />
        <Column key={2} field="nome_calendario" header="Escala" />
      </Cadastro>
    );
  }

}

export default connect((state) => ({
  config: state.config,
  usuario: state.usuario,
}))(CadastroCalendario);
