import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import Modal from '../lib_frontend/componentes/Modal';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils';
import InputCalc from '../lib_frontend/componentes/InputCalc.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import { formataDinheiro } from '../utils/funcoesFront';
import LabeledInput from '../lib_frontend/componentes/LabeledInput';

let Pessoa;
let Caixa;
let codigoCliente;

class ModalBaixarCR extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    // O State será o próprio objeto de contas a receber
    cod_cr: null, // O Código do contas a receber
    vr_receber: 0,
    vr_recebido: 0,
    vr_desconto: 0,
    vr_acrescimo: 0,
    obs_baixa: '',
    realizandoBaixa: false, // Se esta realizando a baixa no momento
  };

  // ------------------------------------------------------------------------------------

  onChangeValor(campo, valor) {
    const { vr_receber, vr_recebido, vr_desconto, vr_acrescimo } = this.state;
    if (campo === 'vr_recebido') {
      if (valor > ((vr_receber + vr_acrescimo) - vr_desconto)) {
        valor = ((vr_receber + vr_acrescimo) - vr_desconto);
      }
    } else if (campo === 'vr_desconto') {
      if (valor > vr_receber) {
        valor = vr_receber;
      }
      if (vr_recebido > ((vr_receber + vr_acrescimo) - valor)) {
        this.setState({ vr_recebido: ((vr_receber + vr_acrescimo) - valor) });
      }
    } else if (campo === 'vr_acrescimo') {
      if (vr_recebido > ((vr_receber + valor) - vr_desconto)) {
        this.setState({ vr_recebido: ((vr_receber + valor) - vr_desconto) });
      }
    }
    this.setState({ [campo]: valor });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Fecha esse modal.
   */
  fecharModal() {
    this.props.onClose && this.props.onClose();
    this.modal.hide();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza a baixa do Contas a Receber
   */
  async realizarBaixa() {
    this.setState({ realizandoBaixa: true });
    try {
      // Agora vamos baixar ela:
      const ret = await Api('baixarModal', Pessoa, this.state, 'CR', codigoCliente);
      if (!ret.status) {
        toastr.error('Atenção!', ret.erro, { attention: true });
        return false;
      }

      toastr.success('Tudo certo!', 'A Conta foi recebida!');
      this.fecharModal();
    } finally {
      this.state = null;
      this.setState({ realizandoBaixa: false });
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   * @param {String|Number} cod_cr O Código do contas a receber.
   */
  async show(cod_cr) {
    Caixa = await this.props.usuario.caixa ? this.props.usuario.caixa.cod_caixa : null;
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      const ret = await Api('buscarPorId', 'tab_cr', cod_cr, codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      // Mesmo que tiver algo traz vazio:
      ret.dados.obs_baixa = '';

      this.setState({ ...ret.dados, cod_cr, cod_caixa: Caixa });
      this.modal.show();
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    const restante =
      (this.state.vr_receber + this.state.vr_acrescimo) -
      (this.state.vr_recebido + this.state.vr_desconto);

    const botoes = [{
      className: 'btn-success',
      label: 'Realizar baixa',
      icon: 'fa-check',
      onClick: this.realizarBaixa.bind(this),
      disabled: this.state.realizandoBaixa,
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      disabled: this.state.realizandoBaixa,
      dismiss: true,
    }];
    return (
      <Modal title="Baixar contas a receber"
        buttons={botoes}
        ref={e => this.modal = e}>
        <div className="relative p-10" ref={e => this.divPrincipal = e}>

          <div className="row">
            <div className="col-sm-12">
              <UpPanel label="Pessoa:"
                tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                value={this.state.cod_pessoa}
                disabled
                onChange={e => this.setState({ cod_pessoa: e })} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-3">
              <InputCalc value={this.state.vr_receber}
                label="Valor da conta:"
                disabled
                onChange={e => this.setState({ vr_receber: e })} />
            </div>
            <div className="col-sm-3">
              <InputCalc value={this.state.vr_acrescimo}
                label="Valor de Acréscimo:"
                onChange={this.onChangeValor.bind(this, 'vr_acrescimo')} />
            </div>
            <div className="col-sm-3">
              <InputCalc value={this.state.vr_desconto}
                label="Valor de Desconto:"
                onChange={this.onChangeValor.bind(this, 'vr_desconto')} />
            </div>
            <div className="col-sm-3">
              <InputCalc value={this.state.vr_recebido}
                label="Valor recebido:"
                onChange={this.onChangeValor.bind(this, 'vr_recebido')} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <LabeledInput label="Observação do recebimento:"
                value={this.state.obs_baixa}
                onChange={e => this.setState({ obs_baixa: e })} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <div className="mov-vr-total">
                {restante > 0 ? (
                  <div>
                    Restante: {formataDinheiro(restante)}
                    <br />
                    <span>Será gerado outra conta com esse restante</span>
                  </div>
                ) : (
                    <div>
                      Recebimento completo
                    <br />
                      <span>O Recebimento será completo</span>
                    </div>
                  )}
              </div>
            </div>
          </div>

        </div>
      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalBaixarCR);
