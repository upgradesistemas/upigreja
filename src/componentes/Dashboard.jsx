import React from 'react';
import { connect } from 'react-redux';
import Api from '../lib_frontend/main/Api.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { trocarRota, formataDinheiro } from '../utils/funcoesFront';
import {
  UpValoresBox,
  UpValorFilho,
  UpValorBotao,
} from '../lib_frontend/componentes/UpValoresConjunto.jsx';
import { Table, Column } from '../lib_frontend/componentes/Table.jsx';

let DepIgreja;
let Logada;
let Caixa;
let Nivel;
let ValorInicialCaixa;
let codigoCliente;

class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    DepIgreja = props.usuario.departamento_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    Nivel = props.usuario.nivel;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    ValorInicialCaixa = props.usuario.caixa ? props.usuario.caixa.vr_inicial : 0;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    totais: {},
    aniversario: {},
    aniversarioCasamento: {},
  }

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    await this.getDashboard();
  }

  // ------------------------------------------------------------------------------------

  onClick() {
    trocarRota('tesouraria/caixa');
  }

  // ------------------------------------------------------------------------------------

  async getDashboard() {
    const ret = await Api('buscarDados', 'getDashboard', {
      dep: DepIgreja, Igreja: Logada, codigoCliente, Caixa,
    });
    if (ret.status) {
      const totais = ret.dados;
      totais.credFuturo = formataDinheiro(totais.credFuturo);
      totais.debFuturo = formataDinheiro(totais.debFuturo);
      totais.saldoFuturo = formataDinheiro(totais.saldoFuturo);
      totais.credito = formataDinheiro(totais.credito);
      totais.debito = formataDinheiro(totais.debito);
      totais.saldo = formataDinheiro(totais.saldo + ValorInicialCaixa);

      await this.setState({
        totais,
        aniversario: ret.dados.aniversario,
        aniversarioCasamento: ret.dados.aniversarioCasamento,
      });
    } else {
      console.log(ret);
    }
  }

  // ------------------------------------------------------------------------------------

  renderDashInicio() {
    return (
      <section className="content-header">
        <h1>Dashboard</h1>
        <ol className="breadcrumb">
          <li><a href=""><i className="fa fa-dashboard" /> Início</a></li>
          <li className="active">Dashboard</li>
        </ol>
      </section>
    );
  }

  // ------------------------------------------------------------------------------------

  renderPessoasCampo(membrosCampo, congregadosCampo) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const pessoasCampo = Nivel.dash_ve_pessoas_campo === 'S';
    if (tudo || pessoasCampo) {
      return (
        <div>
          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-aqua">
              <div className="inner">
                <h3>{membrosCampo}</h3>
                <p>Membros do Campo</p>
              </div>
              <div className="icon">
                <i className="fa fa-users" />
              </div>
              {/* <a href="" className="small-box-footer">
            Mais <i className="fa fa-arrow-circle-right" />
          </a> */}
            </div>
          </div>

          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-aqua">
              <div className="inner">
                <h3>{congregadosCampo}</h3>
                <p>Congregados do Campo</p>
              </div>
              <div className="icon">
                <i className="fa fa-users" />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderPessoasLocal(membrosLocal, congregadosLocal) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const pessoasLocal = Nivel.dash_ve_pessoas_local === 'S';
    if (tudo || pessoasLocal) {
      return (
        <div>
          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-green">
              <div className="inner">
                <h3>{membrosLocal}</h3>
                <p>Total de Membros Local</p>
              </div>
              <div className="icon">
                <i className="fa fa-user" />
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-green">
              <div className="inner">
                <h3>{congregadosLocal}</h3>
                <p>Total de Congregados Local</p>
              </div>
              <div className="icon">
                <i className="fa fa-user" />
              </div>
            </div>
          </div>

        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderIgrejasCadastradas(igrejas) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const igrejasCadastradas = Nivel.dash_ve_igrejas_cadastradas === 'S';
    if (tudo || igrejasCadastradas) {
      return (
        <div>
          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-red">
              <div className="inner">
                <h3>{igrejas}</h3>
                <p>Igrejas Cadastradas</p>
              </div>
              <div className="icon">
                <i className="fa fa-bank" />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderUsuariosRegistrados(usuarios) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const usuariosRegistrados = Nivel.dash_ve_usuarios_registrados === 'S';
    if (tudo || usuariosRegistrados) {
      return (
        <div>
          <div className="col-lg-4 col-xs-6">
            <div className="small-box bg-yellow">
              <div className="inner">
                <h3>{usuarios}</h3>
                <p>Usários Registrados</p>
              </div>
              <div className="icon">
                <i className="fa fa-check" />
              </div>
            </div>
          </div>

        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderValoresFuturos(credFuturo, debFuturo, saldoFuturo) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const valoreFuturos = Nivel.dash_ve_valores_futuros === 'S';
    if (tudo || valoreFuturos) {
      return (
        <div className="row">
          <UpValoresBox titulo="Previsão de Receitas e Despesas Futuras">
            <UpValorFilho className="col-xs-12 col-sm-6 col-lg-3"
              valor={credFuturo} cor="green" titulo="TOTAL A RECEBER" />

            <UpValorFilho className="col-xs-12 col-sm-6 col-lg-3"
              valor={debFuturo} cor="red" titulo="TOTAL A PAGAR" />

            <UpValorFilho className="col-xs-12 col-sm-6 col-lg-4"
              valor={saldoFuturo} cor="blue"
              titulo="PREVISÃO DE SALDO DA IGREJA" />

            <UpValorBotao classNameDiv="col-xs-12 col-sm-6 col-lg-2" className="btn-success"
              label="Ir para o Caixa" icone="fa-arrow-right"
              onClick={this.onClick.bind(this)} />
          </UpValoresBox>
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderValorCaixaAtual(credito, debito, saldo) {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const valorCaixaAtual = Nivel.dash_ve_valor_caixa_atual === 'S';
    if (tudo || valorCaixaAtual) {
      return (
        <div className="row">
          <UpValoresBox titulo="Resumo do Caixa Atual da Igreja">
            <UpValorFilho className="col-xs-12 col-sm-4 col-lg-4"
              valor={credito} cor="green" titulo="TOTAL DE RECEITAS" />

            <UpValorFilho className="col-xs-12 col-sm-4 col-lg-4"
              valor={debito} cor="red" titulo="TOTAL DE DESPESAS" />

            <UpValorFilho className="col-xs-12 col-sm-4 col-lg-4"
              valor={saldo} cor="blue"
              titulo="SALDO DA IGREJA" />
          </UpValoresBox>
        </div>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  // renderEventos() {
  //   const tudo = Nivel.dash_ve_tudo === 'S';
  //   const eventos = Nivel.dash_ve_eventos === 'S';
  //   // if (tudo || eventos) {
  //   if (false) {
  //     return (
  //       <div className="row">
  //         <div className="col-lg-5 col-xs-12">
  //           <div className="box box-primary">
  //             <div className="box-header with-border">
  //               <h3 className="box-title">Próximos Congressos, Festividades e Eventos</h3>
  //             </div>

  //             <div className="box-body">
  //               <ul className="products-list product-list-in-box">
  //                 <li className="item">
  //                   <div>
  //                     <a className="product-title">Congresso Geral do Campo - 08/12 até 09/12</a>
  //                     <span className="product-description">Assembléia de Deus - Sede</span>
  //                   </div>
  //                 </li>

  //                 <li className="item">
  //                   <div>
  //                     <a className="product-title">Congresso das Irmãs - 07/09 até 09/09</a>
  //                     <span className="product-description">Assembléia de Deus - Sede</span>
  //                   </div>
  //                 </li>
  //                 <li className="item">
  //                   <div>
  //                     <a className="product-title">Congresso dos Jovens - 10/08 até 12/08</a>
  //                     <span className="product-description">Assembléia de Deus - Cruzeiro </span>
  //                   </div>
  //                 </li>
  //                 <li className="item">
  //                   <div>
  //                     <a className="product-title">Congresso Unificado - 25/07 até 27/07</a>
  //                     <span className="product-description">Assembléia de Deus Paranacity</span>
  //                   </div>
  //                 </li>
  //               </ul>
  //             </div>
  //             <div className="box-footer text-center">
  //               <a className="uppercase">Ver Todos os Congressos</a>
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     );
  //   }
  // }

  // ------------------------------------------------------------------------------------

  renderTabelaNiver() {
    const tudo = Nivel.dash_ve_tudo === 'S';
    const aniversariantes = Nivel.dash_ve_aniversariantes === 'S';
    if (tudo || aniversariantes) {
      return (
        <React.Fragment>
          <div className="row dashboard-aniversariantes">
            <div className="col-lg-6 col-xs-12">
              <div className="box box-info">

                <div className="box-header with-border">
                  <h3 className="box-title">Aniversariantes do Mês</h3>
                </div>

                <div className="box-body">
                  <div className="table-responsive">
                    <div className="cadastro-table-container">
                      <div>
                        <Table className="h-100"
                          header="Registros"
                          ref={e => this.table = e}
                          data={this.state.aniversario}
                          actions={{}} >

                          <Column key={1} field="nome_pessoa" header="Membro" />
                          <Column key={2} field="nascimento" header="Dt. Nascimento" />
                          <Column key={3} width="60px" field="anos" header="Anos" />
                          <Column key={4} width="60px" field="ativo" header="Ativo" />
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

            <div className="col-lg-6 col-xs-12">
              <div className="box box-info">

                <div className="box-header with-border">
                  <h3 className="box-title">Aniversário de Casamento</h3>
                </div>

                <div className="box-body">
                  <div className="table-responsive">
                    <div className="cadastro-table-container">
                      <div>
                        <Table className="h-100"
                          header="Registros"
                          ref={e => this.table = e}
                          data={this.state.aniversarioCasamento}
                          actions={{}} >

                          <Column key={1} field="nome_pessoa" header="Membro" />
                          <Column key={2} field="conjuge" header="Conjuge" />
                          <Column key={3} width="90px" field="casamento" header="Casamento" />
                          <Column key={4} width="60px" field="anos" header="Anos" />
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </React.Fragment>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderForm() {
    const {
      credFuturo, debFuturo, saldoFuturo,
      credito, debito, saldo,
      igrejas, usuarios, membrosLocal, congregadosLocal,
      membrosCampo, congregadosCampo } = this.state.totais;

    return (
      <section className="content">
        {this.renderDashInicio()}
        <br />
        <div className="row">
          {this.renderPessoasCampo(membrosCampo, congregadosCampo)}
          {this.renderIgrejasCadastradas(igrejas)}
          {this.renderPessoasLocal(membrosLocal, congregadosLocal)}
          {this.renderUsuariosRegistrados(usuarios)}
        </div>

        {this.renderValoresFuturos(credFuturo, debFuturo, saldoFuturo)}
        <br />
        {this.renderValorCaixaAtual(credito, debito, saldo)}
        {this.renderTabelaNiver()}
        {/* {this.renderEventos()} */}
      </section>
    );
  }

  render() {
    return (
      <ContentWrapper>
        {this.renderForm()}
      </ContentWrapper>
    );
  }

}

export default connect(state => ({
  usuario: state.usuario,
}))(Dashboard);
