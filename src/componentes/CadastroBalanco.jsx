import React from 'react';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
let Logada;

/**
 * Cadastro padrão de Balanço.
 */
 class CadastroBalanco extends React.Component {

  constructor(props) {
    super(props);
    Logada = props.usuario.igrejaLogada.Igreja;
  }

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_objeto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_balanco: null,
      nome_objeto: '',
      cod_setor: 0,
      status_ativo: 'S',
      cod_igreja: Logada,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_balanco', true)}
          </div>
          <div className="col-sm-4">
            {componentes.input('Objeto:', 'nome_objeto')}
          </div>
          <div className="col-sm-2">
            {componentes.input('Quantidade:', 'qtd_objeto')}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Setor:', 'cod_setor', 'tab_setor_balanco',
              'cod_setor', 'nome_setor')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}

        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {componentes.upPanel('Igreja:', 'cod_igreja', 'tab_igreja',
              'cod_igreja', 'nome_igreja')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_balanco"
        titulo="Balanço Patrimonial"
        subTitulo="Configure as sua Áreas !"
        paginavel
        where={'cod_igreja = ' + Logada}
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={2} field="nome_objeto" header="Objeto" />
        <Column key={2} field="qtd_objeto" header="Quantidade Lançada" />
        <Column key={3} field="setor.nome_setor" header="Setor" />
      </Cadastro>
    );
  }

}

export default connect(state => ({
  usuario: state.usuario,
}))(CadastroBalanco);
