import React from 'react';
import moment from 'moment';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import ModalBaixarCR from './ModalBaixarCR';
import ModalGerarParcelasCR from './ModalGerarParcelasCR.jsx';
import ModalCaixa from './ModalCaixa.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import tipo from '../tipo';

let Igreja;
let Pessoa;
let Logada;
let Caixa;
let codigoCliente;

/**
 * Componente de movimentação de contas a receber, permitindo incluir
 * registros de contas a receber e listas as contas a receber
 * ou recebidas até o momento.
 */
class MovContasReceber extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
    Pessoa = props.usuario.cod_pessoa;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    if (campo === 'vr_desconto' && valor > (registro.vr_receber + registro.vr_acrescimo)) {
      valor = registro.vr_receber + registro.vr_acrescimo;
    } else if (campo === 'vr_receber' && (valor + registro.vr_acrescimo) < registro.vr_desconto) {
      registro.vr_desconto = valor + registro.vr_acrescimo;
    } else if (campo === 'vr_acrescimo' && (valor + registro.vr_receber) < registro.vr_desconto) {
      registro.vr_desconto = valor + registro.vr_receber;
    }
    if ((campo === 'cod_pessoa' ||
      campo === 'cod_departamento' ||
      campo === 'cod_igreja_transferencia') && valor > 0) {
      this.verificaCodigos(registro);
    }
    return valor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna a mensagem de confirmação ao excluir
   */
  getMsgExcluir(registro) {
    return `Tem certeza que deseja excluir essa 
      conta a receber no valor de ${registro.vr_total_formatado} para
      a pessoa "${registro.pessoa.nome_pessoa}"?`;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna true para poder salvar o registro e false para não,
   * serve para validar as informações colocadas.
   * @param {Object} registro O Registro sendo editado.
   * @param {Cadastro} cadastroRef O Ref do cadastro.
   */
  getValidacoes(registro, cadastroRef) {
    const { cod_pessoa, cod_departamento, cod_igreja_transferencia } = registro;
    const preenchidos = (cod_pessoa && Number(cod_pessoa) !== 0) ||
      (cod_departamento && Number(cod_departamento) !== 0) ||
      (cod_igreja_transferencia && Number(cod_igreja_transferencia) !== 0);

    if (!preenchidos) {
      toastr.error('Atenção!', 'Deve preencher uma Pessoa, Departamento ou uma Igreja!');
      cadastroRef.focarCampo('cod_pessoa');
      return false;
    }
    if (!registro.cod_centro_custo || registro.cod_centro_custo === '0') {
      toastr.error('Atenção!', 'Preencha o Centro de Custo !');
      cadastroRef.focarCampo('cod_centro_custo');
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cr: null,
      cod_pessoa: 0,
      cod_departamento: 0,
      cod_igreja: Igreja,
      cod_tipo_movimento: tipo.movimento.crManual,
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_receber: '',
      dt_hr_recebido: '',
      vr_receber: 0,
      vr_recebido: 0,
      vr_acrescimo: 0,
      vr_desconto: 0,
      cod_origem: 0,
      status_manual: 'S',
      obs: '',
      obs_documento: '',
      status_estornado: 'N',
      cod_funcionario: Pessoa,
      cod_funcionario_baixa: 0,
      cod_forma_pagamento: 1,
      pessoa: {},
      cod_centro_custo: 0,
      cod_igreja_transferencia: 0,
    };
  }

  // ------------------------------------------------------------------------------------

  verificaCodigos(registro) {
    registro.cod_pessoa = 0;
    registro.cod_departamento = 0;
    registro.cod_igreja_transferencia = 0;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    const z = Number(0);
    const codP = Number(registro.cod_pessoa);
    const codD = Number(registro.cod_departamento);
    const codI = Number(registro.cod_igreja_transferencia);

    const pessoa = (codD !== z || codI !== z) && codP === z;
    const depart = (codP !== z || codI !== z) && codD === z;
    const igrTra = (codP !== z || codD !== z) && codI === z;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {componentes.upPanel('Pessoa:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'status_fornecedor = "N" AND cod_igreja = ' + Igreja, pessoa)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Departamento:', 'cod_departamento', 'tab_departamento',
              'cod_departamento', 'nome_departamento', '', depart)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Igreja Transferência:', 'cod_igreja_transferencia', 'tab_igreja',
              'cod_igreja', 'nome_igreja', 'status_igreja <> "S"', igrTra)}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            {componentes.upPanel('Centro de Custo *:', 'cod_centro_custo', 'tab_centro_custo',
              'cod_centro_custo', 'nome_centro_custo')}
          </div>
          <div className="col-sm-7">
            {componentes.input('Descrição:', 'obs')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}

        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            {componentes.inputDate('Data de cadastro:', 'dt_hr_cadastro', true)}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data de vencimento:', 'dt_hr_receber')}
          </div>
          <div className="col-sm-2">
            {componentes.inputCalc('Valor:', 'vr_receber')}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Forma de Pagamento:', 'cod_forma_pagamento',
              'tab_forma_pagamento', 'cod_forma_pagamento', 'nome_forma_pagamento')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}


      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a coluna de recebido.
   */
  renderColunaRecebido(item) {
    if (item.dt_hr_recebido && item.status_baixa_parcial === 'S') {
      return <div className="mov-cr-cp-coluna parcial fa fa-star-half-o" />;
    } else if (item.dt_hr_recebido) {
      return <div className="mov-cr-cp-coluna completa fa fa-star" />;
    }
    return <div className="mov-cr-cp-coluna fa fa-times" />;
  }

  // ------------------------------------------------------------------------------------

  render() {
    const whereInner = {
      tabelaPrincipal: 'cr', // Tabela atual que estamos pesquisando
      tabelaInner: 'pessoa', // Tabela que vamos pesquisar no Inner
      ligacaoTabela1: 'cod_pessoa', // Campo chave de ligação da Tabela Principal
      ligacaoTabela2: 'cod_pessoa', // Campo chave de ligação da Tabela do Inner
      campoWhere: 'nome_pessoa', // Campo do Inner para fazer o Like
    };

    const props = {
      tabela: 'tab_cr',
      titulo: 'Contas a Receber',
      subTitulo: 'Gerencia todas as suas contas a receber!',
      paginavel: true,
      selecionavel: true,
      where: 'cod_igreja = ' + Logada,
      renderForm: this.renderForm.bind(this),
      getRegistro: this.getRegistro.bind(this),
      getMsgExcluir: this.getMsgExcluir.bind(this),
      getValidacoes: this.getValidacoes.bind(this),
      onAlteracaoCampo: this.onAlteracaoCampo.bind(this),
      whereInner: whereInner,
      editarDesabilitado: (cr) => cr.dt_hr_recebido || cr.cod_cr_origem || cr.cod_cr_destino,
      excluirDesabilitado: (cr) => cr.dt_hr_recebido || cr.cod_cr_destino,
      actions: [{
        icone: 'fa-money',
        label: 'Baixar conta',
        className: 'btn-success',
        onClick: (item) => {
          this.modalBaixarCR.getWrappedInstance().show(item.cod_cr);
        },
        disabled: (cr) => !cr || cr.dt_hr_recebido,
      }, {
        icone: 'fa-arrow-left',
        label: 'Estornar conta',
        className: 'btn-warning',
        onClick: async (item) => {
          const ret = await Api('estornarConta', Pessoa, item.cod_cr, 'CR', codigoCliente);
          if (!ret.status) {
            toastr.error('Atenção!', ret.erro, { attention: true });
            return false;
          }
          toastr.success('Tudo certo!', 'O Registro foi estornado!');
          this.cadastro.getWrappedInstance().carregarRegistros();
        },
        disabled: (cr) => !cr || !cr.dt_hr_recebido,
      }, {
        icone: 'fa-list-ol',
        label: 'Gerar parcelas',
        className: 'btn-info',
        onClick: () => this.modalParcelasCR.getWrappedInstance().show(),
        disabled: () => false,
      }],
    };
    return (
      <div>
        <Cadastro ref={e => this.cadastro = e} {...props}>
          {/* <Column width="80px" field="cod_cr" header="Código" /> */}
          <Column width="270px" field="pessoa.nome_pessoa" header="Pessoa" />
          <Column field="vr_total_formatado" header="Valor" />
          <Column field="vr_recebido_formatado" header="Valor recebido" />
          <Column field="dt_hr_receber" header="Dt. Vencimento" />
          <Column field="dt_hr_recebido" header="Dt. Recebimento" />
          <Column field="nome_movimento" header="Origem" />
          <Column header="Recebido"
            render={this.renderColunaRecebido.bind(this)} />
        </Cadastro>

        <ModalBaixarCR ref={e => this.modalBaixarCR = e}
          onClose={() => this.cadastro.getWrappedInstance().carregarRegistros()} />

        <ModalGerarParcelasCR ref={e => this.modalParcelasCR = e}
          onClose={() => this.cadastro.getWrappedInstance().carregarRegistros()} />

        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(MovContasReceber);
