import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import moment from 'moment';
import uuid from 'uuid/v1';
import Modal from '../lib_frontend/componentes/Modal';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import UpAutocomplete from '../lib_frontend/componentes/UpAutocomplete';
import Button from '../lib_frontend/componentes/Button';
import GenericUtils from '../lib_frontend/main/GenericUtils';

let Logada;
let codigoCliente;

class ModalEscala extends React.Component {

  constructor(props) {
    super(props);
    this.sequencial = 0;
    Logada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    cod_escala: 0,
    cod_calendario: 0, // O Código do calendário atual
    dthr_escala: '05/11/2018',
    obs: '',
    grupos: [],
    pessoas: [],
  };

  // ------------------------------------------------------------------------------------

  onChangeNomeGrupo(grupo, nome) {
    grupo.nome_escala_nome = nome;
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  async show(codEscala, dataEscala, codCalendario) {
    this.modal.show();
    try {
      GenericUtils.setElementoCarregando(this.divPrincipal, true);

      if (codEscala) {
        // Código da escala foi informado, vamos buscar por código da escala:    
        const ret = await Api('buscarDados', 'EscalaModal', { codEscala, codigoCliente });
        if (ret && ret.status && ret.dados) {
          const ultimoGrupo = ret.dados.grupos[ret.dados.grupos.length - 1];
          this.sequencial = ultimoGrupo ? ultimoGrupo.sequencial : 0;
          return this.setState(ret.dados);
        }
      }

      this.setState({
        cod_escala: 0,
        dthr_escala: dataEscala,
        cod_calendario: codCalendario,
        obs: '',
        grupos: [],
        pessoas: [],
      });
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  criarGrupo() {
    const grupo = {
      sequencial: ++this.sequencial,
      nome_escala_nome: '',
      pessoas: [],
    };
    this.state.grupos.push(grupo);
    this.forceUpdate(() => {
      grupo.refNome.focus();
    });
  }

  // ------------------------------------------------------------------------------------

  criarPessoa(array = this.state) {
    const pessoa = {
      uuid: uuid(),
    };
    array.pessoas.push(pessoa);
    this.forceUpdate(() => {
      pessoa.refPessoa.focus();
    });
  }

  // ------------------------------------------------------------------------------------

  realizarValidacoes() {
    function validarPessoa(pessoa) {
      if (!pessoa.cod_pessoa) {
        pessoa.refPessoa.focus();
        toastr.error('Atenção!', 'Preencha o nome da pessoa!', { attention: true });
        return false;
      }
      return true;
    }

    for (const pessoa of this.state.pessoas) {
      if (!validarPessoa(pessoa)) {
        return false;
      }
    }
    for (const p of this.state.grupos) {
      if (!p.nome_escala_nome) {
        toastr.error('Atenção!', 'Preencha o nome do grupo!', { attention: true });
        p.refNome.focus();
        return false;
      }
      for (const p2 of p.pessoas) {
        if (!validarPessoa(p2)) {
          return false;
        }
      }
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  async salvarEscala() {
    // Primeiro vamos realizar as validaçoes:
    if (!this.realizarValidacoes()) {
      return;
    }

    // Agora vamos remover as referencias dos componentes react
    // para que não dê problema ao converter a estrutura para um JSON:
    for (const pessoa of this.state.pessoas) {
      delete pessoa.refPessoa;
    }
    for (const grupo of this.state.grupos) {
      delete grupo.refNome;
      for (const pessoa of grupo.pessoas) {
        delete pessoa.refPessoa;
      }
    }

    const ret = await Api('salvarEscala', this.state, codigoCliente);
    if (ret && ret.status) {
      toastr.success('Tudo certo!', 'O Agendamento foi salvo com sucesso!');
      this.modal.hide();
    }

    this.props.onConfirmar && this.props.onConfirmar();
  }

  // ------------------------------------------------------------------------------------

  removerGrupo(grupo) {
    const index = this.state.grupos.findIndex(x => x === grupo);
    if (index >= 0) {
      this.state.grupos.splice(index, 1);
      this.forceUpdate();
    }
  }

  // ------------------------------------------------------------------------------------

  renderGrupo(grupo) {
    const seq = grupo.sequencial + '';
    const ultimoNumeroSequencial = seq.substring(seq.length - 1);
    return (
      <div className={'container-grupo indice-' + ultimoNumeroSequencial}>

        <div className="acoes">
          <div className="titulo">{'Grupo ' + grupo.sequencial}</div>
          <div className="separador" />
          <i className="fa fa-times fa-2x btn-remover"
            onClick={this.removerGrupo.bind(this, grupo)} />
        </div>

        <div className="cabecalho">
          <div className="nome-grupo">
            <UpAutocomplete tabela="tab_escala_nome"
              campo="nome_escala_nome"
              placeholder="Nome do grupo"
              value={grupo.nome_escala_nome}
              ref={e => grupo.refNome = e}
              codigoCliente={codigoCliente}
              onChange={this.onChangeNomeGrupo.bind(this, grupo)}
              permitirVazio />
          </div>

          <Button className="btn-primary"
            label="Nova pessoa"
            icon="fa-user"
            onClick={this.criarPessoa.bind(this, grupo)} />
        </div>

        {grupo.pessoas.map(this.renderPessoa.bind(this, grupo))}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderPessoa(grupo, pessoa) {
    return (
      <div key={pessoa.uuid || pessoa.cod_escala_pessoa} className="container-pessoa">
        <UpPanel tabela="tab_pessoa"
          chave="cod_pessoa"
          desc="nome_pessoa"
          where={'cod_igreja = ' + Logada}
          placeholder="Pessoa a executar"
          ref={e => pessoa.refPessoa = e}
          value={pessoa.cod_pessoa}
          onChange={(e) => { pessoa.cod_pessoa = e; this.forceUpdate(); }} />
        <i className="fa fa-times fa-2x btn-remover"
          onClick={() => {
            const index = grupo.pessoas.findIndex(x => x === pessoa);
            grupo.pessoas.splice(index, 1);
            this.forceUpdate();
          }} />
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderMensagemVazio() {
    const dataHora = moment(this.state.dthr_escala, 'YYYY-MM-DD').format('DD/MM/YYYY');
    return (
      <div className="mensagem-vazio-container">
        <h1>{dataHora}</h1>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Novo grupo',
      icon: 'fa-users',
      onClick: this.criarGrupo.bind(this),
    }, {
      className: 'btn-primary',
      label: 'Nova pessoa',
      icon: 'fa-user',
      onClick: this.criarPessoa.bind(this),
    }, {
      className: 'btn-success',
      label: 'Confirmar',
      icon: 'fa-check',
      onClick: this.salvarEscala.bind(this),
      disabled: this.state.realizandoBaixa,
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      disabled: this.state.realizandoBaixa,
      dismiss: true,
    }];

    const { pessoas, grupos } = this.state;
    return (
      <Modal title="Alteração de escala"
        buttons={botoes}
        ref={e => this.modal = e}>
        <div className="modal-escala relative p-10" ref={e => this.divPrincipal = e}>
          {grupos.map(this.renderGrupo.bind(this))}
          {pessoas.map(this.renderPessoa.bind(this, this.state))}
          {!pessoas.length && !grupos.length && this.renderMensagemVazio()}
        </div>
      </Modal>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalEscala);
