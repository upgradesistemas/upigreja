import React from 'react';
import Box from '../../lib_frontend/componentes/Box.jsx';

/**
 * Configurações gerais da aplicação.
 */
export default class ConfigGeral extends React.Component {

  render() {
    const { checkbox, inputNumber } = this.props;
    return (
      <Box collapsible title="Geral" className="m-0">
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="col-sm-12">
          {checkbox('Usar menu aberto ?', 'status_usa_menu_aberto', 'inline mr-10')}
          {checkbox('Usar input com letras maiusculas ?', 'status_input_maiusculo', 'inline mr-10')}
          {checkbox('Imprimir comprovante de Dízimo após Lançamento ?',
            'status_imp_comp_dizimo', 'inline mr-10')}
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="col-sm-4">
          {inputNumber('Largura das Linhas do Grid', 'largura_linha_grid', 0)}
        </div>
        <div className="col-sm-4">
          {inputNumber('Quantidade de linhas por Tela', 'qtde_linhas_tela', 0)}
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </Box>
    );
  }

}
