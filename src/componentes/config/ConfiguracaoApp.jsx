import React from 'react';
import ConfiguracaoLib from '../../lib_frontend/componentes/config/ConfiguracaoLib.jsx';
import ConfigGeral from './ConfigGeral.jsx';
import ConfigUsuario from './ConfigUsuario.jsx';

/**
 * Componente de configurações específicas do App. Esse componente utiliza
 * a tela de configuração padrão da lib, passando apenas as configurações
 * específicas da aplicação. as Configurações passadas serão mostradas 
 * na tela.
 */
export default class ConfiguracaoApp extends React.Component {

  render() {
    return (
      /* É possível ver que passamos aqui o "tipo" das configurações.
        Esse tipo é onde as configurações daquele bloco serão salvas.
        tipo="config" as configs do bloco serão salvas na tabela de config.
        tipo="usuario" as configs do bloco serão salvas na tabela de config de usuários */
      <ConfiguracaoLib configs={[{
        componente: ConfigGeral,
        tipo: 'configs',
      }, {
        componente: ConfigUsuario,
        tipo: 'usuario',
      }]} />
    );
  }
}
