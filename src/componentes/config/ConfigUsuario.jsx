import React from 'react';
import Box from '../../lib_frontend/componentes/Box.jsx';

/**
 * Configurações específicas do usuário que está logado no momento.
 */
export default class ConfigUsuario extends React.Component {

  render() {
    const { checkbox } = this.props;
    return (
      <Box collapsible title="Preferências do Usuário" className="m-0">
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="col-sm-12">
          {checkbox('Usar layout branco?', 'status_usa_layout_branco', 'inline mr-10')}
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </Box>
    );
  }

}