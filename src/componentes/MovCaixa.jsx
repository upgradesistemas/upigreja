import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import Api from '../lib_frontend/main/Api.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { Table, Column } from '../lib_frontend/componentes/Table.jsx';
import { Aba, PageControl } from '../lib_frontend/componentes/Aba.jsx';
import { formataDinheiro } from '../utils/funcoesFront';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import InputDate from '../lib_frontend/componentes/InputDate.jsx';
import { UpBlocoValores } from '../lib_frontend/componentes/UpBlocoValores.jsx';
import ModalCaixa from './ModalCaixa.jsx';

let Logada;
let Caixa;
let ValorInicialCaixa;
let codigoCliente;

class MovCaixa extends React.Component {

  constructor(props) {
    super(props);
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    ValorInicialCaixa = props.usuario.caixa ? props.usuario.caixa.vr_inicial : 0;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    registros: {},
    totais: {},
    cod_departamento: '',
    cod_pessoa: '',
    dtInicio: '',
    dtFim: moment(new Date()).format('DD/MM/YYYY HH:mm'),
  }

  // ------------------------------------------------------------------------------------

  async componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    } else {
      await this.getdataMes();
      await this.getRegistros();
    }
  }

  // ------------------------------------------------------------------------------------

  async getdataMes() {
    const data = await Api('dataMes');
    if (data.status) {
      await this.setState({
        dtInicio: moment(data.dados.Primeira).format('DD/MM/YYYY HH:mm'),
        dtFim: moment(data.dados.Ultima).format('DD/MM/YYYY HH:mm'),
      });
    } else {
      console.log(data);
    }
  }

  // ------------------------------------------------------------------------------------

  async getRegistros() {
    GenericUtils.setElementoCarregando(this.table, true);
    try {
      let dep = this.state.cod_departamento || '';
      let pessoa = this.state.cod_pessoa || '';
      let dtInicio = this.state.dtInicio || '';
      let dtFim = this.state.dtFim || '';

      dep = (Number(dep) === 0) ? '' : dep;
      pessoa = (Number(pessoa) === 0) ? '' : pessoa;
      dtInicio = (Number(dtInicio) === 0) ? '' : dtInicio;
      dtFim = (Number(dtFim) === 0) ? '' : dtFim;

      const ret = await Api('buscarDados', 'Caixa', {
        dep, pessoa, dtInicio, dtFim, Igreja: Logada, codigoCliente, Caixa,
      });

      if (ret.status) {
        for (const registro of ret.dados.query) {
          // registro.dt_movimento = formatar(registro.dt_movimento, 'DD/MM/YYYY');
          registro.dt_movimento = (moment(registro.dt_movimento)
            .format('DD/MM/YYYY HH:mm:ss'));
          registro.vr_receber = formataDinheiro(registro.vr_receber);
          registro.vr_recebido = formataDinheiro(registro.vr_recebido);
          registro.vr_pagar = formataDinheiro(registro.vr_pagar);
          registro.vr_pago = formataDinheiro(registro.vr_pago);
          registro.vr_credito = formataDinheiro(registro.vr_credito);
          registro.vr_debito = formataDinheiro(registro.vr_debito);
          registro.vr_saldo = formataDinheiro(registro.vr_saldo);
        }

        // Ordenando os registros por data Decrescente
        ret.dados.query.sort((a, b) => {
          // eslint-disable-next-line
          return a.dt_movimento < b.dt_movimento ? 1 : a.dt_movimento > b.dt_movimento ? -1 : 0;
        });

        ret.dados.total.vr_saldo += ValorInicialCaixa;
        await this.setState({
          registros: ret.dados.query,
          totais: ret.dados.total,
        });
      }
    } finally {
      GenericUtils.setElementoCarregando(this.table, false);
    }
  }

  // ------------------------------------------------------------------------------------

  async setValorRegistro(nome, valor) {
    // if (this.props.onAlteracaoCampo) {
    //   valor = this.props.onAlteracaoCampo(this.state.registro, nome, valor);
    // }
    await this.setState({
      ...this.state.registro,
      [nome]: valor,
    });

    await this.getRegistros();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o componente padrão de registros da Upgrade.
   */
  upPanel(label, campo, tabela, chave, desc, where) {
    return (
      <UpPanel label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        value={this.state[campo]}
        onChange={e => this.setValorRegistro(campo, e)} />
      // ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  inputDate(label, campo, disabled) {
    return (
      <InputDate value={this.state[campo]}
        label={label}
        onChange={e => this.setValorRegistro(campo, e.target.value)}
        disabled={disabled} />
      // ref={this.tratarConfigCampos.bind(this, campo)} />
    );
  }

  // ------------------------------------------------------------------------------------

  renderCampos() {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            {this.upPanel('Departamento:', 'cod_departamento', 'tab_departamento',
              'cod_departamento', 'nome_departamento', 'cod_igreja = ' + Logada)}
          </div>
          <div className="col-sm-4">
            {this.upPanel('Pessoa:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'cod_igreja = ' + Logada)}
          </div>
          <div className="col-sm-2">
            {this.inputDate('Dt. Início:', 'dtInicio')}
          </div>
          <div className="col-sm-2">
            {this.inputDate('Dt. Fim:', 'dtFim')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderForm() {
    return (
      <React.Fragment>
        <PageControl>
          <Aba label="- Movimentação"
            icone="usd">
            <div className="cadastro-table-container">
              {this.renderCampos()}
              <div>
                <Table className="h-100"
                  header="Registros"
                  ref={e => this.table = e}
                  data={this.state.registros}
                  actions={{}}>

                  {/* <Column key={1} field="codigo" header="Código" /> */}
                  <Column key={2} field="tipo_movimentacao" header="Movimentação" />
                  <Column key={3} field="tipo_movimento" header="Movimento" />
                  <Column key={4} field="dt_movimento" header="Dt. Movimentação" />
                  <Column key={5} field="vr_recebido" header="Vr. Recebido" />
                  <Column key={6} field="vr_pago" header="Vr. Pago" />
                </Table>
                <ModalCaixa ref={e => this.ModalCaixa = e} />
              </div>
            </div>
          </Aba>
        </PageControl>
        {this.renderFooter()}
      </React.Fragment>
    );
  }

  // ------------------------------------------------------------------------------------

  renderFooter() {
    const credito = this.state.totais.vr_credito;
    const debito = this.state.totais.vr_debito;
    const saldo = this.state.totais.vr_saldo;

    return (
      <div className="row">
        <UpBlocoValores cor="green" titulo="CRÉDITO" icone="bank" >
          {formataDinheiro(credito)}
        </UpBlocoValores>
        <UpBlocoValores cor="red" titulo="DÉBITO" icone="credit-card" >
          {formataDinheiro(debito)}
        </UpBlocoValores>
        <UpBlocoValores cor="blue" titulo="SALDO" icone="money" >
          {formataDinheiro(saldo)}
        </UpBlocoValores>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="cadastro movimentacao h-100">
        <ContentWrapper title="Movimentação do Caixa"
          small="Créditos e Débitos">
          {this.renderForm()}
        </ContentWrapper>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(MovCaixa);
