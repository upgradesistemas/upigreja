import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import { PageControl, Aba } from '../lib_frontend/componentes/Aba';
import Cadastro from './Cadastro';
import ModalMudarSenhaFunc from './ModalMudarSenhaFunc';
import Api from '../lib_frontend/main/Api';
import UtilPermissaoTelas from './UtilPermissaoTelas.jsx';
import ComboList from '../lib_frontend/componentes/ComboList.jsx';
import Button from '../lib_frontend/componentes/Button.jsx';
import ModalAssociativa from '../lib_frontend/componentes/ModalAssociativa.jsx';
import ModalVisualizaInformacoes from './ModalVisualizaInformacoes.jsx';
import { trocarRota } from '../utils/funcoesFront';

let Igreja;
let Nivel;
let Logada;
let codigoCliente;

class CadastroPessoa extends React.Component {

  constructor(props) {
    super(props);
    Nivel = props.usuario.nivel;
    Igreja = props.usuario.cod_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    perfis: [],
  };

  // ------------------------------------------------------------------------------------

  async componentWillMount() {
    // Retorna a lista de Perfis:
    const ret = await Api('getDadosCadPessoa', codigoCliente);
    this.setState(ret.dados);
  }

  // ------------------------------------------------------------------------------------

  onExitCampo(registro, campo) {
    if (campo === 'cep') {
      this.buscarCep(registro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_pessoa;
  }

  // ------------------------------------------------------------------------------------

  getValidacoes(registro) {
    const fornecedor = registro.status_fornecedor === 'S';
    const visitante = registro.status_membro === 'V';
    const funcionario = registro.status_funcionario === 'S';
    const usuario = registro.status_usuario === 'S';
    if (fornecedor && visitante) {
      toastr.error('Erro!',
        'O Cadastro atual não pode ser Visitante e Fornecedor ao mesmo tempo',
        { attention: true });
      return false;
    }
    if (funcionario && visitante) {
      toastr.error('Erro!',
        'O Cadastro atual não pode ser Visitante e Funcionario ao mesmo tempo',
        { attention: true });
      return false;
    }
    if (usuario && visitante) {
      toastr.error('Erro!',
        'O Cadastro atual não pode ser Visitante e Usuário ao mesmo tempo',
        { attention: true });
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta o perfil nessa pessoa.
   * @param {Pessoa} registro a pessoa sendo editada/incluida.
   * @param {String|Number} codPerfil O Código do perfil.
   */
  setPerfil(registro, codPerfil) {
    try {
      registro.cod_perfil = codPerfil;
      const perfil = this.state.perfis.find(x => Number(x.cod_perfil) === Number(codPerfil));
      if (!perfil) {
        registro.permissoes = [];
        toastr.info('Feito!', 'O Perfil foi removido do cliente.');
        return;
      }
      registro.permissoes = perfil.permissoes;
      toastr.info('Feito!', 'O Perfil foi associado ao cliente.');
    } finally {
      this.forceUpdate();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    this.setState({ aba: 'abaDados' });
    return {
      cod_pessoa: null,
      nome_pessoa: '',
      cod_igreja: Igreja,
      status_ativo: 'S',
      status_disciplina: 'N',
      status_membro: 'M',
      status_pertence_sede: 'N',
      status_lider: 'N',
      status_professor: 'N',
      dt_cadastro: moment(new Date()).format('MM/DD/YYYY HH:mm'),
      dt_nascimento: '',
      mae: '',
      pai: '',
      endereco: '',
      numero: '',
      cod_cidade: 0,
      cod_bairro: 0,
      tel_1: '',
      tel_2: '',
      tel_3: '',
      email: '',
      natural_cod_cidade: 0,
      cpf: '',
      rg: '',
      orgao_expedidor: '',
      dt_batismo_agua: '',
      dt_batismo_espirito_santo: '',
      cod_estado_civil: 0,
      conjuge: '',
      dt_casamento: '',
      permissoes: [],
      cod_sexo: 0,
      status_fornecedor: 'N',
      status_usuario: 'N',
      status_funcionario: 'N',
      cod_ministerio: 0,
      cod_profissao: 0,
      salario: 0,
      apelido: '',
      carga_horaria: 0,
      usuario: '',
      cod_nivel: 0,
      dt_admisao: '',
      dt_demissao: '',
      cod_banco: 0,
      forn_razao_social: '',
      forn_cnpj: '',
      forn_inscricao_estadual: '',
      forn_representante: '',
      tel_1_representante: '',
      tel_2_representante: '',
      tel_3_representante: '',
      dt_conversao: '',
      tp_conversao: '',
      igreja_procedencia: '',
      dt_membresia: '',
      cod_adesao: 0,
      status_batismo_es: 'N',
      status_dizimista: 'N',
      dt_disciplina_inicial: '',
      dt_disciplina_final: '',
      cep: '',
      complemento: '',
      cod_escolaridade: 0,
      status_cabeca_familia: 'N',
      status_conjuge_membro: 'N',
      qtde_filhos: 0,
      cod_status: 0,
      status_capelao: 'N',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  async buscarCep(registro) {
    const endereco = await Api('buscarCep', codigoCliente, registro.cep);
    if (endereco.status) {
      registro.cod_bairro = endereco.dados.bairro;
      registro.cod_cidade = endereco.dados.cidade;
      registro.endereco = endereco.dados.logradouro;
      registro.complemento = endereco.dados.complemento;
    } else {
      toastr.error('Erro!', `O CEP é inválido. ${endereco.erro}`, { attention: true });
      registro.cep = '';
    }
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  async imprimirFichaCadastral() {
    const rel = await Api('gerarRelatorio', {
      codigoCliente,
      nomeRelatorio: 'Ficha Cadastral',
    });
    if (rel.status) {
      const relat = window.location.origin + rel.dados;
      trocarRota(relat, '_blank');
      toastr.success('Relatorio', 'O Relatório foi gerado com sucesso!');
    } else {
      toastr.error('Erro!', 'Relatório não encontrado', { attention: true });
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    registro.cod_perfil = registro.cod_perfil || 0;
    const PermissaoTela = Nivel.permite_permissao_tela === 'S';
    const paramsCargo = {
      label1: 'Cargo',
      tabela: 'tab_cargo_pessoa',
      pkNome: 'cod_pessoa',
      pkAtual: registro.cod_pessoa,
      pkAssociNome: 'cod_cargo',
      codBusca: 'cod_cargo',
      coluna: 'nome_cargo',
      tabela2: 'tab_cargo',
    };
    const paramsCurso = {
      label1: 'Curso',
      tabela: 'tab_curso_pessoa',
      pkNome: 'cod_pessoa',
      pkAtual: registro.cod_pessoa,
      pkAssociNome: 'cod_curso',
      codBusca: 'cod_curso',
      coluna: 'nome_curso_comp',
      tabela2: 'tab_curso',
    };
    const paramsFamilia = {
      label1: 'Membro',
      tabela: 'tab_grau_parentesco_pessoa',
      pkNome: 'cod_pessoa',
      pkAtual: registro.cod_pessoa,
      pkAssociNome: 'cod_parentesco_pessoa',
      codBusca: 'cod_pessoa',
      coluna: 'nome_pessoa',
      label2: 'Grau de Parentesco',
      tabela2: 'tab_pessoa',
      codBusca2: 'cod_grau_parentesco',
      coluna2: 'nome_grau_parentesco',
      tabela3: 'tab_grau_parentesco',
    };
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-12">

            <div className="col-sm-5">
              <RadioGroup title="Opções" className="mt-10">
                <div className="row">
                  <br />
                  <div className="col-sm-12 col-xs-12">
                    {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                    {componentes.checkbox('Fornecedor', 'status_fornecedor', 'inline mr-10')}
                    {componentes.checkbox('Usuário', 'status_usuario', 'inline mr-10')}
                    {componentes.checkbox('Funcionário', 'status_funcionario', 'inline mr-10')}
                  </div>
                </div>
              </RadioGroup>
            </div>

            <div className="col-sm-7">
              <RadioGroup title="Membro / Visitante" className="mt-10">
                <div className="row">
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Nenhum', 'status_membro', 'N')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Membro', 'status_membro', 'M')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Visitante', 'status_membro', 'V')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Congregado', 'status_membro', 'C')}
                  </div>
                  <div className="col-sm-2 col-xs-2">
                    {componentes.radioButton('Simpatizante', 'status_membro', 'S')}
                  </div>
                </div>
              </RadioGroup>
            </div>

          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ============================== PAGE CONTROL 1 ============================== */}
        <PageControl aba={this.state.aba} onChange={e => this.setState({ aba: e })}>
          {/* ============================== ABA ============================== */}
          <Aba key="abaDados" icone="book" label="Dados">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-5">
                {componentes.input('Nome:', 'nome_pessoa')}
              </div>
              <div className="col-sm-2">
                {componentes.input('Apelido:', 'apelido')}
              </div>
              <div className="col-sm-3">
                {componentes.upPanel('Profissão:', 'cod_profissao', 'tab_profissao',
                  'cod_profissao', 'nome_profissao')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Dt. Nascimento:', 'dt_nascimento')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Igreja:', 'cod_igreja', 'tab_igreja',
                  'cod_igreja', 'nome_igreja')}
              </div>
              <div className="col-sm-3">
                {componentes.inputMask('CPF:', 'cpf', { cpf: true })}
              </div>
              <div className="col-sm-3">
                {componentes.input('RG:', 'rg')}
              </div>
              <div className="col-sm-2">
                {componentes.input('Orgão Expedidor:', 'orgao_expedidor')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.inputMask('CEP:', 'cep', { cep: true })}
              </div>
              <div className="col-sm-5">
                {componentes.input('Endereço:', 'endereco')}
              </div>
              <div className="col-sm-2">
                {componentes.input('Número:', 'numero')}
              </div>
              <div className="col-sm-3">
                {componentes.input('Complemento:', 'complemento')}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}

            {/* ------------------------- LINHA 4 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Cidade:', 'cod_cidade', 'tab_cidade',
                  'cod_cidade', 'nome_cidade_comp', '', true)}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Bairro:', 'cod_bairro', 'tab_bairro',
                  'cod_bairro', 'nome_bairro')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Nasceu em:', 'natural_cod_cidade', 'tab_cidade',
                  'cod_cidade', 'nome_cidade_comp')}
              </div>
            </div>
            {/* ------------------------- LINHA 4 ------------------------- */}

            {/* ------------------------- LINHA 5 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.inputMask('Telefone:', 'tel_1', { telefone: true })}
              </div>
              <div className="col-sm-4">
                {componentes.inputMask('Celular 1:', 'tel_2', { celular: true })}
              </div>
              <div className="col-sm-4">
                {componentes.inputMask('Celular 2:', 'tel_3', { celular: true })}
              </div>
            </div>
            {/* ------------------------- LINHA 5 ------------------------- */}

            {/* ------------------------- LINHA 6 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.input('Email:', 'email')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Sexo:', 'cod_sexo', 'tab_sexo', 'cod_sexo', 'nome_sexo')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Estado Civil:', 'cod_estado_civil', 'tab_estado_civil',
                  'cod_estado_civil', 'nome_estado_civil')}
              </div>
            </div>
            {/* ------------------------- LINHA 6 ------------------------- */}

            {/* ------------------------- LINHA 7 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Escolaridade:', 'cod_escolaridade', 'tab_escolaridade',
                  'cod_escolaridade', 'nome_escolaridade')}
              </div>
            </div>
            {/* ------------------------- LINHA 7 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaFamiliar" icone="book" label="Dados Familiares" >
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-12">
                <RadioGroup className="flex mt-10">
                  {componentes.checkbox('Cabeça da Família', 'status_cabeca_familia',
                    'inline mr-10')}
                  {componentes.checkbox('Conjuge é Membro', 'status_conjuge_membro',
                    'inline mr-10')}
                </RadioGroup>
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-6">
                {componentes.input('Mãe:', 'mae')}
              </div>
              <div className="col-sm-6">
                {componentes.input('Pai:', 'pai')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-5">
                {componentes.input('Cônjuge:', 'conjuge')}
              </div>
              <div className="col-sm-3">
                {componentes.inputDate('Data Casamento:', 'dt_casamento')}
              </div>
              <div className="col-sm-2">
                {componentes.inputNumber('Qtde Filhos:', 'qtde_filhos', 0)}
              </div>

              <div className="botao-associativa">
                <Button className='btn-info'
                  label='Família'
                  icon='fa-list-ol'
                  disabled={!paramsFamilia.pkAtual}
                  onClick={() =>
                    this.modalAssociativaFamilia.getWrappedInstance().show(paramsFamilia)} />
              </div>

            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaMembro" icone="book" label="Dados Membro"
            disabled={(registro.status_membro !== 'M') && (registro.status_membro !== 'C')}>
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-12">
                <RadioGroup className="flex mt-10">
                  {componentes.checkbox('Líder', 'status_lider', 'inline mr-10')}
                  {componentes.checkbox('Professor', 'status_professor', 'inline mr-10')}
                  {componentes.checkbox('Disciplina', 'status_disciplina', 'inline mr-10')}
                  {componentes.checkbox('Batismo Es. Santo', 'status_batismo_es', 'inline mr-10')}
                  {componentes.checkbox('Dizimista', 'status_dizimista', 'inline mr-10')}
                  {componentes.checkbox('Capelão', 'status_capelao', 'inline mr-10')}

                </RadioGroup>
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-3">
                {componentes.upPanel('Condição do Membro:', 'cod_status', 'tab_status',
                  'cod_status', 'nome_status')}
              </div>
              <div className="col-sm-3">
                {componentes.inputDate('Data Batismo Água:', 'dt_batismo_agua')}
              </div>
              <div className="col-sm-3">
                {componentes.inputDate('Data Batismo Esp. Santo', 'dt_batismo_espirito_santo')}
              </div>
              <div className="col-sm-3">
                {componentes.upPanel('Ministério:', 'cod_ministerio', 'tab_ministerio',
                  'cod_ministerio', 'nome_ministerio')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.inputDate('Data Conversão:', 'dt_conversao')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Membro Desde', 'dt_membresia')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Como se converteu:', 'tp_conversao')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Igreja Procedência:', 'igreja_procedencia')}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}

            {/* ------------------------- LINHA 4 ------------------------- */}
            <div className="row">
              <div className="col-sm-3">
                {componentes.upPanel('Tipo de Adesão:', 'cod_adesao', 'tab_adesao',
                  'cod_adesao', 'nome_adesao')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Disciplina Início:', 'dt_disciplina_inicial',
                  registro.status_disciplina === 'N')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Disciplina Final', 'dt_disciplina_final',
                  registro.status_disciplina === 'N')}
              </div>

              <div className="botao-associativa">
                <Button className='btn-info'
                  label='Cargos'
                  icon='fa-list-ol'
                  disabled={!paramsCargo.pkAtual}
                  onClick={() =>
                    this.modalAssociativaCargo.getWrappedInstance().show(paramsCargo)} />

                <Button className='btn-info'
                  label='Cursos'
                  icon='fa-list-ol'
                  disabled={!paramsCurso.pkAtual}
                  onClick={() =>
                    this.modalAssociativaCurso.getWrappedInstance().show(paramsCurso)} />
              </div>

            </div>
            {/* ------------------------- LINHA 4 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaFuncionario" icone="book" label="Funcionario"
            disabled={registro.status_funcionario === 'N'}>
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.input('Função:', 'funcao')}
              </div>
              <div className="col-sm-4">
                {componentes.inputCalc('Salário(R$):', 'salario')}
              </div>
              <div className="col-sm-4">
                {componentes.inputNumber('Carga Horária(Hr):', 'carga_horaria')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.inputDate('Data de Admissão:', 'dt_admissao')}
              </div>
              <div className="col-sm-4">
                {componentes.inputDate('Data de Demissão:', 'dt_demissao', true)}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Banco:', 'cod_banco', 'tab_nivel', 'cod_banco', 'nome_banco')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.input('Número da Agência:', 'num_agencia')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Número da Conta:', 'num_conta_corrente')}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaUsuario" icone="book" label="Usuario"
            disabled={registro.status_usuario === 'N'}>
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.input('Chave de Acesso:', 'usuario')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Nível de Acesso:', 'cod_nivel', 'tab_nivel',
                  'cod_nivel', 'nome_nivel')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaFornecedor" icone="book" label="Fornecedor"
            disabled={registro.status_fornecedor === 'N'}>
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-3">
                {componentes.input('Razão Social:', 'forn_razao_social')}
              </div>
              <div className="col-sm-3">
                {componentes.inputMask('CNPJ:', 'forn_cnpj', { cnpj: true })}
              </div>
              <div className="col-sm-2">
                {componentes.input('Inscrição Estadual:', 'forn_inscricao_estadual')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Nome do Representante:', 'forn_representante')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-3">
                {componentes.inputMask('Telefone Repres.:', 'tel_3_representante',
                  { telefone: true })}
              </div>
              <div className="col-sm-3">
                {componentes.inputMask('Celular 1 Repres.:', 'tel_1_representante',
                  { celular: true })}
              </div>
              <div className="col-sm-3">
                {componentes.inputMask('Celular 2 Repres.:', 'tel_2_representante',
                  { celular: true })}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaPermissaoTela" icone="book" label="Permissão Tela"
            disabled={!PermissaoTela || registro.status_usuario === 'N'}>

            {/* Foi utilizado o combo da maneira antiga aqui 
              porque eu precisava saber exatamente quando o valor
              era setado para aplicar nas permissões desse usuário: */}
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="mb-10">
              <ComboList value={registro.cod_perfil}
                lista={this.state.perfis}
                campo="nome_perfil"
                label="Perfil:"
                valor="cod_perfil"
                defaultText="NENHUM"
                onChange={this.setPerfil.bind(this, registro)} />
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            <UtilPermissaoTelas value={registro.permissoes} />
          </Aba>
          {/* ============================== ABA ============================== */}
        </PageControl>
        {/* ============================== PAGE CONTROL 1 ============================== */}
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const actions = [{
      className: 'btn-warning',
      icone: 'fa-lock',
      label: 'Mudar senha',
      id: 'mudar-senha',
      onClick: (pessoa) => this.modalMudarSenhaFunc.getWrappedInstance().show(pessoa.cod_pessoa),
      disabled: (pessoa) => !pessoa || (pessoa && pessoa.status_usuario === 'N'),
    },
    {
      className: 'btn-warning',
      icone: 'fa-print',
      label: 'Ficha Cadastral',
      id: 'ficha-cadastral',
      onClick: () => this.imprimirFichaCadastral(),
      disabled: false,
    },
    {
      className: 'btn-info',
      icone: 'fa-search',
      label: 'Visualizar Informações',
      id: 'visualizar-informacoes',
      onClick: (pessoa) => {
        const registro = {};
        registro.codPk = pessoa.cod_pessoa;
        registro.tabela = 'tab_pessoa';
        registro.nomeCodPk = 'cod_pessoa';
        registro.nomeTela = 'Informações da Pessoa';
        registro.campos = `nome_pessoa Nome, endereco Endereço,
          CONCAT("(", SUBSTR(tel_2,1, 2), ")", " ", SUBSTR(tel_2,3, 5), "-", SUBSTR(tel_2,8))
          Telefone, IF(status_ativo = "S", "Sim", "Não") Ativo,
          IF(status_membro = "M", "Membro", IF(status_membro = "V", "Visitante", 
          IF(status_membro = "C", "Congregago", IF(status_membro = "S", "Simpatizante", "Nenhum"
          )))) Ministério, DATE_FORMAT(dt_batismo_agua,"%d/%m/%Y") "Batismo nas águas"`;
        this.ModalVisualizaInformacoesPessoa.getWrappedInstance().show(registro);
      },
      disabled: (pessoa) => !pessoa,
    }];

    return (
      <div>
        <Cadastro tabela="tab_pessoa"
          titulo="Cadastro de Pessoas"
          subTitulo={'Configure os Fornecedores, Funcionários, ' +
            'Usuários e os Membros de sua Igreja!'}

          paginavel
          // onAlteracaoCampo={this.onAlteracaoCampo.bind(this)}
          onExitCampo={this.onExitCampo.bind(this)}
          renderForm={this.renderForm.bind(this)}
          actions={actions}
          getValidacoes={this.getValidacoes.bind(this)}
          getNomeRegistro={this.getNomeRegistro.bind(this)}
          excluirDesabilitado={(p) => p.cod_nivel === 1}
          where={'cod_igreja = ' + Logada}

          getRegistro={this.getRegistro.bind(this)}>
          <Column width="10%" field="cod_pessoa" header="Código" />
          <Column field="nome_pessoa" header="Membro" />
          <Column field="enderecoCompleto" header="Endereço" />
          <Column width="20%" field="telefones" header="Telefone(s)" />
          <Column width="10%" field="ministerio" header="Ministério" />
          <Column width="10%" field="status_a" header="Ativo" />
        </Cadastro>

        <ModalMudarSenhaFunc ref={e => this.modalMudarSenhaFunc = e} />

        <ModalVisualizaInformacoes ref={e => this.ModalVisualizaInformacoesPessoa = e} />

        <ModalAssociativa ref={e => this.modalAssociativaCargo = e}>
          Cargos do Membro </ModalAssociativa>

        <ModalAssociativa ref={e => this.modalAssociativaCurso = e}>
          Cursos do Membro </ModalAssociativa>

        <ModalAssociativa ref={e => this.modalAssociativaFamilia = e}>
          Familiares do Membro </ModalAssociativa>

      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
  redux: state,
}))(CadastroPessoa);
