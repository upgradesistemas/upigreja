import React from 'react';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';

/**
 * Cadastro padrão de setor.
 */
export default class CadastroSetor extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_setor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_setor: null,
      nome_setor: '',
      lider_setor: 0,
      cod_area: 0,
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_setor', true)}
          </div>
          <div className="col-sm-5">
            {componentes.input('Setor:', 'nome_setor')}
          </div>
          <div className="col-sm-5">
            {componentes.upPanel('Lider do Setor:', 'lider_setor', 'tab_pessoa',
              'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            {componentes.upPanel('Área do Setor:', 'cod_area', 'tab_area',
              'cod_area', 'nome_area')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_setor"
        titulo="Cadastro de Setor"
        subTitulo="Configure os seus Setores!"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        {/* <Column key={1} width="80px" field="cod_setor" header="Código" /> */}
        <Column key={2} field="nome_setor" header="Setor" />
        <Column key={2} field="pessoa.nome_pessoa" header="Líder do Setor" />
        <Column key={2} field="area.nome_area" header="Área do Setor" />
      </Cadastro>
    );
  }

}

