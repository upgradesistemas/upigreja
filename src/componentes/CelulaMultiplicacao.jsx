import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Api from '../lib_frontend/main/Api.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { Aba, PageControl } from '../lib_frontend/componentes/Aba.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import UpSelecao from '../lib_frontend/componentes/UpSelecao.jsx';
import LabeledInput from '../lib_frontend/componentes/LabeledInput.jsx';
import Button from '../lib_frontend/componentes/Button.jsx';

let codigoCliente;
let Pessoa;

class CelulaMultiplicacao extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    cod_celula_multiplicar: 0,
    nova_celula: '',
  }

  // ------------------------------------------------------------------------------------

  async setValorRegistro(nome, valor) {
    await this.setState({
      ...this.state.registro,
      [nome]: valor,
    });
  }

  // ------------------------------------------------------------------------------------

  async multiplicarCelula() {
    const nomeCelula = this.state.nova_celula;
    const ladoEsquerdo = this.UpSelecao.state.ladoEsq;
    const ladoDireito = this.UpSelecao.state.ladoDir;

    const multiplicacao = await Api('multiplicarCelula',
      nomeCelula, ladoEsquerdo, ladoDireito, Pessoa, codigoCliente);

    if (multiplicacao.dados.status) {
      await this.setState({ cod_celula_multiplicar: 0, nova_celula: '' });
      toastr.success('Multiplicação', 'Célula multiplicada com sucesso!');
    } else {
      await this.setState({ cod_celula_multiplicar: 0 });
      toastr.error('Erro ao Multiplicar', multiplicacao.dados.erro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o componente padrão de registros da Upgrade.
   */
  upPanel(label, campo, tabela, chave, desc, where) {
    return (
      <UpPanel label={label}
        tabela={tabela}
        chave={chave}
        desc={desc}
        where={where}
        value={this.state[campo]}
        onChange={e => this.setValorRegistro(campo, e)} />
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza um input com label
   */
  labeledEdit(label, campo, disabled) {
    return (
      <LabeledInput label={label}
        value={this.state[campo]}
        disabled={disabled}
        onChange={e => this.setValorRegistro(campo, e)}
        className="mb-5" />
    );
  }

  // ------------------------------------------------------------------------------------

  renderSelecao() {
    if (Number(this.state.cod_celula_multiplicar) > 0) {
      return (
        <UpSelecao ref={e => this.UpSelecao = e}
          cod_celula_multiplicar={this.state.cod_celula_multiplicar}>
        </UpSelecao>
      );
    }
  }

  // ------------------------------------------------------------------------------------

  renderCampos() {
    const state = this.state;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            {this.upPanel('Celula a Multiplicar:', 'cod_celula_multiplicar', 'tab_celula',
              'cod_celula', 'nome_celula')}
          </div>
          <div className="col-sm-5">
            {this.labeledEdit('Nome da nova célula:', 'nova_celula')}
          </div>
          <div className="col-sm-2" style={{ paddingTop: '24px' }}>
            <Button className="btn-success"
              label="Multiplicar"
              onClick={() => this.multiplicarCelula()}
              disabled={state.nova_celula === '' | Number(state.cod_celula_multiplicar) === 0}
              icon="fa-check" />
          </div>
          {this.renderSelecao()}
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="cadastro movimentacao h-100">
        <ContentWrapper title="Multiplicação de Célula"
          small="">
          <PageControl>
            <Aba label="- Multiplicação"
              icone="users">
              <div className="cadastro-table-container">
                {this.renderCampos()}
              </div>
            </Aba>
          </PageControl>
        </ContentWrapper>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(CelulaMultiplicacao);
