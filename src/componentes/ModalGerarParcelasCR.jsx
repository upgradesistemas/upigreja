import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import moment from 'moment';
import { Table, Column } from '../lib_frontend/componentes/Table.jsx';
import Modal from '../lib_frontend/componentes/Modal';
import LabeledInput from '../lib_frontend/componentes/LabeledInput';
import InputDate from '../lib_frontend/componentes/InputDate.jsx';
import InputCalc from '../lib_frontend/componentes/InputCalc.jsx';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import Api from '../lib_frontend/main/Api.jsx';

let Pessoa;
let Igreja;
let Caixa;
let codigoCliente;

class ModalGerarParcelasCR extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    Igreja = props.usuario.cod_igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    intervalo: 30,
    qtde: 0,
    dtInicio: moment(new Date()).format('DD/MM/YYYY'),
    valor: 0,
    codPessoa: 0,
    parcelas: [],
  };

  // ------------------------------------------------------------------------------------

  onChangeCampo(campo, valor) {
    let valorReal = valor.target ? valor.target.value : valor;

    if (campo === 'qtde') {
      if (Number(valorReal) > 100) {
        valorReal = 100;
      } else if (Number(valorReal) < 0) {
        valorReal = 0;
      }
    }

    this.setState({ [campo]: valorReal }, () => {
      if (campo === 'valor' || campo === 'intervalo' || campo === 'dtInicio') {
        this.recalcularParcelasExistentes();
      } else if (campo === 'qtde') {
        this.gerarParcelas();
      }
    });
  }

  // ------------------------------------------------------------------------------------

  gerarParcelas() {
    const { dtInicio, intervalo, qtde } = this.state;
    const parcelas = [];
    const dtInicioDate = moment(dtInicio, 'DD/MM/YYYY').toDate();
    for (let i = 0; i < qtde; i++) {
      parcelas.push(this.criarParcela(i));
      dtInicioDate.setDate(dtInicioDate.getDate() + Number(intervalo));
    }
    this.setState({ parcelas });
  }

  // ------------------------------------------------------------------------------------

  recalcularParcelasExistentes() {
    const { dtInicio, parcelas, intervalo } = this.state;
    const dtInicioDate = moment(dtInicio, 'DD/MM/YYYY').toDate();
    for (let i = 0; i < parcelas.length; i++) {
      parcelas[i] = this.criarParcela(i);
      dtInicioDate.setDate(dtInicioDate.getDate() + Number(intervalo));
    }
    this.setState({ parcelas: [...this.state.parcelas] });
  }

  // ------------------------------------------------------------------------------------

  criarParcela(indice) {
    const { dtInicio, intervalo, valor, qtde } = this.state;
    const valorPorParcela = (valor / qtde).toFixed(2); // toFixed - Qtde Casas decimais
    const dtInicioDate = moment(dtInicio, 'DD/MM/YYYY').toDate();
    dtInicioDate.setDate(dtInicioDate.getDate() + (Number(intervalo) * indice));
    return {
      numero: indice + 1,
      valor: valorPorParcela,
      valor_formatado: 'R$ ' + valorPorParcela,
      data: moment(dtInicioDate).format('DD/MM/YYYY'),
      cod_igreja: Igreja,
    };
  }

  // ------------------------------------------------------------------------------------

  async confirmar() {
    const { parcelas, valor, codPessoa } = this.state;
    if (!codPessoa) {
      // this.panelPessoa.validarCorInput();
      toastr.error('Atenção!', 'Preencha a pessoa!', { attention: true });
      return false;
    }
    if (!valor) {
      this.valor.focus();
      toastr.error('Atenção!', 'Informe um valor!', { attention: true });
      return false;
    }
    if (!parcelas.length) {
      this.parcela.getWrappedInstance().focus();
      toastr.error('Atenção!', 'Gere pelo menos uma parcela!', { attention: true });
      return false;
    }

    const ret = await Api('gerarParcelas', parcelas, codPessoa, Pessoa, Caixa, 'CR', codigoCliente);
    if (!ret.status) {
      toastr.error('Erro!', ret.erro, { attention: true });
      return false;
    }
    this.modal.hide();
    if (this.props.onClose) {
      this.props.onClose();
    }
    this.limpaCampos();
  }

  // ------------------------------------------------------------------------------------

  limpaCampos() {
    this.setState({
      intervalo: 30,
      qtde: 0,
      dtInicio: moment(new Date()).format('DD/MM/YYYY'),
      valor: 0,
      codPessoa: 0,
      parcelas: [],
    });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  async show() {
    Caixa = await this.props.usuario.caixa ? this.props.usuario.caixa.cod_caixa : null;
    this.modal.show();
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Gravar parcelas',
      icon: 'fa-save',
      onClick: this.confirmar.bind(this),
      disabled: this.state.realizandoBaixa,
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      disabled: this.state.realizandoBaixa,
      dismiss: true,
    }];
    const where = 'cod_igreja = ' + Igreja;

    return (
      <Modal title="Gerar parcelas de contas a receber"
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="modal-gerar-parcelas p-10">

          <div className="row">
            <div className="col-sm-12">
              <UpPanel label="Pessoa:"
                tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                where={where}
                ref={e => this.panelPessoa = e}
                value={this.state.codPessoa}
                onChange={e => this.setState({ codPessoa: e })} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-4">
              <InputDate label="Data de ínicio:"
                value={this.state.dtInicio}
                onChange={this.onChangeCampo.bind(this, 'dtInicio')} />
            </div>
            <div className="col-sm-3">
              <InputCalc label="Valor total:"
                ref={e => this.valor = e}
                value={this.state.valor}
                onChange={this.onChangeCampo.bind(this, 'valor')} />
            </div>
            <div className="col-sm-3">
              <LabeledInput label="Intervalo (dias):"
                type="number"
                value={this.state.intervalo}
                onChange={this.onChangeCampo.bind(this, 'intervalo')} />
            </div>
            <div className="col-sm-2">
              <LabeledInput label="Parcelas:"
                type="number"
                min="0"
                max="100"
                value={this.state.qtde}
                inputRef={e => this.parcela = e}
                onChange={this.onChangeCampo.bind(this, 'qtde')} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <Table header="Parcelas" data={this.state.parcelas} actions={[]}>
                <Column key={0} width="80px" field="numero" header="Nº" />
                <Column key={1} field="valor_formatado" header="Valor" />
                <Column key={2} field="data" header="Data" />
              </Table>
            </div>
          </div>
        </div>

      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalGerarParcelasCR);
