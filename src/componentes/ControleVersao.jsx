import React from 'react';
import { connect } from 'react-redux';
import Api from '../lib_frontend/main/Api.jsx';
import ContentWrapper from '../lib_frontend/componentes/ContentWrapper.jsx';
import { Aba, PageControl } from '../lib_frontend/componentes/Aba.jsx';
import Button from '../lib_frontend/componentes/Button.jsx';
import InputTextArea from '../lib_frontend/componentes/InputTextArea.jsx';

const Pull = 'D:\\Upgrade Sistemas\\upgestao\\servico\\pull.bat';
const Build = 'D:\\Upgrade Sistemas\\upgestao\\servico\\build.bat';
const Parar = 'D:\\Upgrade Sistemas\\upgestao\\servico\\pararServico.bat';
const Iniciar = 'D:\\Upgrade Sistemas\\upgestao\\servico\\iniciarServico.bat';
const Reiniciar = 'D:\\Upgrade Sistemas\\upgestao\\servico\\reiniciarServico.bat';

class ControleVersao extends React.Component {

  state = {
    campo: '',
  }

  // ------------------------------------------------------------------------------------

  async executar(caminho) {
    const retorno = await Api('executarBat', caminho);
    this.setState({ campo: retorno.dados });
    console.log(retorno);
  }

  // ------------------------------------------------------------------------------------

  renderCampos() {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">

          <div className="col-xs-6 col-sm-1 col-lg-1">
            <Button label="Pull"
              icon="arrow-down"
              onClick={this.executar.bind(this, Pull)}
              className="btn-success" />
          </div>

          <div className="col-xs-6 col-sm-2 col-lg-2">
            <Button label="Build"
              icon="arrow-down"
              onClick={this.executar.bind(this, Build)}
              className="btn-success" />
          </div>

          <div className="col-xs-6 col-sm-3 col-lg-3">
            <Button label="Parar Serviço"
              icon="arrow-down"
              onClick={this.executar.bind(this, Parar)}
              className="btn-success" />
          </div>

          <div className="col-xs-6 col-sm-3 col-lg-3">
            <Button label="Iniciar Serviço"
              icon="arrow-down"
              onClick={this.executar.bind(this, Iniciar)}
              className="btn-success" />
          </div>

          <div className="col-xs-6 col-sm-3 col-lg-3">
            <Button label="Reiniciar Serviço"
              icon="arrow-down"
              onClick={this.executar.bind(this, Reiniciar)}
              className="btn-success" />
          </div>

        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">

          <div className="col-xs-12 col-sm-12 col-lg-12">
            <InputTextArea label="Retorno"
              style={{ height: '300px' }}
              value={this.state.campo}
              uppercase={false}
              className="mb-5" />
          </div>

        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  renderForm() {
    return (
      <React.Fragment>
        <PageControl>
          <Aba label=" - Versionamento Gestão"
            icone="exchange">
            <div className="cadastro-table-container">
              {this.renderCampos()}
            </div>
          </Aba>
        </PageControl>
      </React.Fragment>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div className="cadastro movimentacao h-100">
        <ContentWrapper title="Controle de Versionamento"
          small="">
          {this.renderForm()}
        </ContentWrapper>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(ControleVersao);
