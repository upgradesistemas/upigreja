import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import moment from 'moment';
import Modal from '../lib_frontend/componentes/Modal';
import UpPanel from '../lib_frontend/componentes/UpPanel.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils';
import InputCalc from '../lib_frontend/componentes/InputCalc.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import { formataDinheiro } from '../utils/funcoesFront';
import LabeledInput from '../lib_frontend/componentes/LabeledInput';

let Pessoa;
let Caixa;
let codigoCliente;

class ModalBaixarCP extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {
    // O State será o próprio objeto de contas a pagar
    cod_cp: null, // O Código do contas a pagar
    vr_pagar: 0,
    vr_pago: 0,
    vr_desconto: 0,
    vr_acrescimo: 0,
    obs_baixa: '',
    realizandoBaixa: false, // Se esta realizando a baixa no momento
    cod_forma_pagamento: 1,
    dtPagamento: moment(new Date()).format('DD/MM/YYYY'),
  };

  // ------------------------------------------------------------------------------------

  onChangeValor(campo, valor) {
    const { vr_pagar, vr_pago, vr_desconto, vr_acrescimo } = this.state;
    if (campo === 'vr_pago') {
      if (valor > ((vr_pagar + vr_acrescimo) - vr_desconto)) {
        valor = ((vr_pagar + vr_acrescimo) - vr_desconto);
      }
    } else if (campo === 'vr_desconto') {
      if (valor > vr_pagar) {
        valor = vr_pagar;
      }
      if (vr_pago > ((vr_pagar + vr_acrescimo) - valor)) {
        this.setState({ vr_pago: ((vr_pagar + vr_acrescimo) - valor) });
      }
    } else if (campo === 'vr_acrescimo') {
      if (vr_pago > ((vr_pagar + valor) - vr_desconto)) {
        this.setState({ vr_pago: ((vr_pagar + valor) - vr_desconto) });
      }
    }
    this.setState({ [campo]: valor });
  }

  // ------------------------------------------------------------------------------------

  onChangeCampo() {
  }

  // ------------------------------------------------------------------------------------

  /**
   * Fecha esse modal.
   */
  fecharModal() {
    this.props.onClose && this.props.onClose();
    this.modal.hide();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza a baixa do Contas a Pagar
   */
  async realizarBaixa() {
    this.setState({ realizandoBaixa: true });
    try {
      // Agora vamos baixar ela:
      const ret = await Api('baixarModal', Pessoa, this.state, 'CP', codigoCliente);
      if (!ret.status) {
        toastr.error('Atenção!', ret.erro, { attention: true });
        return false;
      }

      toastr.success('Tudo certo!', 'A Conta foi paga!');
      this.fecharModal();
    } finally {
      this.state = null;
      this.setState({ realizandoBaixa: false });
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   * @param {String|Number} cod_cp O Código do contas a pagar.
   */
  async show(cod_cp) {
    Caixa = await this.props.usuario.caixa ? this.props.usuario.caixa.cod_caixa : null;
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      const ret = await Api('buscarPorId', 'tab_cp', cod_cp, codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      // Mesmo que tiver algo traz vazio:
      ret.dados.obs_baixa = '';

      await this.setState({ ...ret.dados, cod_cp, cod_caixa: Caixa });
      this.modal.show();
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  render() {
    const restante =
      (this.state.vr_pagar + this.state.vr_acrescimo) -
      (this.state.vr_pago + this.state.vr_desconto);

    const botoes = [{
      className: 'btn-success',
      label: 'Realizar baixa',
      icon: 'fa-check',
      onClick: this.realizarBaixa.bind(this),
      disabled: this.state.realizandoBaixa,
    }, {
      className: 'btn-danger',
      label: 'Cancelar',
      icon: 'fa-times',
      disabled: this.state.realizandoBaixa,
      dismiss: true,
    }];
    return (
      <Modal title="Baixar contas a pagar"
        buttons={botoes}
        ref={e => this.modal = e}>
        <div className="relative p-10" ref={e => this.divPrincipal = e}>

          <div className="row">
            <div className="col-sm-12">
              <UpPanel label="Fornecedor:"
                tabela="tab_pessoa"
                chave="cod_pessoa"
                desc="nome_pessoa"
                value={this.state.cod_pessoa}
                disabled
                onChange={e => this.setState({ cod_pessoa: e })} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-3">
              <InputCalc value={this.state.vr_pagar}
                label="Valor da conta:"
                disabled
                onChange={e => this.setState({ vr_pagar: e })} />
            </div>
            <div className="col-sm-9">
              <UpPanel label="Forma de Pagamento:"
                tabela="tab_forma_pagamento"
                chave="cod_forma_pagamento"
                desc="nome_forma_pagamento"
                value={this.state.cod_forma_pagamento}
                onChange={e => this.setState({ cod_forma_pagamento: e })} />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-4">
              <InputCalc value={this.state.vr_acrescimo}
                label="Valor de Acréscimo:"
                onChange={this.onChangeValor.bind(this, 'vr_acrescimo')} />
            </div>
            <div className="col-sm-4">
              <InputCalc value={this.state.vr_desconto}
                label="Valor de Desconto:"
                onChange={this.onChangeValor.bind(this, 'vr_desconto')} />
            </div>
            <div className="col-sm-4">
              <InputCalc value={this.state.vr_pago}
                label="Valor pago:"
                onChange={this.onChangeValor.bind(this, 'vr_pago')} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <LabeledInput label="Observação do pagamento:"
                value={this.state.obs_baixa}
                onChange={e => this.setState({ obs_baixa: e })} />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <div className="mov-vr-total">
                {restante > 0 ? (
                  <div>
                    Restante: {formataDinheiro(restante)}
                    <br />
                    <span>Será gerado outra conta com esse restante</span>
                  </div>
                ) : (
                    <div>
                      Pagamento completo
                    <br />
                      <span>O Pagamento será completo</span>
                    </div>
                  )}
              </div>
            </div>
          </div>

        </div>
      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalBaixarCP);
