import React from 'react';
import moment from 'moment';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';

/**
 * Cadastro padrão de solicitacao_vis_ora.
 */
export default class SolicitacaoVisOra extends React.Component {

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.membro;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_solicitacao_vis_ora: null,
      cod_pessoa: 0,
      cod_capelao: 0,
      dt_solicitacao: moment(new Date()).format('MM/DD/YYYY HH:mm'),
      dt_agendada: moment(new Date()).format('MM/DD/YYYY HH:mm'),
      tp_solicitacao: 'O',
      status_solicitacao: 'P',
      obs_pre: '',
      obs_pos: '',
      tp_membro_solicitado: 'N',
      local_visita: 'DOM',
      grau_pedido: 'E',
      pessoa_necessitada: ''
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-6">
            <RadioGroup title="Tipo de Solicitação" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Oração', 'tp_solicitacao', 'O')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Visita', 'tp_solicitacao', 'V')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Ambas', 'tp_solicitacao', 'A')}
                </div>
              </div>
            </RadioGroup>
          </div>

          <div className="col-sm-6">
            <RadioGroup title="Status Solicitação" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Pendente', 'status_solicitacao', 'P')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Visitado', 'status_solicitacao', 'V')}
                </div>
              </div>
            </RadioGroup>
          </div>

          <div className="col-sm-6">
            <RadioGroup title="Situação do Membro" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Normal', 'tp_membro_solicitado', 'N')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Idoso', 'tp_membro_solicitado', 'I')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Acamado', 'tp_membro_solicitado', 'A')}
                </div>
              </div>
            </RadioGroup>
          </div>

          <div className="col-sm-6">
            <RadioGroup title="Grau da Solicitação" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Em Espera', 'grau_pedido', 'E')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Urgente', 'grau_pedido', 'U')}
                </div>
              </div>
            </RadioGroup>
          </div>

          <div className="col-sm-6">
            <RadioGroup title="Local da Visita ou Oração" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Domiciliar', 'local_visita', 'DOM')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Hospitalar', 'local_visita', 'HOS')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Presídio', 'local_visita', 'PRES')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Outros', 'local_visita', 'O')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_solicitacao_vis_ora', true)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('Membro solicitante:', 'cod_pessoa', 'tab_pessoa',
              'cod_pessoa', 'nome_pessoa')}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data Solicitação:', 'dt_solicitacao')}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data Agendada:', 'dt_agendada')}
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6">
            {componentes.upPanel('Capelão Responsável:', 'cod_capelao', 'tab_pessoa',
              'cod_pessoa', 'nome_pessoa',
              'status_capelao = "S"')}
          </div>
          <div className="col-sm-6">
            {componentes.input('Obs Pré Visita:', 'obs_pre')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a coluna de recebido.
   */
  renderColunaVisitado(item) {
    if (item.status_solicitacao === 'P') {
      return <div className="mov-cr-cp-coluna fa fa-times" />;
    }
    return <div className="mov-cr-cp-coluna completa fa fa-check" />;
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <Cadastro tabela="tab_solicitacao_vis_ora"
        titulo="Solicitações de Visita e Oração"
        subTitulo="Configure os seus Pedidos!"
        renderForm={this.renderForm.bind(this)}
        getNomeRegistro={this.getNomeRegistro.bind(this)}
        getRegistro={this.getRegistro.bind(this)}>
        <Column key={2} field="pessoa_necessitada" header="Pessoa Necessitada" />
        <Column key={3} field="situacao" header="Situação do Membro" />
        <Column key={4} field="tipo" header="Tipo de Solicitação" />
        <Column key={5} width="80px" header="Visitado"
          render={this.renderColunaVisitado.bind(this)} />
      </Cadastro>
    );
  }

}

