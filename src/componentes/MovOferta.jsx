import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import Cadastro from './Cadastro';
import tipo from '../tipo';
import ModalCaixa from './ModalCaixa.jsx';

let Pessoa;
let Igreja;
let Logada;
let Caixa;

/**
 */
class MovOferta extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    Igreja = props.usuario.cod_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    return valor;
  }

  // ------------------------------------------------------------------------------------

  getDadosModalConfirmacaoSalvar(registro) {
    return registro && registro.cod_cr ? {
      titulo: 'Atenção!',
      mensagem: (
        <div className="modal-confirmacao-mov-oferta-dizimo">
          Este título já foi <b>baixado!</b><br />
          <span>Ele será <b>estornado</b> e outro será baixado com as novas informações.</span>
        </div>
      ),
    } : null;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cr: '',
      cod_igreja: Igreja,
      cod_departamento: '',
      tipo_oferta: 'NOR',
      cod_tipo_movimento: tipo.movimento.oferta,
      cod_funcionario: Pessoa,
      cod_funcionario_baixa: Pessoa,
      dt_hr_receber: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_recebido: '',
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      vr_receber: 0,
      cod_conta_analise: 0,
      cod_centro_custo: 1,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-12">
            <RadioGroup title="Tipo de Oferta" className="mt-10">
              <div className="row">
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Normal', 'tipo_oferta', 'NOR')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Anônima', 'tipo_oferta', 'ANON')}
                </div>
                <div className="col-sm-4 col-xs-4">
                  {componentes.radioButton('Identificada', 'tipo_oferta', 'IDEN')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_cr', true)}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data da oferta:', 'dt_hr_receber')}
          </div>
          <div className="col-sm-5">
            {componentes.upPanel('Departamento:', 'cod_departamento', 'tab_departamento',
              'cod_departamento', 'nome_departamento', 'cod_igreja = ' + Igreja)}
          </div>
          <div className="col-sm-2">
            {componentes.inputCalc('Valor:', 'vr_receber')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}

        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          {/* <div className="col-sm-6">
            {componentes.upPanel('Tipo de Oferta *:', 'cod_conta_analise', 'tab_conta_analise_pc',
              'cod_conta_analise', 'nome_conta_analise_pc')}
          </div> */}
          <div className="col-sm-6">
            {componentes.input('Observação da Oferta:', 'obs_documento')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const whereInner = {
      tabelaPrincipal: 'cr', // Tabela atual que estamos pesquisando
      tabelaInner: 'departamento', // Tabela que vamos pesquisar no Inner
      ligacaoTabela1: 'cod_departamento', // Campo chave de ligação da Tabela Principal
      ligacaoTabela2: 'cod_departamento', // Campo chave de ligação da Tabela do Inner
      campoWhere: 'nome_departamento', // Campo do Inner para fazer o Like
    };

    return (
      <div>
        <Cadastro tabela="tab_cr"
          where={`cod_tipo_movimento = ${tipo.movimento.oferta} AND cod_igreja = ${Logada}`}
          titulo="Lançamento de Oferta"
          subTitulo="Configure as suas Ofertas!"
          renderForm={this.renderForm.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          onAlteracaoCampo={this.onAlteracaoCampo.bind(this)}
          whereInner={whereInner}
          whereAlias={`a.cod_tipo_movimento = ${tipo.movimento.oferta} AND a.cod_igreja = ${Logada}`}
          dadosModalConfirmacaoSalvar={this.getDadosModalConfirmacaoSalvar.bind(this)}>
          <Column key={1} width="80px" field="cod_cr" header="Código" />
          <Column key={2} field="departamento.nome_departamento" header="Departamento" />
          <Column key={3} width="150px" field="vr_total_formatado" header="Valor Oferta" />
          <Column key={4} width="150px" field="dt_hr_recebido" header="Data oferta" />
        </Cadastro>
        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }

}

export default connect(state => ({
  usuario: state.usuario,
}))(MovOferta);
