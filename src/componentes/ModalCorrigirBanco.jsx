import React from 'react';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import Modal from '../lib_frontend/componentes/Modal.jsx';
import GenericUtils from '../lib_frontend/main/GenericUtils.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import Button from '../lib_frontend/componentes/Button.jsx';

let codigoCliente;

class ModalCorrigirBanco extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  /**
   * Chamado quando o usuário clicar em confirmar.
   */
  async salvar() {
    GenericUtils.setElementoCarregando(this.divPrincipal, true);
    try {
      const ret = await Api('corrigirBanco', codigoCliente);
      if (!ret.status) {
        toastr.error('Erro!', ret.erro, { attention: true });
        return false;
      }
      toastr.success('Tudo certo!', 'Corrigido efetuado a correção na Cidade!');
      this.modal.hide();
    } finally {
      GenericUtils.setElementoCarregando(this.divPrincipal, false);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Abre esse modal.
   */
  show() {
    this.modal.show();
  }

  // ------------------------------------------------------------------------------------

  render() {
    const botoes = [{
      className: 'btn-success',
      label: 'Fechar',
      icon: 'fa-check',
      dismiss: true,
    }];

    return (
      <Modal {...this.props}
        title="Corrigir informações do Banco"
        buttons={botoes}
        ref={e => this.modal = e}>

        <div className="relative p-10" ref={e => this.divPrincipal = e}>
          <div className="botao-associativa">
            <Button className='btn-info'
              label='Corrigir Cidade'
              icon='fa-globe'
              onClick={this.salvar.bind(this)} />
          </div>
          <br />

        </div>
      </Modal>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}), (dispatch) => ({
  dispatch,
}), null, { withRef: true })(ModalCorrigirBanco);
