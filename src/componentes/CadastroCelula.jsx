import React from 'react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import Api from '../lib_frontend/main/Api';
import Cadastro from './Cadastro';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import { PageControl, Aba } from '../lib_frontend/componentes/Aba';
import Button from '../lib_frontend/componentes/Button.jsx';
import ModalAssociativa from '../lib_frontend/componentes/ModalAssociativa.jsx';

/**
 * Cadastro padrão de celula.
 */

let codigoCliente;

class CadastroCelula extends React.Component {

  constructor(props) {
    super(props);
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  state = {

  };

  // ------------------------------------------------------------------------------------

  onExitCampo(registro, campo) {
    if (campo === 'cep_celula') {
      this.buscarCep(registro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_celula;
  }

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_celula: null,
      nome_celula: '',
      cod_setor: 0,
      lider_celula: 0,
      lider1_treinamento_celula: 0,
      lider2_treinamento_celula: 0,
      anfitriao_celula: 0,
      cep_celula: '',
      endereco_celula: '',
      numero_celula: '',
      complemento_celula: '',
      cod_bairro_celula: 0,
      cod_cidade_celula: 0,
      cod_dia_reuniao_celula: 0,
      hr_reuniao_celula: '',
      dt_inicial_celula: '',
      status_ativo: 'S',
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  async buscarCep(registro) {
    const endereco = await Api('buscarCep', codigoCliente, registro.cep_celula);
    if (endereco.status) {
      registro.cod_bairro_celula = endereco.dados.bairro;
      registro.cod_cidade_celula = endereco.dados.cidade;
      registro.endereco_celula = endereco.dados.logradouro;
      registro.complemento_celula = endereco.dados.complemento;
    } else {
      toastr.error('Erro!', `O CEP é inválido. ${endereco.erro}`, { attention: true });
      registro.cep = '';
    }
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    const paramsCelula = {
      label1: 'Membro',
      tabela: 'tab_celula_pessoa',  // Tabela associativa
      pkNome: 'cod_celula', // Nome da coluna da tabela atual, nesse caso cod_celula "Cad Célula"
      pkAtual: registro.cod_celula, // Cod da tabela atual, nesse caso tab_celula "Cad Célula"
      pkAssociNome: 'cod_membro', // Nome da coluna que faz referência com a 2ª tabela dentro da tabela associativa
      codBusca: 'cod_pessoa', // Coluna do where da tabela que vai buscar o nome do registro da associativa, nesse caso nome do membro
      coluna: 'nome_pessoa', // Campo que eu quero trazer da tabela que vai buscar o nome do registro da associativa, nesse caso nome do membro
      tabela2: 'tab_pessoa', // Tabela para buscar o nome do registro da associativa, nesse caso nome do membro
    };
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ============================== PAGE CONTROL 1 ============================== */}
        <PageControl aba={this.state.aba} onChange={e => this.setState({ aba: e })}>
          {/* ============================== ABA ============================== */}
          <Aba key="abaDadosCelula" icone="book" label="Dados da Célula">
            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.input('Código:', 'cod_celula', true)}
              </div>
              <div className="col-sm-5">
                {componentes.input('Nome da Célula:', 'nome_celula')}
              </div>
              <div className="col-sm-5">
                {componentes.upPanel('Setor:', 'cod_setor', 'tab_setor',
                  'cod_setor', 'nome_setor')}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.inputMask('CEP:', 'cep_celula', { cep: true })}
              </div>
              <div className="col-sm-5">
                {componentes.input('Endereço:', 'endereco_celula')}
              </div>
              <div className="col-sm-2">
                {componentes.input('Número:', 'numero_celula')}
              </div>
              <div className="col-sm-3">
                {componentes.input('Complemento:', 'complemento_celula')}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}

            {/* ------------------------- LINHA 4 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Cidade:', 'cod_cidade_celula', 'tab_cidade',
                  'cod_cidade', 'nome_cidade_comp', '', true)}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Bairro:', 'cod_bairro_celula', 'tab_bairro',
                  'cod_bairro', 'nome_bairro')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Dia da Reunião:', 'cod_dia_reuniao_celula', 'tab_dia_semana',
                  'cod_dia_semana', 'nome_dia_semana')}
              </div>
            </div>
            {/* ------------------------- LINHA 4 ------------------------- */}

            {/* ------------------------- LINHA 5 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.inputDate('Data Inicial da Célula:', 'dt_inicial_celula')}
              </div>
              <div className="col-sm-4">
                {componentes.inputMask('Horário da Célula:', 'hr_reuniao_celula', { hora: true })}
              </div>
              <div className="botao-associativa">
                <Button className='btn-info'
                  label='Adicionar Membros'
                  icon='fa-list-ol'
                  disabled={!paramsCelula.pkAtual}
                  onClick={() =>
                    this.modalAssociativaMembros.getWrappedInstance().show(paramsCelula)} />
              </div>
            </div>
            {/* ------------------------- LINHA 5 ------------------------- */}
          </Aba>

          <Aba key="abalideres" icone="book" label="Líderes">
            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Lider da Célula:', 'lider_celula', 'tab_pessoa',
                  'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('1º Lider de Treinamento:', 'lider1_treinamento_celula',
                  'tab_pessoa', 'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('2º Lider de Treinamento:', 'lider2_treinamento_celula',
                  'tab_pessoa', 'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Anfitrião da Célula:', 'anfitriao_celula', 'tab_pessoa',
                  'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}
          </Aba>
        </PageControl>
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <Cadastro tabela="tab_celula"
          titulo="Cadastro de Célula"
          paginavel
          onExitCampo={this.onExitCampo.bind(this)}
          renderForm={this.renderForm.bind(this)}
          getNomeRegistro={this.getNomeRegistro.bind(this)}
          getRegistro={this.getRegistro.bind(this)}>
          {/* <Column key={1} width="80px" field="cod_celula" header="Código" /> */}
          <Column key={2} field="nome_celula" header="Célula" />
          <Column field="enderecoCompleto" header="Endereço" />
          <Column width="10%" field="status_a" header="Ativo" />
        </Cadastro>

        <ModalAssociativa ref={e => this.modalAssociativaMembros = e}>
          Membros da Célula</ModalAssociativa>
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
  redux: state,
}))(CadastroCelula);

