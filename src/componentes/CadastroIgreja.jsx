import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import { PageControl, Aba } from '../lib_frontend/componentes/Aba';
import Cadastro from './Cadastro';
import Api from '../lib_frontend/main/Api';

let IgrejaLogada;
let codigoCliente;

class CadastroIgreja extends React.Component {

  constructor(props) {
    super(props);
    IgrejaLogada = props.usuario.igrejaLogada.Igreja;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  state = {

  }

  // ------------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------------

  onExitCampo(registro, campo) {
    if (campo === 'cep') {
      this.buscarCep(registro);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna o Nome do registro para colocar na mensagem do Salvar.
   */
  getNomeRegistro(registro) {
    return registro.nome_igreja;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_igreja: null,
      nome_igreja: '',
      status_igreja: 'S',
      dt_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_inauguracao: null,
      razao_social: '',
      cnpj: '',
      inscricao_estadual: '',
      cep: '',
      endereco_igreja: '',
      numero: '',
      cod_cidade: 0,
      cod_bairro: 0,
      tel_1: '',
      tel_2: '',
      tel_3: '',
      email: '',
      status_ativo: 'S',
      cod_resp_igreja: 0,
      cod_esposa_resp: 0,
      cod_setor_igreja: 0,
      margem_porc: 0,
      cod_presidente: 0,
    };
  }

  // ------------------------------------------------------------------------------------

  async getValidacoes(registro) {
    const ret = await Api('existeRegistro', {
      pkNome: 'cod_igreja',
      pkValor: registro.cod_igreja,
      codigoCliente,
      tabela: 'igreja',
      campo: 'status_igreja',
      valor: 'S',
    });
    const existe = ret.dados;

    if (existe && registro.status_igreja === 'S') {
      toastr.error('Erro!', 'Não pode cadastrar mais de uma sede!', { attention: true });
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  async buscarCep(registro) {
    const endereco = await Api('buscarCep', codigoCliente, registro.cep);
    if (endereco.status) {
      registro.cod_bairro = endereco.dados.bairro;
      registro.cod_cidade = endereco.dados.cidade;
      registro.endereco_igreja = endereco.dados.logradouro;
      registro.complemento = endereco.dados.complemento;
    } else {
      toastr.error('Erro!', `O CEP é inválido. ${endereco.erro}`, { attention: true });
      registro.cep = '';
    }
    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="row col-sm-10">
            <div className="col-sm-2">
              <RadioGroup title="Opções" className="mt-10">
                <div className="row">
                  <div className="col-sm-12 col-xs-12">
                    {componentes.checkbox('Ativo', 'status_ativo', 'inline mr-10')}
                  </div>
                </div>
              </RadioGroup>
            </div>

            <div className="col-sm-7">
              <RadioGroup title="Tipo de Igreja" className="mt-10">
                <div className="row">
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Sede', 'status_igreja', 'S')}
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Sub-Sede', 'status_igreja', 'SUB')}
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    {componentes.radioButton('Congregação', 'status_igreja', 'C')}
                  </div>
                </div>
              </RadioGroup>
            </div>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ============================== PAGE CONTROL 1 ============================== */}
        <PageControl aba={this.state.aba} onChange={e => this.setState({ aba: e })}>
          {/* ============================== ABA ============================== */}
          <Aba key="abaDados" icone="book" label="Dados da Igreja">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.input('Código:', 'cod_igreja', true)}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Data de Cadastro:', 'dt_cadastro', true)}
              </div>
              <div className="col-sm-4">
                {componentes.input('Nome:', 'nome_igreja')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Razão Social:', 'razao_social')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-3">
                {componentes.inputMask('CNPJ:', 'cnpj', { cnpj: true })}
              </div>
              <div className="col-sm-3">
                {componentes.input('Inscrição Estadual:', 'inscricao_estadual')}
              </div>
              <div className="col-sm-3">
                {componentes.inputDate('Dt. Inauguração:', 'dt_inauguracao')}
              </div>
              <div className="col-sm-3">
                {componentes.inputMask('CEP:', 'cep', { cep: true })}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}

            {/* ------------------------- LINHA 3 ------------------------- */}
            <div className="row">
              <div className="col-sm-5">
                {componentes.input('Endereço:', 'endereco_igreja')}
              </div>
              <div className="col-sm-3">
                {componentes.input('Número:', 'numero')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Cidade:', 'cod_cidade', 'tab_cidade',
                  'cod_cidade', 'nome_cidade_comp', '', true)}
              </div>
            </div>
            {/* ------------------------- LINHA 3 ------------------------- */}

            {/* ------------------------- LINHA 4 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Bairro:', 'cod_bairro', 'tab_bairro',
                  'cod_bairro', 'nome_bairro')}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Setor:', 'cod_setor_igreja', 'tab_setor_igreja',
                  'cod_setor_igreja', 'nome_setor_igreja')}
              </div>
              <div className="col-sm-4">
                {componentes.input('Email:', 'email')}
              </div>
            </div>
            {/* ------------------------- LINHA 4 ------------------------- */}

            {/* ------------------------- LINHA 5 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.inputMask('Telefone:', 'tel_1', { telefone: true })}
              </div>
              <div className="col-sm-4">
                {componentes.inputMask('Telefone 2:', 'tel_2', { telefone: true })}
              </div>
              <div className="col-sm-4">
                {componentes.inputMask('Celular 2:', 'tel_3', { celular: true })}
              </div>
            </div>
            {/* ------------------------- LINHA 5 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

          {/* ============================== ABA ============================== */}
          <Aba key="abaResponsavel" icone="book" label="Responsável">
            {/* ------------------------- LINHA 1 ------------------------- */}
            <div className="row">
              <div className="col-sm-4">
                {componentes.upPanel('Responsável pela Igreja:', 'cod_resp_igreja',
                  'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
                  'status_membro = "M" AND cod_igreja = ' + IgrejaLogada)}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Esposa do Responsável:', 'cod_esposa_resp',
                  'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
                  'status_membro = "M" AND cod_igreja = ' + IgrejaLogada)}
              </div>
              <div className="col-sm-4">
                {componentes.upPanel('Pastor Presidente:', 'cod_presidente',
                  'tab_pessoa', 'cod_pessoa', 'nome_pessoa', 'status_membro = "M"')}
              </div>
            </div>
            {/* ------------------------- LINHA 1 ------------------------- */}

            {/* ------------------------- LINHA 2 ------------------------- */}
            <div className="row">
              <div className="col-sm-2">
                {componentes.inputDate('Data da Posse:', 'dt_posse')}
              </div>
              <div className="col-sm-2">
                {componentes.inputDate('Data da Saída:', 'dt_saida')}
              </div>
              <div className="col-sm-2">
                {componentes.inputNumber('Tranferência (%):', 'margem_porc', 0)}
              </div>
            </div>
            {/* ------------------------- LINHA 2 ------------------------- */}
          </Aba>
          {/* ============================== ABA ============================== */}

        </PageControl>
        {/* ============================== PAGE CONTROL 1 ============================== */}
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    return (
      <div>
        <Cadastro tabela="tab_igreja"
          titulo="Cadastro de Igrejas"
          paginavel
          onExitCampo={this.onExitCampo.bind(this)}
          renderForm={this.renderForm.bind(this)}
          getNomeRegistro={this.getNomeRegistro.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          getValidacoes={this.getValidacoes.bind(this)}>
          {/* <Column key={1} width="80px" field="cod_igreja" header="Código" /> */}
          <Column key={2} field="nome_igreja" header="Igreja" />
          <Column key={3} field="enderecoCompleto" header="Endereço" />
          <Column key={4} field="telefones" header="Telefone(s)" />
        </Cadastro>
      </div>
    );
  }
}

export default connect((state) => ({
  usuario: state.usuario,
  redux: state,
}))(CadastroIgreja);
