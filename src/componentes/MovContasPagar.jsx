import React from 'react';
import moment from 'moment';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import ModalBaixarCP from './ModalBaixarCP';
import ModalGerarParcelasCP from './ModalGerarParcelasCP.jsx';
import ModalCaixa from './ModalCaixa.jsx';
import Api from '../lib_frontend/main/Api.jsx';
import tipo from '../tipo';

let Igreja;
let Pessoa;
let Logada;
let Caixa;
let codigoCliente;

/**
 * Componente de movimentação de contas a pagar, permitindo incluir
 * registros de contas a pagar e listar as contas a pagar
 * ou pagas até o momento.
 */
class MovContasPagar extends React.Component {

  constructor(props) {
    super(props);
    Igreja = props.usuario.cod_igreja;
    Pessoa = props.usuario.cod_pessoa;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    codigoCliente = localStorage.getItem('codigoCliente');
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    if (campo === 'vr_desconto' && valor > (registro.vr_pagar + registro.vr_acrescimo)) {
      valor = registro.vr_pagar + registro.vr_acrescimo;
    } else if (campo === 'vr_pagar' && (valor + registro.vr_acrescimo) < registro.vr_desconto) {
      registro.vr_desconto = valor + registro.vr_acrescimo;
    } else if (campo === 'vr_acrescimo' && (valor + registro.vr_pagar) < registro.vr_desconto) {
      registro.vr_desconto = valor + registro.vr_pagar;
    }
    return valor;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna a mensagem de confirmação ao excluir
   */
  getMsgExcluir(registro) {
    return `Tem certeza que deseja excluir essa 
      conta a pagar no valor de ${registro.vr_total_formatado} para
      o fornecedor "${registro.pessoa.nome_pessoa}"?`;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna true para poder salvar o registro e false para não,
   * serve para validar as informações colocadas.
   * @param {Object} registro O Registro sendo editado.
   * @param {Cadastro} cadastroRef O Ref do cadastro.
   */
  getValidacoes(registro, cadastroRef) {
    const { cod_pessoa, cod_departamento } = registro;
    const preenchidos = (cod_pessoa && Number(cod_pessoa) !== 0) ||
      (cod_departamento && Number(cod_departamento) !== 0);

    if (!preenchidos) {
      toastr.error('Atenção!', 'Deve preencher uma Pessoa ou um Departamento!');
      cadastroRef.focarCampo('cod_pessoa');
      return false;
    }
    if (!registro.cod_centro_custo || registro.cod_centro_custo === '0') {
      toastr.error('Atenção!', 'Preencha o Centro de Custo !');
      cadastroRef.focarCampo('cod_centro_custo');
      return false;
    }
    if (!registro.cod_tp_saida || registro.cod_tp_saida === '0') {
      toastr.error('Atenção!', 'Preencha o Tipo de Saída !');
      cadastroRef.focarCampo('cod_tp_saida');
      return false;
    }
    if (!registro.cod_conta_analise || registro.cod_conta_analise === '0') {
      toastr.error('Atenção!', 'Preencha o Tipo de Despesa !');
      cadastroRef.focarCampo('cod_conta_analise');
      return false;
    }
    return true;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cp: null,
      cod_pessoa: 0,
      cod_departamento: 0,
      cod_igreja: Igreja,
      cod_tipo_movimento: tipo.movimento.cpManual,
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_pagar: '',
      dt_hr_pago: '',
      vr_pagar: 0,
      vr_pago: 0,
      vr_acrescimo: 0,
      vr_desconto: 0,
      cod_origem: null,
      status_manual: 'S',
      obs: '',
      obs_documento: '',
      status_estornado: 'N',
      cod_funcionario: Pessoa,
      cod_funcionario_baixa: null,
      num_documento: '',
      pessoa: {},
      cod_igreja_referencia: Igreja,
      cod_centro_custo: 0,
      cod_tp_saida: 0,
      cod_conta_analise: 0,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    // const z = Number(0);
    // const codP = Number(registro.cod_pessoa);
    // const codD = Number(registro.cod_departamento);

    // const pessoa = (codD !== z) && codP === z;
    // const depart = (codP !== z) && codD === z;
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-5">
            {componentes.upPanel('Pessoa:', 'cod_pessoa', 'tab_pessoa',
              'cod_pessoa', 'nome_pessoa', '', /*pessoa*/)}
          </div>
          <div className="col-sm-3">
            {componentes.upPanel('Departamento:', 'cod_departamento', 'tab_departamento',
              'cod_departamento', 'nome_departamento', 'cod_igreja = ' + Igreja, /*depart*/)}
          </div>
          <div className="col-sm-4">
            {componentes.upPanel('De qual Igreja é a Conta ?', 'cod_igreja_referencia',
              'tab_igreja', 'cod_igreja', 'nome_igreja')}
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-12">
            {componentes.input('Descrição:', 'obs')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            {componentes.upPanel('Centro de Custo *:', 'cod_centro_custo', 'tab_centro_custo',
              'cod_centro_custo', 'nome_centro_custo')}
          </div>
          <div className="col-sm-3">
            {componentes.upPanel('Tipo de Saída *:', 'cod_tp_saida', 'tab_tp_saida',
              'cod_tp_saida', 'nome_tp_saida')}
          </div>
          <div className="col-sm-6">
            {componentes.upPanel('Tipo de Despesa *:', 'cod_conta_analise',
              'tab_conta_analise_pc', 'cod_conta_analise_pc', 'nome_conta_analise_pc')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-3">
            {componentes.inputDate('Data de Lançamento:', 'dt_hr_cadastro', true)}
          </div>
          <div className="col-sm-3">
            {componentes.inputDate('Data de vencimento:', 'dt_hr_pagar')}
          </div>
          <div className="col-sm-2">
            {componentes.inputCalc('Valor:', 'vr_pagar')}
          </div>
          <div className="col-sm-2">
            {componentes.input('N.º Nota Fiscal/Doc:', 'obs_documento')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div >
    );
  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza a coluna de pago.
   */
  renderColunaPago(item) {
    if (item.dt_hr_pago && item.status_baixa_parcial === 'S') {
      return <div className="mov-cr-cp-coluna parcial fa fa-star-half-o" />;
    } else if (item.dt_hr_pago) {
      return <div className="mov-cr-cp-coluna completa fa fa-star" />;
    }
    return <div className="mov-cr-cp-coluna fa fa-times" />;
  }

  // ------------------------------------------------------------------------------------

  render() {
    const whereInner = {
      tabelaPrincipal: 'cp', // Tabela atual que estamos pesquisando
      tabelaInner: 'pessoa', // Tabela que vamos pesquisar no Inner
      ligacaoTabela1: 'cod_pessoa', // Campo chave de ligação da Tabela Principal
      ligacaoTabela2: 'cod_pessoa', // Campo chave de ligação da Tabela do Inner
      campoWhere: 'nome_pessoa', // Campo do Inner para fazer o Like
    };

    const props = {
      tabela: 'tab_cp',
      titulo: 'Contas a Pagar',
      subTitulo: 'Gerencia todas as suas contas a pagar!',
      paginavel: true,
      selecionavel: true,
      where: 'cod_igreja = ' + Logada,
      renderForm: this.renderForm.bind(this),
      getRegistro: this.getRegistro.bind(this),
      getMsgExcluir: this.getMsgExcluir.bind(this),
      getValidacoes: this.getValidacoes.bind(this),
      onAlteracaoCampo: this.onAlteracaoCampo.bind(this),
      whereInner: whereInner,
      editarDesabilitado: (cr) => cr.dt_hr_pago || cr.cod_cp_origem || cr.cod_cp_destino,
      excluirDesabilitado: (cr) => cr.dt_hr_pago || cr.cod_cp_destino,
      actions: [{
        icone: 'fa-money',
        label: 'Baixar conta',
        className: 'btn-success',
        onClick: (item) => {
          this.modalBaixarCP.getWrappedInstance().show(item.cod_cp);
        },
        disabled: (cr) => !cr || cr.dt_hr_pago,
      }, {
        icone: 'fa-arrow-left',
        label: 'Estornar conta',
        className: 'btn-warning',
        onClick: async (item) => {
          const ret = await Api('estornarConta', Pessoa, item.cod_cp, 'CP', codigoCliente);
          if (!ret.status) {
            toastr.error('Atenção!', ret.erro, { attention: true });
            return false;
          }
          toastr.success('Tudo certo!', 'O Registro foi estornado!');
          this.cadastro.getWrappedInstance().carregarRegistros();
        },
        disabled: (cr) => !cr || !cr.dt_hr_pago,
      }, {
        icone: 'fa-list-ol',
        label: 'Gerar parcelas',
        className: 'btn-info',
        onClick: () => this.modalParcelasCP.getWrappedInstance().show(),
        disabled: () => false,
      }],
    };
    return (
      <div>
        <Cadastro ref={e => this.cadastro = e} {...props}>
          {/* <Column width="80px" field="cod_cp" header="Código" /> */}
          <Column field="pessoa.nome_pessoa" header="Descrição" />
          <Column field="vr_total_formatado" header="Valor" />
          <Column field="vr_pago_formatado" header="Valor Pagamento" />
          <Column field="dt_hr_pagar" header="Dt. Vencimento" />
          <Column field="dt_hr_pago" header="Dt. Pagamento" />
          <Column field="nome_movimento" header="Origem" />
          <Column header="Pago"
            render={this.renderColunaPago.bind(this)} />
        </Cadastro>

        <ModalBaixarCP ref={e => this.modalBaixarCP = e}
          onClose={() => this.cadastro.getWrappedInstance().carregarRegistros()} />

        <ModalGerarParcelasCP ref={e => this.modalParcelasCP = e}
          onClose={() => this.cadastro.getWrappedInstance().carregarRegistros()} />

        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }

}

export default connect((state) => ({
  usuario: state.usuario,
}))(MovContasPagar);
