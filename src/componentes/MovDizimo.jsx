import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { Column } from '../lib_frontend/componentes/Table';
import Cadastro from './Cadastro';
import tipo from '../tipo';
import Api from '../lib_frontend/main/Api';
import { trocarRota } from '../utils/funcoesFront';
import { RadioGroup } from '../lib_frontend/componentes/RadioGroup';
import ModalCaixa from './ModalCaixa.jsx';

let Pessoa;
let Igreja;
let DepIgreja;
let Logada;
let Caixa;
let ImprimirComp;
let codigoCliente;

/**
 */
class MovDizimo extends React.Component {

  constructor(props) {
    super(props);
    Pessoa = props.usuario.cod_pessoa;
    Igreja = props.usuario.cod_igreja;
    DepIgreja = props.usuario.departamento_igreja;
    Logada = props.usuario.igrejaLogada.Igreja;
    Caixa = props.usuario.caixa ? props.usuario.caixa.cod_caixa : null;
    ImprimirComp = props.config.status_imp_comp_dizimo;
    codigoCliente = localStorage.getItem('codigoCliente');
    this.state = {
      dizimoDesabilitado: false,
    };
  }

  // ------------------------------------------------------------------------------------

  componentDidMount() {
    if (!Caixa) {
      this.ModalCaixa.getWrappedInstance().show();
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Trata e retorna o novo valor do campo ao alterar ele.
   * @param {EntidadeSinc} registro O Registro.
   * @param {String} campo O Nome do campo.
   * @param {*} valor O Valor do campo.
   */
  onAlteracaoCampo(registro, campo, valor) {
    registro.cod_caixa = Caixa;
    return valor;
  }

  // ------------------------------------------------------------------------------------

  getDadosModalConfirmacaoSalvar(registro) {
    return registro && registro.cod_cr ? {
      titulo: 'Atenção!',
      mensagem: (
        <div className="modal-confirmacao-mov-oferta-dizimo">
          Este título já foi <b>baixado!</b><br />
          <span>Ele será <b>estornado</b> e outro será baixado com as novas informações.</span>
        </div>
      ),
    } : null;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna um registro limpo.
   */
  getRegistro() {
    return {
      cod_cr: '',
      cod_pessoa: '',
      cod_igreja: Igreja,
      cod_forma_pagamento: 1,
      cod_departamento: DepIgreja,
      cod_tipo_movimento: tipo.movimento.dizimo,
      cod_funcionario: Pessoa,
      cod_funcionario_baixa: Pessoa,
      dt_hr_receber: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      dt_hr_recebido: '',
      dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      vr_receber: 0,
      imprimir_comp: ImprimirComp,
      cod_conta_analise: 0,
      cod_centro_custo: 1,
    };
  }

  // ------------------------------------------------------------------------------------

  async imprimirDizimo(dizimo) {
    if (dizimo.imprimir_comp === 'S') {
      this.setState({ dizimoDesabilitado: true });
      const rel = await Api('gerarRelatorio', {
        codigoCliente,
        nomeRelatorio: 'Comprovante Dizimo',
        codigoPk: dizimo.cod_cr,
      });
      if (rel.status) {
        const relat = window.location.origin + rel.dados;
        trocarRota(relat, '_blank');
        // GenericUtils.abrirPdfBase64(rel.dados, 'Comprovante Dizimo');
        toastr.success('Relatorio', 'O Relatório foi gerado com sucesso!');
      } else {
        toastr.error('Erro!', 'Relatório não encontrado', { attention: true });
      }
      this.setState({ dizimoDesabilitado: false });
    }

  }

  // ------------------------------------------------------------------------------------

  /**
   * Renderiza o formulário de inclusão/alteração.
   * @param {Object} registro O Registro sendo alterado/incluido.
   * @param {Object} componentes A Lista de componentes disponíveis.
   * @param {(label, campo, className) => {}} componentes.checkbox Checkbox.
   * @param {(label, campo, disable) => {}} componentes.input Input.
   * @param {(label, campo, disabled) => {}} componentes.inputCalc Input de floats.
   * @param {(label, campo, disabled) => {}} componentes.inputDate Input de data.
   * @param {(label, campo, props) => {}} componentes.inputMask Input de máscara.
   * @param {(label, campo, min, max) => {}} componentes.inputNumber Input de Número.
   * @param {(label, campo) => {}} componentes.inputTime Input de time.
   * @param {(label, campo, valor) => {}} componentes.radioButton RadioButton.
   * @param {(label, campo, tabela, chave, desc, where, disabled) => {}} componentes.upPanel
   */
  renderForm(registro, componentes) {
    return (
      <div>
        {/* ------------------------- LINHA 1 ------------------------- */}
        <div className="row">
          <div className="col-sm-4">
            <RadioGroup title="Opções" className="mt-10">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  {componentes.checkbox('Deseja Imprimir Comprovante ?',
                    'imprimir_comp', 'inline mr-10')}
                </div>
              </div>
            </RadioGroup>
          </div>
        </div>
        {/* ------------------------- LINHA 1 ------------------------- */}

        {/* ------------------------- LINHA 2 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.input('Código:', 'cod_cr', true)}
          </div>
          <div className="col-sm-2">
            {componentes.inputDate('Data do dizimo:', 'dt_hr_receber')}
          </div>
          <div className="col-sm-3">
            {componentes.upPanel('Forma de Recebimento:', 'cod_forma_pagamento',
              'tab_forma_pagamento', 'cod_forma_pagamento', 'nome_forma_pagamento')}
          </div>
          {/* <div className="col-sm-6">
            {componentes.upPanel('Tipo de Dízimo *:', 'cod_conta_analise', 'tab_conta_analise_pc',
              'cod_conta_analise', 'nome_conta_analise_pc')}
          </div> */}
          <div className="col-sm-5">
            {componentes.upPanel('Membro:', 'cod_pessoa', 'tab_pessoa', 'cod_pessoa', 'nome_pessoa',
              'status_membro = "M"')}
          </div>
        </div>
        {/* ------------------------- LINHA 2 ------------------------- */}

        {/* ------------------------- LINHA 3 ------------------------- */}
        <div className="row">
          <div className="col-sm-2">
            {componentes.inputCalc('Valor:', 'vr_receber')}
          </div>
        </div>
        {/* ------------------------- LINHA 3 ------------------------- */}
      </div>
    );
  }

  // ------------------------------------------------------------------------------------

  render() {
    const whereInner = {
      tabelaPrincipal: 'cr', // Tabela atual que estamos pesquisando
      tabelaInner: 'pessoa', // Tabela que vamos pesquisar no Inner
      ligacaoTabela1: 'cod_pessoa', // Campo chave de ligação da Tabela Principal
      ligacaoTabela2: 'cod_pessoa', // Campo chave de ligação da Tabela do Inner
      campoWhere: 'nome_pessoa', // Campo do Inner para fazer o Like
    };

    const actions = [{
      className: 'btn-warning',
      icone: 'fa-print',
      label: 'Imprimir Comprovante',
      id: 'imprimir-comprovante',
      onClick: (dizimo) => this.imprimirDizimo(dizimo),
      disabled: (dizimo) => !dizimo || this.state.dizimoDesabilitado,
    }];

    return (
      <div>
        <Cadastro tabela="tab_cr"
          titulo="Lançamento de Dizimo"
          subTitulo="Configure as suas Dizimos!"
          actions={actions}
          where={`cod_tipo_movimento = ${tipo.movimento.dizimo} AND cod_igreja = ${Logada}`}
          renderForm={this.renderForm.bind(this)}
          getRegistro={this.getRegistro.bind(this)}
          onAlteracaoCampo={this.onAlteracaoCampo.bind(this)}
          whereInner={whereInner}
          whereAlias={`a.cod_tipo_movimento = ${tipo.movimento.dizimo} AND a.cod_igreja = ${Logada}`}
          paginavel
          dadosModalConfirmacaoSalvar={this.getDadosModalConfirmacaoSalvar.bind(this)}>
          {/* <Column key={1} width="80px" field="cod_cr" header="Código" /> */}
          <Column key={2} field="pessoa.nome_pessoa" header="Membro" />
          <Column key={3} field="vr_total_formatado" header="Valor Dizimo" />
          <Column key={4} field="dt_hr_receber" header="Data do Recebimento" />
          <Column key={4} field="dt_hr_recebido" header="Data de Lançamento" />
        </Cadastro>
        <ModalCaixa ref={e => this.ModalCaixa = e} />
      </div>
    );
  }

}

export default connect(state => ({
  usuario: state.usuario,
  config: state.config,
}))(MovDizimo);
