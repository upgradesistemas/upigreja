import accounting from 'accounting';

/**
 * @param {String} valor Remove caracteres do parâmetro 'valor'  . - / , ( ) _
 */
export function removeCaracteres(valor) {
    return valor.replace(/\.|-|\/|,|\(|\)|_/gi, '');
}

/**
 * Remove todos os acentos do valor.
 * @param {String} valor O Valor.
 * @return {String} o valor sem acentos.
 */
export function removerAcentos(valor) {
  return valor.replace(/â|ã|á|à/g, 'a')
    .replace(/ô|õ|ó|ò/g, 'o')
    .replace(/ê|é|è/g, 'e');
}

/**
 * Formata valores decimais ex: 100.00 para 100,00
 * @param {Float} valor é o valor a ser formatado
 * @param {Integer} casa_decimais é a quantidade de casas decimais a seram apresentada DEFAULT 2
 */
export function formataDinheiro(valor, casa_decimais) {
  return 'R$ ' + accounting.formatMoney(valor, '', casa_decimais || 2, '.', ',');
}

/**
 * Muda a rota para a passada por parâmetro
 * @param {String} rota A rota a ser enviada
 * @param {String} tipo O tipo que irá abrir a nova rota, default: _self
 * (_blank, _parent, _self, _top, name)
 */
export function trocarRota(rota, tipo) {
  window.open(rota, tipo || '_self');
}
