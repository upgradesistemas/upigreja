exports.movimento = {

  /**
   * Verifica e retorna se o tipo de movimento é valido.
   */
  valido: function (tipo) {
    const keys = Object.keys(this);
    const ultimaProp = this[keys[keys.length - 1]];
    return tipo && tipo <= ultimaProp;
  },

  dizimo              : 1,
  oferta              : 2,
  crManual            : 3,
  crParcelas          : 4,
  crParcial           : 5,
  cpManual            : 6,
  cpParcelas          : 7,
  cpParcial           : 8,
  cantina             : 9,
  venda               : 10,
  compra              : 11,
  ebd                 : 12,
  transfCredito       : 13,
  transfDebito        : 14,
  chamada             : 15,
};

exports.getNomeMovimento = (tipo) => {
  switch (tipo) {
    case 1: return 'Dízimo';
    case 2: return 'Oferta';
    case 3: return 'CR Manual';
    case 4: return 'CR Parcela';
    case 5: return 'CR Parcial';
    case 6: return 'CP Manual';
    case 7: return 'CP Parcela';
    case 8: return 'CP Parcial';
    case 9: return 'Cantina';
    case 10: return 'Venda';
    case 11: return 'Compra';
    case 12: return 'Escola Bíblica';
    case 13: return 'Transferência';
    case 14: return 'Transferência';
    case 15: return 'Chamada';
    default: throw new Error('Tipo de movimento inválido!');
  }
};
