﻿const path = require('path');
const fs = require('fs');
const autenticacao = require('../funcoes/autenticacao');
const arquivos = require('../funcoes/arquivos');

const PRODUCAO = 1;
const HOMOLOGACAO = 0;

// ------------------------------------------------------------------------------------

/**
 * Representa o objeto de dados que é passado por parâmetro
 * para o iniciarAplicacao.
 */
class Dados {
  constructor() {
    this.banco = {
      host: 'localhost',
      port: 3307,
      user: 'root',
      password: 'san01xlz',
      database: 'sc',
    };
    this.ioServer = {
      opcoes: {},
      onInicio: () => { },
    };
    this.controller = '';
    this.sincronizador = {
      banco: '',
      pasta: '',
    };
    this.databases;
  }

  /**
   * Chamado uma vez que a aplicação já foi iniciada e processos
   * externos podem ser executados, específicos da aplicação.
   */
  onInicio() {
    // Pode ser implementado
  }
}

// ------------------------------------------------------------------------------------

/**
 * Seta o hook de commit para commits serem realizados de acordo com
 * o padrão especificado:
 * ID_TICKET - Alterações efetuadas
 */
function setarCommitHookGit() {
  try {
    const caminho = path.join(__dirname, '..', '..', '.git', 'hooks');
    arquivos.criarArquivo(path.join(caminho, 'commit-msg'), (`
      #!/usr/bin/env bash
      # Mensagem de comit deve ser no seguinte padrão:
      # ID_TICKET - Alterações efetuadas
      commit_regex='^([0-9]{1,5}\\s-\\s)'

      RED=$(tput setaf 1)
      NORMAL=$(tput sgr0)
      if ! grep -iqE "$commit_regex" "$1"; then
        echo " • \${RED}Mensagem de commit está errada, deve seguir esse padrão:$NORMAL"
        echo " • \${RED}ID_TICKET - Alterações efetuadas $NORMAL"
        exit 1
      fi

      exit 0
    `).trim());
  } catch (ex) { /* Não foi possível setar a hook, provavelmente permissão */ }
}

// ------------------------------------------------------------------------------------

/**
 * Inicia a aplicação com os dados especificados.
 * @param {Dados} dados Os dados da aplicação.
 * @param {Number} dados.porta A Porta de requisição do backend.
 * @param {Number} dados.portaReactDev A Porta de desenvolvimento do react.
 * @param {Number} dados.ambiente O Ambiente do sistema (PRODUCAO/HOMOLOGACAO).
 * @param {Object} dados.banco Os dados da conexão com o banco de dados.
 * @param {String} dados.controller O Caminho relative do controller de requisição.
 * @param {Object} dados.ioServer Informações sobre o servidor de socket.
 * @param {Object} dados.sincronizador O Sincronizador do banco da aplicação.
 */
async function iniciarAplicacao(dados) {
  try {
    const pjson = require('../../package.json');
    const caminhoDadosUpgrade = path.join(__dirname, '..', '..', '/dadosUpgrade.txt');
    const dadosUpgrade = JSON.parse(fs.readFileSync(caminhoDadosUpgrade, 'utf8'));
    let porta = dadosUpgrade.portaProducao;


    if (dadosUpgrade.ambiente === HOMOLOGACAO) {
      // Se for homologação vamos setar a hook de commit do git:
      setarCommitHookGit();
      porta = dadosUpgrade.porta;
    }

    console.log(
      ' ******************************\n' +
      ' *      Upgrade sistemas      *\n' +
      ' *    Início da aplicação!    *\n' +
      ' ******************************\n'
    );

    global.appDir = path.join(__dirname, '..', '..');
    global.dadosAplicacao = dados;
    global.dadosAplicacao.ambiente = dadosUpgrade.ambiente;

    const servidor = require('../funcoes/servidor');
    const sincronizador = require('../sincronizacao/sincronizador');
    const database = require('../funcoes/database');
    const sockets = require('../funcoes/sockets');

    /**
     * Inicia o servidor do express para realizar requisições para
     * o backend da interface.
     */
    const server = servidor.criarServidor(porta, (serv, express) => {
      serv.use(express.static(path.join(__dirname, './build')));
      serv.use('/static', express.static(path.join(global.appDir, 'build', 'static')));
      serv.get('/app/*', (req, res) => {
        res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html'));
      });
      serv.get('/app', (req, res) => {
        res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html'));
      });
      serv.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html'));
      });
      serv.get('/ping', (req, res) => {
        res.send({
          status: true,
          revisao: pjson.revision,
        });
      });
    });

    /**
     * Cria a conexão padrão com o mysql.
     */
    const porta3306 = dadosUpgrade.usuarioMac ? dadosUpgrade.porta3306 : null;
    database.criarConexao(dados.banco, porta3306);

    /**
     * Configura o servidor de socket.io.
     */
    if (dados.ioServer) {
      dados.ioServer.onInicio(sockets.criarServidor(server, dados.ioServer.opcoes));
    }

    // Cria algumas rotas que não precisam ser colocadas no controller:
    const Controller = require(path.join(__dirname, '..', '..', dados.controller));
    const instancia = new Controller();
    criarRotasPadroes(server, instancia);
    // Cria as rotas do controller genérico:
    criarRotas(server, require(path.join(__dirname, 'GenericController')));
    // Cria as rotas do controller da aplicação:
    criarRotas(server, require(path.join(__dirname, '..', '..', dados.controller)));

    /**
     * Realiza a sincronização do aplicativo com o seu banco de dados
     * se o objeto de sincronizador foi especificado.
     */

    if (dadosUpgrade.sincBanco) {
      if (dados.sincronizador) {
        await sincronizador.sinc(
          process.env.DB_DATABASE || dados.banco.database,
          dados.sincronizador.pasta);
      }
      if (dados.bancos) {
        for (const d of dados.bancos) {
          await sincronizador.sinc(d, dados.sincronizador.pasta);
        }
      }
    }

    if (dados.configTemp) {
      let ConfigTemp = require(path.join(__dirname, '..', '..', dados.configTemp));
      ConfigTemp = new ConfigTemp();
      await ConfigTemp.executarConfigs();
    }

    if (dadosUpgrade.ambiente === HOMOLOGACAO) {
      // Vamos alterar a porta das variáveis de ambiente apenas para iniciar
      // o servidor de desenvolvimento de react (para que seja iniciado na porta especificada)
      const p = process.env.PORT;
      process.env.PORT = dadosUpgrade.portaReactDev;
      require('react-scripts/scripts/start');
      // depois voltamos ao normal para não afetar nada externamente:
      process.env.PORT = p;
    }

    // Chama a inicialização externa do aplicativo se ela foi especificada:
    if (dados.onInicio) {
      dados.onInicio();
    }

    setInterval(() => {
      // 0 = HOMOLOGACAO
      const publicBuild = dadosUpgrade.ambiente === HOMOLOGACAO ? 'public' : 'build/static';
      const pastaPublic = fs.readdirSync(path.join(global.appDir, publicBuild));
      for (const arq of pastaPublic) {
        const caminhoPublic = path.join(global.appDir, publicBuild) + '/' + arq;
        if (arq.indexOf('.pdf') > 0 && fs.existsSync(caminhoPublic)) {
          fs.unlinkSync(caminhoPublic);
        }
      }
    }, 7200000);

  } catch (ex) {
    console.log(ex);
  }
}

// ------------------------------------------------------------------------------------

/**
 * Cria as rotas da aplicação de acordo com os métodos estáticos do
 * Controller principal.
 * @param {Server} server O Servidor do nosso backend.
 * @param {String} nomeArquivo O Nome do arquivo do nosso controlador. 
 */
function criarRotas(server, Controller) {
  const instancia = new Controller();
  // Só vamos expor os métodos estáticos para preservar métodos de pura
  // lógica no nosso controlador:
  const functions = Object.getOwnPropertyNames(Controller)
    .filter(prop => typeof Controller[prop] === 'function');
  // Aqui só temos métodos estáticos:
  for (const funcName of functions) {
    // Vamos descobrir se é um post ou um get com base no nome do método:
    const method = String(funcName).startsWith('get') ? 'get' : 'post';
    // Vamos ver se precisa aplicar o validate.
    const validate = funcName === 'login' ? (req, res, next) => { next(); } : autenticacao.validate;
    // Cria a rota de requisição
    server[method]('/' + funcName, validate, async (req, res) => {
      try {
        let params = req.body;
        if (method === 'get') {
          params = Object.assign([], req.query);
        }
        const dados = await Controller[funcName].apply(instancia, params);
        res.send({ status: true, dados });
      } catch (ex) {
        const erro = ex.message || ex.sql_message || ex.msg || JSON.stringify(ex);
        res.send({ status: false, erro });
      }
    });
  }
}

// ------------------------------------------------------------------------------------

/**
 * Cria as rotas padrões para todos os projetos, todas as rotas que
 * foram genéricas para todos os projetos ou sejam usada por algum
 * componente da lib devem ser colocadas aqui.
 * @param {Express} server Servidor do express.
 * @param {Controller} controller O Controller.
 */
async function criarRotasPadroes(server, controller) {
  // Valida a autenticação do usuário:
  server.post('/validarAuth', autenticacao.validate, async (req, res) => {
    const token = req.headers.token;
    const usuario = autenticacao.getParamsByToken(token);
    try {
      const dados = await controller.onLoginFrontend(usuario, usuario.codigoCliente);
      res.send({ status: true, dados: { token, ...dados } });
    } catch (ex) {
      const erro = ex.message || ex.sql_message || ex.msg || JSON.stringify(ex);
      res.send({ status: false, erro });
    }
  });
}

exports.iniciarAplicacao = iniciarAplicacao;
exports.PRODUCAO = PRODUCAO;
exports.HOMOLOGACAO = HOMOLOGACAO;
exports.Dados = Dados;
