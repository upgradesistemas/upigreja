const database = require('../funcoes/database');
const fs = require('fs');
const path = require('path');
const Config = require('../entidade/Config');
const ConfigPessoa = require('../entidade/ConfigPessoa');
const tabelasBanco = []; // Singleton com os arquivos da pasta de sincronização do banco

/**
 * Classe do controlador genérico da aplicação, aqui ficam os métodos que as
 * rotas genéricas utilizam, é recomendado que os controladores específicos
 * herdem dessa classe e sobrescrevam os métodos ao invés de criar seus
 * próprios métodos sem relação com esses.
 */
module.exports = class GenericController {

  /**
   * Encontra o arquivo de entidade da tabela especificada.
   * @param {String} tabela O Nome da tabela.
   */
  encontrarEntidade(tabela) {
    if (tabelasBanco.length == 0) {
      // Se ainda não foi setado todas as tabelas no array, puxa
      // tudo da pasta e cria as entidades, setando no array tabelasBanco
      const pasta = path.join(global.appDir, global.dadosAplicacao.sincronizador.pasta);
      const entidades = fs.readdirSync(pasta);
      for (let i = 0; i < entidades.length; i++) {
        const Arquivo = require(path.join(pasta, entidades[i]));
        const instancia = new Arquivo();
        tabelasBanco.push({
          instancia,
          instanciaEstatica: Arquivo,
        });
      }

      // Vamos tentar encontrar a entidade na pasta da lib também:
      const pastaLib = path.join(global.appDir, 'lib_backend', 'entidade');
      const entidadesLib = fs.readdirSync(pastaLib);
      for (let i = 0; i < entidadesLib.length; i++) {
        const Arquivo = require(path.join(pastaLib, entidadesLib[i]));
        const instancia = new Arquivo();
        tabelasBanco.push({
          instancia,
          instanciaEstatica: Arquivo,
        });
      }
    }

    for (const tab of tabelasBanco) {
      if (tab.instancia.tabela === tabela) {
        return tab.instanciaEstatica;
      }
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Método de salvar genérico de uma entidade.
   * @param {string} tabela A tabela para salvar no banco
   * @param {object} entidade O Objeto de entidade para ser salvo.
   */
  static async salvar(tabela, entidade, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const Entidade = new GenericController().encontrarEntidade(tabela);
    const nomePk = new Entidade().getFields().find(x => x.isPk).nome;

    if (entidade.length) { // quando a entidade estiver vindo com mais de um parâmetro
      if (entidade[1]) { entidade[0][nomePk] = null; } // if true, é do duplicar
      entidade[0][nomePk] = await Entidade.salvar(entidade[0], nomeBanco);
      return entidade[0][nomePk];
    }

    entidade[nomePk] = await Entidade.salvar(entidade, nomeBanco);
    return entidade[nomePk];
  }

  // ------------------------------------------------------------------------------------

  /**
   * Recupera as configurações em massa.
   */
  static async getConfigs(codPessoa, nomeBanco, codigoCliente) {
    const banco = nomeBanco || await database.getBanco(codigoCliente);
    const gerais = await Config.buscarTodos(banco);
    const usuario = await ConfigPessoa.buscarTodos(codPessoa, banco);
    return {
      gerais,
      usuario,
    };
  }

  // ------------------------------------------------------------------------------------

  /**
   * Carrega todas as telas Liberadas para o Funcionario/Pessoa logado
   */
  static async getTelasLiberadas(codPessoa, nomeBanco) {
    const telas = await database.query(`
      SELECT *
      FROM ${nomeBanco}.tab_pessoa_permissao_tela a
      WHERE a.cod_pessoa = '${codPessoa}'
    `);
    return telas;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Atualiza as configurações do sistema em massa.
   * Os dados são um objeto de configurações cujo cada chave é o nome da config 
   * e o valor é o valor da config.
   * @param {Object} configs.configs Configurações gerais.
   * @param {Object} configs.usuario Configurações do usuário.
   */
  static async setConfigs(configs, codPessoa, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    for (const key in configs.configs) {
      if (configs.configs.hasOwnProperty(key)) {
        await Config.set(key, configs.configs[key], nomeBanco);
      }
    }
    for (const key in configs.usuario) {
      if (configs.usuario.hasOwnProperty(key)) {
        await ConfigPessoa.set(key, configs.usuario[key], codPessoa, nomeBanco);
      }
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza uma busca diretamente do componente padrão da famosa Upgrade
   * sistemas que foi inicialmente pensada em 2017 porém que realmente
   * tomou inicio no término de 2018, se tornando uma das líderes no
   * desenvolvimento de softwares no ramo de igreja no ano de 2020.
   * 
   *   No Ano de 2027 já se tornava líder absoluta do ramo de igrejas no 
   * Brasil inteiro e assim começou seu reinado como o único sistema de
   * software no mundo inteiro, dominando até ramos como fecularias e lojas
   * de roupas. 
   * 
   *   No ano de 2220 já se passavam 3 gerações de dominação total e absoluta
   * desde o ínicio da upgrade sistemas, e foi quando o reinado de Jorge IV 
   * iniciou, e a ditadura militar tornou-se um estigma primatório no reinado
   * da Upgrade, depois de travar inúmeras guerras, Josias, CEO da upgrade sistemas
   * na época acabou aniquilando Jorge IV, e por mais 200 anos, upgrade reinou
   * em paz, dominando. 
   */
  static async buscarUpPanel(tabela, campo, descricao, where, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const Entidade = new GenericController().encontrarEntidade(tabela);
    return await Entidade.buscarTodos(`
      ${campo} like '%${descricao}%'
      ${where ? ' AND ' + where : ''}
    `, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca uma entidade pelo id.
   */
  static async buscarPorId(tabela, id, codigoCliente) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const Entidade = new GenericController().encontrarEntidade(tabela);
    return await Entidade.buscarPorId(id, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Método padrão de excluir. 
   */
  static async excluir(tabela, entidade, codigoCliente, pessoaExclusao) {
    const nomeBanco = await database.getBanco(codigoCliente);
    const Entidade = new GenericController().encontrarEntidade(tabela);
    const nomePk = new Entidade().getFields().find(x => x.isPk).nome;
    await Entidade.excluir(entidade[nomePk], nomeBanco, pessoaExclusao);
  }

};
