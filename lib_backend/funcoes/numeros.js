const accounting = require('accounting');

/**
 * Formata valores decimais ex: 100.00 para R$ 100,00
 * @param {Float} valor é o valor a ser formatado
 */
exports.formatarDinheiro = (valor, casa_decimais) => {
  return 'R$ ' + accounting.formatMoney(valor, '', casa_decimais || 2, '.', ',');
};
