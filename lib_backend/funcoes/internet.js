const network = require('network');

/**
 * @return {String} O Ip da internet desse computador.
 */
function getIpInternet() {
  return new Promise((resolve) => {
    network.get_public_ip((err, ip) => resolve(ip || null));
  });
}

let networkData;

/**
 * Recupera as informações da camada da interface da rede.
 */
function getDadosRede() {
  return new Promise(async (resolve, reject) => {
    if (networkData) {
      return resolve(networkData);
    }
    network.get_active_interface((err, obj) => {
      if (err) {
        reject(err);
      }
      networkData = obj;
      networkData.mac_address = String(networkData.mac_address).replace(/:/g, '-');
      return resolve(networkData);
    });
  });
}

exports.getIpInternet = getIpInternet;
exports.getDadosRede = getDadosRede;
