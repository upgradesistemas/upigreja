/**
 * Calcula o progresso de envio de um stream, ativando o callback para cada porcentagem
 * diferente que o stream já mandou.
 * @param {Stream} stream O Stream.
 * @param {Number} tamanhoArquivo O Tamanho total do stream em bytes.
 * @param {Function} callback A Função de callback para ativar.
 */
function getProgressoFromStream(stream, tamanhoArquivo, callback) {
  let tamanhoEnviado = 0;
  let ultimaPorcentagem = 0;
  stream.on('data', (buffer) => {
    tamanhoEnviado += buffer.length;
    const porcentagem = Number(((tamanhoEnviado / tamanhoArquivo) * 100).toFixed(2));
    if ((ultimaPorcentagem < (porcentagem - 2)) || porcentagem >= 100) {
      callback(porcentagem);
      ultimaPorcentagem = porcentagem;
    }
  });
}

/**
 * @param {String} valor Remove caracteres do parâmetro 'valor'  . - / , ( ) _
 */
function removeCaracteres(valor) {
  if (valor) {
    return valor.replace(/\.|-|\/|,|\(|\)|_/gi, '');
  }
}

/**
 * @param {String} valor Formata o telefone adicionando a mascara, para mostrar corretamente no grid
 */
function formataTelefone(valor) {
  if (valor) {
    if (valor.length === 10) { // Telefone
      const _1 = valor.substring(0, 2);
      const _2 = valor.substring(2, 6);
      const _3 = valor.substring(6, 10);

      return '(' + _1 + ')' + _2 + '-' + _3;

    } else if (valor.length === 11) { // Celular
      const _1 = valor.substring(0, 2);
      const _2 = valor.substring(2, 7);
      const _3 = valor.substring(7, 11);

      return '(' + _1 + ')' + _2 + '-' + _3;

    }
  }
}

/**
 * @param {String} valor Remove caracteres do parâmetro 'valor'  . - / , ( ) _
 */
function concatenarInformacoes(valorA, valorB, valorC) {
  const T = ' - ';
  if (valorA && valorB && valorC) {
    return valorA + T + valorB + T + valorC;

  } else if (valorA && valorB) {
    return valorA + T + valorB;

  } else if (valorA && valorC) {
    return valorA + T + valorC;

  } else if (valorB && valorC) {
    return valorB + T + valorC;

  } else if (valorA) {
    return valorA;

  } else if (valorB) {
    return valorB;

  } else if (valorC) {
    return valorC;
    
  }
  return '';
}

exports.getProgressoFromStream = getProgressoFromStream;
exports.removeCaracteres = removeCaracteres;
exports.formataTelefone = formataTelefone;
exports.concatenarInformacoes = concatenarInformacoes;
