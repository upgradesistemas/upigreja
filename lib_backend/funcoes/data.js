const moment = require('moment');

// Pega qualquer tipo de data e transforma para o formato padrão do Banco (YYYY-MM-DD HH:MM:SS)
function formatar(data, formato, semHora) {
  if (!data) {
    return null;
  } else if (data instanceof Date) {
    return moment(data).format(formato + ' HH:mm:ss');
  }
  
  data = data.trim();
  const complemento = !semHora && data.indexOf(':') >= 0 ? ' HH:mm:ss' : '';
  let novaData = moment(data, 'DD/MM/YYYY' + complemento).format(formato + complemento);
  if (!novaData || novaData === 'Invalid date') {
    novaData = moment(data, 'DD/MM/YY' + complemento).format(formato + complemento);
  }
  if (!novaData || novaData === 'Invalid date') {
    novaData = moment(data, 'YYYY-MM-DD' + complemento).format(formato + complemento);
  }
  if (!novaData || novaData === 'Invalid date') {
    novaData = null;
  } else if (novaData && novaData.indexOf('1899') >= 0) {
    novaData = null;
  }
  return novaData;
}

exports.formatar = formatar;
