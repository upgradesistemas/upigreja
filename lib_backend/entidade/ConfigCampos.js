const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');

module.exports = class ConfigCampos extends EntidadeSinc {

  constructor() {
    super('tab_config_campos');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_config_campos').pk(),
      field('nome_config_campos').varchar(),
      field('tabela').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
