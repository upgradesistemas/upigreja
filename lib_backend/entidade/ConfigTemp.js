const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');

module.exports = class ConfigTemp extends EntidadeSinc {

  constructor() {
    super('tab_config_temp');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta uma configuração no banco de dados.
   * @param {String} nome O Nome da config.
   */
  static async set(nome) { // TESTAR - fazer funcionar com o esquema de codigoCliente igual o set da config
    const config = await ConfigTemp.buscarPorCampo('nome_config_temp', nome);
    if (config) {
      return;
    }
    await ConfigTemp.salvar({ nome_config_temp: nome });
  }

  // ------------------------------------------------------------------------------------

  /**
   * Recupera o valor de uma config.
   * @param {String} nome O Nome da config.
   */
  static async get(nome) {
    return await ConfigTemp.buscarPorCampo('nome_config_temp', nome);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('nome_config_temp').pk().varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
