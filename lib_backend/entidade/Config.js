const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const database = require('../funcoes/database');

module.exports = class Config extends EntidadeSinc {

  constructor() {
    super('tab_config');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna todos os registros da tabela.
   */
  static async buscarTodos(nomeBanco) {
    const registros = await super.buscarTodos('', nomeBanco);
    const objeto = {};
    for (const reg of registros) {
      objeto[reg.nome_config] = reg.valor;
    }
    return objeto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta uma configuração no banco de dados.
   * @param {String} nome_config O Nome da config.
   * @param {*} valor O Valor.
   */
  static async set(nome_config, valor, nomeBanco) {
    const config = await Config.buscarPorCampo('nome_config', nome_config, nomeBanco);
    if (config) {
      await Config.salvar({
        nome_config,
        valor,
      }, nomeBanco);
    } else {
      await database.query(`INSERT INTO ${nomeBanco}.tab_config set ? `, {
        nome_config,
        valor,
      });
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Recupera o valor de uma config.
   * @param {String} nome_config O Nome da config.
   */
  static async get(nome_config, nomeBanco) {
    const objeto = await Config.buscarPorCampo('nome_config', nome_config, nomeBanco);
    return objeto ? objeto.valor : null;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('nome_config').pk().varchar(),
      field('valor').text(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_config: 'status_input_maiusculo', valor: 'S' },
      { nome_config: 'status_imp_comp_dizimo', valor: 'S' },
    ];
  }

};
