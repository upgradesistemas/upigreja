const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const database = require('../funcoes/database');

module.exports = class PerfilPermissaoTela extends EntidadeSinc {

  constructor() {
    super('tab_perfil_permissao_tela');
  }

  // ------------------------------------------------------------------------------------

  static async excluirPermissoesDoPerfil(codPerfil, nomeBanco) {
    await database.query(`DELETE FROM ${nomeBanco}.tab_perfil_permissao_tela 
      WHERE cod_perfil = ?`, codPerfil);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_perfil_permissao_tela').pk(),
      field('cod_perfil').references('tab_perfil(cod_perfil)'),
      field('status_habilita_tela').enum('N', 'S').default('N'),
      field('rota_tela').varchar(200),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
