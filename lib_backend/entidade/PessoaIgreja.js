const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const database = require('../funcoes/database');

module.exports = class PessoaIgreja extends EntidadeSinc {

  constructor() {
    super('tab_pessoa_igreja');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta o código e o nome da igreja logada
   * @param {Number} codPessoa O código da pessoa que está logada
   * @param {Number} codIgreja O código da igreja que irá ser setada
   * @param {String} nomeIgreja O nome da igreja que irá ser setada
   * @param {String} nomeBanco O nome do banco que está conectado
   */
  static async set(codPessoa, codIgreja, nomeIgreja, nomeBanco) {
    const pessoa_igreja = await PessoaIgreja.buscarPorCampo(
      'cod_pessoa_logada', codPessoa, nomeBanco);
    if (pessoa_igreja) {
      await PessoaIgreja.salvar({
        cod_pessoa_igreja: pessoa_igreja.cod_pessoa_igreja,
        cod_pessoa_logada: codPessoa,
        cod_igreja_logada: codIgreja,
        nome_igreja_logada: nomeIgreja,
      }, nomeBanco);
    } else {
      await database.query(`INSERT INTO ${nomeBanco}.tab_pessoa_igreja set ? `, {
        cod_pessoa_logada: codPessoa,
        cod_igreja_logada: codIgreja,
        nome_igreja_logada: nomeIgreja,
      });
    }
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_pessoa_igreja').pk(),
      field('cod_pessoa_logada').int(),
      field('cod_igreja_logada').int(),
      field('nome_igreja_logada').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
