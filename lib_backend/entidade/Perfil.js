const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const PerfilPermissaoTela = require('./PerfilPermissaoTela');

module.exports = class Perfil extends EntidadeSinc {

  constructor() {
    super('tab_perfil');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(codPerfil, nomeBanco, pessoaExclusao) {
    await PerfilPermissaoTela.excluirPermissoesDoPerfil(codPerfil, nomeBanco);
    await super.excluir(codPerfil, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async salvar(perfil, nomeBanco) {
    const codPerfil = await super.salvar(perfil, nomeBanco);
    // Checa se a perfil tem o objeto de permissão de telas:
    if (perfil.permissoes) {
      await PerfilPermissaoTela.excluirPermissoesDoPerfil(codPerfil, nomeBanco);
      // Agora vamos inserir todas as permissões:
      for (const permissao of perfil.permissoes) {
        delete permissao.cod_perfil_permissao_tela;
        permissao.cod_perfil = codPerfil;
        await PerfilPermissaoTela.salvar(permissao, nomeBanco);
      }
    }
    return codPerfil;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    const perfis = await super.buscarDadosCadastro(filtro);
    for (const perfil of perfis) {
      perfil.permissoes = await PerfilPermissaoTela.buscarTodos(
        `cod_perfil = ${perfil.cod_perfil}`, filtro.nomeBanco);
    }
    return perfis;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarTodos(where, nomeDatabase) {
    const perfis = await super.buscarTodos(where, nomeDatabase);
    for (const perfil of perfis) {
      perfil.permissoes = await PerfilPermissaoTela.buscarTodos(
        `cod_perfil = ${perfil.cod_perfil}`, nomeDatabase);
    }
    return perfis;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_perfil').pk(),
      field('nome_perfil').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_perfil: 'Administrador' },
    ];
  }

};
