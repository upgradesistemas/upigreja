const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const database = require('../funcoes/database');

module.exports = class MsgChat extends EntidadeSinc {

  constructor() {
    super('tab_msg_chat');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Marca todas as mensagens da conversa entre as duas pessoas como "lidas"
   */
  static async marcarMensagensComoLida(codPessoaOrigem, codPessoaDestino) {
    await database.query(`
      UPDATE tab_msg_chat SET status = "lido", dt_hr_lido = now()
      WHERE cod_pessoa_origem = '${codPessoaDestino}'
      AND cod_pessoa_destino = '${codPessoaOrigem}'
      AND dt_hr_lido IS NULL
    `);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna uma lista de mensagens entre duas pessoas, mais conhecido
   * como uma "conversa" no nosso mundo moderno.
   * @param {String|Number} codPessoaOrigem A Pessoa 1
   * @param {String|Number} codPessoaDestino A Pessoa 2
   */
  static async getConversa(codPessoa1, codPessoa2) {
    return await database.query(`
      select 
        a.*,
        DATE_FORMAT(dt_hr_envio, '%d/%m/%Y %H:%i:%s') dt_hr_envio,
        DATE_FORMAT(dt_hr_lido, '%d/%m/%Y %H:%i:%s') dt_hr_lido 
      from tab_msg_chat a
      WHERE 
        (cod_pessoa_origem = '${codPessoa1}' AND cod_pessoa_destino = '${codPessoa2}')
      OR
        (cod_pessoa_destino = '${codPessoa1}' AND cod_pessoa_origem = '${codPessoa2}')
    `);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_msg_chat').pk(),
      field('cod_pessoa_origem').int(11),
      field('cod_pessoa_destino').int(11),
      field('mensagem').text(),
      field('dt_hr_envio').datetime(),
      field('dt_hr_lido').datetime(),
      field('status').enum('enviado', 'lido').default('enviado'),
      field('tipo').enum('funcionario', 'suporte').default('funcionario'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
