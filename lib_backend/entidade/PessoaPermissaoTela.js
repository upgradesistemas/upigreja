const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');
const database = require('../funcoes/database');

module.exports = class PessoaPermissaoTela extends EntidadeSinc {

  constructor() {
    super('tab_pessoa_permissao_tela');
  }

  // ------------------------------------------------------------------------------------

  static async excluirPermissoesDaPessoa(codPessoa, nomeDatabase) {
    await database.query(
      `delete from ${nomeDatabase}.tab_pessoa_permissao_tela where cod_pessoa = ?`, codPessoa);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_pessoa_permissao_tela').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('status_habilita_tela').enum('N', 'S').default('N'),
      field('rota_tela').varchar(200),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { cod_pessoa: 1, status_habilita_tela: 'S', rota_tela: 'dashboard' }, 
      { cod_pessoa: 1, status_habilita_tela: 'S', rota_tela: 'config' }, 
      { cod_pessoa: 1, status_habilita_tela: 'S', rota_tela: 'cadastros/pessoa' },
    ];
  }

};
