const EntidadeSinc = require('../sincronizacao/EntidadeSinc');
const field = require('../sincronizacao/field');

module.exports = class ConfigPessoa extends EntidadeSinc {

  constructor() {
    super('tab_config_pessoa');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna todos os registros da tabela.
   */
  static async buscarTodos(codPessoa, nomeBanco) {
    const registros = await super.buscarTodos(`cod_pessoa = '${codPessoa}'`, nomeBanco);
    const objeto = {};
    for (const reg of registros) {
      objeto[reg.nome_config] = reg.valor;
    }
    return objeto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Seta uma configuração no banco de dados.
   * @param {String} nome O Nome da config.
   * @param {*} valor O Valor.
   */
  static async set(nome, valor, codPessoa, nomeBanco) {
    const config = (await super.buscarTodos(`
      cod_pessoa = '${codPessoa}' 
      and nome_config = '${nome}'
    `, nomeBanco))[0];
    await ConfigPessoa.salvar({
      cod_config_pessoa: config ? config.cod_config_pessoa : null,
      cod_pessoa: codPessoa,
      nome_config: nome,
      valor,
    }, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Recupera o valor de uma config.
   * @param {String} nome O Nome da config.
   */
  static async get(nome, codPessoa, nomeBanco) {
    const objeto = await super.buscarTodos(`
      cod_pessoa = '${codPessoa}' 
      and nome_config = '${nome}'
    `, nomeBanco)[0];
    return objeto ? objeto.valor : null;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_config_pessoa').pk(),
      field('nome_config').varchar(),
      field('valor').text(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
