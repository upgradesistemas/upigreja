const data = require('../funcoes/data');
const { whereExclusao, whereExclusaoAlias } = require('../funcoes/variaveis').variaveis;

/**
 * Classe principal de todas as entidades que serão sincronizadas com o banco
 * de dados. Para uma entidade ser persistida no banco, ela deve herdar dessa
 * classe.
 * @see sincronizador.js
 */
module.exports = class EntidadeSinc {

  constructor(tabela) {
    this.tabela = tabela;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Delete um registro da tabela com o id especificado.
   * @param {String|Number} id O Id da tabela.
   */
  static async excluir(id, nomeDatabase, pessoaExclusao) {
    const instance = new this();
    const chave = instance.getFields().find(x => x.isPk).nome;
    return await database.query(
      `UPDATE ${nomeDatabase}.${instance.tabela} SET
        dt_hr_exclusao = CURRENT_TIMESTAMP(),
        status_exclusao = "S",
        status_ativo = "N",
        cod_usuario_exclusao = ${pessoaExclusao.cod_pessoa}
      WHERE ${chave} = ${id}`);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Encontra e retorna o primeiro registro que bate com o campo e valor.
   * @param {String} campo O Nome do campo
   * @param {*} valor O Valor do campo 
   */
  static async buscarPorCampo(campo, valor, nomeDatabase, ignoraExclusao) {
    const instance = new this();
    return await database.queryFindOne(`
      SELECT * FROM ${nomeDatabase}.${instance.tabela}
      WHERE ${ignoraExclusao ? ' 1 = 1 ' : whereExclusao} AND ${campo} = '${valor}' limit 1
    `);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Converte alguns campos do registro para serem compatíveis com os campos
   * do banco, por exemplo as datas, que são formatados para o formato YYYY-MM-DD.
   * @param {Object} registro O Registro a ser salvo.
   * @return {Object} um novo registro com as propriedades convertidas. 
   */
  static converterCamposParaBanco(registro) {
    const instancia = new this();
    const novoRegistro = {};
    const campos = instancia.getFields();
    for (const key in registro) {
      if (campos.find(x => x.nome === key)) {
        if (key.startsWith('dt')) {
          // Se é uma data corrige ela antes de salvar:
          registro[key] = data.formatar(registro[key], 'YYYY-MM-DD');
        }
        novoRegistro[key] = registro[key];
      }
    }
    return novoRegistro;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Salva o registro no banco de dados, caso ele já possua a chave
   * primária definida então ele é apenas atualizado.
   * @param {Object} registro O Registro a ser salvo.
   */
  static async salvar(registro, nomeDatabase) {
    const instancia = new this();
    return await database.salvar(instancia.tabela, registro, nomeDatabase);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna a quantidade de registros dessa tabela.
   * @return {Number} A Quantidade de registros.
   */
  static async getQtdeRegistros(nomeDatabase) {
    const instance = new this();
    const ret = await database.query(`
      SELECT COUNT(*) qtde FROM ${nomeDatabase}.${instance.tabela}
      WHERE ${whereExclusao}
    `);
    return ret[0].qtde;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Exclui todos os registros da tabela.
   * @param {String} where O Where opcional da exclusão.
   * @return {Array} Todos os registros.
   */
  static async excluirTodos(where, nomeDatabase) { // TESTAR verificar de remover isso
    const instance = new this();
    return await database.query(`
      delete from ${nomeDatabase}.${instance.tabela} ${where ? `where ${where}` : ''}
    `);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Retorna todos os registros da tabela.
   * @param {String} where O Where opcional da busca
   * @return {Array} Todos os registros.
   */
  static async buscarTodos(where, nomeDatabase) {
    const instance = new this();
    const registros = await database.query(`
      SELECT * FROM ${nomeDatabase}.${instance.tabela}
      WHERE ${whereExclusao} ${where ? ` AND ${where}` : ''}
    `);

    for (const registro of registros) {
      for (const key in registro) {
        if (key.startsWith('dt') && registro[key]) {
          // Se é uma data corrige ela antes de resgatar do banco:
          registro[key] = data.formatar(registro[key], 'DD/MM/YYYY');
        }
      }
    }
    return registros;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca os dados de cadastro dessa entidade. Esse método deve ser chamado
   * apenas pelos formulários de cadastros do sistema, e em hipótese alguma
   * em outro lugar.
   */
  static async buscarDadosCadastro(filtro) {
    const instancia = new this();
    const de = filtro.de;
    const ate = filtro.ate;
    const like = filtro.filtro;
    const order = filtro.order;
    let where = (filtro.where ? 'and ' + filtro.where : '') + ` AND ${whereExclusao}`;
    const whereInner = filtro.whereInner;
    const nomeBanco = filtro.nomeBanco;

    const whereLike = !like ? '1' : instancia.formarLikeComCampos(like);

    const aux = (de || ate) ? ` LIMIT ${de}, ${ate - de} ` : '';
    let registros = await this.buscarTodos(
      ` (${whereLike}) ${where} order by ${order || 1} ${aux} `, filtro.nomeBanco);

    if (registros.length <= 0 && like !== '' && whereInner.tabelaPrincipal) {
      where = filtro.whereAlias ? filtro.whereAlias + ' AND ' : '';
      registros = await database.query(`SELECT a.*
        FROM ${nomeBanco}.tab_${whereInner.tabelaPrincipal} a
        INNER JOIN ${nomeBanco}.tab_${whereInner.tabelaInner} b
        ON a.${whereInner.ligacaoTabela1} = b.${whereInner.ligacaoTabela2}
        WHERE ${where} ${whereExclusaoAlias} AND 
        b.${whereInner.campoWhere} LIKE "%${like}%"
      `);
    }
    return registros;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Encontra uma entidade pela chave primária.
   * @param {String|Number} id O Id da entidade.
   */
  static async buscarPorId(id, nomeBanco, ignoraExclusao) {
    const instance = new this();
    const chave = instance.getFields().find(x => x.isPk).nome;
    return await this.buscarPorCampo(chave, id, nomeBanco, ignoraExclusao);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Encontra o primeiro registro no banco.
   */
  static async buscarPrimeiro(where, nomeDatabase) {
    const instance = new this();
    return await database.queryFindOne(`SELECT * FROM 
      ${nomeDatabase}.${instance.tabela}
      WHERE ${whereExclusao} ${where ? ` AND ${where} ` : ''} LIMIT 1`);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Forma uma string com um like em todos os campos, retornando algo do tipo:
   * - (nome like '%a%') or (cod like '%a%') or (cidade like '%a%').
   * @param {String} like O Like para ser comparado.
   */
  formarLikeComCampos(like) {
    return this.getFields().map(x => `(${x.nome} like '%${like}%')`).join(' or ');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Field[]} Lista de fields com os tipos.
   */
  getFields() {
    // Filhos devem implementar
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {String[]} Os inserts padrões ao criar a tabela.
   */
  getDefaults() {
    // Filhos devem implementar
  }

};

const database = require('../funcoes/database');
