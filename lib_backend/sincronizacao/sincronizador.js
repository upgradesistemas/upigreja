const EntidadeSinc = require('./EntidadeSinc');
const database = require('../funcoes/database');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');

let NomeDoBanco;

/**
 * Inicia a sincronização de tabelas, só serão sincronizadas as tabelas
 * que herdarem da EntidadeSinc.
 * @param {String} nomeDatabase o nome do banco de dados
 * @param {String} pasta a pasta que contém as entidades
 */
async function sinc(nomeDatabase, pasta) {
  NomeDoBanco = nomeDatabase;
  try {
    if (pasta !== 'lib_backend/entidade') {
      // Também iremos sincronizar as entidades padrão.
      await sinc(NomeDoBanco, 'lib_backend/entidade');
    }

    console.log('[SINCRONIZADOR] Iniciando processo de verificação do banco ' +
      NomeDoBanco + ' da pasta ' + pasta);
    const entidades = fs.readdirSync(path.join(global.appDir, pasta));
    for (let i = 0; i < entidades.length; i++) {
      const Arquivo = require(path.join(global.appDir, pasta, entidades[i]));
      const instancia = new Arquivo();
      if (instancia instanceof EntidadeSinc) {
        await sincTabela(instancia);
      }
    }

    for (let i = 0; i < entidades.length; i++) {
      // Agora que os campos estão todos certos, vamos colocar as referencias:
      const Arquivo = require(path.join(global.appDir, pasta, entidades[i]));
      const instancia = new Arquivo();
      await associarReferencias(instancia);
    }
    console.log(
      ' *******************************\n' +
      ' *  CONCLUIDO A SINCRONIZAÇÃO  *\n' +
      ' *******************************\n'
    );
  } catch (ex) {
    console.log(ex);
    process.exit(1);
  }
}

/**
 * Cria e retorna o sql da criação do campo.
 * @param {object} campo O Campo da entidade
 */
function _getSqlCriacaoCampo(campo) {
  let sql = '';
  if (campo.isPk) {
    const tipo = campo.tipo === 'varchar' ? 'varchar(255)' : 'int AUTO_INCREMENT';
    sql = ('`' + campo.nome + '` ' + tipo + ' PRIMARY KEY NOT NULL');
  } else if (campo.tipo === 'enum') {
    sql += '`' + campo.nome + '` ';
    sql += campo.tipo + '(' + campo.enumValues + ') ';
    if (campo.valorDefault) {
      sql += 'default "' + campo.valorDefault + '"';
    }
  } else if (campo.tipo === 'datetime' && campo.valorDefault === 'auto') {
    sql += '`' + campo.nome + '` timestamp default CURRENT_TIMESTAMP';
  } else {
    sql += '`' + campo.nome + '` ';
    if (campo.tamanhoCampo) {
      sql += campo.tipo + '(' + campo.tamanhoCampo + ') ';
    } else {
      sql += campo.tipo + ' ';
    }
    if (campo.valorDefault) {
      sql += 'default "' + campo.valorDefault + '"';
    }
  }
  return sql;
}

/**
 * Associa as referências do campos às chaves primarias
 * de outras tabelas.
 * @param {EntidadeSinc} entidade A entidade
 */
async function associarReferencias(entidade) {
  const refs = entidade.getFields();
  if (refs) {
    for (let i = 0; i < refs.length; i++) {

      if (!refs[i].referencia) {
        // Não tem referencia, apenas pula esse campo:
        continue;
      }

      // tab_cidade_cod_cidade_tab_estado_cod_estado
      const nomeConstraint = entidade.tabela + '_' +
        refs[i].nome + '_' +
        refs[i].referencia.replace(/\(|\)|\s+/g, '_');

      await database.querySilent(`
        alter table ${NomeDoBanco}.${entidade.tabela}
          ADD CONSTRAINT ${nomeConstraint}
          FOREIGN KEY (${refs[i].nome})
          REFERENCES ${refs[i].referencia}
      `);
    }
  }
}

/**
 * Sincroniza uma tabela em específico.
 */
async function sincTabela(entidade) {
  const tabela = entidade.tabela;
  const campos = entidade.getFields();

  let camposDropados = 0;
  let camposAdicionados = 0;
  const tabelaExiste = await database.checarSeTabelaExiste(tabela, NomeDoBanco);

  if (!tabelaExiste) {
    console.log(`[TABELA] Criando tabela ${tabela} no banco ${NomeDoBanco}`);
    let sql = `CREATE TABLE IF NOT EXISTS ${NomeDoBanco}.${tabela} (`;

    for (let x = 0; x < campos.length; x++) {
      sql += _getSqlCriacaoCampo(campos[x]) + ', ';
    }

    sql = sql.substring(0, sql.length - 2) + ') ENGINE=MyISAM';
    await database.query(sql);

    // Agora colocamos os defaults:
    const defs = entidade.getDefaults();
    if (defs) {
      for (let i = 0; i < defs.length; i++) {
        if (typeof defs[i] === 'string') {
          // Query pura, apenas realiza:
          await database.query(defs[i]);
        } else {
          // Objeto, insere ele
          await database.query(`INSERT INTO ${NomeDoBanco}.${tabela} set ?`, defs[i]);
        }
      }
    }

    for (let x = 0; x < campos.length; x++) {
      if (campos[x].uniqueValue) {
        await database.querySilent(`DROP INDEX ${campos[x].nome} ON ${NomeDoBanco}.${tabela}`);
        await database.query(`ALTER IGNORE TABLE
        ${NomeDoBanco}.${tabela} ADD UNIQUE ( ${campos[x].nome} )`);
      }
    }
    return; // Criamos a tabela já com a estrutura certa
  }

  // removendo campos que não existem no arquivo: ...
  const camposTabela = await database.getCamposTabela(NomeDoBanco, tabela);
  for (let x = 0; x < camposTabela.length; x++) {
    const campoExiste = _.find(campos, (q) => {
      return q.nome === camposTabela[x];
    });
    if (!campoExiste) {
      camposDropados += 1;
      await database.query(`alter table ${NomeDoBanco}.${tabela} drop column ${camposTabela[x]} `);
    }
  }

  // adicionando campos que existem no arquivo: ...
  for (let x = 0; x < campos.length; x++) {
    const res = await database.query(`show fields from ${NomeDoBanco}.${tabela} 
      like '${campos[x].nome}'`);
    if (res && res.length === 0) {
      // Campo não existe
      camposAdicionados += 1;

      const campo = campos[x];
      const sql = `ALTER TABLE ${NomeDoBanco}.${tabela} ADD COLUMN ` + _getSqlCriacaoCampo(campo);

      await database.query(sql);
    }

    if (campos[x].uniqueValue) {
      await database.querySilent(`DROP INDEX ${campos[x].nome} ON ${NomeDoBanco}.${tabela}`);
      await database.query(`ALTER IGNORE TABLE ${NomeDoBanco}.${tabela} ADD UNIQUE ( ${campos[x].nome} )`);
    }
  }

  // Log informativo:
  if (camposAdicionados === 0 && camposDropados === 0) {
    console.log(`- Sincronização da tabela ${NomeDoBanco}.${tabela} terminou, não foi necessário modificá-la!`);
  } else {
    console.log(`- Sincronização da tabela ${NomeDoBanco}.${tabela} terminou, foi necessário
      derrubar ${camposDropados} campos e adicionar
      ${camposAdicionados} campos!`);
  }
}

exports.sinc = sinc;
