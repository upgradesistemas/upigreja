const ConfigTemp = require('../entidade/ConfigTemp');
const Upgrade = require('../main/Upgrade');

module.exports = class ConfigTempLib {

  async executarConfigs() {
    // await this.executarDev('banco', async () => {
    // 
    // });
  }

  async executar(nomeConfig, callback) {
    try {      
      if (!await ConfigTemp.get(nomeConfig)) {
        await callback();
        await ConfigTemp.set(nomeConfig);
      }
    } catch (ex) {
      console.log(ex);      
    }
  }

  async executarDev(nomeConfig, callback) {
    if (global.dadosAplicacao.ambiente == Upgrade.HOMOLOGACAO) {
      await this.executar(nomeConfig, callback);
    }
  }

};
