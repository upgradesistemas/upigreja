const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Estado extends EntidadeSinc {

  constructor() {
    super('tab_estado');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_estado').pk(),
      field('nome_estado').varchar(),
      field('sigla').varchar(2),
      field('cod_ibge').int(2),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_estado: 'PARANÁ', sigla: 'PR', cod_ibge: 41 }, 
      { nome_estado: 'AMAPÁ', sigla: 'AP', cod_ibge: 16 }, 
      { nome_estado: 'SÃO PAULO', sigla: 'SP', cod_ibge: 35 }, 
      { nome_estado: 'SANTA CATARINA', sigla: 'SC', cod_ibge: 42 }, 
      { nome_estado: 'MINAS GERAIS', sigla: 'MG', cod_ibge: 31 }, 
      { nome_estado: 'RIO DE JANEIRO', sigla: 'RJ', cod_ibge: 33 }, 
      { nome_estado: 'MATO GROSSO', sigla: 'MT', cod_ibge: 51 }, 
      { nome_estado: 'MATO GROSSO DO SUL', sigla: 'MS', cod_ibge: 50 }, 
      { nome_estado: 'RIO GRANDE DO SUL', sigla: 'RS', cod_ibge: 43 }, 
      { nome_estado: 'DISTRITO FEDERAL', sigla: 'DF', cod_ibge: 53 }, 
      { nome_estado: 'GOIÁS', sigla: 'GO', cod_ibge: 52 }, 
      { nome_estado: 'BAHIA', sigla: 'BA', cod_ibge: 29 }, 
      { nome_estado: 'ESPÍRITO SANTO', sigla: 'ES', cod_ibge: 32 }, 
      { nome_estado: 'TOCANTINS', sigla: 'TO', cod_ibge: 17 }, 
      { nome_estado: 'PARÁ', sigla: 'PA', cod_ibge: 15 }, 
      { nome_estado: 'RONDONIA', sigla: 'RO', cod_ibge: 11 }, 
      { nome_estado: 'ALAGOAS', sigla: 'AL', cod_ibge: 27 }, 
      { nome_estado: 'AMAZONAS', sigla: 'AM', cod_ibge: 13 }, 
      { nome_estado: 'CEARÁ', sigla: 'CE', cod_ibge: 23 }, 
      { nome_estado: 'PIAUÍ', sigla: 'PI', cod_ibge: 22 }, 
      { nome_estado: 'PERNAMBUCO', sigla: 'PE', cod_ibge: 26 }, 
      { nome_estado: 'SERGIPE', sigla: 'SE', cod_ibge: 28 }, 
      { nome_estado: 'PARAÍBA', sigla: 'PB', cod_ibge: 25 }, 
      { nome_estado: 'RIO GRANDE DO NORTE', sigla: 'RN', cod_ibge: 24 }, 
      { nome_estado: 'MARANHÃO', sigla: 'MA', cod_ibge: 21 }, 
      { nome_estado: 'ACRE', sigla: 'AC', cod_ibge: 12 }, 
      { nome_estado: 'RORAIMA', sigla: 'RR', cod_ibge: 14 },
    ];
  }
};
