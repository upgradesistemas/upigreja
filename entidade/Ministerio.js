const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Ministerio extends EntidadeSinc {

  constructor() {
    super('tab_ministerio');
  }

  // ------------------------------------------------------------------------------------

  static async buscarNome(codMinisterio, nomeBanco) {
    const ministerio = await super.buscarPorCampo('cod_ministerio', codMinisterio, nomeBanco);
    return ministerio ? ministerio.nome_ministerio : '';
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_ministerio').pk(),
      field('nome_ministerio').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('sigla_ministerio').varchar(20),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_ministerio: 'Pastor Presidente', sigla_ministerio: 'Pr. Pres.' },
      { nome_ministerio: 'Pastor', sigla_ministerio: 'Pr.' },
      { nome_ministerio: 'Evangelista', sigla_ministerio: 'Ev.' },
      { nome_ministerio: 'Presbítero', sigla_ministerio: 'Pb.' },
      { nome_ministerio: 'Diácono', sigla_ministerio: 'Dc.' },
      { nome_ministerio: 'Missionário', sigla_ministerio: 'Mss.' },
      { nome_ministerio: 'Cooperador', sigla_ministerio: 'Cp.' },
      { nome_ministerio: 'Membro', sigla_ministerio: 'Mb.' },
      { nome_ministerio: 'Congregado', sigla_ministerio: 'Cong.' },
    ];
  }

};

