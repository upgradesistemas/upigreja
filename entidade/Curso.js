const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Cidade = require('./Cidade');
const Data = require('../lib_backend/funcoes/data');

module.exports = class Curso extends EntidadeSinc {

  constructor() {
    super('tab_curso');
  }

  // ------------------------------------------------------------------------------------

  static async salvar(curso, nomeBanco) {
    const p = ' - ';
    const cidade = await Cidade.buscarPorCampo('cod_cidade', curso.cod_cidade, nomeBanco);
    curso.nome_curso_comp = curso.nome_curso + p + cidade.nome_cidade_comp + p +
    Data.formatar(curso.dt_inicio, 'DD/MM/YY', true) + p +
    Data.formatar(curso.dt_fim, 'DD/MM/YY', true) + p +
    curso.local;

    const codCurso = await super.salvar(curso, nomeBanco);
    return codCurso;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_curso').pk(),
      field('cod_cidade').references('tab_cidade(cod_cidade)'),
      field('nome_curso').varchar(),
      field('local').varchar(),
      field('nome_curso_comp').varchar(),
      field('dt_inicio').datetime(),
      field('dt_fim').datetime(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
