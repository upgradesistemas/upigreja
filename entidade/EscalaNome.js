const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class EscalaNome extends EntidadeSinc {

  constructor() {
    super('tab_escala_nome');
  }

  // ------------------------------------------------------------------------------------

  static async buscarPorNome(nome, nomeDatabase) {
    const escalaNome = (await EscalaNome.buscarTodos(
      `nome_escala_nome = '${nome}'`, nomeDatabase))[0];
    if (escalaNome) {      
      return escalaNome.cod_escala_nome;
    }
    return await EscalaNome.salvar({ nome_escala_nome: nome }, nomeDatabase);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_escala_nome').pk(),
      field('nome_escala_nome').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
