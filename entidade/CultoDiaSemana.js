const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class CargoPessoa extends EntidadeSinc {

  constructor() {
    super('tab_culto_dia_semana');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_culto_dia_semana').pk(),
      field('cod_dia_semana').references('tab_dia_semana(cod_dia_semana)'),
      field('cod_culto').references('tab_culto(cod_culto)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { cod_dia_semana: 1, cod_culto: 1 },
    ];
  }
};
