const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');
const Distrito = require('./Distrito');

module.exports = class Area extends EntidadeSinc {

  constructor() {
    super('tab_area');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_area';
    const areas = await super.buscarDadosCadastro(filtro);
    for (const ar of areas) {
      ar.pessoa = await Pessoa.buscarPorId(ar.lider_area, filtro.nomeBanco, true);
      ar.distrito = await Distrito.buscarPorId(ar.cod_distrito, filtro.nomeBanco, true);
    }
    return areas;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_area').pk(),
      field('nome_area').varchar(),
      field('lider_area').references('tab_pessoa(cod_pessoa)'),
      field('cod_distrito').references('tab_distrito(cod_distrito)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

