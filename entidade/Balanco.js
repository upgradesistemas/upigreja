const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const SetorBalanco = require('./SetorBalanco');

module.exports = class Balanco extends EntidadeSinc {

  constructor() {
    super('tab_balanco');
  }

  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_objeto';
    const balancos = await super.buscarDadosCadastro(filtro);
    for (const balanco of balancos) {
      balanco.setor = await SetorBalanco.buscarPorId(balanco.cod_setor, filtro.nomeBanco, true);
    }
    return balancos;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_balanco').pk(),
      field('nome_objeto').varchar(),
      field('qtd_objeto').decimal(),
      field('cod_setor').references('tab_setor_balanco(cod_setor)'),
      field('dt_cadastro').datetime(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
    ];
  }

  // -----------------------------------------------------------------------------------
};

