const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');

module.exports = class Departamento extends EntidadeSinc {

  constructor() {
    super('tab_departamento');
  }

  // ------------------------------------------------------------------------------------

  static async departamentoIgreja(codIgreja, nomeBanco) {
    return await database.queryFindOne(`
      SELECT cod_departamento FROM ${nomeBanco}.tab_departamento
      WHERE status_dep_igreja = 'S'
      AND cod_igreja = ${codIgreja}
    `);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_departamento').pk(),
      field('nome_departamento').varchar(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('qtde_cadastrado').int(6).default(0),
      field('status_dep_igreja').enum('S', 'N').default('N'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  // Igreja, EBD, Célula, Jovens, Crianças
  getDefaults() {
    return [
      { nome_departamento: 'Igreja', cod_igreja: 1, status_dep_igreja: 'S' },
      { nome_departamento: 'Escola Bíblica', cod_igreja: 0 },
      { nome_departamento: 'Célula', cod_igreja: 0 },
      { nome_departamento: 'Jovens', cod_igreja: 0 },
      { nome_departamento: 'Crianças', cod_igreja: 0 },
      { nome_departamento: 'Curso Teologia', cod_igreja: 0 },
      { nome_departamento: 'Capelania', cod_igreja: 0 },
      { nome_departamento: 'Outros Cursos', cod_igreja: 0 },
    ];
  }

};
