const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class forma_pagamento extends EntidadeSinc {

  constructor() {
    super('tab_forma_pagamento');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_forma_pagamento').pk(),
      field('nome_forma_pagamento').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_forma_pagamento: 'Dinherio' },
      { nome_forma_pagamento: 'Cartão de Crédito' }, 
      { nome_forma_pagamento: 'Cartão de Débito' }, 
      { nome_forma_pagamento: 'Cheque' },  
      { nome_forma_pagamento: 'Boleto' },
    ];
  }

};

