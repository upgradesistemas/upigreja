const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class TipoDespesa extends EntidadeSinc {

  constructor() {
    super('tab_tipo_despesa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_tipo_despesa').pk(),
      field('nome_despesa').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_despesa: '1.1 Combustível' },
      { nome_despesa: '1.2 Oficina' },
      { nome_despesa: '1.3 Lavagens' },
      { nome_despesa: '1.4 Ipva' },
      { nome_despesa: '1.5 Estacionamento' },
      { nome_despesa: '1.6 Seguro' },
      { nome_despesa: '1.7 Ônibus/Taxi' },
      { nome_despesa: '2.1 Eletricidade' },
      { nome_despesa: '2.2 Telefone' },
      { nome_despesa: '2.3 Celular' },
      { nome_despesa: '2.4 Gás' },
      { nome_despesa: '2.5 Tv A Cabo' },
      { nome_despesa: '2.6 Água' },
      { nome_despesa: '2.7 Internet' },
      { nome_despesa: '2.8 Salário' },
      { nome_despesa: '2.9 Eletroeletrônicos' },
      { nome_despesa: '3.1 Empregada' },
      { nome_despesa: '3.2 Faxineira' },
      { nome_despesa: '3.3 Lavanderia' },
      { nome_despesa: '3.4 Manutenção Da Casa' },
      { nome_despesa: '3.5 Eletrodomésticos' },
      { nome_despesa: '3.6 Iptu' },
      { nome_despesa: '3.7 Condomínio' },
      { nome_despesa: '3.8 Aluguel' },
      { nome_despesa: '3.9 Hotel' },
      { nome_despesa: '4.1 Supermercado' },
      { nome_despesa: '4.2 Padaria' },
      { nome_despesa: '4.3 Hortifruti' },
      { nome_despesa: '4.4 Roupas' },
      { nome_despesa: '5.1 Férias' },
      { nome_despesa: '5.2 Restaurantes' },
      { nome_despesa: '6.1 Escola Bíblica' },
      { nome_despesa: '6.2 Curso Teologia' },
      { nome_despesa: '6.3 Cursos' },
      { nome_despesa: '7.1 Médicos' },
      { nome_despesa: '7.2 Dentista' },
      { nome_despesa: '7.3 Plano De Saúde' },
      { nome_despesa: '7.4 Remédios' },
      { nome_despesa: '7.5 Óculos' },
      { nome_despesa: '8.1 Financiamento Automóvel' },
      { nome_despesa: '8.2 Financiamento Imóvel' },
      { nome_despesa: '9.1 Presentes/Festas' },
      { nome_despesa: '9.2 Livros/Revistas/Jornais' },
      { nome_despesa: '9.3 Mesadas' },
      { nome_despesa: '9.4 Outros Gastos' },
      { nome_despesa: 'Ajuda ao Obreiro' },
      { nome_despesa: 'Guia Previdência Social' },
      { nome_despesa: 'Zeladoria' },
      { nome_despesa: 'Serviço de Limpeza' },
      { nome_despesa: 'FGTS' },
      { nome_despesa: '13° Salário' },
    ];
  }

};

