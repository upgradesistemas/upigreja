const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const moment = require('moment');
const CR = require('./CR');
const Tipo = require('../src/tipo');

module.exports = class Chamada extends EntidadeSinc {

  constructor() {
    super('tab_chamada');
  }

  // ------------------------------------------------------------------------------------

  static async salvar(chamada, finalizar, nomeBanco) {
    // await this.removeCaracteres(chamada);

    const codChamada = await super.salvar(chamada, nomeBanco);

    if (finalizar) {
      const cr = {
        cod_tipo_movimento: Tipo.movimento.chamada,
        vr_receber: chamada.vr_oferta,
        cod_igreja: 1,
        cod_departamento: 1,
        dt_hr_receber: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
        dt_hr_cadastro: moment(new Date()).format('DD/MM/YYYY HH:mm:ss'),
      };

      CR.salvar(cr, nomeBanco);
    }
    
    // depois percorrrer os itens setando o id da chamada para salvar a pessoa chamada
    return codChamada;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_chamada').pk(),
      field('cod_palestrante').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('vr_oferta').decimal(),
      field('dthr_chamada').datetime(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('qtde_visitando').int(6),
      field('qtde_biblias').int(6),
      field('qtde_revista').int(6),
      field('turma').enum('ovelhinhas', 'soldadinhos', 'heideiros'),
      field('tp_chamada').enum('ebd', 'ibadep'),
      field('periodos').enum('1', '2', '3', '4'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
