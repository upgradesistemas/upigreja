const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const CompraItem = require('./CompraItem');
const Produto = require('./Produto');
const Pessoa = require('./Pessoa');
const CP = require('./CP');
const tipo = require('../src/tipo');
const formatarDinheiro = require('../lib_backend/funcoes/numeros').formatarDinheiro;
const database = require('../lib_backend/funcoes/database');

module.exports = class Compra extends EntidadeSinc {

  constructor() {
    super('tab_compra');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    const compras = await super.buscarDadosCadastro(filtro);

    for (const compra of compras) {
      const fornecedor = await Pessoa.buscarPorId(compra.cod_pessoa, filtro.nomeBanco, true);
      compra.fornecedor = fornecedor && fornecedor.nome_pessoa;
      compra.status = compra.status_compra === 'A' ? 'Aberta' : 'Fechada';

      compra.itens = await CompraItem.buscarTodos(`cod_compra = ${compra.cod_compra}`,
        filtro.nomeBanco);
      compra.vr_total_formatado = formatarDinheiro(compra.vr_total);

      for (const item of compra.itens) {
        const produto = await Produto.buscarPorId(item.cod_produto, filtro.nomeBanco, true);
        item.nome_produto = produto.nome_produto;
        item.vr_item_formatado = formatarDinheiro(item.vr_item);
        item.vr_total_item_formatado = formatarDinheiro(item.vr_total_item);
      }

    }
    return compras;
  }

  // ------------------------------------------------------------------------------------

  static async salvar(compra, nomeBanco) {
    const fechar = compra.parcelas && compra.fecharCompra;
    if (fechar) { // Se for fechamento, atribuimos o status e a data de fechamento.
      compra.status_compra = 'F';
      compra.dt_hr_fechamento = new Date();
    }
    // Salvamos a compra com as novas informações ou não.
    const codCompra = await super.salvar(compra, nomeBanco);

    if (fechar) { // Se for fechamento, atribuimos novas informações para as parcelas...
      for (const parcela of compra.parcelas) {
        parcela.cod_forma_pagamento = compra.cod_forma_pagamento;
        parcela.cod_tipo_movimento = tipo.movimento.compra;
        parcela.cod_origem = codCompra;
      }
      await CP.gerarParcelas(compra.parcelas, compra.cod_pessoa, // ... e geramos as parcelas.
        compra.cod_usuario_compra, compra.cod_caixa, nomeBanco);
    }

    if (compra.itens) { // Salvamos os itens da compra independente, fechamento ou não.
      await CompraItem.excluirItensDaCompra(codCompra, nomeBanco);
      for (const item of compra.itens) {
        delete item.cod_compra_item;
        item.cod_compra = codCompra;
        await CompraItem.salvar(item, nomeBanco);
      }
    }
    return codCompra;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(codCompra, nomeBanco, pessoaExclusao) {
    await CompraItem.excluirItensDaCompra(codCompra, nomeBanco);
    await super.excluir(codCompra, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  static async abrirCompra(codCompra, movimento, nomeBanco) {
    await database.query(`DELETE FROM ${nomeBanco}.tab_cp
      WHERE cod_origem = ${codCompra} AND cod_tipo_movimento = ${movimento}`);

    await database.query(`UPDATE ${nomeBanco}.tab_compra SET
      status_compra = "A", dt_hr_fechamento = NULL
    WHERE cod_compra = ${codCompra}`);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_compra').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('cod_forma_pagamento').references('tab_forma_pagamento(cod_forma_pagamento)'),
      field('dt_hr_cadastro').datetime(),
      field('dt_hr_compra').datetime(),
      field('dt_hr_fechamento').datetime(),
      field('dt_hr_prim_parcela').datetime(),
      field('vr_total').decimal(),
      field('vr_total_acrescimo').decimal(),
      field('vr_total_desconto').decimal(),
      field('obs_compra').varchar(),
      field('obs_pagamento').varchar(),
      field('cod_usuario_compra').references('tab_pessoa(cod_pessoa)'),
      field('status_compra').enum('A', 'F').default('A'),
      field('frm_pagamento').enum('A', 'P').default('A'),
      field('nr_documento').varchar(),
      field('intervalo').int(),
      field('qtde_parcela').int(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
