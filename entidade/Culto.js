const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Culto extends EntidadeSinc {

  constructor() {
    super('tab_culto');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_culto').pk(),
      field('nome_culto').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

