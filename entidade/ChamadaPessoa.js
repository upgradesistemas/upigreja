const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class ChamadaPessoa extends EntidadeSinc {

  constructor() {
    super('tab_chamada_pessoa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_chamada_pessoa').pk(),
      field('cod_chamada').references('tab_chamada(cod_chamada'),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa'),
      field('cod_departamento').references('tab_departamento(cod_departamento'),
      field('status_revista').enum('S', 'N').default('N'),
      field('status_biblia').enum('S', 'N').default('N'),
      field('status_presenca').enum('P', 'F', 'FJ').default('F'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
