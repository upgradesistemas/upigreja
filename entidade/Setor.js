const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');
const Area = require('./Area');

module.exports = class Setor extends EntidadeSinc {

  constructor() {
    super('tab_setor');
  }

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_setor';
    const setores = await super.buscarDadosCadastro(filtro);
    for (const setor of setores) {
      setor.pessoa = await Pessoa.buscarPorId(setor.lider_setor, filtro.nomeBanco, true);
      setor.area = await Area.buscarPorId(setor.cod_area, filtro.nomeBanco, true);
    }
    return setores;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_setor').pk(),
      field('nome_setor').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('lider_setor').references('tab_pessoa(cod_pessoa)'),
      field('cod_area').references('tab_area(cod_area)'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

