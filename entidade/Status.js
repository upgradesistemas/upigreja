const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Status extends EntidadeSinc {

  constructor() {
    super('tab_status');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_status').pk(),
      field('nome_status').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_status: 'Afastado' },
      { nome_status: 'Em Comunhão' },
      { nome_status: 'Em Observação' },
      { nome_status: 'Faleceu' },
      { nome_status: 'Se Mudou' },
      { nome_status: 'Transferiu-se' },
      { nome_status: 'Saiu da Igreja' },
      { nome_status: 'Desviado' },
      { nome_status: 'Abandono' },
      { nome_status: 'Desaparecimento' },
      { nome_status: 'Outros' },
    ];
  }

};

