const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const TipoCentro = require('./TipoCentroCusto');

module.exports = class CentroCusto extends EntidadeSinc {

  constructor() {
    super('tab_centro_custo');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    const centros = await super.buscarDadosCadastro(filtro);
    for (const centro of centros) {
      centro.tp_centro = await TipoCentro.buscarPorCampo(
        'cod_tp_centro_custo', centro.cod_tp_centro, filtro.nomeBanco);
    }
    return centros;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_centro_custo').pk(),
      field('nome_centro_custo').varchar(),
      field('cod_tp_centro').references('tab_tp_centro_custo(cod_tp_centro_custo)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_centro_custo: 'Administração', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Contribuições Congregações', cod_tp_centro: 3 }, 
      { nome_centro_custo: 'CIEADEP', cod_tp_centro: 2 }, 
      { nome_centro_custo: 'Despesas de Manutenção', cod_tp_centro: 2 }, 
      { nome_centro_custo: 'Despesas Eventuais', cod_tp_centro: 2 }, 
      { nome_centro_custo: 'Manutenção Infra Estrutura', cod_tp_centro: 2 }, 
      { nome_centro_custo: 'Ministério Ação Social', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Ministério Adoração e Louvor', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Ministério Comunhão', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Ministério Educação Cristã', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Ministério Pastoral', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Ministério Patrimônio', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Missões', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Reunião de Presbitério', cod_tp_centro: 1 }, 
      { nome_centro_custo: 'Transporte', cod_tp_centro: 2 }, 
    ];
  }

};

