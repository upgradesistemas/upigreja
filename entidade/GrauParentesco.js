const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class GrauParentesco extends EntidadeSinc {

  constructor() {
    super('tab_grau_parentesco');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_grau_parentesco').pk(),
      field('nome_grau_parentesco').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_grau_parentesco: 'PAI' },
      { nome_grau_parentesco: 'MÃE' },
      { nome_grau_parentesco: 'AVÔ(Ó)' },
      { nome_grau_parentesco: 'TIO(A)' },
      { nome_grau_parentesco: 'IRMÃO(Ã)' },
      { nome_grau_parentesco: 'PADRASTO' },
      { nome_grau_parentesco: 'MADRASTA' },
      { nome_grau_parentesco: 'CONJUGE' },
      { nome_grau_parentesco: 'FILHO(A)' },
      { nome_grau_parentesco: 'NETO(A)'  },
    ];
  }
};
