const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class CursoPessoa extends EntidadeSinc {

  constructor() {
    super('tab_curso_pessoa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_curso_pessoa').pk(),
      field('cod_curso').references('tab_curso(cod_curso)'),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
