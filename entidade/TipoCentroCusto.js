const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class TipoCentroCusto extends EntidadeSinc {

  constructor() {
    super('tab_tp_centro_custo');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_tp_centro_custo').pk(),
      field('nome_tp').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_tp: 'Ambos' }, 
      { nome_tp: 'Despesas' }, 
      { nome_tp: 'Receitas' }, 
    ];
  }

};

