const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class DivisaoSubGrupoPC extends EntidadeSinc {

  constructor() {
    super('tab_divisao_sub_grupo_pc');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_divisao_sub_grupo_pc').pk(),
      field('codigo_divisao_sub_grupo_pc').varchar(),
      field('nome_divisao_sub_grupo_pc').varchar(),
      field('cod_sub_grupo_pc').references('tab_sub_grupo_pc(cod_sub_grupo_pc)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      {
        cod_divisao_sub_grupo_pc: 1,
        codigo_divisao_sub_grupo_pc: '1.1.2.1',
        nome_divisao_sub_grupo_pc: 'Dízimo',
        cod_sub_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 2,
        codigo_divisao_sub_grupo_pc: '1.1.2.2',
        nome_divisao_sub_grupo_pc: 'Ofertas e Contribuições',
        cod_sub_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 3,
        codigo_divisao_sub_grupo_pc: '1.1.2.3',
        nome_divisao_sub_grupo_pc: 'Boleto',
        cod_sub_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 4,
        codigo_divisao_sub_grupo_pc: '1.1.2.4',
        nome_divisao_sub_grupo_pc: 'Ordinárias ou Regular',
        cod_sub_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 5,
        codigo_divisao_sub_grupo_pc: '1.1.2.5',
        nome_divisao_sub_grupo_pc: 'Outras Receitas',
        cod_sub_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 6,
        codigo_divisao_sub_grupo_pc: '1.1.1.1',
        nome_divisao_sub_grupo_pc: 'Caixa',
        cod_sub_grupo_pc: 1,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 7,
        codigo_divisao_sub_grupo_pc: '1.1.1.2',
        nome_divisao_sub_grupo_pc: 'Bancos com Movimento',
        cod_sub_grupo_pc: 1,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 8,
        codigo_divisao_sub_grupo_pc: '2.1.1.1',
        nome_divisao_sub_grupo_pc: 'Empréstimos e Financiamentos a Pagar',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 9,
        codigo_divisao_sub_grupo_pc: '2.1.1.2',
        nome_divisao_sub_grupo_pc: 'Obrigação com Pessoal',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 10,
        codigo_divisao_sub_grupo_pc: '2.1.1.3',
        nome_divisao_sub_grupo_pc: 'Despesas com Ministério',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 11,
        codigo_divisao_sub_grupo_pc: '2.1.1.4',
        nome_divisao_sub_grupo_pc: 'Despesas Administrativas',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 12,
        codigo_divisao_sub_grupo_pc: '2.1.1.5',
        nome_divisao_sub_grupo_pc: 'Despesas Litúrgicas',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 13,
        codigo_divisao_sub_grupo_pc: '2.1.1.6',
        nome_divisao_sub_grupo_pc: 'Despesas Gerais',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 14,
        codigo_divisao_sub_grupo_pc: '2.1.1.7',
        nome_divisao_sub_grupo_pc: 'Despesas Tributáveis',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 15,
        codigo_divisao_sub_grupo_pc: '2.1.1.8',
        nome_divisao_sub_grupo_pc: 'Manutenção e Reparos',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 16,
        codigo_divisao_sub_grupo_pc: '2.1.1.9',
        nome_divisao_sub_grupo_pc: 'Associação de Curso',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_divisao_sub_grupo_pc: 17,
        codigo_divisao_sub_grupo_pc: '2.1.1.10',
        nome_divisao_sub_grupo_pc: 'Despesas de Serviços',
        cod_sub_grupo_pc: 3,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
    ];
  }

};

