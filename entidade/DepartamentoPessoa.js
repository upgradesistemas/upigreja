const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class DepartamentoPessoa extends EntidadeSinc {

  constructor() {
    super('tab_departamento_pessoa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_departamento_pessoa').pk(),
      field('cod_departamento').references('tab_departamento(cod_departamento)'),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { cod_departamento: 1, cod_pessoa: 1 },
    ];
  }

};
