const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Adesao extends EntidadeSinc {

  constructor() {
    super('tab_adesao');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_adesao').pk(),
      field('nome_adesao').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_adesao: 'Aceitação' },
      { nome_adesao: 'Batismo' },
      { nome_adesao: 'Carta' },
      { nome_adesao: 'Outros' },
      { nome_adesao: 'Recomendação' },
      { nome_adesao: 'Transferência' },
    ];
  }

};

