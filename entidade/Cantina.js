const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const CantinaItem = require('./CantinaItem');
const Produto = require('./Produto');
const Pessoa = require('./Pessoa');
const CR = require('./CR');
const tipo = require('../src/tipo');
const formatarDinheiro = require('../lib_backend/funcoes/numeros').formatarDinheiro;
const database = require('../lib_backend/funcoes/database');

module.exports = class Cantina extends EntidadeSinc {

  constructor() {
    super('tab_cantina');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    const cantinas = await super.buscarDadosCadastro(filtro);

    for (const cantina of cantinas) {
      const pessoa = await Pessoa.buscarPorId(cantina.cod_pessoa, filtro.nomeBanco, true);
      cantina.pessoa = pessoa && pessoa.nome_pessoa;
      cantina.status = cantina.status_cantina === 'A' ? 'Aberta' : 'Fechada';

      cantina.itens = await CantinaItem.buscarTodos(`cod_cantina = ${cantina.cod_cantina}`,
        filtro.nomeBanco);
      cantina.vr_total_formatado = formatarDinheiro(cantina.vr_total);

      for (const item of cantina.itens) {
        const produto = await Produto.buscarPorId(item.cod_produto, filtro.nomeBanco, true);
        item.nome_produto = produto.nome_produto;
        item.vr_item_formatado = formatarDinheiro(item.vr_item);
        item.vr_total_item_formatado = formatarDinheiro(item.vr_total_item);
      }

    }
    return cantinas;
  }

  // ------------------------------------------------------------------------------------

  static async salvar(cantina, nomeBanco) {
    const fechar = cantina.parcelas && cantina.fecharCantina;
    if (fechar) { // Se for fechamento, atribuimos o status e a data de fechamento.
      cantina.status_cantina = 'F';
      cantina.dt_hr_fechamento = new Date();
    }
    // Salvamos a cantina com as novas informações ou não.
    const codCantina = await super.salvar(cantina, nomeBanco);

    if (fechar) { // Se for fechamento, atribuimos novas informações para as parcelas...
      for (const parcela of cantina.parcelas) {
        parcela.cod_forma_pagamento = cantina.cod_forma_pagamento;
        parcela.cod_tipo_movimento = tipo.movimento.cantina;
        parcela.cod_origem = codCantina;
      }
      await CR.gerarParcelas(cantina.parcelas, cantina.cod_pessoa, // ... e geramos as parcelas.
        cantina.cod_usuario_cantina, cantina.cod_caixa, nomeBanco);
    }

    if (cantina.itens) { // Salvamos os itens da cantina independente, fechamento ou não.
      await CantinaItem.excluirItensDaCantina(codCantina, nomeBanco);
      for (const item of cantina.itens) {
        delete item.cod_cantina_item;
        item.cod_cantina = codCantina;
        await CantinaItem.salvar(item, nomeBanco);
      }
    }
    return codCantina;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(codCantina, nomeBanco, pessoaExclusao) {
    await CantinaItem.excluirItensDaCantina(codCantina, nomeBanco);
    await super.excluir(codCantina, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  static async abrirCantina(codCantina, movimento, nomeBanco) {
    await database.query(`DELETE FROM ${nomeBanco}.tab_cr
      WHERE cod_origem = ${codCantina} AND cod_tipo_movimento = ${movimento}`);

    await database.query(`UPDATE ${nomeBanco}.tab_cantina SET
      status_cantina = "A", dt_hr_fechamento = NULL
    WHERE cod_cantina = ${codCantina}`);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cantina').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('cod_forma_pagamento').references('tab_forma_pagamento(cod_forma_pagamento)'),
      field('dt_hr_cadastro').datetime(),
      field('dt_hr_cantina').datetime(),
      field('dt_hr_fechamento').datetime(),
      field('dt_hr_prim_parcela').datetime(),
      field('vr_total').decimal(),
      field('vr_total_acrescimo').decimal(),
      field('vr_total_desconto').decimal(),
      field('obs_cantina').varchar(),
      field('obs_pagamento').varchar(),
      field('cod_usuario_cantina').references('tab_pessoa(cod_pessoa)'),
      field('status_cantina').enum('A', 'F').default('A'),
      field('frm_pagamento').enum('A', 'P').default('A'),
      field('nr_documento').varchar(),
      field('intervalo').int(),
      field('qtde_parcela').int(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
