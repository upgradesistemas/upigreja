const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Bairro extends EntidadeSinc {

  constructor() {
    super('tab_bairro');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_bairro').pk(),
      field('nome_bairro').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_bairro: 'Centro' }, 
    ];
  }

};

