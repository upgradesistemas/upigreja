const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class SubGrupoPC extends EntidadeSinc {

  constructor() {
    super('tab_sub_grupo_pc');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_sub_grupo_pc').pk(),
      field('codigo_sub_grupo_pc').varchar(),
      field('nome_sub_grupo_pc').varchar(),
      field('cod_grupo_pc').references('tab_grupo_pc(cod_grupo_pc)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      {
        cod_sub_grupo_pc: 1,
        codigo_sub_grupo_pc: '1.1.1',
        nome_sub_grupo_pc: 'CAIXA E EQUIVALENTE A CAIXA',
        cod_grupo_pc: 1,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_sub_grupo_pc: 2,
        codigo_sub_grupo_pc: '1.1.2',
        nome_sub_grupo_pc: 'VALORES A RECEBER',
        cod_grupo_pc: 1,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
      {
        cod_sub_grupo_pc: 3,
        codigo_sub_grupo_pc: '2.1.1 ',
        nome_sub_grupo_pc: 'EXIGIBILIDADES A PAGAR',
        cod_grupo_pc: 2,
        status_ativo: 'S',
        cod_usuario_exclusao: null,
        dt_hr_exclusao: null,
        status_exclusao: 'N',
      },
    ];
  }

};

