const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Igreja = require('./Igreja');

module.exports = class Evento extends EntidadeSinc {

  constructor() {
    super('tab_evento');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_evento';
    const eventos = await super.buscarDadosCadastro(filtro);
    for (const evento of eventos) {      
      await this.formataCamposEvento(evento, filtro.nomeBanco);
    }
    return eventos;
  }

  // ------------------------------------------------------------------------------------

  static async formataCamposEvento(evento, nomeBanco) {
    // Adicionamos ao "local" o local digitado na mão, porém como o local igreja
    // é mais importante, antes de retornar verificamos se ele foi informado,
    // caso sim fazemos uma busca pelo código e pegamos o nome da igreja
    evento.local = evento.local_evento;

    if (evento.local_cod_igreja) {
      const igreja = await Igreja.buscarPorId(evento.local_cod_igreja, nomeBanco, true);
      evento.local = igreja.nome_igreja;
    }
    return;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_evento').pk(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('nome_evento').varchar(),
      field('local_evento').varchar(),
      field('cod_cidade').references('tab_cidade(cod_cidade)'),
      field('local_cod_igreja').references('tab_igreja(cod_igreja)'),
      field('dth_inicio').datetime().default('auto'),
      field('dth_fim').datetime(),
      field('obs_evento').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('endereco').varchar(200),
      field('numero').varchar(20),
      field('cod_bairro').references('tab_bairro(cod_bairro)'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};
