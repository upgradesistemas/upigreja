const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');
const Departamento = require('./Departamento');
const data = require('../lib_backend/funcoes/data');
const formatarDinheiro = require('../lib_backend/funcoes/numeros').formatarDinheiro;
const moment = require('moment');
const tipo = require('../src/tipo');
const database = require('../lib_backend/funcoes/database');
const { whereExclusao } = require('../lib_backend/funcoes/variaveis').variaveis;

module.exports = class CP extends EntidadeSinc {

  constructor() {
    super('tab_cp');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Gera parcelas de contas a pagar.
   * @param {Object[]} parcelas As parcelas a gerar.
   * @param {Number} parcelas.numero O Número sequencial da parcela iniciando com 1
   * @param {Number} parcelas.valor O Valor dessa parcela.
   * @param {String} parcelas.data A Data da parcela no formato DD/MM/YYYY.
   * @param {String|Number} codPessoa O Código da pessoa das parcelas.
   * @param {String|Number} codFunc O Código do funcionário que gerou as parcelas.
   */
  static async gerarParcelas(parcelas, codPessoa, codFunc, codCaixa, nomeBanco) {
    for (let i = 0; i < parcelas.length; i++) {
      const parcela = parcelas[i];
      await database.salvar('tab_cp', {
        cod_igreja: parcela.cod_igreja,
        cod_caixa: codCaixa,
        cod_origem: parcela.cod_origem,
        vr_pagar: parcela.valor,
        cod_pessoa: codPessoa,
        cod_funcionario: codFunc,
        cod_tipo_despesa: parcela.cod_tipo_despesa,
        cod_forma_pagamento: parcela.cod_forma_pagamento,
        dt_hr_pagar: moment(parcela.data, 'DD/MM/YYYY').toDate(),
        dt_hr_cadastro: new Date(),
        obs_documento: CP.montarObsDocumento(parcela.cod_origem, i + 1, parcelas.length),
        cod_tipo_movimento: parcela.cod_tipo_movimento || tipo.movimento.cpParcelas,
      }, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(id, nomeBanco, pessoaExclusao) {
    const cp = await CP.buscarPorId(id, nomeBanco);
    if (cp.dt_hr_pago) {
      // Já foi paga, deve ser estornada primeiro:
      throw new Error('Conta a pagar de código ' + id + ' já foi paga, estorne-a primeiro!');
    } else if (cp.cod_cp_origem) {
      // Tem uma origem, nesse caso devemos desvincular a origem e estornar
      await database.salvar('tab_cp', {
        cod_cp: cp.cod_cp_origem,
        cod_cp_destino: null,
      }, nomeBanco);
      await CP.estornar(pessoaExclusao.cod_pessoa, cp.cod_cp_origem, nomeBanco, true);
    }

    // Realmente realiza a exclusão:
    return await super.excluir(id, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Estorna um contas a pagar, se ele não for encontrado no banco
   * ou não estiver sido baixado, um erro é levantado.
   * @param {String|Number} id O Id do contas a pagar. 
   */
  static async estornar(codFuncionarioEstorno, id, nomeBanco) {
    const cp = await CP.buscarPorId(id, nomeBanco);
    if (!cp) {
      throw new Error('Conta a pagar de código ' + id + ' inexistente!');
    } else if (!cp.dt_hr_pago) {
      throw new Error('Conta a pagar de código ' + id + ' não foi paga!');
    } else if (cp.cod_cp_destino) {
      throw new Error('Essa baixa já gerou outras contas, estorne-as primeiro!');
    }

    // Salva as novas informações, limpando todos os atributos dessa conta a pagar.
    await database.salvar('tab_cp', {
      cod_cp: id,
      vr_pago: 0,
      vr_desconto: 0,
      vr_acrescimo: 0,
      status_baixa_parcial: 'N',
      status_estornado: 'S',
      dt_hr_pago: null,
      cod_funcionario_estorno: codFuncionarioEstorno,
      cod_funcionario_baixa: null,
    }, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza uma baixa parcial de uma conta a pagar, isso irá gerar
   * uma nova conta a pagar no valor restante para a mesma pessoa.
   * @param {String|Number} codFuncionario O Código da pessoa que irá pagar a conta.
   * @param {String|Number} codCp O Código da conta a ser paga.
   * @param {Number} valor O Valor a ser pago.
   */
  static async baixarParcial(codFuncionario, codCp, valor, nomeBanco) {
    const cp = await CP.buscarPorId(codCp, nomeBanco);
    if (CP.calcularTotal(cp) === valor) {
      // Os valores a pagar e pago são iguais, não pode!!
      throw new Error(
        'Valor a pagar e pago são iguais! não foi possível ' +
        'realizar a baixa parcial!');
    }

    // Ok, agora vamos atualizar as informações:
    await database.salvar('tab_cp', {
      cod_cp: codCp,
      vr_pago: valor,
      obs_baixa: cp.obs_baixa,
      dt_hr_pago: new Date(),
      status_baixa_parcial: 'S',
      status_estornado: 'N',
      cod_funcionario_baixa: codFuncionario,
      cod_funcionario_estorno: null,
    }, nomeBanco);

    // Agora, como era um recebimento parcial, vamos gerar uma outra conta
    // com o restante que sobrou da primeira:
    const novoId = await database.salvar('tab_cp', {
      ...cp,
      cod_cp: null,
      vr_desconto: 0,
      vr_pago: 0,
      vr_acrescimo: 0,
      obs_documento: 'Ref. ao doc ' + codCp,
      vr_pagar: CP.calcularTotal(cp) - valor,
      obs_baixa: '',
      cod_tipo_movimento: tipo.movimento.cpParcial,
      cod_cp_origem: codCp,
      cod_funcionario: codFuncionario,
      status_estornado: 'N',
      cod_funcionario_baixa: null,
      cod_funcionario_estorno: null,
    }, nomeBanco);

    // Agora vamos colocar o novoId como a cp de destino para a cp original.
    await database.salvar('tab_cp', {
      cod_cp: cp.cod_cp,
      cod_cp_destino: novoId,
    }, nomeBanco);
    return novoId;
  }

  // ------------------------------------------------------------------------------------

  static async salvar(cp, nomeBanco) {
    if (!tipo.movimento.valido(cp.cod_tipo_movimento)) {
      throw new Error('Tipo de movimento ' + cp.cod_tipo_movimento + ' inválido');
    }

    cp.cod_cp = await super.salvar(cp, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza uma baixa completa da conta a pagar. Na real esse processo é 
   * bem simples, vamos apenas realizar algumas validações básicas e simplesmente
   * setar o valor pago = valor pagar.
   * @param {String|Number} codFuncionario O Código da pessoa que irá pagar a conta.
   * @param {String|Number} codCp O Código da conta a ser paga.
   * @param {Number} valor O Valor a ser pago.
   */
  static async baixarCompleto(codFuncionario, codCp, nomeBanco) {
    // Ok, agora vamos salvar as informações:
    const cp = await CP.buscarPorId(codCp, nomeBanco);
    await database.salvar('tab_cp', {
      cod_cp: codCp,
      vr_pago: CP.calcularTotal(cp), // Joga o total como pago
      vr_desconto: cp.vr_desconto,
      vr_acrescimo: cp.vr_acrescimo,
      obs_baixa: cp.obs_baixa,
      dt_hr_pago: new Date(),
      status_estornado: 'N',
      cod_funcionario_baixa: codFuncionario,
      cod_funcionario_estorno: null,
    }, nomeBanco);
    // Pronto, não precisamos fazer mais nada, já foi baixado completo <:-^)--´
  }

  // ------------------------------------------------------------------------------------

  /**
   * Baixa uma conta a pagar.
   * @param {String|Number} codFuncionario O Código da pessoa que irá pagar a conta.
   * @param {String|Number} codCp O Código da conta a ser paga.
   * @param {Number} valor O Valor a ser pago.
   */
  static async baixar(codFuncionario, codCp, valor, nomeBanco) {
    const contaPagar = await super.buscarPorId(codCp, nomeBanco);

    if (!codCp) {
      // "Vamos verificar se a conta a pagar foi salva, não podemos baixar algo
      // que não existe" - Aristóteles.
      throw new Error('Conta a pagar ainda não foi salva, salve antes de pagar!');
    } else if (!contaPagar) {
      throw new Error('Conta a pagar de código ' + codCp + ' inexistente!');
    } else if (contaPagar.dt_hr_pago) {
      throw new Error('Conta a pagar de código ' + codCp + ' já foi paga!');
    } else if (!valor) {
      throw new Error('Valor de recebimento inválido, não é possível baixar a conta!');
    }

    // Se chegou até aqui é porque podemos baixar ela sem problemas
    // agora, vamos identificar se é uma baixa parcial ou não:
    if (CP.calcularTotal(contaPagar) !== valor) {
      // Baixa parcial
      return await CP.baixarParcial(codFuncionario, codCp, valor, nomeBanco);
    }

    // Baixa completa
    return await CP.baixarCompleto(codFuncionario, codCp, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Calcula e retorna o total de uma conta a pagar.
   * @param {CP} cp 
   */
  static calcularTotal(cp) {
    const acrescimo = cp.vr_acrescimo || 0;
    const desconto = cp.vr_desconto || 0;
    return (cp.vr_pagar + acrescimo) - desconto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'dt_hr_pago, dt_hr_pagar';
    const cps = await super.buscarDadosCadastro(filtro);
    for (const cp of cps) {
      cp.pessoa = await Pessoa.buscarPorId(cp.cod_pessoa, filtro.nomeBanco, true);
      cp.departamento = await Departamento.buscarPorId(cp.cod_departamento, filtro.nomeBanco, true);
      cp.dt_hr_pagar = data.formatar(cp.dt_hr_pagar, 'DD/MM/YYYY', true);
      cp.dt_hr_pago = data.formatar(cp.dt_hr_pago, 'DD/MM/YYYY', true);
      cp.vr_pagar_formatado = formatarDinheiro(cp.vr_pagar);
      cp.vr_pago_formatado = formatarDinheiro(cp.vr_pago);
      cp.vr_acrescimo_formatado = formatarDinheiro(cp.vr_acrescimo);
      cp.vr_desconto_formatado = formatarDinheiro(cp.vr_desconto);
      cp.nome_movimento = tipo.getNomeMovimento(cp.cod_tipo_movimento);
      cp.vr_total_formatado =
        formatarDinheiro((cp.vr_pagar + cp.vr_acrescimo) - cp.vr_desconto);

      if (!cp.pessoa && cp.departamento) {
        cp.pessoa = {
          nome_pessoa: tipo.getNomeMovimento(cp.cod_tipo_movimento) +
            ' - ' + cp.departamento.nome_departamento,
        };
      } else if (!cp.pessoa && !cp.departamento) {
        cp.pessoa = {
          nome_pessoa: cp.obs_baixa,
        };
      }
    }
    return cps;
  }

  // ------------------------------------------------------------------------------------

  static montarObsDocumento(codOrigem, indice, qtdeParcelas) {
    return codOrigem ? `${codOrigem} ${indice}/${qtdeParcelas}` : `${indice}/${qtdeParcelas}`;
  }

  // ------------------------------------------------------------------------------------

  static async retornaVrPagamentoCaixaAtual(caixa, nomeBanco) {
    const pago = await database.queryFindOne(`
      SELECT SUM(vr_pago) vr_pagamento FROM ${nomeBanco}.tab_cp
      WHERE ${whereExclusao}
      AND cod_caixa = ${caixa} AND dt_hr_pago IS NOT NULL
    `);
    return pago.vr_pagamento;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cp').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('cod_igreja_referencia').references('tab_igreja(cod_igreja)'),
      field('cod_caixa').references('tab_caixa(cod_caixa)'),
      field('cod_tipo_despesa').references('tab_tipo_despesa(cod_tipo_despesa)'),
      field('dt_hr_cadastro').datetime(),
      field('dt_hr_pagar').datetime(),
      field('dt_hr_pago').datetime(),
      field('vr_pagar').decimal(),
      field('vr_pago').decimal(),
      field('vr_acrescimo').decimal(),
      field('vr_desconto').decimal(),
      field('cod_departamento').int(11).references('tab_departamento(cod_departamento)'),
      field('cod_origem').int(11),
      field('status_baixa_parcial').enum('S', 'N').default('N'),
      field('obs').text(),
      field('obs_documento').text(),
      field('obs_baixa').text(),
      field('status_estornado').enum('S', 'N').default('N'),
      field('cod_funcionario').references('tab_pessoa(cod_pessoa)'),
      field('cod_funcionario_baixa').references('tab_pessoa(cod_pessoa)'),
      field('cod_funcionario_estorno').references('tab_pessoa(cod_pessoa)'),
      field('cod_forma_pagamento').references('tab_forma_pagamento(cod_forma_pagamento)'),
      field('cod_tipo_movimento').int(6),
      field('cod_cp_destino').references('tab_cp(cod_cp)'), // A Cp de destino em caso de parcial
      field('cod_cp_origem').references('tab_cp(cod_cp)'), // A que originou essa em caso de parcial
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
      field('cod_centro_custo').references('tab_centro_custo(cod_centro_custo)'),
      field('cod_tp_saida').references('tab_tp_saida(cod_tp_saida)'),
      field('cod_conta_analise').references('tab_conta_analise_pc(cod_conta_analise_pc)'),
    ];
  }

};
