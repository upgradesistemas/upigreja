const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class EscalaPessoa extends EntidadeSinc {

  constructor() {
    super('tab_escala_pessoa');
  }

  // ------------------------------------------------------------------------------------

  /**
   */
  static async buscarAvulsos(codEscala, nomeDatabase) {
    return await super.buscarTodos(
      `cod_escala = ${codEscala} and cod_escala_grupo is null`, nomeDatabase);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_escala_pessoa').pk(),
      field('cod_escala').references('tab_escala(cod_escala)'),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_departamento').references('tab_departamento(cod_departamento)'),
      field('cod_escala_grupo').references('tab_escala_grupo(cod_escala_grupo)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
