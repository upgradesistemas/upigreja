const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Classe extends EntidadeSinc {

  constructor() {
    super('tab_classe');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_classe').pk(),
      field('nome_classe').varchar(),
      field('fx_etaria').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('qtde_cadastrado').int(6),
      // field('cod_tp_classe').references('tab_tp_classe(cod_tp_classe)'),
      field('cod_departamento').references('tab_departamento(cod_departamento)'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};
