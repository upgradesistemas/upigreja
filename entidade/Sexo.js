const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Sexo extends EntidadeSinc {

  constructor() {
    super('tab_sexo');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_sexo').pk(),
      field('nome_sexo').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_sexo: 'MASCULINO' },
      { nome_sexo: 'FEMININO' },
    ];
  }
};
