const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const data = require('../lib_backend/funcoes/data');
const Bairro = require('./Bairro');
const CelulaPessoa = require('./CelulaPessoa');

module.exports = class Celula extends EntidadeSinc {

  constructor() {
    super('tab_celula');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_celula';
    const celulas = await super.buscarDadosCadastro(filtro);
    for (const celula of celulas) {
      await this.formataCamposCelula(celula, filtro.nomeBanco);
    }
    return celulas;
  }

  // ------------------------------------------------------------------------------------

  static async formataCamposCelula(celula, nomeBanco) {
    // Para datas que estão no banco como Date e não DateTime
    celula.dt_inicial_celula = data.formatar(celula.dt_inicial_celula, 'DD/MM/YYYY', true);
    const T = ' - ';

    // Formatando o campo de endereço
    if (celula.endereco_celula && celula.numero_celula && celula.cod_bairro_celula) {
      const bairro = await Bairro.buscarPorId(celula.cod_bairro_celula, nomeBanco, true);
      celula.enderecoCompleto = celula.endereco_celula + T + celula.numero_celula
        + T + bairro.nome_bairro;

    } else if (celula.endereco_celula && celula.numero_celula) {
      celula.enderecoCompleto = celula.endereco_celula + T + celula.numero_celula;

    } else if (celula.endereco_celula && celula.cod_bairro_celula) {
      const bairro = await Bairro.buscarPorId(celula.cod_bairro_celula, nomeBanco, true);
      celula.enderecoCompleto = celula.endereco_celula + T + bairro.nome_bairro;

    } else {
      celula.enderecoCompleto = celula.endereco_celula;
    }

    celula.status_a = celula.status_ativo === 'S' ? 'Sim' : 'Não';
  }

  // ------------------------------------------------------------------------------------

  static async multiplicarCelula(nomeCelula, ladoDireito, codPessoaExclusao, nomeBanco) {
    const celula = {
      nome_celula: nomeCelula,
    };
    // Salvamos uma nova célula
    const codNovaCelula = await Celula.salvar(celula, nomeBanco);

    // Percorremos o lado direito removendo da célula antiga e já adicionamos a nova célula
    for (let membro of ladoDireito) {
      membro = {
        ...membro,
        status_exclusao: 'S',
        dt_hr_exclusao: new Date(),
        status_ativo: 'N',
        cod_usuario_exclusao: codPessoaExclusao,
      };
      // Removendo o membro da célula antiga
      await CelulaPessoa.salvar(membro, nomeBanco);

      membro.cod_celula = codNovaCelula;
      delete membro.cod_celula_pessoa;
      delete membro.status_exclusao;
      delete membro.dt_hr_exclusao;
      delete membro.status_ativo;
      delete membro.cod_usuario_exclusao;
      // Adicionando o membro a nova célula
      await CelulaPessoa.salvar(membro, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_celula').pk(),
      field('nome_celula').varchar(),
      field('cod_setor').references('tab_setor(cod_setor)'),
      field('lider_celula').references('tab_pessoa(cod_pessoa)'),
      field('lider1_treinamento_celula').references('tab_pessoa(cod_pessoa)'),
      field('lider2_treinamento_celula').references('tab_pessoa(cod_pessoa)'),
      field('anfitriao_celula').references('tab_pessoa(cod_pessoa)'),
      field('cep_celula').varchar(),
      field('endereco_celula').varchar(),
      field('numero_celula').varchar(),
      field('complemento_celula').varchar(),
      field('cod_bairro_celula').references('tab_bairro(cod_bairro)'),
      field('cod_cidade_celula').references('tab_cidade(cod_cidade)'),
      field('cod_dia_reuniao_celula').references('tab_dia_semana(cod_dia_semana)'),
      field('hr_reuniao_celula').time(),
      field('dt_inicial_celula').date(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

