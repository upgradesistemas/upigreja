const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class TipoSaida extends EntidadeSinc {

  constructor() {
    super('tab_tp_saida');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_tp_saida').pk(),
      field('nome_tp_saida').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_tp_saida: 'Fixa' }, // Contas que todo mês tem (Ex: Água, Luz, Telefone)
      { nome_tp_saida: 'Variável' }, // Contas que acontecem uma vez ou outra.
      { nome_tp_saida: 'Investimeno' }, // Compra de Terreno, casa, veículos e etc.
    ];
  }

};

