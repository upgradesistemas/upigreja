const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class GrauParentescoPessoa extends EntidadeSinc {

  constructor() {
    super('tab_grau_parentesco_pessoa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_grau_parentesco_pessoa').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_parentesco_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_grau_parentesco').references('tab_grau_parentesco(cod_grau_parentesco)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
