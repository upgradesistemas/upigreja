const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class SetorIgreja extends EntidadeSinc {

  constructor() {
    super('tab_setor_igreja');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_setor_igreja').pk(),
      field('nome_setor_igreja').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_setor_igreja: 'Setor 1' },
      { nome_setor_igreja: 'Setor 2' },
    ];
  }

};

