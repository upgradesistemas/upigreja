const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');

module.exports = class EscalaGrupo extends EntidadeSinc {

  constructor() {
    super('tab_escala_grupo');
  }

  // ------------------------------------------------------------------------------------

  static async buscarPorEscala(codEscala, nomeDatabase) {
    const grupos = await database.query(`
      SELECT a.*, b.nome_escala_nome from ${nomeDatabase}.tab_escala_grupo a 
      INNER JOIN ${nomeDatabase}.tab_escala_nome b USING(cod_escala_nome) 
      WHERE cod_escala = '${codEscala}' 
      ORDER BY 1 ASC
    `);
    let sequencial = 0;
    for (const grupo of grupos) {
      grupo.sequencial = ++sequencial;
      grupo.pessoas = await database.query(`
        SELECT * from ${nomeDatabase}.tab_escala_pessoa 
        WHERE cod_escala_grupo = '${grupo.cod_escala_grupo}'
        ORDER BY 1 ASC
      `);
    }
    return grupos;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_escala_grupo').pk(),
      field('cod_escala_nome').references('tab_escala_nome(cod_escala_nome)'),
      field('cod_escala').references('tab_escala(cod_escala)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
