const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');

module.exports = class SolicitacaoVisOra extends EntidadeSinc {

  constructor() {
    super('tab_solicitacao_vis_ora');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'status_solicitacao, grau_pedido';
    const solicitacoes = await super.buscarDadosCadastro(filtro);
    for (const solicitacao of solicitacoes) {
      await this.formataCampos(solicitacao, filtro.nomeBanco);
    }
    return solicitacoes;
  }

  // ------------------------------------------------------------------------------------

  static async formataCampos(solicitacao, nomeBanco) {
    const pessoa = await Pessoa.buscarPorId(solicitacao.cod_pessoa, nomeBanco);
    solicitacao.membro = pessoa.nome_pessoa;
    solicitacao.situacao = solicitacao.grau_pedido === 'U' ? 'Urgente' : 'Em Espera';
    if (solicitacao.tp_solicitacao === 'V') {
      solicitacao.tipo = 'Visita';

    } else if (solicitacao.tp_solicitacao === 'O') {
      solicitacao.tipo = 'Oração';

    } else {
      solicitacao.tipo = 'Ambos';

    }
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_solicitacao_vis_ora').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_capelao').references('tab_pessoa(cod_pessoa)'),
      field('dt_solicitacao').datetime(),
      field('dt_agendada').datetime(),
      field('tp_solicitacao').enum('V', 'O', 'A').default('V'), // Visita, Oracao ou Ambos
      field('status_solicitacao').enum('P', 'V').default('P'), // Pendente ou Visitado
      field('obs_pre').text(), // Antes da visita
      field('obs_pos').text(), // Depois da visita
      field('tp_membro_solicitado').enum('N', 'I', 'A'), // Normal, Idoso, aclamado
      field('local_visita').enum('DOM', 'HOS', 'PRES', 'O'), // Domici., Hospi., Presídio, Outros
      field('grau_pedido').enum('U', 'E').default('E'), // Urgente, Espera
      field('pessoa_necessitada').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};
