const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const EscalaPessoa = require('./EscalaPessoa');
const EscalaGrupo = require('./EscalaGrupo');
const database = require('../lib_backend/funcoes/database');

module.exports = class Escala extends EntidadeSinc {

  constructor() {
    super('tab_escala');
  }  

  // ------------------------------------------------------------------------------------

  /**
   * Delete um registro da tabela com o id especificado.
   * @param {String|Number} id O Id da tabela.
   */
  static async excluir(id, nomeBanco, pessoaExclusao) {
    await super.excluir(id, nomeBanco, pessoaExclusao);
    const grupos = await EscalaGrupo.buscarPorEscala(id, nomeBanco);
    for (const c of grupos) {
      await EscalaGrupo.excluir(c.cod_escala_grupo, nomeBanco);
      for (const d of c.pessoas) {
        await EscalaPessoa.excluir(d.cod_escala_pessoa, nomeBanco);
      }
    }
    const pessoas = await EscalaPessoa.buscarAvulsos(id, nomeBanco);
    for (const d of pessoas) {
      await EscalaPessoa.excluir(d.cod_escala_pessoa, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Busca e retorna uma escala de acordo com o código especificado. É retornado
   * uma escala com todos os objetos formatados dentro dela, ou seja, além dos dados
   * da escala tem um objeto de grupos e outro de pessoas.
   */
  static async buscarEscalaModal(codEscala, nomeBanco) {
    const escala = (await super.buscarTodos(`cod_escala = '${codEscala}'`, nomeBanco))[0];
    if (escala) {
      escala.grupos = await EscalaGrupo.buscarPorEscala(escala.cod_escala, nomeBanco);
      escala.pessoas = await EscalaPessoa.buscarAvulsos(escala.cod_escala, nomeBanco);
    }
    return escala;
  }

  // ------------------------------------------------------------------------------------

  static async buscarEscalasCalendario(codCalendario, nomeBanco) {
    return await database.query(`
      SELECT a.*, 
        (SELECT nome_escala_nome FROM ${nomeBanco}.tab_escala_grupo x 
          INNER JOIN ${nomeBanco}.tab_escala_nome q USING(cod_escala_nome)
          WHERE x.cod_escala = a.cod_escala limit 1) AS nomePrimeiroGrupo
      FROM ${nomeBanco}.tab_escala a 
      WHERE a.cod_calendario = '${codCalendario}'
    `);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_escala').pk(),
      field('cod_calendario').references('tab_calendario(cod_calendario)'),
      field('dthr_escala').datetime(),
      field('obs').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
