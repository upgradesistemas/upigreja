const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');

module.exports = class CantinaItem extends EntidadeSinc {

  constructor() {
    super('tab_cantina_item');
  }

  // ------------------------------------------------------------------------------------

  static async excluirItensDaCantina(codCantina, nomeDatabase) {
    await database.query(
      `DELETE FROM ${nomeDatabase}.tab_cantina_item WHERE cod_cantina = ?`, codCantina);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cantina_item').pk(),
      field('cod_cantina').references('tab_cantina(cod_cantina)'),
      field('cod_produto').references('tab_produto(cod_produto)'),
      field('qtde_item').int(6),
      field('vr_item').decimal(),
      field('vr_total_item').decimal(),
      field('vr_acrescimo_item').decimal(),
      field('vr_desconto_item').decimal(),
      field('obs_item').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
  
};
