const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class EstruturaPC extends EntidadeSinc {

  constructor() {
    super('tab_estrutura_pc');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_estrutura_pc').pk(),
      field('codigo_estrutura_pc').varchar(), // Ex: "1". Ativo
      field('nome_estrutura_pc').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { codigo_estrutura_pc: '1.', nome_estrutura_pc: 'Ativo' }, 
      { codigo_estrutura_pc: '2.', nome_estrutura_pc: 'Passivo' }, 
    ];
  }

};

