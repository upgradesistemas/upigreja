const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Escala = require('./Escala');

module.exports = class Calendario extends EntidadeSinc {

  constructor() {
    super('tab_calendario');
  }
  
  // ------------------------------------------------------------------------------------

  /**
   * Delete um registro da tabela com o id especificado.
   * @param {String|Number} id O Id da tabela.
   */
  static async excluir(id, nomeDatabase, pessoaExclusao) {
    const escalas = await Escala.buscarTodos(` cod_calendario = ${id} `, nomeDatabase);
    for (const escala of escalas) {
      await Escala.excluir(escala.cod_escala);
    }
    await super.excluir(id, nomeDatabase, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_calendario').pk(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('nome_calendario').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
