const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');
const Bairro = require('./Bairro');
const Cidade = require('./Cidade');
const PessoaPermissaoTela = require('../lib_backend/entidade/PessoaPermissaoTela');
const data = require('../lib_backend/funcoes/data');
const funcoes = require('../lib_backend/funcoes/utils');
const Ministerio = require('./Ministerio');
const Igreja = require('./Igreja');

module.exports = class Pessoa extends EntidadeSinc {

  constructor() {
    super('tab_pessoa');
  }

  // ------------------------------------------------------------------------------------

  /**
   * @return {Pessoa[]} Todos os usuários do sistema;
   */
  static async getUsuarios(nomeBanco) {
    return await Pessoa.buscarTodos('status_usuario = "S"', nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda a senha da pessoa especificada.
   * @param {String|Number} codPessoa O código da pessoa.
   * @param {String} senha A senha da pessoa.
   */
  static async mudarSenha(codPessoa, senha, nomeBanco) {
    await database.query(`
      UPDATE ${nomeBanco}.tab_pessoa SET senha = ${senha ? `md5("${senha.toUpperCase()}")` : '""'}
      WHERE cod_pessoa = ?`, [codPessoa]);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda o código da igreja do usuário enviado por parâmetro
   * @param {String|Number} codUsuario O código do usuário que irá modificar a igreja.
   * @param {String} igreja A igreja a ser salva.
   */
  static async mudarIgreja(codUsuario, igreja, nomeBanco) {
    await database.query(`
      UPDATE ${nomeBanco}.tab_pessoa SET cod_igreja = ?
      WHERE cod_pessoa = ?`, [igreja, codUsuario]);
  }

  // ------------------------------------------------------------------------------------

  static async salvar(pessoa, nomeBanco, retornaPessoa) {
    await this.removeCaracteres(pessoa);

    const codPessoa = await super.salvar(pessoa, nomeBanco);
    // Checa se a pessoa tem o objeto de permissão de telas:
    if (pessoa.permissoes) {
      await PessoaPermissaoTela.excluirPermissoesDaPessoa(codPessoa, nomeBanco);
      // Agora vamos inserir todas as permissões:
      for (const permissao of pessoa.permissoes) {
        delete permissao.cod_pessoa_permissao_tela;
        permissao.cod_pessoa = codPessoa;
        await PessoaPermissaoTela.salvar(permissao, nomeBanco);
      }
    }

    return retornaPessoa ? await this.buscarPessoaComplementos(codPessoa, nomeBanco) : codPessoa;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(codPessoa, nomeBanco, pessoaExclusao) {
    await PessoaPermissaoTela.excluirPermissoesDaPessoa(codPessoa, nomeBanco);
    await super.excluir(codPessoa, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_pessoa';
    const pessoas = await super.buscarDadosCadastro(filtro);
    for (const pessoa of pessoas) {
      await this.formataCamposPessoa(pessoa, filtro.nomeBanco);
      pessoa.permissoes = await
        PessoaPermissaoTela.buscarTodos(`cod_pessoa = ${pessoa.cod_pessoa}`, filtro.nomeBanco);
      const ministerio = await
        Ministerio.buscarTodos(`cod_ministerio = ${pessoa.cod_ministerio}`, filtro.nomeBanco);
      pessoa.ministerio = ministerio.length ? ministerio[0].nome_ministerio : '';
    }
    return pessoas;
  }

  // ------------------------------------------------------------------------------------

  static async formataCamposPessoa(pessoa, nomeBanco) {
    // Para datas que estão no banco como Date e não DateTime
    pessoa.dt_nascimento = data.formatar(pessoa.dt_nascimento, 'DD/MM/YYYY', true);
    const T = ', ';

    // Formatando o campo de endereço
    if (pessoa.endereco && pessoa.numero && pessoa.cod_bairro) {
      const bairro = await Bairro.buscarPorId(pessoa.cod_bairro, nomeBanco, true);
      pessoa.enderecoCompleto = pessoa.endereco + T + pessoa.numero + T + pessoa.complemento +
        T + bairro.nome_bairro;

    } else if (pessoa.endereco && pessoa.numero) {
      pessoa.enderecoCompleto = pessoa.endereco + T + pessoa.numero;

    } else if (pessoa.endereco && pessoa.cod_bairro) {
      const bairro = await Bairro.buscarPorId(pessoa.cod_bairro, nomeBanco, true);
      pessoa.enderecoCompleto = pessoa.endereco + T + bairro.nome_bairro;

    } else {
      pessoa.enderecoCompleto = pessoa.endereco;

    }

    // Formatando o campo de cidade estado
    if (pessoa.cod_cidade) {
      const filtro = {
        where: ` cod_cidade = ${pessoa.cod_cidade}`,
        nomeBanco,
      };
      const cid = await Cidade.buscarDadosCadastro(filtro);
      if (cid) {
        pessoa.cidadeCompleta = cid[0].nome_cidade_comp;
      }
    }

    // Formatando o campo de telefone
    pessoa.tel_1 = funcoes.formataTelefone(pessoa.tel_1);
    pessoa.tel_2 = funcoes.formataTelefone(pessoa.tel_2);
    pessoa.tel_3 = funcoes.formataTelefone(pessoa.tel_3);
    pessoa.telefones = funcoes.concatenarInformacoes(pessoa.tel_1, pessoa.tel_2, pessoa.tel_3);

    pessoa.status_a = pessoa.status_ativo === 'S' ? 'Sim' : 'Não';
  }

  // ------------------------------------------------------------------------------------

  static async removeCaracteres(pessoa) {
    pessoa.tel_1 = funcoes.removeCaracteres(pessoa.tel_1);
    pessoa.tel_2 = funcoes.removeCaracteres(pessoa.tel_2);
    pessoa.tel_3 = funcoes.removeCaracteres(pessoa.tel_3);
    pessoa.cpf = funcoes.removeCaracteres(pessoa.cpf);
    pessoa.cep = funcoes.removeCaracteres(pessoa.cep);
    pessoa.forn_cnpj = funcoes.removeCaracteres(pessoa.forn_cnpj);
    pessoa.tel_1_representante = funcoes.removeCaracteres(pessoa.tel_1_representante);
    pessoa.tel_2_representante = funcoes.removeCaracteres(pessoa.tel_2_representante);
    pessoa.tel_3_representante = funcoes.removeCaracteres(pessoa.tel_3_representante);

    return;
  }

  // ------------------------------------------------------------------------------------

  static async retornaUsuarios(igreja, nomeBanco) {
    let usuarios = await database.query(`
      SELECT * FROM ${nomeBanco}.tab_pessoa WHERE status_usuario = 'S'
      AND cod_igreja = ${igreja}`);
    usuarios = {
      usuarios,
      qtde: usuarios.length,
    };
    return usuarios;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Muda o código da igreja do usuário enviado por parâmetro
   * @param {String} memLocal Passar true se for para Membros do Local
   * @param {String} congreLocal Passar true se for para Congregados do Local
   * @param {String} memCampo Passar true se for para Membros do Campo
   * @param {String} congreCampo Passar true se for para Congregados do Campo
   */
  static async retornaMembrosCongregados(igreja, nomeBanco, memLocal, congreLocal,
    memCampo, congreCampo) {
      let pessoas;

      if (memLocal) {
        pessoas = await database.query(`
          SELECT * FROM ${nomeBanco}.tab_pessoa
          WHERE status_membro = 'M' AND status_ativo = 'S'
          AND cod_igreja = ${igreja}
        `);
      } else if (congreLocal) {
        pessoas = await database.query(`
          SELECT * FROM ${nomeBanco}.tab_pessoa
          WHERE status_membro = 'C' AND status_ativo = 'S'
          AND cod_igreja = ${igreja}
        `);
      } else if (memCampo) {
        pessoas = await database.query(`
          SELECT * FROM ${nomeBanco}.tab_pessoa
          WHERE status_membro = 'M' AND status_ativo = 'S'
        `);
      } else if (congreCampo) {
        pessoas = await database.query(`
          SELECT * FROM ${nomeBanco}.tab_pessoa
          WHERE status_membro = 'C' AND status_ativo = 'S'
        `);
      }

    pessoas = {
      pessoas,
      qtde: pessoas.length,
    };
    return pessoas;
  }

  // ------------------------------------------------------------------------------------

  static async retornaAniversariantes(igreja, nomeBanco) {
    let aniversariantes = await database.query(`
      SELECT
        nome_pessoa,
        DAY(dt_nascimento) dia,
        DATE_FORMAT(dt_nascimento, "%d/%m/%Y") nascimento,
        (year(curdate()) - year(dt_nascimento)) anos,
        if(status_ativo = "S", "Sim", "Não") ativo
      FROM ${nomeBanco}.tab_pessoa 
      WHERE MONTH(dt_nascimento) = MONTH(curdate())
      AND cod_igreja = ${igreja}
      ORDER BY dia
    `);

    aniversariantes = {
      aniversariantes,
      qtde: aniversariantes.length,
    };
    return aniversariantes;
  }

  // ------------------------------------------------------------------------------------

  static async retornaAniversariantesCasamento(igreja, nomeBanco) {
    let niverCasamento = await database.query(`
      SELECT
        nome_pessoa,
        conjuge,
        DAY(dt_casamento) dia,
        DATE_FORMAT(dt_casamento, "%d/%m/%Y") casamento,
        (year(curdate()) - year(dt_casamento)) anos
      FROM ${nomeBanco}.tab_pessoa 
      WHERE MONTH(dt_casamento) = MONTH(curdate())
      AND cod_igreja = ${igreja}
      ORDER BY dia
    `);

    niverCasamento = {
      niverCasamento,
      qtde: niverCasamento.length,
    };
    return niverCasamento;
  }

  // ------------------------------------------------------------------------------------

  static async alteraStatusAtivoPessoa(codPessoa, status, nomeBanco) {
    const pessoa = await Pessoa.buscarPorId(codPessoa, nomeBanco);
    if (pessoa) {
      pessoa.status_ativo = status;
      return Pessoa.salvar(pessoa, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  static async buscarMembros(codPessoa, nomeBanco, codigoCliente) {
    let pessoa;

    if (codPessoa) {
      pessoa = await this.buscarPessoaComplementos(codPessoa, nomeBanco);
    } else {
      pessoa = await super.buscarTodos(`status_membro = "M" and cod_igreja = ${codigoCliente}`,
        nomeBanco);
    }

    return pessoa;
  }

  // ------------------------------------------------------------------------------------

  static async buscarPessoaComplementos(codPessoa, nomeBanco) {
    const pessoa = await super.buscarPorId(codPessoa, nomeBanco);
    await this.formataCamposPessoa(pessoa, nomeBanco);

    pessoa.nome_ministerio = await Ministerio.buscarNome(pessoa.cod_ministerio, nomeBanco);
    pessoa.nome_Igreja = await Igreja.buscarNome(pessoa.cod_igreja, nomeBanco);

    return pessoa;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_pessoa').pk(),
      field('nome_pessoa').varchar(),
      field('apelido').varchar(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('status_disciplina').enum('S', 'N').default('N'),
      // N NENHUM / M MEMBRO / V VISITANTE / C CONGREGADO / S SIMPATIZANTE
      field('status_membro').enum('N', 'M', 'V', 'C', 'S').default('M'),
      field('status_lider').enum('S', 'N').default('N'),
      field('status_professor').enum('S', 'N').default('N'),
      field('dt_cadastro').datetime().default('auto'),
      field('dt_nascimento').date(),
      field('mae').varchar(),
      field('pai').varchar(),
      field('endereco').varchar(200),
      field('numero').varchar(20),
      field('cod_cidade').references('tab_cidade(cod_cidade)'),
      field('cod_bairro').references('tab_bairro(cod_bairro)'),
      field('tel_1').varchar(15),
      field('tel_2').varchar(15),
      field('tel_3').varchar(15),
      field('email').varchar(100),
      field('natural_cod_cidade').references('tab_cidade(cod_cidade)'),
      field('cpf').varchar(20),
      field('rg').varchar(20),
      field('orgao_expedidor').varchar(20),
      field('dt_batismo_agua').datetime(),
      field('dt_batismo_espirito_santo').datetime(),
      field('cod_estado_civil').references('tab_estado_civil(cod_estado_civil)'),
      field('conjuge').varchar(),
      field('dt_casamento').datetime(),
      field('cod_sexo').references('tab_sexo(cod_sexo)'),
      field('status_fornecedor').enum('S', 'N').default('N'),
      field('status_usuario').enum('S', 'N').default('N'),
      field('status_funcionario').enum('S', 'N').default('N'),
      field('usuario').varchar(),
      field('senha').varchar(),
      field('cep').varchar(10),
      field('cod_nivel').references('tab_nivel(cod_nivel)'),
      field('cod_perfil').references('tab_perfil(cod_perfil)'),
      field('cod_ministerio').references('tab_ministerio(cod_ministerio)'),
      field('salario').decimal(),
      field('carga_horaria').decimal(),
      field('funcao').varchar(),
      field('dt_admissao').datetime(),
      field('dt_demissao').datetime(),
      field('num_conta_corrente').varchar(),
      field('num_agencia').varchar(),
      field('cod_banco').references('tab_banco(cod_banco)'),
      field('forn_razao_social').varchar(),
      field('forn_cnpj').varchar(50),
      field('forn_inscricao_estadual').varchar(20),
      field('forn_representante').varchar(),
      field('tel_1_representante').varchar(50),
      field('tel_2_representante').varchar(50),
      field('tel_3_representante').varchar(50),
      field('cod_status').references('tab_status(cod_status)'),
      field('condicao_membro').varchar(),
      field('dt_conversao').datetime(),
      field('dt_membresia').datetime(),
      field('tp_conversao').varchar(),
      field('igreja_procedencia').varchar(),
      field('cod_adesao').references('tab_adesao(cod_adesao)'),
      field('status_batismo_es').enum('S', 'N').default('N'),
      field('status_dizimista').enum('S', 'N').default('N'),
      field('dt_disciplina_inicial').datetime(),
      field('dt_disciplina_final').datetime(),
      field('complemento').varchar(),
      field('cod_escolaridade').references('tab_escolaridade(cod_escolaridade)'),
      field('status_cabeca_familia').enum('S', 'N').default('N'),
      field('status_conjuge_membro').enum('S', 'N').default('N'),
      field('qtde_filhos').int(6),
      field('cod_profissao').references('tab_profissao(cod_profissao)'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
      field('status_capelao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [{
      cod_pessoa: 1,
      cod_nivel: 1,
      cod_igreja: 1,
      nome_pessoa: 'Upgrade Sistemas',
      status_usuario: 'S',
      status_fornecedor: 'S',
      status_membro: 'N',
      usuario: 'Suporte',
      senha: '1abf95ea6311e59937b8e2b0d034ab22', // Senha Padrão do Suporte
    }];
  }
};

/**
 * Importação de dados de outro banco do cliente:
 * insert into igreja.tab_pessoa (
 *    nome_pessoa, cod_igreja, status_membro, dt_nascimento, mae, pai, cep,
 *    endereco, numero, complemento, tel_1, tel_2, tel_3, email, cpf, rg,
 *    dt_batismo_agua, dt_batismo_espirito_santo, conjuge, dt_casamento)
 */
