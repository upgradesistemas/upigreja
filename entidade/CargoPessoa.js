const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class CargoPessoa extends EntidadeSinc {

  constructor() {
    super('tab_cargo_pessoa');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cargo_pessoa').pk(),
      field('cod_cargo').references('tab_cargo(cod_cargo)'),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { cod_cargo: 1, cod_pessoa: 1 },
    ];
  }

};
