const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Nivel extends EntidadeSinc {

  constructor() {
    super('tab_nivel');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_nivel').pk(),
      field('nome_nivel').varchar(),
      field('permite_permissao_tela').enum('S', 'N').default('N'),
      field('dash_ve_tudo').enum('S', 'N').default('N'),
      field('dash_ve_pessoas_campo').enum('S', 'N').default('N'),
      field('dash_ve_igrejas_cadastradas').enum('S', 'N').default('N'),
      field('dash_ve_pessoas_local').enum('S', 'N').default('N'),
      field('dash_ve_usuarios_registrados').enum('S', 'N').default('N'),
      field('dash_ve_valores_futuros').enum('S', 'N').default('N'),
      field('dash_ve_valor_caixa_atual').enum('S', 'N').default('N'),
      field('dash_ve_aniversariantes').enum('S', 'N').default('N'),
      field('dash_ve_eventos').enum('S', 'N').default('S'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_nivel: 'Administrador', permite_permissao_tela: 'S' },
    ];
  }

};
