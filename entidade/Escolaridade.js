const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Escolaridade extends EntidadeSinc {

  constructor() {
    super('tab_escolaridade');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_escolaridade').pk(),
      field('nome_escolaridade').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_escolaridade: 'NÃO ALFABETIZADO' },
      { nome_escolaridade: 'ENSINO PRIMÁRIO' },
      { nome_escolaridade: 'ENSINO FUNDAMENTAL' },
      { nome_escolaridade: 'ENSINO FUNDAMENTAL INCOMPLETO' },
      { nome_escolaridade: 'ENSINO MÉDIO CURSANDO' },
      { nome_escolaridade: 'ENSINO MÉDIO INCOMPLETO' },
      { nome_escolaridade: 'ENSINO MÉDIO COMPLETO' },
      { nome_escolaridade: 'ENSINO SUPERIOR CURSANDO' },
      { nome_escolaridade: 'ENSINO SUPERIOR INCOMPLETO' },
      { nome_escolaridade: 'ENSINO SUPERIOR COMPLETO' },
      { nome_escolaridade: 'CURSO TÉCNICO CURSANDO' },
      { nome_escolaridade: 'CURSO TÉCNICO INCOMPLETO' },
      { nome_escolaridade: 'CURSO TÉCNICO COMPLETO' },
      { nome_escolaridade: 'PÓS - GRADUAÇÃO CURSANDO' },
      { nome_escolaridade: 'PÓS - GRADUAÇÃO INCOMPLETO' },
      { nome_escolaridade: 'PÓS - GRADUAÇÃO COMPLETO' },
      { nome_escolaridade: 'MESTRADO CURSANDO' },
      { nome_escolaridade: 'MESTRADO INCOMPLETO' },
      { nome_escolaridade: 'MESTRADO COMPLETO' },
      { nome_escolaridade: 'DOUTORADO CURSANDO' },
      { nome_escolaridade: 'DOUTORADO INCOMPLETO' },
      { nome_escolaridade: 'DOUTORADO COMPLETO' },
    ];
  }

};
