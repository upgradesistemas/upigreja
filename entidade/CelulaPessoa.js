const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');

module.exports = class CelulaPessoa extends EntidadeSinc {

  constructor() {
    super('tab_celula_pessoa');
  }

  // ------------------------------------------------------------------------------------

  static async buscarMembrosCelula(codCelula, nomeBanco) {
    const membrosCelulas = await CelulaPessoa.buscarTodos(`cod_celula = ${codCelula}`, nomeBanco);
    for (const membroCelula of membrosCelulas) {
      const membro = await Pessoa.buscarPorId(membroCelula.cod_membro, nomeBanco);
      membroCelula.nome_membro = membro.nome_pessoa;
    }
    return membrosCelulas;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_celula_pessoa').pk(),
      field('cod_celula').references('tab_celula(cod_celula)'),
      field('cod_membro').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

