const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class SetorBalanco extends EntidadeSinc {

  constructor() {
    super('tab_setor_balanco');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_setor').pk(),
      field('nome_setor').varchar(),
      field('cod_resp_setor').references('tab_pessoa(cod_pessoa)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // -----------------------------------------------------------------------------------
};

