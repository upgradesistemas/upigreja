const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const funcoes = require('../lib_backend/funcoes/utils');
const database = require('../lib_backend/funcoes/database');
const Bairro = require('./Bairro');
const Pessoa = require('./Pessoa');

module.exports = class Igreja extends EntidadeSinc {

  constructor() {
    super('tab_igreja');
  }

  // ------------------------------------------------------------------------------------

  static async buscarNome(codIgreja, nomeBanco) {
    const igreja = await super.buscarPorCampo('cod_igreja', codIgreja, nomeBanco);
    return igreja ? igreja.nome_igreja : '';
  }

  // ------------------------------------------------------------------------------------

  static async salvar(igreja, nomeBanco) {
    await this.removeCaracteres(igreja);

    return await super.salvar(igreja, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_igreja';
    const igrejas = await super.buscarDadosCadastro(filtro);
    for (const igreja of igrejas) {
      await this.formataCamposIgreja(igreja, filtro.nomeBanco);
    }
    return igrejas;
  }

  // ------------------------------------------------------------------------------------

  static async removeCaracteres(igreja) {
    igreja.cnpj = funcoes.removeCaracteres(igreja.cnpj);
    igreja.cep = funcoes.removeCaracteres(igreja.cep);
    igreja.tel_1 = funcoes.removeCaracteres(igreja.tel_1);
    igreja.tel_2 = funcoes.removeCaracteres(igreja.tel_2);
    igreja.tel_3 = funcoes.removeCaracteres(igreja.tel_3);

    return;
  }

  // ------------------------------------------------------------------------------------

  static async formataCamposIgreja(igreja, nomeBanco) {
    const T = ' - ';
    // Formatando o campo de endereço
    if (igreja.endereco_igreja && igreja.numero && igreja.cod_bairro) {
      const bairro = await Bairro.buscarPorId(igreja.cod_bairro, nomeBanco, true);
      igreja.enderecoCompleto = igreja.endereco_igreja + T + igreja.numero + T + bairro.nome_bairro;

    } else if (igreja.endereco_igreja && igreja.numero) {
      igreja.enderecoCompleto = igreja.endereco_igreja + T + igreja.numero;

    } else if (igreja.endereco_igreja && igreja.cod_bairro) {
      const bairro = await Bairro.buscarPorId(igreja.cod_bairro, nomeBanco, true);
      igreja.enderecoCompleto = igreja.endereco_igreja + T + bairro.nome_bairro;

    } else {
      igreja.enderecoCompleto = igreja.endereco_igreja;

    }

    // Formatando o campo de telefone
    igreja.tel_1 = funcoes.formataTelefone(igreja.tel_1);
    igreja.tel_2 = funcoes.formataTelefone(igreja.tel_2);
    igreja.tel_3 = funcoes.formataTelefone(igreja.tel_3);
    igreja.telefones = funcoes.concatenarInformacoes(igreja.tel_1, igreja.tel_2, igreja.tel_3);

    igreja.status_a = igreja.status_ativo === 'S' ? 'Sim' : 'Não';
  }

  // ------------------------------------------------------------------------------------

  static async retornaPresidente(nomeBanco, igrejaLogada) {
    const igreja = await Igreja.buscarPorId(igrejaLogada, nomeBanco);
    if (igreja && igreja.cod_presidente) {
      const pessoa = await Pessoa.buscarPorId(igreja.cod_presidente, nomeBanco);
      return pessoa;
    }
  }

  // ------------------------------------------------------------------------------------

  static async retornaIgrejaLogada(pessoaLogada, nomeBanco) {
    return await database.queryFindOne(`
      SELECT cod_igreja_logada AS Igreja, nome_igreja_logada AS NomeIgreja
      FROM ${nomeBanco}.tab_pessoa_igreja WHERE cod_pessoa_logada = ${pessoaLogada}
    `);
  }

  // ------------------------------------------------------------------------------------

  static async nomeIgreja(codIgreja, nomeBanco) {
    return await database.queryFindOne(`SELECT nome_igreja FROM ${nomeBanco}.tab_igreja
      WHERE cod_igreja = ${codIgreja}`);
  }

  // ------------------------------------------------------------------------------------

  static async retornaTodasIgrejas(nomeBanco) {
    let igrejas = await database.query(`SELECT * FROM ${nomeBanco}.tab_igreja`);
    igrejas = {
      igrejas,
      qtde: igrejas.length,
    };
    return igrejas;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_igreja').pk(),
      // S SEDE / SUB SUB-SEDE/ C CONGREGAÇÃO
      field('status_igreja').enum('S', 'SUB', 'C').default('C'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('nome_igreja').varchar(),
      field('razao_social').varchar(),
      field('cnpj').varchar(50),
      field('inscricao_estadual').varchar(20),
      field('cep').varchar(10),
      field('endereco_igreja').varchar(),
      field('numero').varchar(20),
      field('cod_cidade').references('tab_cidade(cod_cidade)'),
      field('cod_bairro').references('tab_bairro(cod_bairro)'),
      field('tel_1').varchar(15),
      field('tel_2').varchar(15),
      field('tel_3').varchar(15),
      field('email').varchar(),
      field('dt_cadastro').datetime().default('auto'),
      field('dt_inauguracao').datetime(),
      field('cod_resp_igreja').references('tab_pessoa(cod_pessoa)'),
      field('cod_esposa_resp').references('tab_pessoa(cod_pessoa)'),
      field('dt_posse').date(),
      field('dt_saida').date(),
      field('cod_setor_igreja').references('tab_setor_igreja(cod_setor_igreja)'),
      field('margem_porc').double(),
      field('cod_presidente').references('tab_pessoa(cod_pessoa)'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [{
      cod_igreja: 1, status_igreja: 'S', nome_igreja: 'NOME DA IGREJA',
    }];
  }
};
