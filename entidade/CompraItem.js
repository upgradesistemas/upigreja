const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');

module.exports = class CompraItem extends EntidadeSinc {

  constructor() {
    super('tab_compra_item');
  }

  // ------------------------------------------------------------------------------------

  static async excluirItensDaCompra(codCompra, nomeDatabase) {
    await database.query(
      `DELETE FROM ${nomeDatabase}.tab_compra_item WHERE cod_compra = ?`, codCompra);
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_compra_item').pk(),
      field('cod_compra').references('tab_compra(cod_compra)'),
      field('cod_produto').references('tab_produto(cod_produto)'),
      field('qtde_item').int(6),
      field('vr_item').decimal(),
      field('vr_total_item').decimal(),
      field('vr_acrescimo_item').decimal(),
      field('vr_desconto_item').decimal(),
      field('obs_item').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
  
};
