const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Transferencia extends EntidadeSinc {

  constructor() {
    super('tab_transferencia');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_transferencia').pk(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('nome_igreja').varchar(),
      field('cod_usuario_transferencia').references('tab_pessoa(cod_pessoa)'),
      field('dt_transferencia').datetime(),
      field('vr_transferido').double(),
      field('porc_transferido').varchar(),
      field('mes_transferido').varchar(),
      field('ano_transferido').varchar(),
      field('status_transferido').enum('S', 'N').default('N'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
