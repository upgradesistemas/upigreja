const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');
const Departamento = require('./Departamento');
const data = require('../lib_backend/funcoes/data');
const formatarDinheiro = require('../lib_backend/funcoes/numeros').formatarDinheiro;
const moment = require('moment');
const tipo = require('../src/tipo');
const database = require('../lib_backend/funcoes/database');
const Igreja = require('./Igreja');
const { whereExclusao } = require('../lib_backend/funcoes/variaveis').variaveis;

module.exports = class CR extends EntidadeSinc {

  constructor() {
    super('tab_cr');
  }

  // ------------------------------------------------------------------------------------

  /**
   * Gera parcelas de contas a receber.
   * @param {Object[]} parcelas As parcelas a gerar.
   * @param {Number} parcelas.numero O Número sequencial da parcela iniciando com 1
   * @param {Number} parcelas.valor O Valor dessa parcela.
   * @param {String} parcelas.data A Data da parcela no formato DD/MM/YYYY.
   * @param {String|Number} codPessoa O Código da pessoa das parcelas.
   * @param {String|Number} codFunc O Código do funcionário que gerou as parcelas.
   */
  static async gerarParcelas(parcelas, codPessoa, codFunc, codCaixa, nomeBanco) {
    for (let i = 0; i < parcelas.length; i++) {
      const parcela = parcelas[i];
      await database.salvar('tab_cr', {
        cod_igreja: parcela.cod_igreja,
        cod_caixa: codCaixa,
        cod_origem: parcela.cod_origem,
        vr_receber: parcela.valor,
        cod_pessoa: codPessoa,
        cod_funcionario: codFunc,
        dt_hr_receber: moment(parcela.data, 'DD/MM/YYYY').toDate(),
        dt_hr_cadastro: new Date(),
        obs_documento: CR.montarObsDocumento(parcela.cod_origem, i + 1, parcelas.length),
        cod_tipo_movimento: parcela.cod_tipo_movimento || tipo.movimento.crParcelas,
        cod_forma_pagamento: parcela.cod_forma_pagamento,
      }, nomeBanco);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async excluir(id, nomeBanco, pessoaExclusao) {
    const cr = await CR.buscarPorId(id, nomeBanco);
    if (cr.dt_hr_recebido) {
      // Já foi recebida, deve ser estornada primeiro:
      throw new Error('Conta a receber de código ' + id + ' já foi recebida, estorne-a primeiro!');
    } else if (cr.cod_cr_origem) {
      // Tem uma origem, nesse caso devemos desvincular a origem e estornar
      await database.salvar('tab_cr', {
        cod_cr: cr.cod_cr_origem,
        cod_cr_destino: null,
      }, nomeBanco);
      await CR.estornar(pessoaExclusao.cod_pessoa, cr.cod_cr_origem, nomeBanco, true);
    }

    // Realmente realiza a exclusão:
    return await super.excluir(id, nomeBanco, pessoaExclusao);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Estorna um contas a receber, se ele não for encontrado no banco
   * ou não estiver sido baixado, um erro é levantado.
   * @param {String|Number} id O Id do contas a receber. 
   */
  static async estornar(codFuncionarioEstorno, id, nomeBanco) {
    const cr = await CR.buscarPorId(id, nomeBanco);
    if (!cr) {
      throw new Error('Conta a receber de código ' + id + ' inexistente!');
    } else if (!cr.dt_hr_recebido) {
      throw new Error('Conta a receber de código ' + id + ' não foi recebida!');
    } else if (cr.cod_cr_destino) {
      throw new Error('Essa baixa já gerou outras contas, estorne-as primeiro!');
    }

    // Salva as novas informações, limpando todos os atributos dessa conta a receber.
    await database.salvar('tab_cr', {
      cod_cr: id,
      vr_recebido: 0,
      vr_desconto: 0,
      vr_acrescimo: 0,
      status_baixa_parcial: 'N',
      status_estornado: 'S',
      dt_hr_recebido: null,
      cod_funcionario_estorno: codFuncionarioEstorno,
      cod_funcionario_baixa: null,
    }, nomeBanco);

    const mov = cr.cod_tipo_movimento;
    const oferta = tipo.movimento.oferta;
    const dizimo = tipo.movimento.dizimo;

    if ((mov === oferta) || (mov === dizimo)) { // TESTAR esse metodo, pois não vejo utilidade
      // em ter ele. Se estornamos uma oferta já vamos excluir ela:
      const pessoaExclusao = await Pessoa.buscarPorId(codFuncionarioEstorno, nomeBanco);
      await CR.excluir(id, nomeBanco, pessoaExclusao);
    }
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza uma baixa parcial de uma conta a receber, isso irá gerar
   * uma nova conta a receber no valor restante para a mesma pessoa.
   * @param {String|Number} codFuncionario O Código da pessoa que irá receber a conta.
   * @param {String|Number} codCr O Código da conta a ser recebida.
   * @param {Number} valor O Valor a ser recebido.
   */
  static async baixarParcial(codFuncionario, codCr, valor, nomeBanco) {
    const cr = await CR.buscarPorId(codCr, nomeBanco);
    if (CR.calcularTotal(cr) === valor) {
      // Os valores a receber e recebido são iguais, não pode!!
      throw new Error(
        'Valor a receber e recebido são iguais! não foi possível ' +
        'realizar a baixa parcial!');
    }

    // Ok, agora vamos atualizar as informações:
    await database.salvar('tab_cr', {
      cod_cr: codCr,
      vr_recebido: valor,
      obs_baixa: cr.obs_baixa,
      dt_hr_recebido: new Date(),
      status_baixa_parcial: 'S',
      status_estornado: 'N',
      cod_funcionario_baixa: codFuncionario,
      cod_funcionario_estorno: null,
    }, nomeBanco);

    // Agora, como era um recebimento parcial, vamos gerar uma outra conta
    // com o restante que sobrou da primeira:
    const novoId = await database.salvar('tab_cr', {
      ...cr,
      cod_cr: null,
      vr_desconto: 0,
      vr_recebido: 0,
      vr_acrescimo: 0,
      obs_documento: 'Ref. ao doc ' + codCr,
      vr_receber: CR.calcularTotal(cr) - valor,
      obs_baixa: '',
      cod_tipo_movimento: tipo.movimento.crParcial,
      cod_cr_origem: codCr,
      cod_funcionario: codFuncionario,
      status_estornado: 'N',
      cod_funcionario_baixa: null,
      cod_funcionario_estorno: null,
    }, nomeBanco);

    // Agora vamos colocar o novoId como a cr de destino para a cr original.
    await database.salvar('tab_cr', {
      cod_cr: cr.cod_cr,
      cod_cr_destino: novoId,
    }, nomeBanco);
    return novoId;
  }

  // ------------------------------------------------------------------------------------

  static async salvar(cr, nomeBanco) {
    if (!tipo.movimento.valido(cr.cod_tipo_movimento)) {
      throw new Error('Tipo de movimento ' + cr.cod_tipo_movimento + ' inválido');
    }

    cr.cod_cr = await super.salvar(cr, nomeBanco);

    const mov = cr.cod_tipo_movimento;
    const oferta = tipo.movimento.oferta;
    const dizimo = tipo.movimento.dizimo;
    const chamada = tipo.movimento.chamada;

    if ((mov === oferta) || (mov === dizimo) || (mov === chamada)) {
      // Se é uma oferta devemos automaticamente baixar ela:
      await CR.baixarCompleto(cr.cod_funcionario, cr.cod_cr, nomeBanco);
    }

    if (cr.cod_igreja_transferencia &&
      cr.cod_igreja_transferencia !== Number(0) &&
      cr.cod_tipo_movimento !== tipo.movimento.transfCredito) {
      await this.receberTransferencia(cr, nomeBanco);
    }
    return cr.cod_cr;
  }

  // ------------------------------------------------------------------------------------

  /**
   * Realiza uma baixa completa da conta a receber. Na real esse processo é 
   * bem simples, vamos apenas realizar algumas validações básicas e simplesmente
   * setar o valor recebido = valor receber.
   * @param {String|Number} codFuncionario O Código da pessoa que irá receber a conta.
   * @param {String|Number} codCr O Código da conta a ser recebida.
   * @param {Number} valor O Valor a ser recebido.
   */
  static async baixarCompleto(codFuncionario, codCr, nomeBanco) {
    // Ok, agora vamos salvar as informações:
    const cr = await CR.buscarPorId(codCr, nomeBanco);
    await database.salvar('tab_cr', {
      cod_cr: codCr,
      vr_recebido: CR.calcularTotal(cr), // Joga o total como recebido
      vr_desconto: cr.vr_desconto,
      vr_acrescimo: cr.vr_acrescimo,
      obs_baixa: cr.obs_baixa,
      dt_hr_recebido: new Date(),
      status_estornado: 'N',
      cod_funcionario_baixa: codFuncionario,
      cod_funcionario_estorno: null,
    }, nomeBanco);
    // Pronto, não precisamos fazer mais nada, já foi baixado completo <:-^)--´
  }

  // ------------------------------------------------------------------------------------

  /**
   * Baixa uma conta a receber.
   * @param {String|Number} codFuncionario O Código da pessoa que irá receber a conta.
   * @param {String|Number} codCr O Código da conta a ser recebida.
   * @param {Number} valor O Valor a ser recebido.
   */
  static async baixar(codFuncionario, codCr, valor, nomeBanco) {
    const contaReceber = await super.buscarPorId(codCr, nomeBanco);

    if (!codCr) {
      // "Vamos verificar se a conta a receber foi salva, não podemos baixar algo
      // que não existe" - Aristóteles.
      throw new Error('Conta a receber ainda não foi salva, salve antes de receber!');
    } else if (!contaReceber) {
      throw new Error('Conta a receber de código ' + codCr + ' inexistente!');
    } else if (contaReceber.dt_hr_recebido) {
      throw new Error('Conta a receber de código ' + codCr + ' já foi recebida!');
    } else if (!valor) {
      throw new Error('Valor de recebimento inválido, não é possível baixar a conta!');
    }

    // Se chegou até aqui é porque podemos baixar ela sem problemas
    // agora, vamos identificar se é uma baixa parcial ou não:
    if (CR.calcularTotal(contaReceber) !== valor) {
      // Baixa parcial
      return await CR.baixarParcial(codFuncionario, codCr, valor, nomeBanco);
    }

    // Baixa completa
    return await CR.baixarCompleto(codFuncionario, codCr, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  /**
   * Calcula e retorna o total de uma conta a receber.
   * @param {CR} cr 
   */
  static calcularTotal(cr) {
    const acrescimo = cr.vr_acrescimo || 0;
    const desconto = cr.vr_desconto || 0;
    return (cr.vr_receber + acrescimo) - desconto;
  }

  // ------------------------------------------------------------------------------------

  /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    // filtro.order = 'dt_hr_recebido desc, dt_hr_receber desc';
    filtro.order = 'dt_hr_cadastro desc';
    const crs = await super.buscarDadosCadastro(filtro);
    for (const cr of crs) {
      cr.pessoa = await Pessoa.buscarPorId(cr.cod_pessoa, filtro.nomeBanco, true);
      cr.departamento = await Departamento.buscarPorId(cr.cod_departamento, filtro.nomeBanco, true);
      cr.dt_hr_receber = data.formatar(cr.dt_hr_receber, 'DD/MM/YYYY', true);
      cr.dt_hr_recebido = data.formatar(cr.dt_hr_recebido, 'DD/MM/YYYY', true);
      cr.vr_receber_formatado = formatarDinheiro(cr.vr_receber);
      cr.vr_recebido_formatado = formatarDinheiro(cr.vr_recebido);
      cr.vr_acrescimo_formatado = formatarDinheiro(cr.vr_acrescimo);
      cr.vr_desconto_formatado = formatarDinheiro(cr.vr_desconto);
      cr.nome_movimento = tipo.getNomeMovimento(cr.cod_tipo_movimento);
      cr.vr_total_formatado =
        formatarDinheiro((cr.vr_receber + cr.vr_acrescimo) - cr.vr_desconto);
      // Se não tiver pessoa informada é porque é uma oferta, então coloca
      // no "nome_pessoa" o nome do tipo de movimento, só para não ficar vazio.
      if (!cr.pessoa && cr.departamento) {
        cr.pessoa = {
          nome_pessoa: tipo.getNomeMovimento(cr.cod_tipo_movimento) +
            ' - ' + cr.departamento.nome_departamento,
        };
      } else if (!cr.pessoa && !cr.departamento) {
        cr.pessoa = {
          nome_pessoa: cr.obs_baixa,
        };
      }
    }
    return crs;
  }

  // ------------------------------------------------------------------------------------

  static montarObsDocumento(codOrigem, indice, qtdeParcelas) {
    return codOrigem ? `${codOrigem} ${indice}/${qtdeParcelas}` : `${indice}/${qtdeParcelas}`;
  }

  // ------------------------------------------------------------------------------------

  static async receberTransferencia(cReceber, nomeBanco) {
    const igreja = await Igreja.nomeIgreja(cReceber.cod_igreja_transferencia, nomeBanco);
    cReceber.dt_hr_receber = cReceber.dt_hr_receber ||
      moment(new Date()).format('YYYY-MM-DD HH:mm');
    cReceber.dt_hr_recebido = cReceber.dt_hr_receber;
    cReceber.vr_recebido = cReceber.vr_receber;
    cReceber.obs = 'Transferência de Valores';
    cReceber.obs_baixa = 'Transf. da Igreja ' + igreja.nome_igreja;
    cReceber.cod_funcionario_baixa = cReceber.cod_funcionario;
    cReceber.cod_tipo_movimento = tipo.movimento.transfCredito;
    await CR.salvar(cReceber, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  static async retornaVrRecebimentoCaixaAtual(caixa, nomeBanco) {
    const recebido = await database.queryFindOne(`
      SELECT SUM(vr_recebido) vr_recebimento FROM ${nomeBanco}.tab_cr
      WHERE ${whereExclusao}
      AND cod_caixa = ${caixa} AND dt_hr_recebido IS NOT NULL
    `);
    return recebido.vr_recebimento;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cr').pk(),
      field('cod_pessoa').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('cod_caixa').references('tab_caixa(cod_caixa)'),
      field('dt_hr_cadastro').datetime(),
      field('dt_hr_receber').datetime(),
      field('dt_hr_recebido').datetime(),
      field('vr_receber').decimal(),
      field('vr_recebido').decimal(),
      field('vr_acrescimo').decimal(),
      field('vr_desconto').decimal(),
      field('cod_departamento').int(11).references('tab_departamento(cod_departamento)'),
      field('cod_origem').int(11),
      field('status_baixa_parcial').enum('S', 'N').default('N'),
      field('tipo_oferta').enum('NOR', 'ANON', 'IDENT').default('NOR'),
      field('obs').text(),
      field('obs_documento').text(),
      field('obs_baixa').text(),
      field('status_estornado').enum('S', 'N').default('N'),
      field('cod_funcionario').references('tab_pessoa(cod_pessoa)'),
      field('cod_funcionario_baixa').references('tab_pessoa(cod_pessoa)'),
      field('cod_funcionario_estorno').references('tab_pessoa(cod_pessoa)'),
      field('cod_forma_pagamento').references('tab_forma_pagamento(cod_forma_pagamento)'),
      field('cod_tipo_movimento').int(6),
      field('cod_cr_destino').references('tab_cr(cod_cr)'), // A Cr de destino em caso de parcial
      field('cod_cr_origem').references('tab_cr(cod_cr)'), // A que originou essa em caso de parcial
      field('cod_igreja_transferencia').references('tab_igreja(cod_igreja)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
      field('imprimir_comp').enum('S', 'N').default('N'),
      field('cod_centro_custo').references('tab_centro_custo(cod_centro_custo)'),
      field('cod_conta_analise').references('tab_conta_analise_pc(cod_conta_analise_pc)'),
    ];
  }

};
