const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class EstadoCivil extends EntidadeSinc {

  constructor() {
    super('tab_estado_civil');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_estado_civil').pk(),
      field('nome_estado_civil').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_estado_civil: 'Solteiro(a)' },
      { nome_estado_civil: 'Casado(a)' },
      { nome_estado_civil: 'Divorciado(a)' },
      { nome_estado_civil: 'Viúvo(a)' },
      { nome_estado_civil: 'Outros' },
      { nome_estado_civil: 'Amasiado(a)' },
    ];
  }

};
