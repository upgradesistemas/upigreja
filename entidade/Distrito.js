const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const Pessoa = require('./Pessoa');
const Igreja = require('./Igreja');

module.exports = class Distrito extends EntidadeSinc {

  constructor() {
    super('tab_distrito');
  }

  // ------------------------------------------------------------------------------------

    /**
   * @override
   */
  static async buscarDadosCadastro(filtro) {
    filtro.order = 'nome_distrito';
    const dists = await super.buscarDadosCadastro(filtro);
    for (const dt of dists) {
      dt.pessoa = await Pessoa.buscarPorId(dt.lider_distrito, filtro.nomeBanco, true);
      dt.igreja = await Igreja.buscarPorId(dt.cod_igreja, filtro.nomeBanco, true);
    }
    return dists;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_distrito').pk(),
      field('nome_distrito').varchar(),
      field('lider_distrito').references('tab_pessoa(cod_pessoa)'),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};

