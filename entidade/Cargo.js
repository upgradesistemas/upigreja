const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Cargo extends EntidadeSinc {

  constructor() {
    super('tab_cargo');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_cargo').pk(),
      field('nome_cargo').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_cargo: 'Presbitero' },
      { nome_cargo: 'Auxiliar do Pastor' },
      { nome_cargo: 'Louvor' },
      { nome_cargo: 'Mocidade' },
      { nome_cargo: 'Senhoras' },
      { nome_cargo: 'Senhores' },
      { nome_cargo: 'Dirtetor de Diaconato' },
      { nome_cargo: 'Direteor de Patrimônio' },
      { nome_cargo: 'Vice Presidente' },
      { nome_cargo: 'Secretário' },
      { nome_cargo: 'Tesoureiro' },
      { nome_cargo: 'Lider GP' },
      { nome_cargo: 'Lets ( Lider em treinamento )' },
      { nome_cargo: 'Grup. Louvor' },
    ];
  }
};
