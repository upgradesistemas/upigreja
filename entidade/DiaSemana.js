const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class DiaSemana extends EntidadeSinc {

  constructor() {
    super('tab_dia_semana');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_dia_semana').pk(),
      field('nome_dia_semana').varchar(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { nome_dia_semana: 'Segunda-Feira' }, 
      { nome_dia_semana: 'Terça-Feira' }, 
      { nome_dia_semana: 'Quarta-Feira' }, 
      { nome_dia_semana: 'Quinta-Feira' }, 
      { nome_dia_semana: 'Sexta-Feira' }, 
      { nome_dia_semana: 'Sábado' }, 
      { nome_dia_semana: 'Domingo' }, 
    ];
  }

};

