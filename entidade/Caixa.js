const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');
const database = require('../lib_backend/funcoes/database');
const data = require('../lib_backend/funcoes/data');
const { whereExclusao } = require('../lib_backend/funcoes/variaveis').variaveis;
const CP = require('./CP');
const CR = require('./CR');

module.exports = class Caixa extends EntidadeSinc {

  constructor() {
    super('tab_caixa');
  }

  // ------------------------------------------------------------------------------------

  static async caixaAberto(igrejaLogada, nomeBanco) {
    const caixa = await
      Caixa.buscarPrimeiro(`cod_igreja = ${igrejaLogada} AND status_caixa = "A"`, nomeBanco);
    return caixa;
  }

  // ------------------------------------------------------------------------------------

  static async valoresCaixaDashboard(filtro, nomeBanco) {
    const caixa = filtro.Caixa || 0;
    const igreja = filtro.Igreja || 0;
    let sql;

    async function getCredito(futuro) {
      sql = `SELECT cr.cod_cr AS codigo, cr.vr_receber, cr.vr_recebido 
      FROM ${nomeBanco}.tab_cr cr WHERE ${whereExclusao}
      AND (cod_igreja_transferencia IS NULL OR cod_igreja_transferencia >= 0)
      AND cod_caixa = ${caixa}
      AND cr.dt_hr_recebido ` + (futuro ? ' IS NULL ' : ' IS NOT NULL ') +
        (igreja ? ' AND cr.cod_igreja = ' + igreja : '');

      return await database.query(sql);
    }
    async function getDebito(futuro) {
      sql = `SELECT cp.cod_cp AS codigo, cp.vr_pagar, cp.vr_pago
      FROM ${nomeBanco}.tab_cp cp WHERE ${whereExclusao}
      AND cod_caixa = ${caixa}
      AND cp.dt_hr_pago ` + (futuro ? ' IS NULL ' : ' IS NOT NULL ') +
        (igreja ? ' AND cp.cod_igreja = ' + igreja : '');

      return await database.query(sql);
    }

    let credFuturo = 0;
    let debFuturo = 0;
    let saldoFuturo = 0;

    let credito = 0;
    let debito = 0;
    let saldo = 0;

    let creditos = {};
    let debitos = {};

    // ---------------------------------

    creditos = await getCredito(true);
    debitos = await getDebito(true);

    for (const c of creditos) {
      credFuturo += c.vr_receber;
    }
    for (const d of debitos) {
      debFuturo += d.vr_pagar;
    }

    saldoFuturo += credFuturo - debFuturo;

    // ---------------------------------

    creditos = await getCredito();
    debitos = await getDebito();

    for (const c of creditos) {
      credito += c.vr_recebido;
    }
    for (const d of debitos) {
      debito += d.vr_pago;
    }

    saldo = credito - debito;

    const valores = {
      credFuturo,
      debFuturo,
      saldoFuturo,
      credito,
      debito,
      saldo,
    };
    return valores;
  }

  // ------------------------------------------------------------------------------------

  static async abrirFecharCaixa(caixa, caixaState, pessoaExecutando, Igreja, nomeBanco, abertura) {
    if (abertura) {
      caixa = {};
      caixa.cod_igreja = Igreja;
      caixa.cod_pessoa_abertura = pessoaExecutando;
      caixa.dt_hr_abertura = new Date();
      caixa.obs = caixaState.obs;
      caixa.vr_inicial = caixaState.vr_inicial;
      return await Caixa.salvar(caixa, nomeBanco);
    }
    // Se não for abertura de Caixa, então fecha
    caixa.dt_hr_fechamento = new Date();
    caixa.cod_pessoa_fechamento = pessoaExecutando;
    caixa.status_caixa = 'F';
    caixa.obs = caixaState.obs;
    caixa.vr_inicial = caixaState.vr_inicial;
    caixa.vr_recebimento = await CR.retornaVrRecebimentoCaixaAtual(caixa.cod_caixa, nomeBanco);
    caixa.vr_pagamento = await CP.retornaVrPagamentoCaixaAtual(caixa.cod_caixa, nomeBanco);
    caixa.vr_saldo = (caixa.vr_recebimento + caixa.vr_inicial) - caixa.vr_pagamento;
    return await Caixa.salvar(caixa, nomeBanco);
  }

  // ------------------------------------------------------------------------------------

  static async retornaCaixa(filtro, nomeBanco) {
    let credito = 0;
    let debito = 0;
    const igreja = filtro.Igreja || 0;
    const dep = Number(filtro.dep) !== 0 ? filtro.dep : '';
    const pessoa = Number(filtro.pessoa) !== 0 ? filtro.pessoa : '';
    const dtInicio = data.formatar(filtro.dtInicio, 'YYYY-MM-DD HH:mm:ss') || '';
    const dtFim = data.formatar(filtro.dtFim, 'YYYY-MM-DD 23:59:59') || '';
    const caixa = filtro.Caixa || 0;
    const query = [];
    let total = [];

    const creditoQuerySql = `
      SELECT 
        cr.cod_cr AS codigo,
        cr.cod_pessoa,
        cr.dt_hr_receber AS dt_vencimento,
        cr.dt_hr_recebido AS dt_movimento,
        cr.vr_receber,
        cr.vr_recebido,
        cr.cod_tipo_movimento,
        CASE cr.cod_tipo_movimento
          WHEN 1 THEN "Dízimo"
          WHEN 2 THEN "Oferta"
          WHEN 3 THEN "CR Manual"
          WHEN 4 THEN "CR Parcela"
          WHEN 5 THEN "CR Parcial"
          WHEN 6 THEN "CP Manual"
          WHEN 7 THEN "CP Parcela"
          WHEN 8 THEN "CP Parcial"
          WHEN 9 THEN "Cantina"
          WHEN 10 THEN "Venda"
          WHEN 11 THEN "Compra"
          WHEN 12 THEN "Escola Bíblica"
          WHEN 13 THEN "Transferência"
          WHEN 14 THEN "Transferência"
        END AS tipo_movimento,
        CASE cr.cod_tipo_movimento
          WHEN 1 THEN "Crédito"
          WHEN 2 THEN "Crédito"
          WHEN 3 THEN "Crédito"
          WHEN 4 THEN "Crédito"
          WHEN 5 THEN "Crédito"
          WHEN 6 THEN "Débito"
          WHEN 7 THEN "Débito"
          WHEN 8 THEN "Débito"
          WHEN 9 THEN "Crédito"
          WHEN 10 THEN "Crédito"
          WHEN 11 THEN "Débito"
          WHEN 12 THEN "Crédito"
          WHEN 13 THEN "Crédito"
          WHEN 14 THEN "Débito"
        END AS tipo_movimentacao
      FROM ${nomeBanco}.tab_cr cr
      WHERE ${whereExclusao} AND cr.dt_hr_recebido IS NOT NULL
      AND cod_igreja = ${igreja} AND cod_caixa = ${caixa}
      AND (cod_igreja_transferencia IS NULL OR cod_igreja_transferencia >= 0)`;

    const debitoQuerySql = `
      SELECT 
        cp.cod_cp AS codigo,
        cp.cod_pessoa,
        cp.dt_hr_pagar AS dt_vencimento,
        cp.dt_hr_pago AS dt_movimento,
        cp.vr_pagar,
        cp.vr_pago,
        cp.cod_tipo_movimento,
        CASE cp.cod_tipo_movimento
          WHEN 1 THEN "Dízimo"
          WHEN 2 THEN "Oferta"
          WHEN 3 THEN "CR Manual"
          WHEN 4 THEN "CR Parcela"
          WHEN 5 THEN "CR Parcial"
          WHEN 6 THEN "CP Manual"
          WHEN 7 THEN "CP Parcela"
          WHEN 8 THEN "CP Parcial"
          WHEN 9 THEN "Cantina"
          WHEN 10 THEN "Venda"
          WHEN 11 THEN "Compra"
          WHEN 12 THEN "Escola Bíblica"
          WHEN 13 THEN "Transferência"
          WHEN 14 THEN "Transferência"
          END AS tipo_movimento,
        CASE cp.cod_tipo_movimento
          WHEN 1 THEN "Crédito"
          WHEN 2 THEN "Crédito"
          WHEN 3 THEN "Crédito"
          WHEN 4 THEN "Crédito"
          WHEN 5 THEN "Crédito"
          WHEN 6 THEN "Débito"
          WHEN 7 THEN "Débito"
          WHEN 8 THEN "Débito"
          WHEN 9 THEN "Crédito"
          WHEN 10 THEN "Crédito"
          WHEN 11 THEN "Débito"
          WHEN 12 THEN "Crédito"
          WHEN 13 THEN "Crédito"
          WHEN 14 THEN "Débito"
        END AS tipo_movimentacao
      FROM ${nomeBanco}.tab_cp cp
      WHERE ${whereExclusao} AND cp.dt_hr_pago IS NOT NULL
      AND cod_igreja = ${igreja} AND cod_caixa = ${caixa}`;

    let creditoQueryTudo = creditoQuerySql;
    if (dep) {
      creditoQueryTudo += ` AND cr.cod_departamento = ${dep} `;
    }
    if (pessoa) {
      creditoQueryTudo += ` AND cr.cod_pessoa = ${pessoa} `;
    }
    if (dtInicio && dtFim) {
      creditoQueryTudo += ` AND cr.dt_hr_recebido BETWEEN '${dtInicio}' AND '${dtFim}' `;
    } else if (dtInicio) {
      creditoQueryTudo += ` AND cr.dt_hr_recebido > '${dtInicio}' `;
    } else if (dtFim) {
      creditoQueryTudo += ` AND cr.dt_hr_recebido < '${dtFim}' `;
    }

    let debitoQueryTudo = debitoQuerySql;
    if (dep) {
      debitoQueryTudo += ` AND cp.cod_departamento = ${dep} `;
    }
    if (pessoa) {
      debitoQueryTudo += ` AND cp.cod_pessoa = ${pessoa} `;
    }
    if (dtInicio && dtFim) {
      debitoQueryTudo += ` AND cp.dt_hr_pago BETWEEN '${dtInicio}' AND '${dtFim}' `;
    } else if (dtInicio) {
      debitoQueryTudo += ` AND cp.dt_hr_pago > '${dtInicio}' `;
    } else if (dtFim) {
      debitoQueryTudo += ` AND cp.dt_hr_pago < '${dtFim}' `;
    }

    const creditoQuery = await database.query(creditoQueryTudo);
    const debitoQuery = await database.query(debitoQueryTudo);

    if (creditoQuery) {
      for (const reg of creditoQuery) {
        credito += reg.vr_recebido;
      }
    }

    if (debitoQuery) {
      for (const reg of debitoQuery) {
        debito += reg.vr_pago;
      }
    }

    query.push(
      ...creditoQuery,
      ...debitoQuery);

    total.push({
      vr_credito: credito,
      vr_debito: debito,
      vr_saldo: credito - debito,
    });
    total = total[0];

    return {
      query,
      total,
    };
  }

  // ------------------------------------------------------------------------------------

  static async retornaValorCaixaAnterior(igrejaLogada, nomeBanco) {
    const valorAnterior = await database.queryFindOne(`
      SELECT cod_caixa, vr_saldo FROM ${nomeBanco}.tab_caixa 
      WHERE ${whereExclusao}
      AND cod_igreja = ${igrejaLogada} AND status_caixa = "F"
      ORDER BY 1 DESC LIMIT 1
    `);
    return valorAnterior.vr_saldo;
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_caixa').pk(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('status_caixa').enum('A', 'F').default('A'),
      field('dt_hr_abertura').datetime(),
      field('dt_hr_fechamento').datetime(),
      field('cod_pessoa_abertura').references('tab_pessoa(cod_pessoa)'),
      field('cod_pessoa_fechamento').references('tab_pessoa(cod_pessoa)'),
      field('obs').varchar(),
      field('vr_inicial').decimal(),
      field('vr_recebimento').decimal(),
      field('vr_pagamento').decimal(),
      field('vr_saldo').decimal(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

};
