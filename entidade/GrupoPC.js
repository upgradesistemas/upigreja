const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class GrupoPC extends EntidadeSinc {

  constructor() {
    super('tab_grupo_pc');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_grupo_pc').pk(),
      field('nome_grupo_pc').varchar(),
      field('codigo_grupo_pc').varchar(),
      field('cod_estrutura_pc').references('tab_estrutura_pc(cod_estrutura_pc)'),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }

  // ------------------------------------------------------------------------------------

  getDefaults() {
    return [
      { cod_estrutura_pc: 1, codigo_grupo_pc: '1.1', nome_grupo_pc: 'Ativo Circulante' }, 
      { cod_estrutura_pc: 2, codigo_grupo_pc: '2.1', nome_grupo_pc: 'Passivo Circulante' }, 
    ];
  }

};

