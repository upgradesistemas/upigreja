const EntidadeSinc = require('../lib_backend/sincronizacao/EntidadeSinc');
const field = require('../lib_backend/sincronizacao/field');

module.exports = class Produto extends EntidadeSinc {

  constructor() {
    super('tab_produto');
  }

  // ------------------------------------------------------------------------------------

  getFields() {
    return [
      field('cod_produto').pk(),
      field('cod_igreja').references('tab_igreja(cod_igreja)'),
      field('nome_produto').varchar(),
      field('valor').decimal(),
      field('dt_hr_cadastro').datetime(),
      field('status_ativo').enum('S', 'N').default('S'),
      field('cod_usuario_exclusao').references('tab_pessoa(cod_pessoa)'),
      field('dt_hr_exclusao').datetime(),
      field('status_exclusao').enum('S', 'N').default('N'),
    ];
  }
};
